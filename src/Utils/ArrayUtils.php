<?php

namespace InerziaSoft\Core\Utils;

use InerziaSoft\Core\Db\DbObject\Definitions\DbObjectDefinition;
use InerziaSoft\Core\Parser\JsonParser;

class ArrayUtils {
	
	public static function valueForKeyInArray($key, $array) {
		$valueKeyExploded = explode(".", $key);
		
		$value = $array;
		foreach ($valueKeyExploded as $valueKey) {
			if (isset($value) && array_key_exists($valueKey, $value)) {
				$value = $value[$valueKey];
			}
			else {
				$value = null;
			}
		}
		
		return $value;
	}
	
	public static function isAssociativeArray($arr) {
	    if (!is_array($arr)) return false;
		if ([] === $arr) return false;
		return array_keys($arr) !== range(0, count($arr) - 1);
	}
	
	/**
	 * Converts an existing DbObject JSON response to a valid
	 * indexed array of values that can be used to instantiate a
	 * new DbObject subclass.
	 *
	 * @discussion This function does two things:
	 * - Removes any kind of parent->child connection, by
	 * making the array flat.
	 * - Converts all the keys into the form "tableName.keyName".
	 *
	 * @param $json array
	 * @param $responseClassKey
	 * @param null $initialKey
	 * @return array
	 * @internal param string $key
	 */
	public static function flatDbObjectJson($json, $responseClassKey = JsonParser::ResponseClassKey, $initialKey = null) {
	    if (array_key_exists($responseClassKey, $json)) {
			$responseClass = $json[$responseClassKey];
			
			if (!isset($initialKey)) {
				$initialKey = strtolower(DbObjectDefinition::getTableName($responseClass));
			}
			
			$convertedValues = [];
			foreach ($json as $key => $value) {
				if (is_array($value)) {
					$convertedValues = array_merge($convertedValues, static::flatDbObjectJson($value, $responseClassKey, $initialKey.".".$key));
                }
				else {
					$convertedValues[$initialKey.".".$key] = $value;
                }
            }

			return $convertedValues;
		}
		else if (is_array($json)) {
			return [$initialKey => JsonParser::parseArrayInJson($json, $responseClassKey)];
		}
		
		return $json;
	}
	
	/**
	 * This function converts all the keys of an array
	 * to be compliant to the DbObject ReadOnly convention.
	 *
	 * An array such as the following:
	 *  table.key => "something"
	 *  table.key.anotherKey => "somethingElse"
	 *
	 * is converted to something like:
	 *  key => "something"
	 *  key.anotherKey => "somethingElse"
	 *
	 * In other words, the first part of the key (before the dot)
	 * is removed.
	 *
	 * @param $array
	 * @return array
	 */
	public static function normalizeArrayForReadOnly($array) {
		$result = [];
		foreach ($array as $key => $value) {
			$keyExploded = explode(".", $key);
			if (count($keyExploded) > 0) {
				$resultingKey = str_replace($keyExploded[0] . ".", "", $key);
			} else {
				$resultingKey = $key;
			}
			
			$result[$resultingKey] = $value;
			
		}
		
		return $result;
	}
	
	/**
	 * Returns an associative array with all the keys
	 * that starts with a specified string.
	 *
	 * @param $array
	 * @param $string
	 * @return array
	 */
	public static function arrayKeysStartingWith($array, $string) {
		$result = [];
		$stringCount = strlen($string);
		
		foreach ($array as $key => $value) {
			if (substr($key, 0, $stringCount) == $string) {
				$result[$key] = $value;
			}
		}
		
		return $result;
	}
	
	/**
	 * Returns the value associated with a specified key by looking
	 * for the standalone key or the namespaced version.
	 *
	 * @param $array array: An associative array.
	 * @param $namespace string: The namespace to use.
	 * @param $key string: The key to look for.
	 * @param string $junction: An optional string to use when joining the namespace with the key (defaults to ".").
	 * @return mixed|null
	 */
	public static function namespaceOrStandaloneKeyValue($array, $namespace, $key, $junction = ".") {
		$completeKey = $namespace.$junction.$key;
		
		$value = null;
		if (array_key_exists($key, $array)) {
			$value = $array[$key];
		}
		else if (array_key_exists($completeKey, $array)) {
			$value = $array[$completeKey];
		}
		
		return $value;
	}
	
	/**
	 * Takes an associative array and returns another array
	 * containing two separated
	 * arrays with keys [0] and values [1].
	 * @param $associativeArray array
	 * @return array
	 */
	public static function splitKeysAndValues($associativeArray) {
		$keys = [];
		$values = [];
		
		foreach ($associativeArray as $key => $value) {
			array_push($keys, $key);
			array_push($values, $value);
		}
		
		return [$keys, $values];
	}
	
	public static function mergeKeysAndValues($keys, $values) {
		$keysAndValues = [];
		for ($i = 0; $i < count($keys); $i++) {
			$keysAndValues[$keys[$i]] = $values[$i];
		}
		return $keysAndValues;
	}
	
}