<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Utils;

class UriUtils {
	
	public static function getApiFolder() {
		$api = "api/";
		if (defined("API")) {
			$api = API;
			
			if (substr($api, -1) != "/") {
				$api .= "/";
			}
		}
		
		return $api;
	}
	
	public static function getApiVersion() {
		$apiVersion = "v1";
		if (defined("API_VERSION")) {
			$apiVersion = API_VERSION;
		}
		
		return $apiVersion;
	}

	public static function getRootPath() {
		return str_replace("index.php", "", \Base::instance()->get("ROOT").\Base::instance()->get("BASE"));
	}

	public static function getConfig($configFilename) {
		return @parse_ini_file(static::getRootPath()."/config/".$configFilename);
	}
	
	public static function getBaseUri($uri = null) {
		if (!isset($uri) || substr($uri, 0, 1) == "/") {
			$requestAddress = $_SERVER["PHP_SELF"];
			$requestExploded = explode("/", $requestAddress);
			
			$additionalFolder = "";
			foreach ($requestExploded as $part) {
				if ($part == "" || $part == " " || strlen($part) == 0) continue;
				
				$api = "api/";
				if (defined("API")) {
					$api = API;
				}
				if (strpos($part, ".php") === false && strpos($api, $part) === false) {
					$additionalFolder .= $part;
					break;
				}
			}
			
			if (strlen($additionalFolder) > 0) {
				$additionalFolder = "/" . $additionalFolder;
			}
			
			if (strlen($additionalFolder) > 0 && strpos($uri, $additionalFolder) !== false) {
				$additionalFolder = "";
			}
		} else {
			$additionalFolder = "";
		}
		
		return $additionalFolder;
	}
	
	public static function getHostUri() {
		$httpHost = $_SERVER["HTTP_HOST"];
		$httpScheme = $_SERVER["REQUEST_SCHEME"];
		
		return $httpScheme."://".$httpHost;
	}
	
	public static function getFullRequestUri() {
		return self::getHostUri().$_SERVER["REQUEST_URI"];
	}
	
}