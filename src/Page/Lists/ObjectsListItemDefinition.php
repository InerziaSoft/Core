<?php

namespace InerziaSoft\Core\Page\Lists;
use InerziaSoft\Core\Page\Lists\Exceptions\UnknownForeignKeyNameException;
use InerziaSoft\Core\Page\Lists\Items\PlaceholderObjectsListItem;
use InerziaSoft\Core\Page\Page;

/**
 * Class ObjectsListItemDefinition
 *
 * Subclasses of an ObjectsListItemDefinition defines
 * the behavior of each single item of the list,
 * besides settling options on the general functioning of the list itself.
 *
 * Static methods provide generic configuration and options for the list,
 * while instance methods defines each item.
 *
 * @package InerziaSoft\ObjectsList
 */
abstract class ObjectsListItemDefinition {
	
	protected $objectItem;
	
	/**
	 * @var Page
	 */
	protected $page;
	
	/**
	 * ObjectsListItemDefinition constructor.
	 *
	 * Instantiates a new ObjectsListItemDefinition with
	 * a specific dictionary that represents an object.
	 *
	 * The returned class can be queried to gather
	 * generic information on the objectItem that are used
	 * to represent the item in the list.
	 *
	 * @param $objectItem
	 * @param $page Page
	 */
	public function __construct($objectItem, $page) {
		$this->objectItem = $objectItem;
		$this->page = $page;
	}
	
	/**
	 * Returns the identifier that should be used in the list.
	 *
	 * @discussion Except for extraordinary scenarios, this should be
	 * the primary key of the passed objectItem.
	 *
	 * @discussion ObjectsList does not support objects with multiple
	 * primary keys, at the moment.
	 *
	 * @return int|string
	 */
	public abstract function getId();
	
	/**
	 * Returns the title that represents the object in the list.
	 *
	 * @discussion This is the primary visual identifier of the object
	 * and should corresponds to what the user consider the actual identifier
	 * of items in the list (i.e. the name).
	 *
	 * @return string
	 */
	public abstract function getTitle();
	
	/**
	 * Returns the subtitle of the object.
	 *
	 * @discussion A subtitle can append additional information to the
	 * object and provides a more specific way to identify items in the list.
	 * By default, the subtitle appears below the title.
	 *
	 * @return string
	 */
	public abstract function getSubtitle();
	
	/**
	 * Returns an absolute or specific path to the image
	 * representing the object in the list.
	 *
	 * @discussion The image must be square and should
	 * provide a visual cue to the user to easily distinguish
	 * different kinds or statuses of each items.
	 *
	 * @return string
	 */
	public abstract function getImage();
	
	/**
	 * Returns an array of CSS classes
	 * to be applied to the item.
	 *
	 * @discussion The default implementation
	 * returns an empty array.
	 *
	 * @return array
	 */
	public function getClasses() {
		return [];
	}
	
	/**
	 * Returns an associative array with a list
	 * of data attributes that should be added to
	 * the item of the list.
	 *
	 * @discussion The data attributes specified here
	 * follow the HtmlTag class convention, hence the "data-"
	 * prefix is not required.
	 *
	 * @discussion The default implementation returns an empty array.
	 *
	 * @return array
	 */
	public function getDataAttributes() {
		return [];
	}
	
	// Generic Configuration
	
	/**
	 * Returns the name of the underlying database table
	 * that supports this ObjectsList.
	 *
	 * @discussion The default implementation returns the name
	 * of this class (or its subclass) without the static string
	 * "ObjectsListItemDefinition". Consequently, it's a good idea
	 * to name your ObjectsListItemDefinition subclasses something like:
	 *      "<APIClassName>ObjectsListItemDefinition"
	 * When using a DbObject Automatic Route, this convention can be easily
	 * converted to:
	 *      "<TableName>ObjectsListItemDefinition"
	 *
	 * The default implementation requires a valid definition of the
	 * OBJECTSLISTITEMDEFINITIONS_CLASSES_NAMESPACE constant.
	 *
	 * @return string
	 */
	public static function getTableName() {
		$tableName = strtolower(
			str_replace("\\", "",
				str_replace("ObjectsListItemDefinition", "",
					str_replace(OBJECTSLISTITEMDEFINITIONS_CLASSES_NAMESPACE, "",
						get_called_class()
					)
				)
			)
		);
		
		return $tableName;
	}
	
	/**
	 * Returns the CSS class that identifies the Inspector
	 * container of this ObjectsList (if any).
	 *
	 * @discussion The default implementation returns the
	 * result of getApiClass() preceded by a dot and followed
	 * by the static string "InspectorContainer".
	 *
	 * @return string
	 */
	public static function getInspectorContainer() {
		return ".".static::getApiClass()."InspectorContainer";
	}
	
	/**
	 * Returns the API class which represents and handles
	 * the underlying table that supports this ObjectsList.
	 *
	 * @discussion The default implementation returns
	 * the result of getTableName().
	 *
	 * @return string
	 */
	public static function getApiClass() {
		return static::getTableName();
	}
	
	/**
	 * Returns the method that should be called to gather all items
	 * of the underlying database table that supports this list.
	 *
	 * @discussion The default implementation follows the DbObject
	 * Automatic Routes standard, hence returns "all".
	 *
	 * @return string
	 */
	public static function getApiCall() {
		return "all";
	}
	
	/**
	 * Returns the method that should be called to gather all
	 * items of the underlying database table considering a filter.
	 *
	 * @discussion The default implementation follows the DbObject
	 * Automatic Routes standard, hence returns "where".
	 *
	 * @return string
	 */
	public static function getWhereApiCall() {
		return "where";
	}
	
	/**
	 * Returns the name of the foreign key which connects to
	 * lists.
	 *
	 * In grouped lists, this method is called to properly query
	 * the child list based on what has been selected in the main
	 * list.
	 *
	 * @discussion The default implementation throws an exception:
	 * if you are using a grouped list, you must override this method.
	 * In any other case, this method won't be called.
	 *
	 * @return string
	 * @throws UnknownForeignKeyNameException
	 */
	public static function getForeignKeyName() {
		throw new UnknownForeignKeyNameException(
			"Your implementation of ".get_called_class()." does not specify a foreign key name which is required for grouped lists to work. 
			Please, override 'getForeignKeyName()' in your ObjectsListItemDefinition subclass.");
	}
	
	/**
	 * Returns the label to display when the list
	 * contains zero items.
	 *
	 * @discussion If this method returns an empty string,
	 * as the default implementation, nothing will be displayed.
	 *
	 * @return string
	 */
	public static function getNoItemsLabel() {
		return "";
	}
	
	/**
	 * Returns the label to use in the "Add Item" placeholder
	 * row.
	 *
	 * @discussion If this method returns null, as the default
	 * implementation, no "Add Item" placeholder will be added.
	 *
	 * @return string|null
	 */
	public static function getAddItemPlaceholderLabel() {
		return null;
	}
	
	/**
	 * Returns the JavaScript click handler to be called
	 * when a click is performed on the "Add Item" placeholder.
	 *
	 * @discussion If addItemPlaceholderLabel() returns null,
	 * this method is ignored.
	 *
	 * @return string|null
	 */
	public static function getAddItemPlaceholderOnClick() {
		return null;
	}
	
	/**
	 * Returns the position of the "Add Item" placeholder row.
	 *
	 * @discussion Use values defined in `PlaceholderObjectsListItem`.
	 *
	 * @return string
	 */
	public static function getAddItemPlaceholderPosition() {
		return PlaceholderObjectsListItem::$PositionEnd;
	}
	
}