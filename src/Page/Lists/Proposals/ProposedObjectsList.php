<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
namespace InerziaSoft\Core\Page\Lists\Proposals;

use InerziaSoft\Core\Page\Interfaces\HtmlConvertible;
use InerziaSoft\Core\Page\Page;

class ProposedObjectsList implements HtmlConvertible {
	
	private $className;
	
	private $tableName;
	
	private $listContainer;
	
	private $objectsListItemDefinitionClass;
	
	/**
	 * ProposedObjectsList constructor.
	 *
	 * Instantiates a new ProposedObjectsList.
	 *
	 * @param $className Page: The page which contains this list.
	 * @param $listUrl string: The url to associate to this list.
	 * @param $listContainer string|null: The jQuery selector that will contain this list (if not specified, uses $listUrl."ListContainer").
	 * @param $objectsListItemDefinitionClass string
	 */
	public function __construct($className, $listUrl, $listContainer = null, $objectsListItemDefinitionClass = null) {
		$this->className = get_class($className);
		$this->className = str_replace(PAGES_CLASSES_NAMESPACE, "", $this->className);
		$this->className = str_replace("\\", "/", $this->className);
		$this->className = strtolower($this->className);
		$this->objectsListItemDefinitionClass = $objectsListItemDefinitionClass;
		
		$this->tableName = $listUrl;
		
		if (!isset($listContainer)) {
			$listContainer = $listUrl."ListContainer";
		}
		$this->listContainer = $listContainer;
	}
	
	public function toHtml() {
		return "<div class='objectsListDefinition' style='display:none;'>
				<var class='objectsListUrl'>" . $this->className . "/lists/" . $this->tableName . "</var>
				<var class='objectsListContainer'>{$this->listContainer}</var>
				</div>";
	}
	
	/**
	 * @return string
	 */
	public function getTableName() {
		return $this->tableName;
	}
	
	public function getClassName() {
		return $this->className;
	}
	
	public function getListContainer() {
		return $this->listContainer;
	}
	
	public function getObjectsListItemDefinitionClass() {
		return $this->objectsListItemDefinitionClass;
	}
}