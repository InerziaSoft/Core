<?php

namespace InerziaSoft\Core\Page\Lists\Proposals;

use InerziaSoft\Core\Page\Interfaces\HtmlConvertible;

class ProposedGroupedObjectsList implements HtmlConvertible {
	
	private $mainListDefinition;
	private $childListDefinition;
	
	/**
	 * GroupedObjectsListDefinition constructor.
	 * @param $mainListDefinition ProposedObjectsList
	 * @param $childListDefinition ProposedObjectsList
	 */
	public function __construct($mainListDefinition, $childListDefinition) {
		$this->mainListDefinition = $mainListDefinition;
		$this->childListDefinition = $childListDefinition;
	}
	
	public function toHtml() {
		return "
				<div class='groupedObjectsListDefinition' style='display:none;'>
				<var class='mainObjectsListUrl'>".$this->mainListDefinition->getClassName()."/lists/".$this->mainListDefinition->getTableName()."</var>
				<var class='mainObjectsListContainer'>{$this->mainListDefinition->getListContainer()}</var>
				<var class='childObjectsListUrl'>".$this->childListDefinition->getClassName()."/lists/".$this->childListDefinition->getTableName()."</var>
				<var class='childObjectsListContainer'>{$this->childListDefinition->getListContainer()}</var>
				</div>
		";
	}
	
	public function getMainListDefinition() {
		return $this->mainListDefinition;
	}
	
	public function getChildListDefinition() {
		return $this->childListDefinition;
	}
	
}