<?php

namespace InerziaSoft\Core\Page\Lists\Items;

use InerziaSoft\Core\Page\Html\Header;
use InerziaSoft\Core\Page\Html\ListItem;
use InerziaSoft\Core\Page\Html\SimpleText;

class EmptyObjectsListItem extends ListItem {
	
	public function __construct($label) {
		parent::__construct(null, ["emptyItemPlaceholder"], null, [
			new Header(2, null, null, null, [
				new SimpleText($label)
			], null)
		], null);
	}
	
}