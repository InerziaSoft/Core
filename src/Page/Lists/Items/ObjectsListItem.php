<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
namespace InerziaSoft\Core\Page\Lists\Items;

use InerziaSoft\Core\Page\Html\Div;
use InerziaSoft\Core\Page\Html\Image;
use InerziaSoft\Core\Page\Html\ListItem;
use InerziaSoft\Core\Page\Html\SimpleText;
use InerziaSoft\Core\Page\Html\UnorderedList;
use InerziaSoft\Core\Page\Lists\ObjectsListItemDefinition;

class ObjectsListItem extends ListItem {
	
	private $objectListsItemDefinition;
	
	/**
	 * ObjectsListItem constructor.
	 *
	 * Instantiates a new ObjectsListItem using an ObjectsListItemDefinition.
	 *
	 * @param $objectsListItemDefinition ObjectsListItemDefinition: A definition of the represented object.
	 */
	public function __construct($objectsListItemDefinition) {
		$this->objectListsItemDefinition = $objectsListItemDefinition;
		
		$content = [
			new Div(null, ["objectsListImage"], null, [new Image(null, null, null, $objectsListItemDefinition->getImage(), null)], null),
			new Div(null, ["objectsListTitlesContainer"], null, [
				new UnorderedList(null, null, null, [
					new ListItem(null, ["objectsListTitle"], null, [new SimpleText($objectsListItemDefinition->getTitle())], null),
					new ListItem(null, ["objectsListSubtitle"], null, [new SimpleText($objectsListItemDefinition->getSubtitle())], null)
				])
			], null)
		];
		
		parent::__construct($objectsListItemDefinition->getId(), $objectsListItemDefinition->getClasses(), $objectsListItemDefinition->getDataAttributes(), $content, "selectItem(this, event)");
	}
	
}