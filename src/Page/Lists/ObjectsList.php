<?php

/*
MIT License

Copyright (c) 2016 Alessio Moiso - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Page\Lists;

use InerziaSoft\Core\Api\Outcomes\ListResponse;
use InerziaSoft\Core\Page\ApiRequest;
use InerziaSoft\Core\Page\Exceptions\InvalidListResponseException;
use InerziaSoft\Core\Page\Html\UnorderedList;
use InerziaSoft\Core\Page\Lists\Items\EmptyObjectsListItem;
use InerziaSoft\Core\Page\Lists\Items\ObjectsListItem;
use InerziaSoft\Core\Page\Lists\Items\PlaceholderObjectsListItem;
use InerziaSoft\Core\Page\Page;
use InerziaSoft\Core\Page\PartialPage;

class ObjectsList extends PartialPage {

	private $tableName;

	private $apiClass;
	private $apiCall;

	private $inspectorUrl;
	private $inspectorContainer;
	
	private $objectsListItemDefinitionClass;
	
	private $page;
	
	/**
	 * ObjectsList constructor.
	 *
	 * @param $page Page
	 * @param $objectsListItemDefinitionClass ObjectsListItemDefinition
	 * @param null $foreignKeyValue
	 * @throws Exceptions\UnknownForeignKeyNameException
	 */
	public function __construct($page, $objectsListItemDefinitionClass, $foreignKeyValue = null) {
		if (!class_exists($objectsListItemDefinitionClass)) {
			throw new \InvalidArgumentException("Unable to retrieve ObjectsListItemDefinition class named '{$objectsListItemDefinitionClass}'");
		}
		
		$this->page = $page;
		$this->objectsListItemDefinitionClass = $objectsListItemDefinitionClass;
		$this->tableName = $objectsListItemDefinitionClass::getTableName();

        $this->inspectorUrl = str_replace(PAGES_CLASSES_NAMESPACE, "", get_class($page));
        $this->inspectorUrl = str_replace("\\", "/", $this->inspectorUrl);
        $this->inspectorUrl = strtolower($this->inspectorUrl."/inspectors/".$this->tableName."/");

        $this->inspectorContainer = $objectsListItemDefinitionClass::getInspectorContainer();
		
        $this->apiClass = $objectsListItemDefinitionClass::getApiClass();
        
		if (isset($foreignKeyValue)) {
			$this->apiCall = $objectsListItemDefinitionClass::getWhereApiCall()."/";
			
			if ($foreignKeyValue == "null") {
				// TODO: handle null
			}
			else {
				$this->apiCall .= $objectsListItemDefinitionClass::getForeignKeyName()."/".$foreignKeyValue;
			}
		}
		else {
			$this->apiCall = $objectsListItemDefinitionClass::getApiCall();
		}
	}

	public function displayList() {
		$hive = [];

		if (defined("LIST_LAYOUT_PATH")) {
			$template = LIST_LAYOUT_PATH;
		}
		else {
			throw new \InvalidArgumentException("The default layout file of an ObjectsList must be defined in a constant named 'LIST_LAYOUT_PATH'!");
		}

		$request = new ApiRequest($this->apiClass, $this->apiCall, ApiRequest::$getMethod, null, $this->getSessionToken());
		$response = $request->request();

		if ($response instanceof ListResponse) {
			$items = $response->items;
			
			$objectsListItemDefinitionClass = $this->objectsListItemDefinitionClass;
			
			$listCustomAttributes = [
				"objectsListInspector" => $this->inspectorUrl,
				"objectsListInspectorContainer" => $this->inspectorContainer
			];
			
			$itemsToList = [];
			
			if (count($items) > 0) {
				$hive["objects_list"] = "";
				
				foreach ($items as $item) {
					$objectsListItemDefinition = new $objectsListItemDefinitionClass($item, $this->page);
					
					array_push($itemsToList, new ObjectsListItem($objectsListItemDefinition));
				}
			}
			else {
				array_push($itemsToList, new EmptyObjectsListItem($this->page->getLocalizedString($objectsListItemDefinitionClass::getNoItemsLabel())));
			}
			
			$itemsToList = $this->conditionallyAppendPlaceholder($itemsToList, $objectsListItemDefinitionClass);
			
			$list = new UnorderedList(
				null,
				["objectsList"],
				$listCustomAttributes,
				$itemsToList);
			
			$hive["objects_list"] .= $list->toHtml();
		}
		else {
			throw new InvalidListResponseException($this->tableName, print_r($response, true));
		}

		return parent::display($hive, $template);
	}
	
	/**
	 * @param $itemsToList array
	 * @param $objectsListItemDefinitionClass ObjectsListItemDefinition
	 * @return array
	 */
	private function conditionallyAppendPlaceholder($itemsToList, $objectsListItemDefinitionClass) {
		$placeholderAddItemLabel = $objectsListItemDefinitionClass::getAddItemPlaceholderLabel();
		if (isset($placeholderAddItemLabel)) {
			$placeholderAddItemOnClick = $objectsListItemDefinitionClass::getAddItemPlaceholderOnClick();
			$placeholderPosition = $objectsListItemDefinitionClass::getAddItemPlaceholderPosition();
			
			$placeholderObject = new PlaceholderObjectsListItem($this->page->getLocalizedString($placeholderAddItemLabel), $placeholderAddItemOnClick);
			
			if ($placeholderPosition == PlaceholderObjectsListItem::$PositionBeginning) {
				array_unshift($itemsToList, $placeholderObject);
			}
			else {
				array_push($itemsToList, $placeholderObject);
			}
		}
		
		return $itemsToList;
	}

}