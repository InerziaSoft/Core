<?php

/*
MIT License

Copyright (c) 2016 Alessio Moiso - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Page\Lists;

use InerziaSoft\Core\Page\ApiRequest;
use InerziaSoft\Core\Page\Html\UnorderedList;
use InerziaSoft\Core\Page\Lists\Interfaces\ObjectEvaluator;
use InerziaSoft\Core\Page\Page;
use InerziaSoft\Core\Page\PartialPage;

class ObjectsListInspector extends PartialPage {

	private $tableName;
	
	private $apiClass;
	private $apiCall;
	private $objectId;
	private $objectIdKey;

    private $containerPage;
	
	/**
	 * ObjectsListInspector constructor.
	 *
	 * @param $tableName string
	 * @param $apiClass string
	 * @param $apiCall string
	 * @param $objectId string|int
	 * @param string $objectIdKey string
	 * @param $page Page
	 */
	public function __construct($tableName, $apiClass, $apiCall, $objectId, $objectIdKey = "id", $page) {
		$this->apiClass = $apiClass;
		$this->apiCall = $apiCall;
		$this->objectId = $objectId;
		$this->objectIdKey = $objectIdKey;
        $this->containerPage = $page;
        $this->tableName = $tableName;
	}

	public function displayInspector() {
		$hive = [];
		if (defined("LIST_INSPECTOR_LAYOUT_PATH")) {
			$template = LIST_INSPECTOR_LAYOUT_PATH;
		}
		else {
			throw new \InvalidArgumentException("The default layout file of an ObjectsListInspector must be defined in a constant named 'LIST_INSPECTOR_LAYOUT_PATH'!");
		}

		if (isset($this->objectId)) {
            $request = new ApiRequest($this->apiClass, $this->apiCall, ApiRequest::$getMethod, [$this->objectIdKey => $this->objectId], $this->getSessionToken());
            $response = $request->request();

			if ($response["status"] == API_RESPONSE_SUCCESSFUL) {
				$components = $this->containerPage->inspectorComponentsForObject($this->tableName, $response);
				
				/** @var ObjectEvaluator $component */
				foreach ($components as $component) {
					$component->evaluateObject($response, $this->containerPage);
				}

				$containerList = new UnorderedList(null, null, null, $components);

				$hive["list_inspector"] = $containerList->toHtml();
			}
			else {
                $this->objectId = null;
                return $this->displayInspector();
			}
		}
		else {
			$hive["list_inspector"] = $this->containerPage->getNoSelectionRepresentationForInspector($this->tableName);
		}

		return parent::display($hive, $template);
	}

}