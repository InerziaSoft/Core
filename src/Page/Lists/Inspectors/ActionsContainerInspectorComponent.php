<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
namespace InerziaSoft\Core\Page\Lists\Inspectors;

use InerziaSoft\Core\Page\Html\ListItem;
use InerziaSoft\Core\Page\Html\SimpleText;
use InerziaSoft\Core\Page\Lists\Interfaces\ObjectEvaluator;

class ActionsContainerInspectorComponent extends InspectorComponent {
	
	private $actionLinks;
	private $actionsSeparator;
	
	/**
	 * ActionsContainerInspectorComponent constructor.
	 * @param array $actionLinks
	 * @param string $actionsSeparator
	 */
	public function __construct($actionLinks, $actionsSeparator = " | ") {
		parent::__construct(null, null, null);
		$this->actionLinks = $actionLinks;
		$this->actionsSeparator = $actionsSeparator;
	}
	
	public function evaluateObject($object, $page) {
		/** @var ObjectEvaluator $actionLink */
		foreach ($this->actionLinks as $actionLink) {
			$actionLink->evaluateObject($object, $page);
		}
	}
	
	protected function getClass() {
		return ["inspectorActionsContainer"];
	}
	
	public function toHtml() {
		$finalArray = [];
		$index = 0;
		$count = count($this->actionLinks);
		foreach ($this->actionLinks as $actionLink) {
			array_push($finalArray, $actionLink);
			if ($index < $count - 1) {
				array_push($finalArray, new SimpleText(" $this->actionsSeparator "));
			}
			$index++;
		}
		
		$listItem = new ListItem(null, $this->getClass(), null, $finalArray, null);
		
		return $listItem->toHtml();
	}
	
}