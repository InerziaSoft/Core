<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
namespace InerziaSoft\Core\Page\Lists\Inspectors;

use InerziaSoft\Core\Page\Html\Span;
use InerziaSoft\Core\Page\Interfaces\HtmlConvertible;
use InerziaSoft\Core\Page\Lists\Interfaces\ObjectEvaluator;

class ActionLink implements HtmlConvertible, ObjectEvaluator {
	
	public static $ParametersPlaceholder = "%@";
	
	protected $actionLabelTrue;
	protected $actionLabelFalse;
	protected $actionOnClickTrue;
	protected $actionOnClickFalse;
	protected $objectKey;
	
	protected $actionLabel;
	protected $actionOnClick;
	
	protected $functionParameters;
	
	public function __construct($actionTrueLabel, $actionTrueOnClick, $actionFalseLabel, $actionFalseOnClick, $objectKey, $functionParameters = []) {
		$this->actionLabelTrue = $actionTrueLabel;
		$this->actionLabelFalse = $actionFalseLabel;
		$this->actionOnClickTrue = $actionTrueOnClick;
		$this->actionOnClickFalse = $actionFalseOnClick;
		$this->objectKey = $objectKey;
		$this->functionParameters = $functionParameters;
	}
	
	public function evaluateObject($object, $page) {
		if (array_key_exists($this->objectKey, $object) && $object[$this->objectKey]) {
			$this->actionLabel = $this->actionLabelTrue;
			$this->actionOnClick = $this->actionOnClickTrue;
		} else {
			$this->actionLabel = $this->actionLabelFalse;
			$this->actionOnClick = $this->actionOnClickFalse;
		}
		
		$this->appendFunctionParameters($object);
	}
	
	protected function appendFunctionParameters($object) {
		$parametersString = [];
		foreach ($this->functionParameters as $functionParameter) {
			if (array_key_exists($functionParameter, $object)) {
				array_push($parametersString, "\"".$object[$functionParameter]."\"");
			}
			else {
				array_push($parametersString, "undefined");
			}
		}
		
		$this->actionOnClick = str_replace(self::$ParametersPlaceholder, implode(", ", $parametersString), $this->actionOnClick);
	}
	
	public function toHtml() {
		$linkItem = new Span(null, null, null, $this->actionLabel, $this->actionOnClick);
		
		return $linkItem->toHtml();
	}
	
}