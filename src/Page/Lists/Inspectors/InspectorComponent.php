<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
namespace InerziaSoft\Core\Page\Lists\Inspectors;

use InerziaSoft\Core\Page\Html\ListItem;
use InerziaSoft\Core\Page\Html\SimpleText;
use InerziaSoft\Core\Page\Html\Span;
use InerziaSoft\Core\Page\Interfaces\HtmlConvertible;
use InerziaSoft\Core\Page\Lists\Interfaces\ObjectEvaluator;
use InerziaSoft\Core\Page\Page;
use InerziaSoft\Core\Utils\ArrayUtils;

class InspectorComponent implements HtmlConvertible, ObjectEvaluator {
	
	protected $name;
	protected $valueKey;
	
	protected $localizedValue;
	protected $localizedPageTitle;
	
	protected $nullValue;
	protected $value;
	
	public function __construct($displayName, $nullValue, $valueKey, $localizedValue = false, $localizedPageTitle = null) {
		$this->name = $displayName;
		$this->nullValue = $nullValue;
		$this->valueKey = $valueKey;
		$this->localizedValue = $localizedValue;
		$this->localizedPageTitle = $localizedPageTitle;
	}
	
	/**
	 * Extracts the corresponding value of the $valueKey
	 * property of $object.
	 *
	 * This function also handles localization, if
	 * $localizedValue is true.
	 *
	 * @param $object mixed
	 * @param $page Page
	 */
	public function evaluateObject($object, $page) {
		$this->value = ArrayUtils::valueForKeyInArray($this->valueKey, $object);
		
		if ($this->localizedValue) {
			$this->value = $page->getLocalizedString($this->value, $this->localizedPageTitle);
		}
	}
	
	protected function toHtmlTag() {
		return new ListItem(null, null, null, [
			new Span(null, ["smallTitle"], null, $this->name),
			new SimpleText($this->getValue())
		], null);
	}
	
	public function toHtml() {
		return $this->toHtmlTag()->toHtml();
	}
	
	protected function getValue() {
		if (!(isset($this->value)) || strlen($this->value) == 0) {
			return $this->nullValue;
		}
		return $this->value;
	}
	
}