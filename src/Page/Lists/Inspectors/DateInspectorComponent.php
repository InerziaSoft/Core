<?php

/*
MIT License

Copyright (c) 2016 Alessio Moiso - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Page\Lists\Inspectors;

use InerziaSoft\Core\Page\Formatters\DateFormatter;

class DateInspectorComponent extends InspectorComponent {

    protected $dateFormatType;
    protected $timeFormatType;

    /**
     * DateInspectorComponent constructor.
     *
     * @param $displayName string
     * @param $nullValue string
     * @param $valueKey string
     * @param bool|int $dateFormatType integer
     * @param int|null $timeFormatType integer
     */
    public function __construct($displayName, $nullValue, $valueKey, $dateFormatType = \IntlDateFormatter::MEDIUM, $timeFormatType = \IntlDateFormatter::SHORT) {
        parent::__construct($displayName, $nullValue, $valueKey);

        $this->dateFormatType = $dateFormatType;
        $this->timeFormatType = $timeFormatType;
    }

    public function evaluateObject($object, $page) {
        parent::evaluateObject($object, $page);

        if (isset($this->value)) {
            $this->value = $page->getFormatter(DateFormatter::class)->format($this->value, [$this->dateFormatType, $this->timeFormatType]);
        }
    }

}