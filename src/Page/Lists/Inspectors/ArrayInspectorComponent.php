<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Page\Lists\Inspectors;


use InerziaSoft\Core\Utils\ArrayUtils;

class ArrayInspectorComponent extends InspectorComponent {
	
	protected $glueChar;
	protected $innerDisplayValueKey;
	
	/**
	 * ArrayInspectorComponent constructor.
	 * @param $displayName string
	 * @param $nullValue string
	 * @param $arrayKey string
	 * @param string $glueChar
	 * @param bool $localizedValue
	 * @param $localizedPageTitle string|null
	 */
	public function __construct($displayName, $nullValue, $arrayKey, $innerDisplayValueKey = null, $glueChar = ", ", $localizedValues = true, $localizedPageTitle = null) {
		parent::__construct($displayName, $nullValue, $arrayKey, $localizedValues, $localizedPageTitle);
		$this->glueChar = $glueChar;
		$this->innerDisplayValueKey = $innerDisplayValueKey;
	}
	
	public function evaluateObject($object, $page) {
		$this->valueKey = ArrayUtils::valueForKeyInArray($this->valueKey, $object);
		$this->value = "";
		
		if ($this->localizedValue) {
			$finalValues = [];
			if (isset($this->valueKey)) {
				foreach ($this->valueKey as $item) {
					if (isset($this->innerDisplayValueKey)) {
						$finalValue = ArrayUtils::valueForKeyInArray($this->innerDisplayValueKey, $item);
					}
					else {
						$finalValue = $item;
					}
					
					array_push($finalValues, $page->getLocalizedString($finalValue, $this->localizedPageTitle));
				}
			}
		}
		else {
			$finalValues = $this->valueKey;
		}
		
		if (count($finalValues) > 0) {
			$this->value = implode($this->glueChar, $finalValues);
		}
		else {
			$this->value = $this->nullValue;
		}
	}
	
}