<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
namespace InerziaSoft\Core\Page\Lists\Inspectors;

use InerziaSoft\Core\Page\Html\Header;
use InerziaSoft\Core\Page\Html\ListItem;
use InerziaSoft\Core\Page\Html\SimpleText;

class TitleInspectorComponent extends InspectorComponent {
	
	protected $size;
	
	public function __construct($headerSize = 1, $nullValue = null, $valueKey = null) {
		parent::__construct(null, $nullValue, $valueKey);
		
		$this->size = $headerSize;
	}
	
	public function toHtml() {
		$listItem = new ListItem(null, ["bigTitle"], null, [
			new Header($this->size, null, null, null, [new SimpleText($this->getValue())], null)
		], null);
		
		return $listItem->toHtml();
	}
	
}