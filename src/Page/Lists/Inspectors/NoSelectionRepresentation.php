<?php

namespace InerziaSoft\Core\Page\Lists\Inspectors;

use InerziaSoft\Core\Page\Html\Div;
use InerziaSoft\Core\Page\Html\Header;
use InerziaSoft\Core\Page\Html\SimpleText;
use InerziaSoft\Core\Page\Interfaces\HtmlConvertible;
use InerziaSoft\Core\Page\Page;

class NoSelectionRepresentation implements HtmlConvertible {
	
	public $title;
	public $subtitle;
	
	/**
	 * NoSelectionRepresentation convenience initializer.
	 *
	 * Initializes a NoSelectionRepresentation using localized strings as
	 * title and subtitle.
	 *
	 * @discussion This function assumes that $titleKey and $subtitleKey
	 * are localized strings in $page.
	 *
	 * @param $titleKey string
	 * @param $subtitleKey string
	 * @param $page Page
	 * @return static
	 */
	public static function withLocalizedStrings($titleKey, $subtitleKey, $page) {
		$strings = $page->getLocalizedStrings([
			["key" => $titleKey],
			["key" => $subtitleKey]
		]);
		
		return new static($strings[0], $strings[1]);
	}
	
	public function __construct($title, $subtitle) {
		$this->title;
		$this->subtitle;
	}
	
	public function toHtml() {
		$noSelectionContainer = new Div(null, ["noSelectionContainer"], null, [
			new Header(2, null, ["noSelectionTitle"], null, [new SimpleText($this->title)], null),
			new Header(3, null, ["noSelectionSubtitle"], null, [new SimpleText($this->subtitle)], null)
		], null);
		
		return $noSelectionContainer->toHtml();
	}
}