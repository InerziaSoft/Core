<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
namespace InerziaSoft\Core\Page\Html;

use InerziaSoft\Core\Api\Outcomes\ListResponse;
use InerziaSoft\Core\Utils\ArrayUtils;

class UnorderedList extends HtmlTag {
	
	public function __construct($id, $classes, $dataAttributes, $items) {
		parent::__construct("ul", $id, $classes, null, $dataAttributes, false, $items);
	}
	
	public static function fromArray($items, $idKey, $displayNameKey, $onclick = null, $dataAttributes = []) {
		$liItems = [];
		foreach ($items as $item) {
			$attrWithValues = [];
			
			if (isset($dataAttributes)) {
				foreach ($dataAttributes as $key => $value) {
					$attrWithValues[$key] = ArrayUtils::valueForKeyInArray($value, $item);
				}
			}

			$id = ArrayUtils::valueForKeyInArray($idKey, $item);
			$displayName = ArrayUtils::valueForKeyInArray($displayNameKey, $item);

			array_push($liItems, new ListItem($id, null, $attrWithValues, [new SimpleText($displayName)], $onclick));
		}
		
		return new static(null, null, null, $liItems);
	}
	
	/**
	 * @param $response ListResponse
	 * @param $idKey string
	 * @param $displayNameKey string
	 * @param array $dataAttributes
	 * @param string $onclick
	 * @return UnorderedList
	 */
	public static function fromListResponse($response, $idKey, $displayNameKey, $onclick = null, $dataAttributes = []) {
		return static::fromArray($response->items, $idKey, $displayNameKey, $onclick, $dataAttributes);
	}
	
	public function removeAtIndexes($indexes) {
		$remainingItems = [];
		$index = 0;
		foreach ($this->content as $li) {
			if (!in_array($index, $indexes)) {
				array_push($remainingItems, $li);
			}
			$index++;
		}
		
		$this->content = $remainingItems;
	}
}