<?php

/*
MIT License

Copyright (c) 2016 Alessio Moiso - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Page\Html;

use InerziaSoft\Core\Page\Interfaces;
use InerziaSoft\Core\Utils\UriUtils;
use PHPUnit\Framework\Constraint\Count;

class HtmlTag implements Interfaces\HtmlConvertible {

	/**
	 * @var string: The name of this tag
	 */
	private $tagName;

	/**
	 * @var string: An id to identify this tag.
	 */
	protected $id;

	/**
	 * @var array: An array of CSS classes (as strings).
	 */
	protected $classes;

	/**
	 * @var array: An associative array of attributes.
	 */
	protected $attributes;

	/**
	 * @var array: An associative array of custom attributes.
	 */
	protected $dataAttributes;

	/**
	 * @var bool: If true, this tag cannot have any content.
	 */
	protected $autoClose;

	/**
	 * @var \ArrayObject: An array of HtmlTags.
	 */
	protected $content;

	/**
	 * @var string: A JavaScript function to perform when this item is clicked.
	 */
	protected $onclick;

	public function __construct($tagName, $id = null, $classes = array(), $attributes = array(), $dataAttributes = array(), $autoClose = false, $content = array(), $onclick = null) {
		$this->tagName = $tagName;
		$this->id = $id;
		$this->classes = $classes;
		$this->dataAttributes = $dataAttributes;
		$this->autoClose = $autoClose;
		$this->onclick = $onclick;
		$this->attributes = $attributes;

		if (!$this->autoClose) {
			$this->content = $content;
		}
	}

	public function addAttribute($key, $value = ""){
	    $this->attributes[$key] = $value;
    }
    
    public function addDataAttribute($key, $value = null) {
		if (!isset($value)) $value = $key;
		
		$this->dataAttributes[$key] = $value;
    }

	public function addClass($className) {
		if (!isset($this->classes)) $this->classes = [];

		array_push($this->classes, $className);
	}
	
	public function getChildren() {
		return $this->content;
	}
	
	/**
	 * @param array $children
	 */
	public function setChildren($children) {
		$this->content = $children;
	}
	
	public function getDataAttribute() {
		return $this->dataAttributes;
	}
	
	public function getId() {
		return $this->id;
	}

    /**
     * @param array $children
     */
	public function addChildren($children) {
	    foreach ($children as $child){
            if($child instanceof Interfaces\HtmlConvertible) array_push($this->content, $child);
            else {
                // TODO #3 Lanciare eccezzione
            }
        }
    }

	public function toHtml() {
		$html = "<{$this->tagName}";

		if (!$this->isNullOrEmpty($this->id)) {
			$html .= " id='{$this->id}'";
		}

		if (isset($this->classes) && count($this->classes) > 0) {
			$html .= " class='".implode(" ", $this->classes)."'";
		}

		if (isset($this->attributes)) {
			foreach ($this->attributes as $attributeKey => $attributeValue) {
				$realValue = $attributeValue;
				if ($attributeValue instanceof \DateTime) {
					$realValue = $attributeValue->format("Y-m-d H:i:s");
				}
				
				$html .= " {$attributeKey}='{$realValue}'";
			}
		}

		if (isset($this->dataAttributes)) {
			foreach ($this->dataAttributes as $attributeKey => $attributeValue) {
				$realValue = $attributeValue;
				if ($attributeValue instanceof \DateTime) {
					$realValue = $attributeValue->format("Y-m-d H:i:s");
				}
				
				$html .= " data-{$attributeKey}='{$realValue}'";
			}
		}

		if (!$this->isNullOrEmpty($this->onclick)) {
			$html .= " onclick='{$this->onclick}'";
		}

		if ($this->autoClose) {
			$html .= " />";
		}
		else {
			$html .= ">";

			/** @var HtmlTag $item */
			if (isset($this->content)) {
				foreach ($this->content as $item) {
					if ($item instanceof Interfaces\HtmlConvertible) {
						$html .= $item->toHtml();
					}
					else {
						$html .= $item;
					}
				}
			}

			$html .= "</{$this->tagName}>";
		}

		return $html;
	}

	private function isNullOrEmpty($string) {
		return (!isset($string) || $string == null || ($string instanceof \Countable && count($string) == 0));
	}

	public static function getBaseUri($uri) {
        return UriUtils::getBaseUri($uri);
    }

}