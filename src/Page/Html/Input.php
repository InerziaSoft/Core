<?php

namespace InerziaSoft\Core\Page\Html;

use InerziaSoft\Core\Page\Exceptions\InvalidOperationException;

class Input extends HtmlTag {
	
	/**
	 * Input constructor.
	 * @param $id string
	 * @param $classes array
	 * @param $type string
	 * @param $placeholder string
	 * @param $onChange string
	 * @param $dataAttributes array
	 */
    public function __construct($id, $classes, $type, $placeholder = null, $onChange = null, $dataAttributes = null, $value = null, $name = null, $attributes = array()) {
        $attributes["onchange"] = $onChange;
        $attributes["type"] = $type;
        $attributes["placeholder"] = $placeholder;
        $attributes["value"] = $value;
        $attributes["name"] = $name;
        parent::__construct("input", $id, $classes, $attributes, $dataAttributes, true);
    }
    
    public function getType() {
    	return $this->attributes["type"];
    }
    
    public function check() {
    	$this->attributes["checked"] = "checked";
    }
    
    public function disable() {
    	$this->attributes["disabled"] = "disabled";
    }
    
}