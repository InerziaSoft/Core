<?php

/*
MIT License

Copyright (c) 2016 Alessio Moiso - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Page\Html;

use InerziaSoft\Core\Page\Interfaces\HtmlConvertible;

define("NAVIGATIONBAR_ITEM_ID_SUFFIX", "NavLink");

class NavigationBarItem implements HtmlConvertible {

	private $name;
	private $page;

	private $liTag;

	public function __construct($name, $icon, $page, $classes = null, $innerClasses = null, $imageClasses = null, $onclick = null, $additionalContent = null) {
		$this->name = $name;
		$this->page = $page;
		
		$contentToAdd = [];

        if (isset($icon)) {
            $content = new Image(null, $imageClasses, null, $icon, null);
        }
        else {
            $content = new SimpleText($this->name);
        }

        $listItemContent = new Hyperlink(null, $innerClasses, null, null, [$content], $page, null);
        if($page == null)
            $listItemContent = $content;
        
        array_push($contentToAdd, $listItemContent);
        if (isset($additionalContent)) array_push($contentToAdd, $additionalContent);

		$this->liTag = new ListItem(strtolower($this->name).NAVIGATIONBAR_ITEM_ID_SUFFIX, $classes, null, $contentToAdd, $onclick );
	}

	public function select($className = "navSelected") {
	    $this->liTag->select($className);
    }

	public function toHtml() {
		return $this->liTag->toHtml();
	}
}