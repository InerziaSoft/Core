<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
namespace InerziaSoft\Core\Page\Html;

use InerziaSoft\Core\Api\Outcomes\ListResponse;
use InerziaSoft\Core\Utils\ArrayUtils;

class Select extends HtmlTag {
	
	/**
	 * Select constructor.
	 * @param array $content
	 * @param string $name
	 * @param array $classes
	 * @param string $onchange
	 * @param bool $addNoSelectionOption
	 * @param string $defaultSelectionText
	 * @param array $attribute
	 * @param array $dataAttributes
	 * @param array $id
	 */
	public function __construct($content, $name, $classes, $onchange, $addNoSelectionOption = false, $defaultSelectionText = "", $attribute = null, $dataAttributes = null, $id = null) {
		
		$items = [];
		
		if ($addNoSelectionOption) {
			$option = new Option(-1, $defaultSelectionText == "" ? "No selection" : $defaultSelectionText);
			array_push($items, $option);
		}
		
		parent::__construct("select", $id, $classes, ["onchange" => $onchange, "name" => $name], $dataAttributes, false, array_merge($items, $content), null);
	}
	
	/**
	 * Generates a new Select using the content of a ListResponse.
	 *
	 * @param $id string
	 * @param $name string
	 * @param $classes array
	 * @param $dataAttributes array
	 * @param $listResponse ListResponse
	 * @param $value string: The key to be used as value for each option.
	 * @param $displayName string: The key to be displayed as title for each option.
	 * @param string $onchange : A JavaScript function to be called when the "onchange" event is fired.
	 * @param string $addRowLabel : A label to be displayed as a separated option which represents the ability to add items to the list.
	 * @param bool $addNoSelectionOption : If true, adds a default option which represents the absence of a selection.
	 * @param string $defaultSelectionText : If the previous parameter is true, the label can be customized by passing a string to this parameter.
	 * @param array $dataAttributesKeyDbColumn : Data attributes to be added to each option.
	 * @param $groupedByKey string|null: If not null, the select will use the specified key to group items under option groups.
	 * @param $groupDisplayColumn string|null: The key to be displayed as label of each option group.
	 *
	 * @return Select
	 */
	static function fromListResponse($id, $name, $classes, $dataAttributes, $listResponse, $value, $displayName, $onchange = "", $addRowLabel = "", $addNoSelectionOption = false, $defaultSelectionText = "", $dataAttributesKeyDbColumn = array(), $groupedByKey = null, $groupDisplayColumn = null, $selectedValue = null) {
		
		$items = [];
		if ($addRowLabel != "") {
			$option = new Option(-2, $addRowLabel);
			$optionGroup = new OptionGroup([$option], null, null, ["addRow"]);
			array_push($items, $optionGroup);
		}
		
		$lastGroup = "";
		$optionGroupContent = array();
		
		foreach ($listResponse->items as $item) {
			$dataAttributeOption = [];
			if (isset($dataAttributesKeyDbColumn)) {
				$keys = array_keys($dataAttributesKeyDbColumn);
				foreach ($dataAttributesKeyDbColumn as $key => $dbKey) {
					$dataAttributeOption[$key] = ArrayUtils::valueForKeyInArray($dbKey, $item);
				}
			}
			
			$option = new Option($item[$value], $item[$displayName], null, $dataAttributeOption);
			
			if ($groupedByKey != null) {
				$currentValue = $item[$groupedByKey][$groupDisplayColumn];
				if (count($optionGroupContent) > 0 && $lastGroup != $currentValue) {
					$optionGroup = new OptionGroup($optionGroupContent, $lastGroup);
					$lastGroup = $currentValue;
					$optionGroupContent = array();
					array_push($optionGroupContent, $option);
				} else {
					$lastGroup = $currentValue;
					array_push($optionGroupContent, $option);
					continue;
				}
				
				array_push($items, $optionGroup);
			} else {
				array_push($items, $option);
			}
		}
		
		$select = new Select($items, $name, $classes, $onchange, $addNoSelectionOption, $defaultSelectionText, null, $dataAttributes, $id);
		
		if (isset($selectedValue)) {
			$select->selectOption($selectedValue);
		}
		
		return $select;
	}
	
	/**
	 * Selects the option with the specified id.
	 *
	 * @param $id int|string
	 * @param $markAsOriginal bool: If true, a data attribute named "data-original" will be appended to the selected option.
	 *
	 * @return bool true if the option has been selected, false otherwise.
	 */
	function selectOption($id, $markAsOriginal = false) {
		/** @var Option $option */
		foreach ($this->content as $option) {
			if ($option instanceof OptionGroup) {
				if ($option->selectOption($id)) return true;
			} else {
				if ($option->getValue() == $id) {
					$option->select($markAsOriginal);
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Returns the list of options contained by this select.
	 *
	 * @return array|\ArrayObject
	 */
	final function getOptions() {
		return $this->content;
	}
	
}