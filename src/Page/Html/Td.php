<?php

namespace InerziaSoft\Core\Page\Html;

class Td extends HtmlTag {

    function __construct($id, $classes=array(), $content=array(), $attributes=array(), $dataAttributes=array())
    {
        parent::__construct("td", $id, $classes, $attributes, $dataAttributes, false, $content, null);
    }

}