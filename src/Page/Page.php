<?php

/*
MIT License

Copyright (c) 2016 Alessio Moiso - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Page;

#region Definitions

/**
 * Defines the localized string key that will be used to represent page titles in the correct language.
 *
 * This key must be present in the database and must be connected to the correct page.
 */

use InerziaSoft\Core\Api\Localization\Language;
use InerziaSoft\Core\Api\Localization\Responses\DefaultLanguageResponse;
use InerziaSoft\Core\Api\Localization\Responses\LocalizedStringResponse;
use InerziaSoft\Core\Api\Localization\Responses\MultipleLocalizedStringResponse;
use InerziaSoft\Core\Definitions\ClassNameInitializable;
use InerziaSoft\Core\Api\Outcomes\Outcome;
use InerziaSoft\Core\Api\Outcomes\Success;
use InerziaSoft\Core\Logger\Logger;
use InerziaSoft\Core\Page\Documents\DocumentsSet;
use InerziaSoft\Core\Page\Documents\JsDocument;
use InerziaSoft\Core\Page\Exceptions\ConfigurationErrorException;
use InerziaSoft\Core\Page\Exceptions\UnexistingPageException;
use InerziaSoft\Core\Page\Formatters\Formatter;
use InerziaSoft\Core\Page\Html\FooterBar;
use InerziaSoft\Core\Page\Html\HtmlTag;
use InerziaSoft\Core\Page\Html\Meta;
use InerziaSoft\Core\Page\Html\NavigationBar;
use InerziaSoft\Core\Page\Interfaces\HtmlConvertible;
use InerziaSoft\Core\Page\Lists\Inspectors\NoSelectionRepresentation;
use InerziaSoft\Core\Page\Lists\ObjectsList;
use InerziaSoft\Core\Page\Lists\ObjectsListInspector;
use InerziaSoft\Core\Page\Lists\ObjectsListItemDefinition;
use InerziaSoft\Core\Page\Lists\Proposals\ProposedObjectsList;
use InerziaSoft\Core\Page\Seo\FacebookSeo;
use InerziaSoft\Core\Utils\ArrayUtils;

define("PAGE_TITLE_HIVE_KEY", "page_title");

define("PAGE_DESCRIPTION_HIVE_KEY", "page_description");

/**
 * Defines the key of the hive that will contain the included CSS documents in every page.
 */
define("PAGE_CSS_INCLUDES", "page_css");

/**
 * Defines the key of the hive that will contain the Facebook SEO meta tags in every page.
 */
define("FACEBOOK_SEO_META", "facebook_seo_meta");

/**
 * Defines the key of the hive that will contain the included JS documents in every page.
 */
define("PAGE_JS_INCLUDES", "page_js");

/**
 * Defines the key of the hive that will contain the included JS documents that should be placed at the top of every page.
 */
define("PAGE_JS_TOP_INCLUDES", "top_page_js");

/**
 * Defines the key of the hive that will contain a boolean indicating whether the user is authenticated or not.
 */
define("PAGE_AUTHENTICATION_HIVE_KEY", "is_auth");

/**
 * Defines the abstract page name that is connected to localized string keys that appears in all pages.
 */
define("GLOBAL_PAGE_TITLE", "global");

/**
 * Defines the key that will be used in the hive to store the application name.
 */
define("APPLICATION_NAME_TITLE", "appName");

/**
 * Defines the key of the hive that will contain a list of definitions to auto-configure ObjectsList in a page.
 */
define("AUTO_CONFIGURE_LISTS_HIVE_KEY", "auto_configure_lists");

/**
 * Defines the key of the hive that will contain if the page will responsive.
 */
define("PAGE_RESPONSIVE", "responsive");

/**
 * Defines the default template filename which contains the layout that has to be applied to each page.
 */
define("DEFAULT_LAYOUT_FILE", "_layout");

/**
 * Defines the hive key that should be filled with the base URI.
 */
define("BASE_URI_HIVE_KEY", "base_uri");

/**
 * Defines the cookie name that contains the preferred language ISO code.
 */
define("PAGE_PREFERRED_LANGUAGE_COOKIE", "preferredLang");

/**
 * Defines the hive key that should be filled with the default language that JavaScript should use
 * when nothing else is specified.
 */
define("PAGE_DEFAULT_LANGUAGE_HIVE_KEY", "defaultLanguage");

#endregion

/**
 * Class Page
 *
 * This class represents a generic page in the application.
 * Provides implementations for default methods that are shared among all pages
 * and includes a convenient initializer that takes the name of a page and instantiates its corresponding subclass.
 *
 * @package InerziaSoft
 */
class Page implements ClassNameInitializable {

    private $sessionToken;
	
	/**
	 * Returns the HTTP method which should be used to load this page.
	 *
	 * @return string
	 */
    public function getRouteMethod() {
    	return ApiRequest::$getMethod;
    }
	
	/**
	 * Instantiates a new page with its representative class
	 *
	 * @param $className string: The name of the class that represents this page.
	 * @return Page : an object of class Page or one of its subclasses that represents the requested page.
	 * @throws ConfigurationErrorException
	 * @throws UnexistingPageException If the specified class cannot be found.
	 */
    public static function instanceFromClassName($className) {
    	if (!defined("PAGES_CLASSES_NAMESPACE")) throw ConfigurationErrorException::missingDefine("PAGES_CLASSES_NAMESPACE");
    	
        $className = PAGES_CLASSES_NAMESPACE.$className;

        if (class_exists($className)) {
            return new $className();
        }
        else {
            throw new UnexistingPageException($className);
        }
    }
	
	/**
	 * Returns whether the current session is authenticated or not.
	 *
	 * @return bool
	 */
    final public function isAuthenticated() {
        return isset($this->sessionToken);
    }
	
	/**
	 * Sets the authentication token to the corresponding session.
	 *
	 * @param $sessionToken
	 */
    final public function setSessionToken($sessionToken) {
        $this->sessionToken = $sessionToken;
    }
	
	/**
	 * Returns the current session token.
	 *
	 * @return mixed
	 */
    final public function getSessionToken() {
        return $this->sessionToken;
    }
	
	/**
	 * Returns the path corresponding to the view/template
	 * that should be loaded along this page.
	 *
	 * @discussion The default implementation returns
	 * a sub-path of the FatFree default views folder corresponding
	 * to the namespace of this class.
	 *
	 * @discussion Subclasses can override this method to use a different
	 * auto-location mechanism.
	 * @return string
	 * @throws ConfigurationErrorException
	 */
    protected function viewAutoLocation() {
	    if (!defined("PAGES_CLASSES_NAMESPACE")) throw ConfigurationErrorException::missingDefine("PAGES_CLASSES_NAMESPACE");
    	
        $viewUrl = get_class($this);
        $viewUrl = str_replace([PAGES_CLASSES_NAMESPACE, "\\"], ["", "/"], $viewUrl);

        return strtolower($viewUrl);
    }

    private function objectsListHtml() {
        $objectsList = "";
        /** @var ProposedObjectsList $list */
        foreach ($this->getLists() as $list) {
            $objectsList .= $list->toHtml();
        }
        return $objectsList;
    }
	
	/**
	 * Parses and optionally returns the HTML representation of an arbitrary value.
	 *
	 * @discussion
	 *  - If $item is an instance of HtmlConvertible, this method returns the result of its implementation of toHtml().
	 *  - If $item is an array, this method attempts to recursively parse it to convert instances of HtmlConvertible.
	 *  - If $item is none of the above, this method just returns $item with no further modifications.
	 *
	 * @param $item
	 * @return string|array
	 */
    protected function handleHtmlConvertible($item) {
    	if ($item instanceof HtmlConvertible) {
    		return $item->toHtml();
	    }
	    else if ($item instanceof \DateTime) {
		    return $item->format("Y-m-d H:i:s");
	    }
        else if (ArrayUtils::isAssociativeArray($item)) {
            $result = [];
            foreach ($item as $key => $insideItem) {
                $result[$key] = $this->handleHtmlConvertible($insideItem);
            }
            return $result;
        }
	    else if (is_array($item)) {
    		$result = [];
    		foreach ($item as $insideItem) {
    			array_push($result, $this->handleHtmlConvertible($insideItem));
		    }
		    return $result;
	    }
	    else {
    		return $item;
	    }
    }
	
	/**
	 * Returns the HTML representation of this page.
	 *
	 * @discussion Anything passed in $hive should be either a primitive value
	 * or an instance of an object compliant with HtmlConvertible.
	 *
	 * @param $hive array: An associative array to be passed to the view as its database.
	 * @param null $template string: A template name to use instead of the default view.
	 * @return string The HTML representation of the page.
	 * @throws ConfigurationErrorException
	 * @throws \InerziaSoft\Core\Parser\Exceptions\InvalidClassMapException
	 * @throws \InerziaSoft\Core\Parser\Exceptions\UnexistingClassMapException
	 */
    function display($hive = null, $template = null) {
	    if (!defined("PAGE_LAYOUT_CONTENT_KEY")) throw ConfigurationErrorException::missingDefine("PAGE_LAYOUT_CONTENT_KEY");
	    if (!defined("PAGE_LAYOUT_FOLDER")) throw ConfigurationErrorException::missingDefine("PAGE_LAYOUT_FOLDER");
    	
        if ($hive == null) $hive = [];

        foreach ($hive as $key => $value) {
            $hive[$key] = $this->handleHtmlConvertible($value);
        }

        if ($template == null) {
            if (!array_key_exists(PAGE_TITLE_HIVE_KEY, $hive)) {
                $localizedTitle = $this->getLocalizedTitle();
                $pageTitle = $this->getTitle();
                if (isset($localizedTitle)) {
                    $hive[PAGE_TITLE_HIVE_KEY] = $localizedTitle;
                } else {
                    $hive[PAGE_TITLE_HIVE_KEY] = $pageTitle;
                }
            }

            $template = $this->viewAutoLocation();
        }

        $hive[BASE_URI_HIVE_KEY] = HtmlTag::getBaseUri("/");

        // Define if the page will be responsive
        $hive[PAGE_RESPONSIVE] = $this->isResponsive();

        // Including Js and Css documents
        $hive[PAGE_CSS_INCLUDES] = $this->cssDocuments();
        
        $facebookSeo = $this->getFacebookSeo();
        if (isset($facebookSeo)) {
            $hive[FACEBOOK_SEO_META] = $facebookSeo->toHtml();
        }
        
        $description = $this->getDescription();
        if (isset($description)) {
            $metaDescription = new Meta("description", $description);
            $hive[PAGE_DESCRIPTION_HIVE_KEY] = $metaDescription->toHtml();
        }
        
        $jsDocs = new DocumentsSet();
        $jsTopDocs = new DocumentsSet();
        /** @var JsDocument $document */
	    foreach ($this->jsDocuments()->getDocuments() as $document) {
        	if ($document->shouldBePlacedInHead()) {
        		$jsTopDocs->push($document);
	        }
	        else {
        		$jsDocs->push($document);
	        }
        }
        
        $hive[PAGE_JS_INCLUDES] = $jsDocs;
        $hive[PAGE_JS_TOP_INCLUDES] = $jsTopDocs;

        // Setting Authentication boolean
        $hive[PAGE_AUTHENTICATION_HIVE_KEY] = $this->isAuthenticated();

        // Initializing ObjectsLists
        $hive[AUTO_CONFIGURE_LISTS_HIVE_KEY] = $this->objectsListHtml();

        if (!$this->hasPreferredLanguage()) {
            $request = (new ApiRequest("language", "default", ApiRequest::$getMethod, null, $this->getSessionToken(), null, $this->getPreferredLanguage()))->request();
        	if ($request instanceof DefaultLanguageResponse) {
                $hive[PAGE_DEFAULT_LANGUAGE_HIVE_KEY] = $request->code;
	        }
        }
        
        // Rendering and including the page
        $pageContent = \Template::instance()->render($template.".htm", "text/html", $hive);
        $hive[PAGE_LAYOUT_CONTENT_KEY] = $pageContent;

        // Rendering and including the navigation bar (if any)
        $navBar = $this->getNavigationBar();
        if ($navBar != null) {
        	if (defined("PAGE_LAYOUT_NAVIGATION_BAR_KEY")) {
		        $hive[PAGE_LAYOUT_NAVIGATION_BAR_KEY] = $navBar->toHtml();
	        }
	        else {
		        throw new \InvalidArgumentException("Page '".get_called_class()."' returns a navigation bar but environment does not define PAGE_LAYOUT_NAVIGATION_BAR_KEY.");
	        }
        }

        // Rendering and including the footer bar (if any)
        $footerBar = $this->getFooterBar();
        if ($footerBar != null) {
        	if (defined("PAGE_LAYOUT_FOOTER_BAR")) {
		        /** @noinspection PhpUndefinedConstantInspection */
		        $hive[PAGE_LAYOUT_FOOTER_BAR] = $footerBar->toHtml();
	        }
        	else {
        		throw new \InvalidArgumentException("Page '".get_called_class()."' returns a footer bar but environment does not define PAGE_LAYOUT_FOOTER_BAR.");
	        }
        }
        
        $onLoad = $this->getOnLoadFunction();
        if (isset($onLoad) && strlen($onLoad)) {
        	if (!defined("PAGE_ONLOAD_FUNCTION_KEY")) {
        		throw new ConfigurationErrorException("Page of class '".get_called_class()."' defines an OnLoad function, but no 'PAGE_ONLOAD_FUNCTION_KEY' define can be found.");
	        }
        	$hive[PAGE_ONLOAD_FUNCTION_KEY] = "onload='$onLoad'";
        }

        return \Template::instance()->render(PAGE_LAYOUT_FOLDER."/".$this->getBaseLayout().".htm", "text/html", $hive);
    }
	
	/**
	 * Returns the filename of the base layout template,
	 * which defaults to the constant DEFAULT_LAYOUT_FILE.
	 *
	 * @return string
	 */
    protected function getBaseLayout() {
	    return DEFAULT_LAYOUT_FILE;
    }
    
    /**
     * Returns the HTML representation of a partial page child of this page.
     *
     * @param $hive array: An associative array to be passed to the view as its database.
     * @param $template string: A path to a custom template.
     * @return string The HTML representation of the page.
     * @discussion A partial page is a view that does not include navbar and footer and is designed to be embedded in another
     * web page. The standard protocol name for a partial view is the following: <pageName>_<partialName>.htm
     */
    final function displayPartial($hive = null, $template = null) {
        if ($hive == null) $hive = [];

        foreach ($hive as $key => $value) {
            $hive[$key] = $this->handleHtmlConvertible($value);
        }

        if (isset($template)) {
            $pageTitle = $template;
        }
        else {
            $pageTitle = $this->viewAutoLocation();
        }

        $hive[BASE_URI_HIVE_KEY] = HtmlTag::getBaseUri("/");

        $hive[PAGE_AUTHENTICATION_HIVE_KEY] = $this->isAuthenticated();
        $hive[AUTO_CONFIGURE_LISTS_HIVE_KEY] = $this->objectsListHtml();

        return \Template::instance()->render(strtolower($pageTitle.".htm"), "text/html", $hive);
    }
	
	/**
	 * Handles and returns a PartialPage with the specified name child of this page.
	 *
	 * @param $partialName string: The class name of the Partial to handle.
	 * @param array $hive
	 * @return string A partial page ready to be displayed.
	 * @throws ConfigurationErrorException
	 * @discussion The namespace of this page will be automatically prefixed to the name of the PartialPage.
	 */
    final function handlePartial($partialName, $hive = array()) {
	    if (!defined("PAGES_CLASSES_NAMESPACE")) throw ConfigurationErrorException::missingDefine("PAGES_CLASSES_NAMESPACE");
    	
        $reflectionClass = new \ReflectionClass($this);
        $namespace = str_replace(PAGES_CLASSES_NAMESPACE, "", $reflectionClass->getNamespaceName());

        $page = Page::instanceFromClassName($namespace."\\".$partialName);
        $page->setSessionToken($this->getSessionToken());

        return $page->display($hive);
    }
	
	/**
	 * Handles and returns an ObjectsList on a specified table name.
	 *
	 * @param $objectsListDefinitionClass ObjectsListItemDefinition: An instance that describes an ObjectsListItem.
	 * @param $foreignKeyValue string|null: The value of the foreign key to match (if this is a child of a grouped list; "null" literal matches rows with foreign key value equal to null).
	 * @return string The list ready to be displayed.
	 */
    final function handleList($objectsListDefinitionClass, $foreignKeyValue = null) {
	    $objectsList = new ObjectsList($this, $objectsListDefinitionClass, $foreignKeyValue);
	    $objectsList->setSessionToken($this->getSessionToken());
	    
        return $objectsList->displayList();
    }
	
	/**
	 * Handles and returns an ObjectsListInspector on a specified object.
	 *
	 * @param $tableName string: The name of the table to query.
	 * @param $apiClass string: An optional string identify the API class to call (defaults to $tableName).
	 * @param $apiCall string: An optional string with a trailing slash to prefix to the objectId in the API call (defaults to an empty string).
	 * @param $objectId int: The identifier of the required object.
	 * @param string $objectIdKey : The parameter name to use when querying the API call.
	 * @return string The inspector ready to be displayed.
	 */
    final public function handleListInspector($tableName, $apiClass = null, $apiCall = "", $objectId, $objectIdKey = "id") {
        if (!isset($apiClass)) $apiClass = $tableName;

        $listInspector = new ObjectsListInspector($tableName, $apiClass, $apiCall, $objectId, $objectIdKey, $this);
        $listInspector->setSessionToken($this->getSessionToken());

        return $listInspector->displayInspector();
    }

    /**
     * Calls API to retrieve the localized version of a specified string.
     *
     * @discussion The database associated to this application must be configured properly.
     *
     * @param $key string: The key that represents the requested string.
     * @param $pageTitle string: The page title to use when looking for the requested key.
     * @param $language string: The language identifier (in short ISO) to use.
     *
     * @return string The corresponding string in the specified language or $key if the string cannot be found.
     */
    public function getLocalizedString($key, $pageTitle = null, $language = null) {
        if (!isset($pageTitle)) $pageTitle = $this->getTitle();
        if (!isset($language)) $language = $this->getPreferredLanguage();
        
        $request = new ApiRequest(ApiRequest::$languageClass, "string", ApiRequest::$getMethod,
	        [
	            $key,
		        $pageTitle,
		        $language
            ]
        );
        
        $result = $request->request();
	    
        if ($result instanceof LocalizedStringResponse) {
            return $result->string;
        }
        return $key;
    }
	
	/**
	 * Calls API to retrieve a localized versions of a set of strings.
	 *
	 * @param $keysAndPages array: An indexed array like ["key" => <key>, "page" => <page>] (page is optional).
	 * @param string $language string: The language identifier (in short ISO) to use.
	 * @return array An array of LocalizedStringResponse.
	 */
    public function getLocalizedStrings($keysAndPages, $language = null) {
	    if (!isset($language)) $language = $this->getPreferredLanguage();
	    $pageTitle = $this->getTitle();
	    
	    $keysAndPagesComplete = [];
	    foreach ($keysAndPages as $keyAndPage) {
		    if (!array_key_exists("page", $keyAndPage)) {
			    $keyAndPage["page"] = $pageTitle;
		    }
		    array_push($keysAndPagesComplete, $keyAndPage);
	    }
	    
	    $request = new ApiRequest(ApiRequest::$languageClass, "strings", ApiRequest::$postMethod, ["keysAndPages" => $keysAndPagesComplete, "language" => $language]);
	    $result = $request->request();
	    
	    if ($result instanceof MultipleLocalizedStringResponse) {
		    return $result->strings;
	    }
	    return [];
    }
	
    public function hasPreferredLanguage() {
    	return isset($_COOKIE[PAGE_PREFERRED_LANGUAGE_COOKIE]);
    }
    
	/**
	 * Returns the ISO identifier of the preferred language set by the
	 * user or the client application.
	 *
	 * @return string
	 */
    public function getPreferredLanguage() {
    	if ($this->hasPreferredLanguage()) {
    		return $_COOKIE[PAGE_PREFERRED_LANGUAGE_COOKIE];
	    }
	    $reqLanguage = substr($_SERVER["HTTP_ACCEPT_LANGUAGE"], 0, 2);
    	return Language::getLanguageOrFallback($reqLanguage);
    }
	
	/**
	 * Applies the requested localized strings to the specified hive.
	 *
	 * @discussion This function automatically converts localized keys
	 * from camelCase style to underscore_case.
	 *
	 * @param $keysAndPages array
	 * @param $hive array
	 * @param $language string
	 *
	 * @return array
	 */
    public function applyLocalizedStringsToHive($keysAndPages, $hive = null, $language = null) {
        if (!isset($hive)) $hive = [];

        $strings = $this->getLocalizedStrings($keysAndPages, $language);

        $newHive = [];
        /** @var LocalizedStringResponse $string */
        foreach ($strings as $string) {
            $newHive[$this->camelCaseToUnderscore($string->key)] = $string->string;
        }

        return array_merge($hive, $newHive);
    }

    private function camelCaseToUnderscore($string) {
        preg_match_all("!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!", $string, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match === strtoupper($match) ? strtolower($match) : lcfirst($match);
        }

        return implode("_", $ret);
    }

    /**
     * Returns the page title.
     *
     * @discussion The default implementation returns the class name.
     * @return string The page title.
     */
    function getTitle() {
        $className = get_class($this);
        $className = explode("\\", $className);

        if (count($className) >= 2) {
            return $className[count($className)-1];
        }
        return $className[0];
    }

    /**
     * Decides whether this page should required authentication or not.
     *
     * @discussion If this method returns true, the standard implementation will redirect the user to a login page.
     * @return bool true if this page can be viewed by authenticated users only.
     */
    function requiresAuthentication() {
        return false;
    }

    /**
     * Identifies authentication pages from standard ones.
     *
     * @discussion An authentication page can be automatically skipped if the user is already authenticated.
     * @return bool true if this page is or contains the authentication mask.
     */
    function isAuthenticationGateway() {
        return false;
    }

    /**
     * Returns a set of CSS documents to include in the page.
     *
     * @return DocumentsSet A set of CSS documents.
     */
    function cssDocuments() {
        return new DocumentsSet();
    }

    /**
     * Returns a set of JS documents to include in the page.
     *
     * @return DocumentsSet A set of JS documents.
     */
    function jsDocuments() {
        return new DocumentsSet();
    }

    /**
     * Returns an array of ObjectsListDefinition presented by this page.
     *
     * @discussion The default implementation returns an empty array.
     * @return array
     */
    function getLists() {
        return [];
    }
	
	/**
	 * Returns an array of InspectorComponents objects or subclasses
	 * that will be used to represent an inspector.
	 *
	 * @discussion The default implementation returns an empty array;
	 *
	 * @param $tableName string
	 * @param $object array
	 * @return array
	 */
	function inspectorComponentsForObject($tableName, $object) {
		return [];
	}
	
	/**
	 * Returns a representation of "No Selection" for the specified table
	 * or null, if none is required.
	 *
	 * @discussion The default implementation returns null.
	 *
	 * @param $tableName
	 * @return NoSelectionRepresentation|null
	 */
	function getNoSelectionRepresentationForInspector($tableName) {
		return null;
	}
    
    /**
     * Returns a NavigationBar or null if no navigation bar is needed.
     *
     * @discussion Subclasses are required to override this method to return a custom navigation bar.
     * The default implementation returns null.
     *
     * @return NavigationBar|null
     */
    function getNavigationBar() {
        return null;
    }

    /**
     * Returns a FooterBar or null if no footer bar is needed.
     *
     * @discussion Subclasses are required to override this method to return a custom footer bar.
     * The default implementation returns null.
     *
     * @return FooterBar|null
     */
    function getFooterBar() {
        return null;
    }

    /**
     * Returns a custom route path for this page.
     *
     * @discussion By default, this method returns null, leaving the choice of
     * a route path to the Route Initializer. Subclasses can override this method
     * to modify this behavior.
     *
     * @return string: A custom route path.
     */
    function getRoutePath() {
        return null;
    }
	
	/**
	 * Returns an array of PageRouteParameter that defines
	 * the parameters that this page accepts.
	 *
	 * @discussion The default implementation returns an empty array.
	 *
	 * @return array
	 */
    function getRouteParameters() {
        return [];
    }

    /**
     * Returns if the page supports responsiveness.
     *
     * @discussion By default, this method return true. Subclasses can override this method
     * to modify this behavior.
     *
     * @return bool
     */
    function isResponsive() {
        return true;
    }
	
	/**
	 * Returns a JavaScript function reference to be performed
	 * as the "onload" event.
	 *
	 * @discussion If this function returns null, as the default
	 * implementation, the onload event will not fire any function.
	 *
	 * @return string|null
	 */
    function getOnLoadFunction() {
    	return null;
    }
    
    /**
     * Returns a properly initialized formatter for this page
     * based on the specified class.
     *
     * @param $class string
     * @return Formatter
     */
    final function getFormatter($class) {
        return new $class($this);
    }
	
	/**
	 * Returns the FatFree Cache System.
	 *
	 * @return \Cache
	 */
    protected function getCacheSystem() {
    	return \Cache::instance();
    }
	
	/**
	 * Returns whether an item is available in cache or not.
	 *
	 * @param $key
	 * @return mixed
	 */
    protected function isCached($key) {
    	$cache = $this->getCacheSystem();
    	return $cache->exists($key);
    }
    
	/**
	 * Returns a previously cached value or FALSE.
	 *
	 * @param $key string
	 * @return FALSE|mixed
	 */
    protected function getCachedValue($key) {
    	$cache = $this->getCacheSystem();
    	return $cache->get($key);
    }
	
	/**
	 * Sets a specific value in cache. Returns FALSE if the value cannot be set.
	 *
	 * @param $key string
	 * @param $value mixed
	 * @param $ttl int
	 * @return FALSE|mixed
	 */
    protected function cache($key, $value, $ttl) {
    	$cache = $this->getCacheSystem();
    	return $cache->set($key, $value, $ttl);
    }

    /**
     * Get the default page name.
     *
     * This function throws an exception, if
     * the `DefaultPage` constant is not defined.
     *
     * @return string
     */
    protected function getDefaultPageName() {
        if (!\Base::instance()->exists("DefaultPage")) {
            throw new \InvalidArgumentException("Page '".get_called_class()."' requires a redirect because of a unauthorized access attempt, but default hive does not define the 'DefaultPage' key.");
        }

        return \Base::instance()->get("DefaultPage");
    }

	/**
	 * Reroutes an attempt to access a unauthorized
	 * page to the default route.
	 */
    protected function rerouteUnauthorized() {
	    \Base::instance()->reroute("/".$this->getDefaultPageName());
    }

    /**
     * Reroute to the specified destination.
     *
     * If `destination` is not set, it will reroute to the
     * default page.
     *
     * @param $permanent bool Indicates if the redirect is permanent or not.
     * @param $destination string|null The page name.
     */
    protected function reroute($permanent, $destination = null) {
        if (!isset($destination)) {
            $destination = $this->getDefaultPageName();
        }

        \Base::instance()->reroute("/".$destination, $permanent, true);
    }
	
	/**
	 * Returns true if this page should support
	 * an additional "upload" route to handle
	 * receiving files.
	 *
	 * @return bool
	 */
    function supportsFileUpload() {
    	return false;
    }
	
	/**
	 * Handles the upload of a file with application
	 * specific parameters.
	 *
	 * @param $parameters array
	 * @return Outcome
	 */
    function handleFileUpload($parameters) {
    	return new Success();
    }
	
	/**
	 * Get the Facebook SEO options for this page.
	 *
	 * The default implementation returns `null`.
	 *
	 * @return FacebookSeo|null
	 */
    function getFacebookSeo() {
    	return null;
    }

    /**
     * Get the page localized title.
     * This title is used as page title, if defined.
     *
     * The default implementation returns `null`.
     *
     * @return string|null
     */
    function getLocalizedTitle() {
        return null;
    }

	/**
	 * Get the page description.
	 *
	 * The default implementation returns `null`.
	 *
	 * @return string|null;
	 */
    function getDescription() {
    	return null;
    }
}