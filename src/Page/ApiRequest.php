<?php

/*
MIT License

Copyright (c) 2016 Alessio Moiso - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Page;
use InerziaSoft\Core\Page\Exceptions\ApiRequestException;
use InerziaSoft\Core\Page\Exceptions\GenericRequestErrorException;
use InerziaSoft\Core\Page\Exceptions\InternalServerErrorException;
use InerziaSoft\Core\Page\Exceptions\InvalidApiResponseException;
use InerziaSoft\Core\Page\Exceptions\InvalidRequestStatusException;
use InerziaSoft\Core\Page\Exceptions\RequestNotFoundException;
use InerziaSoft\Core\Parser\Exceptions\InvalidClassMapException;
use InerziaSoft\Core\Parser\Exceptions\UnexistingClassMapException;
use InerziaSoft\Core\Parser\JsonParser;
use InerziaSoft\Core\Utils\UriUtils;

define("API_RESPONSE_CLASS", "InerziaSoft\\Core\\Api\\Outcomes\\Response");
define("DBOBJECT_CLASS", "InerziaSoft\\Core\\Db\\DbObject\\DbObject");
define("API_LIST_RESPONSE_CLASS", "InerziaSoft\\Core\\Api\\Outcomes\\ListResponse");

/**
 * Class ApiRequest
 *
 * Coordinates and handles HTTP requests to the API system.
 * Provides methods to work with localized strings
 * and defines a list of constants that represents the API commands available.
 *
 * @package InerziaSoft
 */
class ApiRequest {

	/* == BEGIN API commands == */
	/* Use this space to define constants that represent API commands. */
	public static $languageClass = "language";
	/* == END API commands == */

	/* == BEGIN HTTP methods == */
	public static $postMethod = "POST";
	public static $putMethod = "PUT";
	public static $getMethod = "GET";
	public static $deleteMethod = "DELETE";
	/* == END HTTP methods == */

	private $class;
	private $command;
	
	private $request;
	private $options;
	private $response;
	private $originalResponse;
	private $status;
    private $method;
    
    private $cacheTtl;
	
	const RESPONSE_CLASS_JSON_KEY = "responseClass";
	
	private function apiAddress() {
		$host = "http://localhost";
		if (defined("USE_HOST_IN_API_REQUESTS") && USE_HOST_IN_API_REQUESTS === "true") {
			$host = ($_SERVER["HTTPS"] ? "https" : "http")."://".$_SERVER["HTTP_HOST"];
		}
		
		return $host.getBaseUri()."/";
	}
	
	private function authenticationHeaderForToken($token) {
		return 'Authentication: JWT token='.$token;
	}
	
	private function languageHeaderForLanguage($language) {
		if (isset($language)) {
			return "Accept-Language: $language";
		}
		return "Accept-Language: *";
	}
	
	/**
	 * ApiRequest constructor.
	 * Initializes a new request by providing its class, command, HTTP method and optional parameters.
	 *
	 * @param $class string: a constant specified in the "API commands" section of this file.
	 * @param $command string: an optional command to call in the specified class.
	 * @param $method string: an HTTP method (constants are already defined in the "HTTP methods" of this section)
	 * @param $parameters array: an optional array of parameters that will be passed as the "content" key.
	 * @param $token string: an optional token to authenticate the request.
	 * @param int $cacheTtl : if a value greater than 0 is passed, the ApiRequest will cache the response for the specified amount of seconds.
	 * @param string $language: Optionally pass a value for the Accept-Language header of the request.
	 *
	 * @discussion If $parameters contains one or more array(s), they will be converted to JSON
	 * in all methods except GET.
	 *
	 * @discussion If $cacheTtl is greater than 0, the underlying FatFree Framework must be configured to enable Cache.
	 * To do that, set the FatFree Environment Constant "CACHE" to true, with $f3->set("CACHE", true); in your config.inc.php file.
	 */
	public function __construct($class, $command, $method, $parameters = null, $token = null, $cacheTtl = 0, $language = null) {
		$this->class = $class;
		$this->command = $command;
		$this->cacheTtl = $cacheTtl;
		
		$this->request = $this->apiAddress().UriUtils::getApiFolder().UriUtils::getApiVersion()."/".$class;

		if (isset($command) && $command != "") {
			$this->request .= "/".strtolower($command);
		}

        $this->method = $method;

		if (isset($parameters) && $method != ApiRequest::$getMethod) {
			foreach ($parameters as $key => $parameter) {
				if (is_array($parameter)) {
					$parameters[$key] = json_encode($parameter);
				}
			}
			
			// TODO: send pars as JSON
			$options = array(
				'method'  => $method,
				'content' => http_build_query($parameters),
				'header' => [
					$this->authenticationHeaderForToken($token),
					$this->languageHeaderForLanguage($language)
				]
			);
		}
		else {
			$options = array(
				'method'  => $method,
				'header' => [
					$this->authenticationHeaderForToken($token),
					$this->languageHeaderForLanguage($language)
				]
			);

			if (isset($parameters)) {
				foreach ($parameters as $parameter) {
					$this->request .= "/".$parameter;
				}
			}
		}

		$this->options = $options;
	}

	public function __toString() {
		return $this->request;
	}

    /**
     * Makes the request and returns the associated result.
     *
     * @discussion The response can be handled in the following ways:
     * - Mapped Response: if the response includes a key named 'responseClass', ApiRequest will attempt to map a class using the properties from the JSON response.
     * - Decoded JSON: if the 'responseClass' is not defined or cannot be instantiated, ApiRequest will attempt to decode the JSON response in the corresponding dictionary.
     * - Raw response: if any of the above fails, ApiRequest will return the raw response as text/html.
     * @return mixed $className : if the response is not a valid JSON or the API does not return a "status" key.
     *
     * @throws InvalidClassMapException If the class in the 'responseClass' key does not subclass 'InerziaSoft\Api\Response'.
     * @throws UnexistingClassMapException If the class in the 'responseClass' is either not visible or cannot be found.
     */
	public function request() {
		if ($this->cacheTtl > 0) {
			$cache = \Cache::instance();
			$value = null;
			if ($cache->exists($this->getCacheName(), $value)) {
				return $value;
			}
		}
		
		$request = \Web::instance()->request($this->request, $this->options);
		$json = json_decode($request['body'], true);

		$this->status = $request["headers"][0];
		$this->response = ($json != null)?$json:$request['body'];
		
		try {
			$this->checkValidStatus();
		}
		catch (ApiRequestException $ex) {
			return $ex->toArray();
		}

		try {
			$this->checkResponseSyntax();
		}
		catch (InvalidApiResponseException $ex) {
			return $ex->toArray();
		}

		if ($json != null) {
			$this->originalResponse = $this->response;
			
			$parser = new JsonParser($this->originalResponse);
            $this->response = $parser->parse();
        }

        if ($this->cacheTtl > 0) {
			$cache = \Cache::instance();
			$cache->set($this->getCacheName(), $this->response, $this->cacheTtl);
        }
        
		return $this->response;
	}

	private function checkValidStatus() {
		if ($this->status != null) {
			if (strpos($this->status, "404") !== false) {
				throw new RequestNotFoundException($this->request, $this->method);
			}
			else if (strpos($this->status, "500") !== false) {
				throw new InternalServerErrorException($this->response, $this->request, $this->method);
			}
			else if (strpos($this->status, "200") === false && strpos($this->status, "100") === false) {
				throw new GenericRequestErrorException($this->method." ".$this->request.": ".print_r($this->response, true));
			}
		}
		else {
			throw new InvalidRequestStatusException("Request hasn't been performed yet.");
		}
	}

	private function checkResponseSyntax() {
		if (!is_array($this->response) || !array_key_exists("status", $this->response)) {
			throw new InvalidApiResponseException($this->response);
		}
	}
	
	private function getCacheName() {
    	return $this->class."_".str_replace("/", "", $this->command);
	}

}
