<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Page\Seo;

use InerziaSoft\Core\Page\Html\Meta;
use InerziaSoft\Core\Page\Interfaces\HtmlConvertible;
use InerziaSoft\Core\Utils\UriUtils;

class FacebookSeo implements HtmlConvertible {
	
	private $url;
	private $title;
	private $description;
	private $image;
	private $facebookAppId;
	private $type;
	
	public function __construct($title = null, $description = null, $image = null, $facebookAppId = null, $url = null, $type = "website") {
		if (isset($url)) {
			$this->url = $url;
		} else {
			$this->url = UriUtils::getFullRequestUri();
		}
		$this->title = $title;
		$this->description = $description;
		$this->image = $image;
		$this->facebookAppId = $facebookAppId;
		$this->type = $type;
	}
	
	public function toHtml() {
		$tags = array();
		if (isset($this->url))
			array_push($tags, new Meta("og:url", $this->url));
		if (isset($this->title))
			array_push($tags, new Meta("og:title", $this->title));
		if (isset($this->description))
			array_push($tags, new Meta("og:description", $this->description));
		if (isset($this->image))
			array_push($tags, new Meta("og:image", UriUtils::getHostUri().UriUtils::getBaseUri().$this->image));
		if (isset($this->facebookAppId))
			array_push($tags, new Meta("fb:app_id", $this->facebookAppId));
		if (isset($this->type))
			array_push($tags, new Meta("og:type", $this->type));
		
		$tagsHtml = array_map(function ($metaTag) { return $metaTag->toHtml(); }, $tags);
		return implode("\n", $tagsHtml);
	}
	
}