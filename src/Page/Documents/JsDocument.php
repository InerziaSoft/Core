<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
namespace InerziaSoft\Core\Page\Documents;

use InerziaSoft\Core\Utils\UriUtils;

/**
 * Class JsDocument
 *
 * Represents a single JavaScript document.
 *
 * @package InerziaSoft
 */
class JsDocument implements Document {
	
	protected $filename;
	
	protected $async;
	protected $defer;
	
	protected $top;
	
	/**
	 * CssDocument constructor.
	 *
	 * Instantiates a new JavaScript document.
	 *
	 * @discussion If a define called "JS_DEFAULT_FOLDER" is found anywhere in the app, it will be used as root directory for any filename.
	 * Moreover, the $filename can also omit the ".js" extension that will be added by default.
	 *
	 * @param $fileName
	 * @param bool $async
	 * @param bool $defer
	 * @param bool $top
	 */
	public function __construct($fileName, $async = false, $defer = false, $top = false) {
		if (filter_var($fileName, FILTER_VALIDATE_URL) === false) {
			$startsWithSlash = substr($fileName, 0, 1) == "/";
			if (!$startsWithSlash) {
				$fileName = $this->getDefaultPathPrefix().$fileName;
			}
			else {
				$fileName = substr($fileName, 1);
			}
			
			if (strpos($fileName, ".js") === false) {
				$fileName .= ".js";
			}
		}
		
		$this->filename = $fileName;
		$this->async = $async;
		$this->defer = $defer;
		$this->top = $top;
	}
	
	protected function getDefaultPathPrefix() {
		if (defined("JS_DEFAULT_FOLDER")) {
			return JS_DEFAULT_FOLDER;
		}
		return "";
	}
	
	public function shouldBePlacedInHead() {
		return $this->top;
	}
	
	protected function getSrc() {
		$additionalFolder = UriUtils::getBaseUri();
		
		$filename = $additionalFolder . "/" . $this->filename;
		
		return $filename;
	}
	
	public function toHtml() {
		$filename = $this->getSrc();
		
		$additionalOptions = "";
		if ($this->async) {
			$additionalOptions = " async ";
		}
		
		if ($this->defer) {
			$additionalOptions = " defer ";
		}
		
		return "<script $additionalOptions src=\"" . $filename . "\"></script>";
	}
	
}