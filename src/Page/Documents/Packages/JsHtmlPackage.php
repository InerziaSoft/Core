<?php
/**
 * Created by PhpStorm.
 * User: andrea@INERZIASOFT.private
 * Date: 1/6/17
 * Time: 9:47 PM
 */

namespace InerziaSoft\Core\Page\Documents\Packages;


use InerziaSoft\Core\Page\Documents\CoreJsDocument;

class JsHtmlPackage extends JsPackage {

    static function getAllDocuments() {
        return [
            new CoreJsDocument("page/html/tag.js"),
            new CoreJsDocument("page/html/select.js")
        ];
    }

}