<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
namespace InerziaSoft\Core\Page\Documents;

use InerziaSoft\Core\Utils\UriUtils;

/**
 * Class CssDocument
 *
 * Represents a single CSS document.
 *
 * @package InerziaSoft
 */
class CssDocument implements Document {
	
	private $filename;
	
	/**
	 * CssDocument constructor.
	 *
	 * Instantiates a new CSS document.
	 *
	 * @discussion If a define called "CSS_DEFAULT_FOLDER" is found anywhere in the app, it will be used as root directory for any filename.
	 * @param $fileName
	 */
	public function __construct($fileName) {
		if (defined("CSS_DEFAULT_FOLDER")) {
			$fileName = CSS_DEFAULT_FOLDER . $fileName;
		}
		
		if (strpos($fileName, ".css") === false) {
			$fileName .= ".css";
		}
		
		$this->filename = $fileName;
	}
	
	public function toHtml() {
		$additionalFolder = UriUtils::getBaseUri();
		
		$filename = $additionalFolder . "/" . $this->filename;
		
		return "<link href=\"" . $filename . "\" rel=\"stylesheet\" type=\"text/css\">";
	}
	
}