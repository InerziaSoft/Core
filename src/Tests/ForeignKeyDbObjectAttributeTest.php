<?php
/**
 * Created by PhpStorm.
 * User: amois
 * Date: 04/04/2017
 * Time: 19:04
 */

namespace InerziaSoft\Core\Tests;

use InerziaSoft\Core\Db\DbObject\Attributes\ForeignKeyDbObjectAttribute;
use PHPUnit\Framework\TestCase;

class ForeignKeyDbObjectAttributeTest extends TestCase {

	const DbObjectClass = "InerziaSoft\\Core\\Tests\\Additions\\TestDbObject";
	const DbObjectId = "id";

	const ValidForeignKeyAttributeValue = "InerziaSoft\\Core\\Tests\\Additions\\TestDbObject";
	const ValidForeignKeyWithOnAttributeValue = "InerziaSoft\\Core\\Tests\\Additions\\TestDbObject on columnName";

	const InvalidForeignKeyAttributeValue = "InerziaSoft\\UnexistingClass";
	const InvalidForeignKeyWithOnAttributeValue = "InerziaSoft\\Core\\Tests\\Additions\\TestDbObject on";

	const MultipleForeignKeyAttributeValue = "InerziaSoft\\Core\\Tests\\Additions\\TestDbObject InerziaSoft\\Something\\Else";

	/**
	 * @test
	 */
	public function initWithValidValue_CheckingValues() {
		$foreignKeyAttribute = new ForeignKeyDbObjectAttribute(self::ValidForeignKeyAttributeValue);

		$this->assertEquals(ForeignKeyDbObjectAttribute::AutomaticKeyType, $foreignKeyAttribute->getType());
		$this->assertEquals(self::DbObjectClass, $foreignKeyAttribute->getClassName());
		$this->assertEquals(self::DbObjectId, $foreignKeyAttribute->getForeignPropertyKey());
	}

	/**
	 * @test
	 */
	public function initWithValidOnValue_CheckingValues() {
		$foreignKeyAttribute = new ForeignKeyDbObjectAttribute(self::ValidForeignKeyWithOnAttributeValue);

		$this->assertEquals(ForeignKeyDbObjectAttribute::ManualKeyType, $foreignKeyAttribute->getType());
		$this->assertEquals(self::DbObjectClass, $foreignKeyAttribute->getClassName());
		$this->assertEquals("columnName", $foreignKeyAttribute->getForeignPropertyKey());
	}

	/**
	 * @test
	 *
	 * @expectedException InerziaSoft\Core\Db\DbObject\Exceptions\UnexistingDbObjectClassException
	 */
	public function initWithInvalidValue_ShouldThrow() {
		new ForeignKeyDbObjectAttribute(self::InvalidForeignKeyAttributeValue);
	}

	/**
	 * @test
	 *
	 * @expectedException InerziaSoft\Core\Db\DbObject\Attributes\Exceptions\InvalidDefinitionDbObjectAttributeException
	 */
	public function initWithInvalidOnValue_ShouldThrow() {
		new ForeignKeyDbObjectAttribute(self::InvalidForeignKeyWithOnAttributeValue);
	}

	/**
	 * @test
	 *
	 * @expectedException InerziaSoft\Core\Db\DbObject\Attributes\Exceptions\InvalidDefinitionDbObjectAttributeException
	 */
	public function initWithMultipleValue_ShouldThrow() {
		new ForeignKeyDbObjectAttribute(self::MultipleForeignKeyAttributeValue);
	}

}
