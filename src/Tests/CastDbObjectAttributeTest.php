<?php

namespace InerziaSoft\Core\Tests;

use InerziaSoft\Core\Db\DbObject\Attributes\CastDbObjectAttribute;
use PHPUnit\Framework\TestCase;

class CastDbObjectAttributeTest extends TestCase {

	const IntegerValidCastAttributeValues = ["int", "Integer", "INT", "INTEGER"];
	const DecimalValidCastAttributeValues = ["double", "FLOAT", "float", "DOUBLE"];
	const DateTimeValidCastAttributeValues = ["DateTime", "\\DateTime", "DATETIME", "\\DATETIME"];
	const StringValidCastAttributeValues = ["string", "STRING"];

	const InvalidCastAttributeValue = "";
	const UnknownTypeCastAttributeValue = "unknown";

	/**
	 * @test
	 */
	public function initWithValidIntegerValue() {
		$this->initWithValuesAsserting(self::IntegerValidCastAttributeValues, CastDbObjectAttribute::IntegerType);
	}

	/**
	 * @test
	 */
	public function initWithValidDecimalValue() {
		$this->initWithValuesAsserting(self::DecimalValidCastAttributeValues, CastDbObjectAttribute::DecimalType);
	}

	/**
	 * @test
	 */
	public function initWithValidDateTimeValue() {
		$this->initWithValuesAsserting(self::DateTimeValidCastAttributeValues, CastDbObjectAttribute::DateTimeType);
	}

	/**
	 * @test
	 */
	public function initWithValidStringValue() {
		$this->initWithValuesAsserting(self::StringValidCastAttributeValues, CastDbObjectAttribute::StringType);
	}

	/**
	 * @test
	 *
	 * @expectedException InerziaSoft\Core\Db\DbObject\Attributes\Exceptions\InvalidDefinitionDbObjectAttributeException
	 */
	public function initWithInvalidValue_ShouldThrow() {
		new CastDbObjectAttribute(self::InvalidCastAttributeValue);
	}

	/**
	 * @test
	 *
	 * @expectedException InerziaSoft\Core\Db\DbObject\Attributes\Exceptions\UnknownCastDbObjectAttributeException
	 */
	public function initWithUnknownTypeValue_ShouldThrow() {
		new CastDbObjectAttribute(self::UnknownTypeCastAttributeValue);
	}

	private function initWithValuesAsserting($values, $expectedType) {
		foreach ($values as $attributeValue) {
			$attribute = new CastDbObjectAttribute($attributeValue);
			$this->assertEquals($expectedType, $attribute->getType());
		}
	}

}
