<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Tests;

use InerziaSoft\Core\Db\DbObject\TablesEnvironment;
use PHPUnit\Framework\TestCase;

class TablesEnvironmentTest extends TestCase {
	
	const TestTableName = "testTable";
	
	/**
	 * @test
	 */
	public function init() {
		$env = new TablesEnvironment();
		
		$this->assertInstanceOf(TablesEnvironment::class, $env);
	}
	
	/**
	 * @test
	 */
	public function init_arrayShouldBeEmpty() {
		$env = new TablesEnvironment();
		
		$this->assertEquals(0, $env->getTablesCount());
	}
	
	/**
	 * @test
	 *
	 * @return TablesEnvironment@
	 */
	public function useName_shouldReturnSameNameWithOne() {
		$env = new TablesEnvironment();
		$resultingName = $env->useName(self::TestTableName);
		
		$this->assertEquals(self::TestTableName."1", $resultingName);
		
		return $env;
	}
	
	/**
	 * @test
	 * @depends useName_shouldReturnSameNameWithOne
	 *
	 * @param $env TablesEnvironment
	 */
	public function useNameTwice_shouldNotReturnSameName($env) {
		$resultingName = $env->useName(self::TestTableName);
		
		$this->assertNotEquals(self::TestTableName, $resultingName);
	}
	
}
