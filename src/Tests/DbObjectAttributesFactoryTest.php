<?php

namespace InerziaSoft\Core\Tests;

use InerziaSoft\Core\Db\DbObject\Attributes\CastDbObjectAttribute;
use InerziaSoft\Core\Db\DbObject\Attributes\DbObjectAttributesFactory;
use InerziaSoft\Core\Db\DbObject\Attributes\ForeignKeyDbObjectAttribute;
use InerziaSoft\Core\Db\DbObject\Attributes\ManualDbObjectAttribute;
use PHPUnit\Framework\TestCase;

class DbObjectAttributesFactoryTest extends TestCase {

	/**
	 * @test
	 */
	public function commentFromForeignValue_ShouldReturnForeignAttribute() {
		$attribute = DbObjectAttributesFactory::getAttributeForComment("@foreign ".ForeignKeyDbObjectAttributeTest::ValidForeignKeyAttributeValue);

		$this->assertInstanceOf(ForeignKeyDbObjectAttribute::class, $attribute);
	}

	/**
	 * @test
	 */
	public function commentFromCast_ShouldReturnCastAttribute() {
		$attribute = DbObjectAttributesFactory::getAttributeForComment("@var ".CastDbObjectAttributeTest::StringValidCastAttributeValues[0]);

		$this->assertInstanceOf(CastDbObjectAttribute::class, $attribute);
	}

	/**
	 * @test
	 */
	public function commentFromManual_ShouldReturnManualAttribute() {
		$attribute = DbObjectAttributesFactory::getAttributeForComment("@manual");

		$this->assertInstanceOf(ManualDbObjectAttribute::class, $attribute);
	}

}
