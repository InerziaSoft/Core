<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
namespace InerziaSoft\Core\Tests\Additions;

use InerziaSoft\Core\Db\DbObject\Bindings\DatabaseBinding;
use InerziaSoft\Core\Db\DbObject\DbObject;
use InerziaSoft\Core\Db\DbObject\Evaluators\ReflectionQueryEvaluator;
use InerziaSoft\Core\Db\DbObject\Interfaces\QueryEvaluator;
use InerziaSoft\Core\Db\DbObject\Interfaces\QueryPerformer;
use InerziaSoft\Core\Db\DbObject\Interfaces\TypesSystem;
use InerziaSoft\Core\Db\DbObject\Performers\FakePostgreSqlQueryPerformer;
use InerziaSoft\Core\Db\DbObject\Types\PostgreSqlTypesSystem;

final class TestDbObject extends DbObject {
	
	public $id;
	public $key;
	public $key2;
	public $key3;

	protected static function getDatabaseBinding() {
		$dbBinding = new DatabaseBinding(static::class, [
			TypesSystem::class => PostgreSqlTypesSystem::class,
			QueryEvaluator::class => ReflectionQueryEvaluator::class,
			QueryPerformer::class => FakePostgreSqlQueryPerformer::class
		]);

		$performer = $dbBinding->getQueryPerformer();
		if ($performer instanceof FakePostgreSqlQueryPerformer) {
			$performer->setDataSet([
				["id" => 1, "key" => "value11", "key2" => "value12", "key3" => "value13"],
				["id" => 2, "key" => "value21", "key2" => "value22", "key3" => "value23"],
				["id" => 3, "key" => "value31", "key2" => "value32", "key3" => "value33"]
			]);
		}

		return $dbBinding;
	}
	
}