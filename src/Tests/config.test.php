<?php

return [
	\InerziaSoft\Core\Db\DbObject\Interfaces\TypesSystem::class => DI\object(\InerziaSoft\Core\Db\DbObject\Types\PostgreSqlTypesSystem::class),
	\InerziaSoft\Core\Db\DbObject\Interfaces\QueryEvaluator::class => DI\object(\InerziaSoft\Core\Db\DbObject\Evaluators\ReflectionQueryEvaluator::class),
	\InerziaSoft\Core\Db\DbObject\Interfaces\QueryPerformer::class => DI\object(\InerziaSoft\Core\Db\DbObject\Performers\FakePostgreSqlQueryPerformer::class),
	\InerziaSoft\Core\Db\DbObject\Operations\CastOperation::class => DI\object(\InerziaSoft\Core\Db\DbObject\Operations\PostgreSql\PostgreSqlCastOperation::class),
	\InerziaSoft\Core\Db\DbObject\Operations\CountOperation::class => DI\object(\InerziaSoft\Core\Db\DbObject\Operations\CountOperation::class),
	\InerziaSoft\Core\Db\DbObject\Operations\LeftJoinOperation::class => DI\object(\InerziaSoft\Core\Db\DbObject\Operations\PostgreSql\PostgreSqlLeftJoinOperation::class),
	\InerziaSoft\Core\Db\DbObject\Operations\RenameOperation::class => DI\object(\InerziaSoft\Core\Db\DbObject\Operations\PostgreSql\PostgreSqlRenameOperation::class)
];