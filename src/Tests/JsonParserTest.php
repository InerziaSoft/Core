<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Tests;

use InerziaSoft\Core\Api\Outcomes\ListResponse;
use InerziaSoft\Core\Parser\JsonParser;
use InerziaSoft\Core\Tests\Additions\TestResponse;
use PhpUnit\Framework\TestCase;

final class JsonParserTest extends TestCase {
	
	/**
	 * @test
	 */
	public function initFromEmptyJson() {
		$this->assertInstanceOf(JsonParser::class, JsonParser::fromRawJson("{}"));
	}
	
	/**
	 * @test
	 */
	public function parseResponseJson_ShouldReturnResponse() {
		$response = Additions\TestResponse::response("value", "value2", "value3");
		$responseJson = $response->toArray();
		
		$parser = new JsonParser($responseJson);
		$result = $parser->parse();
		
		$this->assertInstanceOf(Additions\TestResponse::class, $result);
		
		return $result;
	}
	
	/**
	 * @test
	 * @depends parseResponseJson_ShouldReturnResponse
	 *
	 * @param $result
	 */
	public function parseResponseJson_ShouldReturnResponse_CheckingValues($result) {
		if ($result instanceof Additions\TestResponse) {
			$this->assertEquals("value", $result->key);
			$this->assertEquals("value2", $result->key2);
			$this->assertEquals("value3", $result->key3);
		}
		else {
			$this->fail();
		}
	}
	
	/**
	 * @test
	 */
	public function parseDbObjectJson_ShouldReturnDbObject() {
		$dbObject = new Additions\TestDbObject(["key" => "value", "key2" => "value2", "key3" => "value3"]);
		$dbObjectJson = $dbObject->toArray();
		
		$parser = new JsonParser($dbObjectJson);
		$result = $parser->parse();
		
		$this->assertInstanceOf(Additions\TestDbObject::class, $result);
		
		return $result;
	}
	
	/**
	 * @test
	 * @depends parseDbObjectJson_ShouldReturnDbObject
	 *
	 * @param $result
	 */
	public function parseDbObjectJson_ShouldReturnDbObject_CheckingValues($result) {
		if ($result instanceof Additions\TestDbObject) {
			$this->assertEquals("value", $result->key);
			$this->assertEquals("value2", $result->key2);
			$this->assertEquals("value3", $result->key3);
		}
		else {
			$this->fail();
		}
	}
	
	/**
	 * @test
	 * @expectedException InerziaSoft\Core\Parser\Exceptions\InvalidClassMapException
	 */
	public function parseNotResponseJson_ShouldThrow() {
		$array = ["key" => "value", "responseClass" => "InerziaSoft\\Core\\Tests\\JsonParserTest"];
		
		$parser = new JsonParser($array);
		$parser->parse();
	}
	
	/**
	 * @test
	 * @expectedException InerziaSoft\Core\Parser\Exceptions\UnexistingClassMapException
	 */
	public function parseUnexistingResponseJson_ShouldThrow() {
		$array = ["key" => "value", "responseClass" => "InerziaSoft\\Core\\UnexistingClass"];
		
		$parser = new JsonParser($array);
		$parser->parse();
	}
	
	/**
	 * @test
	 */
	public function parseJsonNoDefaultResponseClassKey_ShouldReturnResponse() {
		$array = ["key" => "value", "key2" => "value2", "key3" => "value3", "classResponse" => "InerziaSoft\\Core\\Tests\\Additions\\TestResponse"];
		
		$parser = new JsonParser($array, "classResponse");
		$result = $parser->parse();
		
		$this->assertInstanceOf(Additions\TestResponse::class, $result);
	}
	
	/**
	 * @test
	 */
	public function parseRecursiveResponseJson_ShouldReturnResponse_CheckingValues() {
		$response = Additions\TestResponse::response("value", "value2", Additions\TestResponse::response("insideValue", "insideValue2", "insideValue3"));
		$responseJson = $response->toArray();
		
		$parser = new JsonParser($responseJson);
		$result = $parser->parse();
		
		$this->assertInstanceOf(Additions\TestResponse::class, $result);
		
		if ($result instanceof Additions\TestResponse) {
			$thirdValue = $result->key3;
			
			$this->assertInstanceOf(Additions\TestResponse::class, $thirdValue);
			
			if ($thirdValue instanceof Additions\TestResponse) {
				$this->assertEquals("insideValue", $thirdValue->key);
				$this->assertEquals("insideValue2", $thirdValue->key2);
				$this->assertEquals("insideValue3", $thirdValue->key3);
			}
			else {
				$this->fail();
			}
		}
		else {
			$this->fail();
		}
	}
	
	/**
	 * @test
	 */
	public function parseArrayInResponseJson_ShouldReturnResponse_CheckingValues() {
		$response = Additions\TestResponse::response("value", "value2", ["item1", "item2", Additions\TestResponse::response("insideItem", "insideItem2", "insideItem3")]);
		$responseJson = $response->toArray();
		
		$parser = new JsonParser($responseJson);
		$result = $parser->parse();
		
		$this->assertInstanceOf(Additions\TestResponse::class, $result);
		
		if ($result instanceof Additions\TestResponse) {
			$this->assertInternalType("array", $result->key3);
			$thirdItem = $result->key3[2];
			
			$this->assertInstanceOf(Additions\TestResponse::class, $thirdItem);
			
			if ($thirdItem instanceof Additions\TestResponse) {
				$this->assertEquals("insideItem", $thirdItem->key);
				$this->assertEquals("insideItem2", $thirdItem->key2);
				$this->assertEquals("insideItem3", $thirdItem->key3);
			}
			else {
				$this->fail();
			}
		}
		else {
			$this->fail();
		}
	}
	
	/**
	 * @test
	 */
	public function initFromStringSpecifyingClassName() {
		$json = '{"key": "value", "key2": "value2", "key3": "value3"}';
		$parser = JsonParser::fromRawJsonWithClassName($json, "InerziaSoft\\Core\\Tests\\Additions\\TestResponse");
		
		$this->assertInstanceOf(JsonParser::class, $parser);
		
		return $parser;
	}
	
	/**
	 * @test
	 * @depends initFromStringSpecifyingClassName
	 *
	 * @param $parser JsonParser
	 */
	public function parseResponseSpecifyingClassName_ShouldReturnResponse($parser) {
		$result = $parser->parse();
		
		$this->assertInstanceOf(Additions\TestResponse::class, $result);
	}
	
	/**
	 * @test
	 */
	public function parseListResponse_ShouldReturnListResponse() {
		$listResponse = ListResponse::response([
			TestResponse::response("value", "value2", "value3"),
			TestResponse::response("value", "value2", "value3"),
			TestResponse::response("value", "value2", "value3"),
			TestResponse::response("value", "value2", "value3")
		]);
		$jsonResponse = $listResponse->toArray();
		
		$parser = new JsonParser($jsonResponse);
		$result = $parser->parse();
		
		$this->assertInstanceOf(ListResponse::class, $result);
		
		return $result;
	}
	
	/**
	 * @test
	 * @depends parseListResponse_ShouldReturnListResponse
	 *
	 * @param $result
	 */
	public function parseListResponse_ShouldReturnListResponse_CheckingValues($result) {
		if ($result instanceof ListResponse) {
			foreach ($result->items as $item) {
				$this->assertInstanceOf(TestResponse::class, $item);
			}
		}
		else {
			$this->fail();
		}
	}
	
}
