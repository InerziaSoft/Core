<?php
/*/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Tests;

use InerziaSoft\Core\Db\DbObject\Conditions\Equal;
use InerziaSoft\Core\Tests\Additions\ForeignDbObject;
use InerziaSoft\Core\Tests\Additions\NoKeyDbObject;
use InerziaSoft\Core\Tests\Additions\TestDbObject;
use InerziaSoft\Core\Utils\DIUtils;
use PHPUnit\Framework\TestCase;

class DbObjectTest extends TestCase {
	
	protected function setUp() {
		DIUtils::setUp("src/Tests/config.test.php");
	}
	
	/**
	 * @test
	 */
	public function init() {
		$testDbObject = TestDbObject::make();

		$this->assertInstanceOf(TestDbObject::class, $testDbObject);
	}

	/**
	 * @test
	 */
	public function initFromValues() {
		$testDbObject = new TestDbObject([
			"id" => 1,
			"key" => "value",
			"key2" => "value2",
			"key3" => "value3"
		]);

		$this->assertInstanceOf(TestDbObject::class, $testDbObject);

		return $testDbObject;
	}

	/**
	 * @test
	 * @depends initFromValues
	 * @param $testDbObject
	 */
	public function initFromValues_CheckingValues($testDbObject) {
		if ($testDbObject instanceof TestDbObject) {
			$this->assertEquals("value", $testDbObject->key);
			$this->assertEquals("value2", $testDbObject->key2);
			$this->assertEquals("value3", $testDbObject->key3);
		}
		else {
			$this->fail();
		}
	}

	/**
	 * @test
	 */
	public function insert() {
		$testDbObject = TestDbObject::make();

		$testDbObject->key = "value";
		$testDbObject->key2 = "value2";
		$testDbObject->key3 = "value3";

		$this->assertTrue($testDbObject->insert());
	}

	/**
	 * @test
	 */
	public function update() {
		$testDbObject = new TestDbObject([
			"id" => 1,
			"key" => "value",
			"key2" => "value2",
			"key3" => "value3"
		]);

		$testDbObject->key2 = "valueChanged";

		$this->assertTrue($testDbObject->update());
	}

	/**
	 * @test
	 */
	public function updateRelevantValues() {
		$testDbObject = new TestDbObject([
			"id" => 1,
			"key" => "value",
			"key2" => "value2",
			"key3" => "value3"
		]);

		$testDbObject->key2 = "valueChanged";

		$this->assertTrue($testDbObject->updateRelevantValues(["key2"]));
	}

	/**
	 * @test
	 */
	public function delete() {
		$testDbObject = new TestDbObject([
			"id" => 1,
			"key" => "value",
			"key2" => "value2",
			"key3" => "value3"
		]);

		$this->assertTrue($testDbObject->delete());
	}

	/**
	 * @test
	 * @expectedException \InerziaSoft\Core\Db\DbObject\Exceptions\UnexistingKeyException
	 */
	public function objectWithoutKey_ShouldThrow() {
		$invalidDbObject = new NoKeyDbObject([
			"column" => "value",
			"column2" => "value2",
			"column3" => "value3"
		]);

		$invalidDbObject->update();
	}

	/**
	 * @test
	 */
	public function getAll() {
		$results = TestDbObject::all();

		$this->assertCount(0, $results);

		return $results;
	}

	/**
	 * @test
	 */
	public function getWhere() {
		$results = TestDbObject::where([
			new Equal("key", "value")
		]);

		$this->assertCount(0, $results);

		return $results;
	}

	/**
	 * @test
	 */
	public function getAll_WithForeignKeys() {
		$results = ForeignDbObject::all();

		$this->assertCount(0, $results);
	}

	/**
	 * @test
	 */
	public function initWithForeignKeysFromValues() {
		$values = [
			"foreigndbobject.id" => 1,
			"foreigndbobject.column" => "columnValue",
			"testdbobject.id" => 1,
			"testdbobject.key" => "value",
			"testdbobject.key2" => "value2",
			"testdbobject.key3" => "value3"
		];

		$dbObject = new ForeignDbObject($values);

		$this->assertInstanceOf(ForeignDbObject::class, $dbObject);

		return $dbObject;
	}

	/**
	 * @test
	 * @depends initWithForeignKeysFromValues
	 *
	 * @param $dbObject ForeignDbObject
	 */
	public function initWithForeignKeysFromValues_CheckingValues($dbObject) {
		$this->assertEquals(1, $dbObject->id);
		$this->assertEquals("columnValue", $dbObject->column);

		$this->assertInstanceOf(TestDbObject::class, $dbObject->foreign);
		if ($dbObject->foreign instanceof TestDbObject) {
			$this->assertEquals(1, $dbObject->foreign->id);
			$this->assertEquals("value", $dbObject->foreign->key);
			$this->assertEquals("value2", $dbObject->foreign->key2);
			$this->assertEquals("value3", $dbObject->foreign->key3);
		}
	}

	/**
	 * @test
	 */
	public function initReadOnly() {
		$values = [
			"id" => 1,
			"key" => "value",
			"key2" => "value2",
			"key3" => "value3"
		];

		$dbObject = TestDbObject::readOnly($values);

		$this->assertInstanceOf(TestDbObject::class, $dbObject);

		return $dbObject;
	}

	/**
	 * @test
	 * @depends initReadOnly
	 *
	 * @param $dbObject TestDbObject
	 */
	public function initReadOnly_CheckingValues($dbObject) {
		$this->assertEquals(1, $dbObject->id);
		$this->assertEquals("value", $dbObject->key);
		$this->assertEquals("value2", $dbObject->key2);
		$this->assertEquals("value3", $dbObject->key3);
	}

	/**
	 * @test
	 */
	public function initReadOnlyWithForeignKeys_CheckingValues() {
		$values = [
			"foreigndbobject.id" => 1,
			"foreigndbobject.column" => "value",
			"foreigndbobject.foreign.id" => 1,
			"foreigndbobject.foreign.key" => "value",
			"foreigndbobject.foreign.key2" => "value2",
			"foreigndbobject.foreign.key3" => "value3",
		];

		$dbObject = ForeignDbObject::readOnly($values);

		$this->assertInstanceOf(ForeignDbObject::class, $dbObject);

		$this->assertEquals(1, $dbObject->id);
		$this->assertEquals("value", $dbObject->column);

		if ($dbObject->foreign instanceof TestDbObject) {
			$this->assertEquals(1, $dbObject->foreign->id);
			$this->assertEquals("value", $dbObject->foreign->key);
			$this->assertEquals("value2", $dbObject->foreign->key2);
			$this->assertEquals("value3", $dbObject->foreign->key3);
		}

		return $dbObject;
	}

	/**
	 * @test
	 * @depends initReadOnlyWithForeignKeys_CheckingValues
	 *
	 * @param $dbObject ForeignDbObject
	 *
	 * @expectedException \InerziaSoft\Core\Db\DbObject\Exceptions\ReadOnlyException
	 */
	public function readOnlyDbObject_ShouldThrowOnInsert($dbObject) {
		$dbObject->insert();
	}

	/**
	 * @test
	 * @depends initReadOnlyWithForeignKeys_CheckingValues
	 *
	 * @expectedException \InerziaSoft\Core\Db\DbObject\Exceptions\ReadOnlyException
	 *
	 * @param $dbObject ForeignDbObject
	 */
	public function readOnlyDbObject_ShouldThrowOnUpdate($dbObject) {
		$dbObject->update();
	}

	/**
	 * @test
	 * @depends initReadOnlyWithForeignKeys_CheckingValues
	 *
	 * @expectedException \InerziaSoft\Core\Db\DbObject\Exceptions\ReadOnlyException
	 *
	 * @param $dbObject ForeignDbObject
	 */
	public function readOnlyDbObject_ShouldThrowOnDelete($dbObject) {
		$dbObject->delete();
	}



}
