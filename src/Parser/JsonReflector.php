<?php

namespace InerziaSoft\Core\Parser;

class JsonReflector implements JsonInitializable {

    public static function fromJson($json, $responseClassKey, $responseClassName) {
        foreach ($json as $key => $item) {
            $json[$key] = (new JsonParser($item, $responseClassKey, $responseClassName))->parse();
        }

        $instance = new static();
        $reflectionClass = new \ReflectionClass($instance);
        $properties = $reflectionClass->getProperties(\ReflectionProperty::IS_PUBLIC);

        foreach ($properties as $property) {
            $nameMatches = static::getPropertyNamePossibleMatches($property->getName());

            foreach ($nameMatches as $match) {
                if (array_key_exists($match, $json)) {
                    $property->setValue($instance, $json[$match]);
                    break;
                }
            }
        }

        return $instance;
    }

    protected static function getPropertyNamePossibleMatches($propertyName) {
        return [
            $propertyName,
            strtolower($propertyName),
            strtoupper($propertyName),
            ucfirst($propertyName)
        ];
    }
}