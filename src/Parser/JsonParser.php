<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Parser;

use InerziaSoft\Core\Api\Outcomes\Response;
use InerziaSoft\Core\Logger\Logger;
use InerziaSoft\Core\Parser\Exceptions\InvalidClassMapException;
use InerziaSoft\Core\Parser\Exceptions\UnexistingClassMapException;
use InerziaSoft\Core\Utils\ArrayUtils;

define("JSON_INITIALIZABLE_CLASS_NAME", "InerziaSoft\\Core\\Parser\\JsonInitializable");

class JsonParser {
	
	protected $response;
	protected $responseClassKey;
	protected $responseClassName;
	
	const ResponseClassKey = "responseClass";
	
	public static function fromRawJsonWithClassName($responseJson, $responseClassName, $responseClassKey = JsonParser::ResponseClassKey) {
		$jsonDecoded = json_decode($responseJson, true);
		
		return new JsonParser($jsonDecoded, $responseClassKey, $responseClassName);
	}
	
	public static function fromRawJson($responseJson, $responseClassKey = JsonParser::ResponseClassKey) {
		return new static(json_decode($responseJson, true), $responseClassKey);
	}
	
	public static function parseArrayInJson($json, $responseClassKey = JsonParser::ResponseClassKey, $responseClassName = null) {
		$convertedValues = [];
		if (ArrayUtils::isAssociativeArray($json)) {
			if (array_key_exists("date", $json) && array_key_exists("timezone_type", $json) && array_key_exists("timezone", $json)) {
				return new \DateTime($json["date"], new \DateTimeZone($json["timezone"]));
			}
			else {
				foreach ($json as $key => $item) {
					$convertedValues[$key] = (new JsonParser($item, $responseClassKey, $responseClassName))->parse();
				}
			}
		}
		else {
			foreach ($json as $item) {
				array_push($convertedValues, (new JsonParser($item, $responseClassKey, $responseClassName))->parse());
			}
		}
		return $convertedValues;
	}
	
	function __construct($response, $responseClassKey = JsonParser::ResponseClassKey, $responseClassName = null) {
		$this->response = $response;
		$this->responseClassKey = $responseClassKey;
		$this->responseClassName = $responseClassName;
	}
	
	public function parse() {
		return $this->mapJsonToResponseClass($this->response);
	}
	
	/**
	 * If possible, returns the mapped class of
	 * a well-formed JSON API response.
	 *
	 * @discussion The JSON response must contain
	 * a key called "responseClass" to be correctly mapped.
	 * If this key cannot be found, this method will just return
	 * the original JSON.
	 *
	 * @param $json
	 * @return Response|array
	 * @throws InvalidClassMapException If the JSON response maps to a class which is not a subclass of Response
	 * @throws UnexistingClassMapException If the JSON response maps to a class which cannot be found.
	 * @throws \ReflectionException
	 */
	private function mapJsonToResponseClass($json) {
		if (is_array($json)) {
			if (array_key_exists($this->responseClassKey, $json)) {
				if (class_exists($json[$this->responseClassKey])) {
					$className = $json[$this->responseClassKey];
					$reflectionClass = new \ReflectionClass($className);
					
					if ($reflectionClass->implementsInterface(JSON_INITIALIZABLE_CLASS_NAME)) {
						return $className::fromJson($json, $this->responseClassKey, $this->responseClassName);
					}
					else {
						throw new InvalidClassMapException($className);
					}
				}
				else {
					throw new UnexistingClassMapException($json[$this->responseClassKey]);
				}
			}
			else if (ArrayUtils::isAssociativeArray($json) && isset($this->responseClassName)) {
				$json[$this->responseClassKey] = $this->responseClassName;
				return $this->mapJsonToResponseClass($json);
			}
			else {
				return static::parseArrayInJson($json, $this->responseClassKey, $this->responseClassName);
			}
		}
		
		return $json;
	}
	
}