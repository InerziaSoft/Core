<?php

namespace InerziaSoft\Core\Attributes;

interface AttributesFactory {

    public static function getAttributeForComment($comment);

}