<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
namespace InerziaSoft\Core\Attributes;

class AttributesParser {

    private $attributes;
    private $factory;

    /**
     * AttributeParser constructor.
     * @param $property \ReflectionProperty
     * @param $factory AttributesFactory
     */
    public function __construct($property, $factory) {
        $this->attributes = [];
        $comments = self::getAttributesFromComment($property->getDocComment())[0];

        foreach ($comments as $comment) {
        	$attribute = $factory::getAttributeForComment($comment);
            if (isset($attribute)) array_push($this->attributes, $attribute);
        }
        $this->factory = $factory;
    }

    public function getAttributes() {
        return $this->attributes;
    }
	
	/**
	 * @param $attributeClassName string: A DbObjectAttribute subclass.
	 * @return bool
	 */
    public function contains($attributeClassName) {
	    /** @var Attribute $attribute */
	    foreach ($this->getAttributes() as $attribute) {
		    if ($attribute instanceof $attributeClassName) return true;
	    }
	
	    return false;
    }
    
    private static function getAttributesFromComment($phpComment) {
        $pattern = "#(@[a-zA-Z]+\\s*[a-zA-Z0-9, ()_].*)#";

        preg_match_all($pattern, $phpComment, $matches, PREG_PATTERN_ORDER);

        return $matches;
    }

}

