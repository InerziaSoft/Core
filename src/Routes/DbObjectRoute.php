<?php

/*
MIT License

Copyright (c) 2016 Alessio Moiso - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Routes;

use InerziaSoft\Core\Db\DbObject\DbObject;
use InerziaSoft\Core\Utils\UriUtils;

class DbObjectRoute extends Route {

    protected $dbObject;
	
	protected $idKeys;

    /**
     * Applies transaction routes to the specified f3.
     *
     * @param $f3 \Base
     */
    public static function applyTransactionRoute($f3) {
        $transactionRoute = "POST /".UriUtils::getApiFolder().UriUtils::getApiVersion()."/db/transaction";

        $f3->route($transactionRoute, function ($f3) {
            /** @var \Base $f3 */
            handleTransaction($_POST, getAuthorizationToken($f3->hive()));
        });
    }
	
	/**
	 * DbObjectRoute constructor.
	 *
	 * Initializes a DbObjectRoute based on a DbObject.
	 *
	 * @param $f3 \Base
	 * @param $dbObject DbObject
	 */
    public function __construct($f3, $dbObject) {
        parent::__construct($f3);

        $this->dbObject = $dbObject;
	    $this->idKeys = [];
	    foreach ($dbObject->getIdKeys() as $idKey) {
		    array_push($this->idKeys, "@$idKey");
	    }
    }

    protected function compute() {
        return null;
    }

    /**
     * For all DbObjects, the following routes will be generated:
     * - GET /<className>/all: returns all the objects in the corresponding table.
     * - GET /<className>/id/@id: returns the object whose primary key equals to @id.
     *
     * For DbObjects who do not specify the @readonly annotation:
     * - PUT /<className: reads a DbObject JSON representation from the request body and adds it to the corresponding table.
     * - POST /<className>/: reads a DbObject JSON representation from the request body and updates the corresponding object.
     * - DELETE /<className>/: deletes the object which is represented by the JSON contained in the request body.
     *
     * @param null $handler
     * @param null $route
     */
    public function apply($handler = null, $route = null) {
        $reflectionClass = new \ReflectionClass($this->dbObject);

        $comments = $reflectionClass->getDocComment();

        if (strpos($comments, "@ignore") === false) {
            $noAccess = (strpos($comments, "@noaccess") !== false);

            if (!$noAccess) {
                $readOnly = (strpos($comments, "@readonly") !== false);
                $requiresAuthorization = (strpos($comments, "@public") === false);

                $className = get_class($this->dbObject);
                $classNameExploded = explode("\\", $className);
                $countClassNameExploded = count($classNameExploded);
                $realClassName = strtolower($classNameExploded[$countClassNameExploded - 1]);

                $this->applyReadMethods($requiresAuthorization, $realClassName);

                if (!$readOnly) {
                    $this->applyWriteMethods($requiresAuthorization, $realClassName);
                }
            }
        }
    }
	
	/**
	 * @param $requiresAuthorization
	 * @param $realClassName
	 */
	private function applyReadMethods($requiresAuthorization, $realClassName) {
        $getAllRoute = "GET /".UriUtils::getApiFolder().UriUtils::getApiVersion()."/$realClassName/all";
        $whereRoute = "GET /".UriUtils::getApiFolder().UriUtils::getApiVersion()."/$realClassName/".implode("/", $this->idKeys);
	    $whereOnKey = "GET /".UriUtils::getApiFolder().UriUtils::getApiVersion()."/$realClassName/where/@key/@value";
		
		/** @var \Base $f3 */
		$f3 = $this->f3;
	    $idKeys = $this->idKeys;

        parent::apply(function () use ($f3, $requiresAuthorization, $realClassName) {
            getOnDbObject($realClassName, DBOBJECT_OPERATION_GET_ALL, $requiresAuthorization, getAuthorizationToken($f3->hive()));
        }, $getAllRoute);

        parent::apply(function () use ($f3, $requiresAuthorization, $realClassName, $idKeys) {
            getOnDbObject($realClassName, DBOBJECT_OPERATION_WHERE, $requiresAuthorization, getAuthorizationToken($f3->hive()), extractIdKeysAndValues($f3, $idKeys));
        }, $whereRoute);
	
	    parent::apply(function () use ($f3, $requiresAuthorization, $realClassName, $idKeys) {
		    $key = $f3->hive()["PARAMS"]["key"];
		    $value = $f3->hive()["PARAMS"]["value"];
		
		    getOnDbObjectWhere($realClassName, $requiresAuthorization, getAuthorizationToken($f3->hive()), $key, $value);
	    }, $whereOnKey);
    }

    private function applyWriteMethods($requiresAuthorization, $realClassName) {
        $insertRoute = "PUT /".UriUtils::getApiFolder().UriUtils::getApiVersion()."/$realClassName";
        $updateRoute = "POST /".UriUtils::getApiFolder().UriUtils::getApiVersion()."/$realClassName";
        $deleteRoute = "DELETE /".UriUtils::getApiFolder().UriUtils::getApiVersion()."/$realClassName/".implode("/", $this->idKeys);
	
	    /** @var \Base $f3 */
	    $f3 = $this->f3;
	    $idKeys = $this->idKeys;

        parent::apply(function () use ($f3, $requiresAuthorization, $realClassName) {
            $dbObjectJson = json_decode($f3->get("BODY"), true);

            writeOnDbObject($realClassName, DBOBJECT_OPERATION_INSERT, $requiresAuthorization, getAuthorizationToken($f3->hive()), $dbObjectJson);
        }, $insertRoute);

        parent::apply(function () use ($f3, $requiresAuthorization, $realClassName) {
            $dbObjectJson = json_decode($_POST["object"], true);

            writeOnDbObject($realClassName, DBOBJECT_OPERATION_UPDATE, $requiresAuthorization, getAuthorizationToken($f3->hive()), $dbObjectJson);
        }, $updateRoute);

        parent::apply(function () use ($f3, $requiresAuthorization, $realClassName, $idKeys) {
            writeOnDbObject($realClassName, DBOBJECT_OPERATION_DELETE, $requiresAuthorization, getAuthorizationToken($f3->hive()), null, extractIdKeysAndValues($f3, $idKeys));
        }, $deleteRoute);
    }
}