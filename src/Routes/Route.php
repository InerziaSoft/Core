<?php

/*
MIT License

Copyright (c) 2016 Alessio Moiso - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Routes;

use Base;

/**
 * Class Route
 *
 * Represents an abstract and generic route
 * that can be registered on $f3.
 *
 * @package InerziaSoft\Routes
 */
abstract class Route {

    /**
     * @var Base
     */
    protected $f3;

    public function __construct($f3) {
        $this->f3 = $f3;
    }

    /**
     * Returns the route path or an array of paths.
     *
     * @return string|array
     */
    protected abstract function compute();

    /**
     * Applies the route to $f3.
     *
     * @param $handler callable: A function to handle this route.
     * @param string $route: An optional route. If not set, the result of the compute() method will be used.
     */
    public function apply($handler, $route = null) {
        if (!isset($route)) $route = $this->compute();
	
	    $this->f3->route($route, $handler);
    }
	
	/**
	 * Returns the appropriate source from which the parameter
	 * values should be extracted.
	 *
	 * @discussion This function follows this rule:
	 * - GET: returns the FatFree PARAMS hive key.
	 * - POST: returns the $_POST super-global array.
	 * - PUT/DELETE: tries to parse a JSON in the request body.
	 *
	 * @param $method string
	 * @return array
	 */
	static final function parametersSourceFromHttpMethod($method) {
		switch ($method) {
			case "GET":
			case "DELETE":
				return Base::instance()->hive()["PARAMS"];
			
			case "POST":
				return $_POST;
			
			default:
				return json_decode(Base::instance()->get("BODY"), true);
		}
	}

}