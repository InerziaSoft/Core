<?php

// FIXME: we should move all of these in a class.

use InerziaSoft\Core\Api\Authentication\SessionToken;
use InerziaSoft\Core\Api\Outcomes\ListResponse;
use InerziaSoft\Core\Api\Outcomes\Outcome;
use InerziaSoft\Core\Api\Outcomes\Success;
use InerziaSoft\Core\Api\Outcomes\Unauthorized;
use InerziaSoft\Core\Db\DbObject\Conditions\ConditionsList;
use InerziaSoft\Core\Db\DbObject\Conditions\PostgreSql\Equal;
use InerziaSoft\Core\Db\DbObject\DbObject;
use InerziaSoft\Core\Db\DbObject\Definitions\DbObjectDefinition;
use InerziaSoft\Core\Db\DbObject\Exceptions\DbQueryException;
use InerziaSoft\Core\Db\DbObject\Exceptions\RollbackDbObjectException;
use InerziaSoft\Core\Db\DbObject\Exceptions\UnexistingDbObjectException;
use InerziaSoft\Core\Db\DbObject\Transactions\Transaction;
use InerziaSoft\Core\Page\ApiRequest;
use InerziaSoft\Core\Page\Exceptions\InvalidApiResponseException;
use InerziaSoft\Core\Page\Lists\ObjectsListItemDefinition;
use InerziaSoft\Core\Page\Page;
use InerziaSoft\Core\Routes\Responses\DbObjectTransactionResponse;

/*
MIT License

Copyright (c) 2016 Alessio Moiso - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/**
 * Handles a standard page by checking authentication (if required)
 * and calling the display() method.
 *
 * @param $className string: The page to display
 * @param $f3 Base: the FatFree framework.
 * @param $pageDelegate callable: A function that will be called (if any) before displaying the page, allowing to configure additional parameters.
 * @throws \InerziaSoft\Core\Page\Exceptions\ConfigurationErrorException
 * @throws \InerziaSoft\Core\Page\Exceptions\UnexistingPageException
 * @throws \InerziaSoft\Core\Parser\Exceptions\InvalidClassMapException
 * @throws \InerziaSoft\Core\Parser\Exceptions\UnexistingClassMapException
 */
function handleStandardPage($className, $f3, $pageDelegate = null) {
	$className = str_replace(PARTIAL_PAGE_ROUTE_NAME."\\", "", $className);

	$page = Page::instanceFromClassName($className);

	$page->setSessionToken(isAuthenticated($page, $f3));

	if (isset($pageDelegate)) {
		$pageDelegate($page);
	}
	
	echo $page->display();
}

/**
 * @param $className
 * @param $f3 Base
 * @param $parameters
 * @throws Exception
 * @throws InvalidApiResponseException
 * @throws \InerziaSoft\Core\Page\Exceptions\ConfigurationErrorException
 * @throws \InerziaSoft\Core\Page\Exceptions\UnexistingPageException
 */
function handleFileUpload($className, $f3, $parameters) {
	$className = str_replace(PARTIAL_PAGE_ROUTE_NAME."\\", "", $className);
	
	$page = Page::instanceFromClassName($className);
	
	$token = getAuthorizationToken($f3->hive());
	
	try {
		\InerziaSoft\Core\Api\Checker::forToken($token);
		
		$page->setSessionToken($token);
		handleStandardApiResponse($page->handleFileUpload($parameters));
	}
	catch (\InerziaSoft\Core\Api\Exceptions\InvalidParameterException $exception) {
		handleStandardApiResponse(Unauthorized::operationNotPermitted());
	}
}

/**
 * Handles a standard partial page by checking authentication (if required)
 * and calling the displayPartial() method.
 *
 * @param $mainPage string: The container page of the required partial.
 * @param $partialName string: The name of the class representing the partial page.
 * @param $f3 Base: the FatFree framework.
 * @throws \InerziaSoft\Core\Page\Exceptions\ConfigurationErrorException
 * @throws \InerziaSoft\Core\Page\Exceptions\UnexistingPageException
 * @throws \InerziaSoft\Core\Parser\Exceptions\InvalidClassMapException
 * @throws \InerziaSoft\Core\Parser\Exceptions\UnexistingClassMapException
 */
function handlePartialPage($mainPage, $partialName, $f3){
    $page = Page::instanceFromClassName($mainPage);

	$page->setSessionToken(isAuthenticated($page, $f3));

	echo $page->handlePartial($partialName);
}

/**
 * Handles a standard ObjectsList by checking authentication (if required)
 * and calling the handleList() method.
 *
 * @discussion This method tries to retrieve the associated ObjectsListItemDefinition
 * by guessing the name of the proper class with the following rules:
 * - The OBJECTSLISTITEMDEFINITIONS_CLASSES_NAMESPACE constant must be defined
 * - A class named OBJECTSLISTITEMDEFINITIONS_CLASSES_NAMESPACE\<$tableName>ObjectsListItemDefinition must exists
 *
 * If any of the above fails, the handleList() method is called with $tableName as the ObjectsListItemDefinition:
 * in that case, your subclass is responsible to pass the proper class the its parent.
 *
 * @param $mainPage string: The page responsible to handle this list.
 * @param $tableName string: The name of the table to gather data from.
 * @param $f3 Base: the FatFree framework.
 * @param $foreignKeyValue string|null: the value of the foreignKey to match (if the list is a child of a grouped list).
 * @param $objectsListItemDefinitionClass string
 * @throws \InerziaSoft\Core\Page\Exceptions\ConfigurationErrorException
 * @throws \InerziaSoft\Core\Page\Exceptions\UnexistingPageException
 * @throws \InerziaSoft\Core\Parser\Exceptions\InvalidClassMapException
 * @throws \InerziaSoft\Core\Parser\Exceptions\UnexistingClassMapException
 */
function handleListPage($mainPage, $tableName, $f3, $foreignKeyValue = null, $objectsListItemDefinitionClass = null) {
	$page = Page::instanceFromClassName($mainPage);

	$page->setSessionToken(isAuthenticated($page, $f3));
	
	if (!isset($objectsListItemDefinitionClass)) {
		/** @var ObjectsListItemDefinition $objectsListItemDefinitionClass */
		$objectsListItemDefinitionClass = $tableName;
		if (defined("OBJECTSLISTITEMDEFINITIONS_CLASSES_NAMESPACE")) {
			$proposedClassName = OBJECTSLISTITEMDEFINITIONS_CLASSES_NAMESPACE . "\\" . ucfirst($tableName) . "ObjectsListItemDefinition";
			
			if (class_exists($proposedClassName)) {
				$objectsListItemDefinitionClass = $proposedClassName;
			}
		}
	}

	echo $page->handleList($objectsListItemDefinitionClass, $foreignKeyValue);
}

/**
 * @param $mainPage
 * @param $tableName
 * @param $objectId
 * @param $f3
 * @throws \InerziaSoft\Core\Page\Exceptions\ConfigurationErrorException
 * @throws \InerziaSoft\Core\Page\Exceptions\UnexistingPageException
 * @throws \InerziaSoft\Core\Parser\Exceptions\InvalidClassMapException
 * @throws \InerziaSoft\Core\Parser\Exceptions\UnexistingClassMapException
 */
function handleInspectorPage($mainPage, $tableName, $objectId, $f3) {
	$page = Page::instanceFromClassName($mainPage);

	$page->setSessionToken(isAuthenticated($page, $f3));

	echo $page->handleListInspector($tableName, null, null, $objectId);
}

/**
 * Returns whether a request has been authenticated or not.
 *
 * @param $page Page: A page (optional). If this parameter is not specified,
 * this function will assume that the caller will handle a failed authentication.
 * @param $f3 Base: the FatFree framework.
 * @param $autoRedirect bool: Toggles automatic redirect to the login page.
 *
 * @return bool
 * @throws \InerziaSoft\Core\Parser\Exceptions\InvalidClassMapException
 * @throws \InerziaSoft\Core\Parser\Exceptions\UnexistingClassMapException
 */
function isAuthenticated($page, $f3, $autoRedirect = true) {
	$token = null;
	if (isset($_COOKIE[AUTHORIZATION_TOKEN_COOKIE])) {
		$token = $_COOKIE[AUTHORIZATION_TOKEN_COOKIE];

        $token = verifyAuthentication($token);
	}

	if (isset($page)) {
        if ($page->requiresAuthentication() && !isset($token)) {
        	if ($autoRedirect) {
		        $f3->reroute($f3->get("AuthUrl"));
	        }
	        else {
        		$token = null;
	        }
        }

        if ($page->isAuthenticationGateway() && isset($token)) {
        	if ($autoRedirect) {
		        $f3->reroute($f3->get("DefaultPage"));
	        }
	        else {
        	    $token = null;
	        }
        }
    }

	return $token;
}

/**
 * @param $token
 * @return null
 * @throws \InerziaSoft\Core\Parser\Exceptions\InvalidClassMapException
 * @throws \InerziaSoft\Core\Parser\Exceptions\UnexistingClassMapException
 */
function verifyAuthentication($token) {
    if (!isset($token)) return null;

    $authRouteClass = "auth";
    $authRouteMethod = "verify";
    if (defined("AUTHORIZATION_VERIFY_ROUTE_CLASS")) {
    	$authRouteClass = AUTHORIZATION_VERIFY_ROUTE_CLASS;
    }
    if (defined("AUTHORIZATION_VERIFY_ROUTE_METHOD")) {
    	$authRouteMethod = AUTHORIZATION_VERIFY_ROUTE_METHOD;
    }
    
    $apiRequest = new ApiRequest($authRouteClass, $authRouteMethod, ApiRequest::$getMethod, null, $token);
    $authResponse = $apiRequest->request();

    if ($authResponse["status"] == API_RESPONSE_NOT_FOUND) {
        throw new InvalidArgumentException("The application API system is unable to handle a verify() call. 
			This is required by functions.inc.php to actively verify an authentication token.
			Please, check that the API system correctly implements a valid route like the following: GET ". $apiRequest."
			(if the previous URL contains the literal strings AUTHORIZATION_VERIFY_ROUTE_CLASS and/or AUTHORIZATION_VERIFY_ROUTE_METHOD,
			you should also defines these constants before proceeding.)");
    }
    else if ($authResponse["status"] == API_RESPONSE_ERROR) {
        throw new InvalidArgumentException("The application API system is unable to handle a verify() call, because of an
			Internal Server Error. Please, solve this error before proceeding. Server response is: ".print_r($authResponse));
    }
    else if ($authResponse["status"] != API_RESPONSE_SUCCESSFUL) {
        $token = null;
    }

    return $token;
}

/**
 * @param $hive
 * @return mixed
 * @throws Exception
 */
function getAuthorizationToken($hive) {
	$token = $hive["HEADERS"]["Authorization"];
	if ($token != null) {
		$tokenHeader = explode(" ", $token);
		
		if (count($tokenHeader) == 2) {
			$token = $tokenHeader[1];
		}
		else {
			throw new Exception("Invalid token format!");
		}
		
		return $token;
	}
	
	$token = $hive["HEADERS"]["Authentication"];

	if ($token != null) {
		$tokenHeader = explode("=", $token);

		if (count($tokenHeader) == 2) {
			$token = $tokenHeader[1];
		}
		else {
			throw new Exception("Invalid token format!");
		}
	}

	return $token;
}

function valueForParameter($parName, $phpDocs, $parametersSource) {
	$value = null;
	if (array_key_exists($parName, $parametersSource)) {
		$value = $parametersSource[$parName];
	}
	else {
		$phpDocsByEndLine = explode("\n", $phpDocs);

		foreach ($phpDocsByEndLine as $phpDoc) {
			if (strpos($phpDoc, "@synonyms") !== false && strpos($phpDoc, $parName) !== false) {
				error_log("PhpFunctions | found synonyms for {$parName}.");

				$phpDocBySeparator = explode(":", $phpDoc);

				if (count($phpDocBySeparator) == 2) {
					error_log("PhpFunctions | synonyms are: {$phpDocBySeparator[1]}.");

					$synonyms = explode(",", $phpDocBySeparator[1]);

					foreach ($synonyms as $synonym) {
						$trimmedSyn = trim($synonym);
						$trimmedSyn = str_replace("$", "", $trimmedSyn);

						if (array_key_exists($trimmedSyn, $parametersSource)) {
							error_log("PhpFunctions | values for synonym {$trimmedSyn} has been found and will be used.");

							$value = $parametersSource[$trimmedSyn];
							break;
						}
					}
				}

				if ($value != null) {
					break;
				}
			}
		}
	}

	return $value;
}

define("TOKEN_API_PARAMETER_NAME", "token");

/**
 * @param $class
 * @param $methodName
 * @param $httpMethod
 * @param null $token
 * @throws InvalidApiResponseException
 * @throws ReflectionException
 */
function callApiMethod($class, $methodName, $httpMethod, $token = null) {
	$parametersSource = \InerziaSoft\Core\Routes\Route::parametersSourceFromHttpMethod($httpMethod);
	if ($httpMethod != ApiRequest::$getMethod || $httpMethod != ApiRequest::$deleteMethod) {
		$jsonBody = json_decode(Base::instance()->get("BODY"), true);
		if (isset($jsonBody)) {
			$parametersSource = $jsonBody;
		}
	}

    if (!is_string($class)) {
        $class = get_class($class);
    }

	$method = new \ReflectionMethod($class, $methodName);
	$comments = $method->getDocComment();
	
	$pars = $method->getParameters();
	$values = [];
	
	if (\InerziaSoft\Core\Utils\ArrayUtils::isAssociativeArray($parametersSource)) {
		$parametersSource[TOKEN_API_PARAMETER_NAME] = $token;
		foreach ($pars as $par) {
			array_push($values, valueForParameter($par->getName(), $comments, $parametersSource));
		}
	}
	else {
		foreach ($pars as $par) {
			if (strcasecmp($par->getName(), TOKEN_API_PARAMETER_NAME) == 0) {
				array_push($values, $token);
			}
			else {
				array_push($values, $parametersSource);
			}
		}
	}

	handleStandardApiResponse($method->invokeArgs(null, $values));
	die();
}

/**
 * @param $response
 * @throws InvalidApiResponseException
 */
function handleStandardApiResponse($response) {
	if ($response instanceof Outcome) {
		header("Content-Type: application/json; charset=UTF-8");
		header("X-Framework: Core by InerziaSoft");
		echo json_encode($response->toArray());
		return;
	}

	throw new InvalidApiResponseException("Api response does not conform to \\InerziaSoft\\Api\\Outcome interface: ".$response);
}

function getAttributesFromComment($phpComment) {
	$pattern = "#(@[a-zA-Z]+\\s*[a-zA-Z0-9, ()_].*)#";

	preg_match_all($pattern, $phpComment, $matches, PREG_PATTERN_ORDER);

    return $matches;
}

define("DBOBJECT_OPERATION_GET_ALL", "all");
define("DBOBJECT_OPERATION_WHERE", "where");

/**
 * @param $className
 * @param $operation
 * @param bool $requiresAuth
 * @param null $token
 * @param null $idKeysAndValues
 * @throws InvalidApiResponseException
 * @throws UnexistingDbObjectException
 * @throws \DI\DependencyException
 * @throws \DI\NotFoundException
 * @throws \InerziaSoft\Core\Db\DbObject\Exceptions\UnexistingKeyException
 * @throws \InerziaSoft\Core\Db\DbObject\Exceptions\UnknownIdKeyException
 * @throws \InerziaSoft\Core\Parser\Exceptions\InvalidClassMapException
 * @throws \InerziaSoft\Core\Parser\Exceptions\UnexistingClassMapException
 */
function getOnDbObject($className, $operation, $requiresAuth = false, $token = null, $idKeysAndValues = null) {
    if ($requiresAuth) {
        $token = verifyAuthentication($token);

        if (!isset($token)) {
            $response = new Unauthorized("DbObjectOperationNotPermitted", -12002);
        }
    }

    if (!isset($response)) {
        $completeClassName = API_CLASSES_NAMESPACE.$className;

        if (class_exists($completeClassName)) {
            /** @var DbObject $completeClassName */

            if ($operation == DBOBJECT_OPERATION_GET_ALL) {
                $response = new Success(ListResponse::response($completeClassName::all()));
            }
            else if ($operation == DBOBJECT_OPERATION_WHERE) {
            	try {
		            $response = new Success($completeClassName::objectWithIds($idKeysAndValues));
	            }
	            catch (UnexistingDbObjectException $exc) {
            		$response = new InerziaSoft\Core\Api\Outcomes\Error("UnexistingDbObject", -12002, $exc);
	            }
	            catch (\InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseException $exc) {
            		$response = new \InerziaSoft\Core\Api\Outcomes\InvalidParameter("id", "int");
	            }
            }
        }
        else {
            $response = new InerziaSoft\Core\Api\Outcomes\Error("UnexistingDbObjectClass", -12001, new InvalidArgumentException("Class '$completeClassName' is either not visible or doesn't exists."));
        }
    }

    /** @noinspection PhpUndefinedVariableInspection */
    handleStandardApiResponse($response);
    die();
}

/**
 * // FIXME: this function works only when a PostgreSQL DB is connected.
 *
 * @param $className
 * @param bool $requiresAuth
 * @param null $token
 * @param $key
 * @param $value
 * @throws InvalidApiResponseException
 * @throws UnexistingDbObjectException
 * @throws \DI\DependencyException
 * @throws \DI\NotFoundException
 * @throws \InerziaSoft\Core\Db\DbObject\Exceptions\UnexistingKeyException
 * @throws \InerziaSoft\Core\Db\DbObject\Exceptions\UnknownIdKeyException
 * @throws \InerziaSoft\Core\Parser\Exceptions\InvalidClassMapException
 * @throws \InerziaSoft\Core\Parser\Exceptions\UnexistingClassMapException
 */
function getOnDbObjectWhere($className, $requiresAuth = false, $token = null, $key, $value) {
	if ($requiresAuth) {
		$token = verifyAuthentication($token);
		
		if (!isset($token)) {
			$response = new Unauthorized("DbObjectOperationNotPermitted", -12002);
		}
	}
	
	if (!isset($response)) {
		$completeClassName = API_CLASSES_NAMESPACE.$className;
		
		if (class_exists($completeClassName)) {
			/** @var DbObject $completeClassName */
			
			$response = new Success(
				ListResponse::response(
					$completeClassName::where(new ConditionsList([
						new Equal($completeClassName, strtolower($key), $value)
					]))
				));
		}
		else {
			$response = new InerziaSoft\Core\Api\Outcomes\Error("UnexistingDbObjectClass", -12001, new InvalidArgumentException("Class '$completeClassName' is either not visible or doesn't exists."));
		}
	}
	
	/** @noinspection PhpUndefinedVariableInspection */
	handleStandardApiResponse($response);
	die();
}

define("DBOBJECT_OPERATION_INSERT", "insert");
define("DBOBJECT_OPERATION_UPDATE", "update");
define("DBOBJECT_OPERATION_DELETE", "delete");

/**
 * @param $className
 * @param $operation
 * @param bool $requiresAuth
 * @param null $token
 * @param null $object
 * @param null $idKeysAndValues
 * @throws InvalidApiResponseException
 * @throws \InerziaSoft\Core\Parser\Exceptions\InvalidClassMapException
 * @throws \InerziaSoft\Core\Parser\Exceptions\UnexistingClassMapException
 */
function writeOnDbObject($className, $operation, $requiresAuth = false, $token = null, $object = null, $idKeysAndValues = null) {
    if ($requiresAuth) {
        $token = verifyAuthentication($token);

        if (!isset($token)) {
            $response = new Unauthorized("DbObjectOperationNotPermitted", -12002);
        }
    }

    if (!(isset($response))) {
        $completeClassName = API_CLASSES_NAMESPACE.$className;

        if (class_exists($completeClassName)) {

            /** @var DbObject $dbObject */
            try {
                switch ($operation) {
                    case DBOBJECT_OPERATION_INSERT:
                        $dbObject = new $completeClassName($object);
                        $dbObject->insert();
                        break;

                    case DBOBJECT_OPERATION_UPDATE:
                        $dbObject = new $completeClassName($object);
                        $dbObject->update();
                        break;

                    case DBOBJECT_OPERATION_DELETE:

                        /** @var DbObject $completeClassName */
                        $dbObject = $completeClassName::objectWithIds($idKeysAndValues);
                        $dbObject->delete();
                        break;
                }

                $response = new Success();
            }
            catch (DbQueryException $exc) {
                $response = new InerziaSoft\Core\Api\Outcomes\Error("DbQueryException", -12003, $exc);
            }
            catch (UnexistingDbObjectException $exc) {
                $response = new InerziaSoft\Core\Api\Outcomes\Error("DbInitException", -12004, $exc);
            }
            catch (\Exception $exc) {
                $response = new InerziaSoft\Core\Api\Outcomes\Error("DbGenericException", -12005, $exc);
            }
        }
        else {
            $response = new InerziaSoft\Core\Api\Outcomes\Error("UnexistingDbObjectClass", -12001, new InvalidArgumentException("Class '$completeClassName' is either not visible or doesn't exists."));
        }
    }

    /** @noinspection PhpUndefinedVariableInspection */
    handleStandardApiResponse($response);
    die();
}

/**
 * @param $f3 Base
 * @param $idKeys array
 * @return array
 */
function extractIdKeysAndValues($f3, $idKeys) {
	$idKeysAndValues = [];
	foreach ($idKeys as $idKey) {
		$realIdKey = str_replace("@", "", $idKey);
		$idKeysAndValues[$realIdKey] = $f3->get("PARAMS")[$realIdKey];
	}
	return $idKeysAndValues;
}

/**
 * This function handles and perform a transaction on a series of DbObjectActions.
 *
 * The input data should be located under the "actions" key in the $source parameter.
 *
 * DbObjectTransactions can only be performed if the client is authenticated.
 *
 * @param $source array
 * @param $token SessionToken
 * @return void
 * @throws InvalidApiResponseException
 */
function handleTransaction($source, $token) {
    try {
        $token = \InerziaSoft\Core\Api\Checker::forToken($token);

        $actions = $source["actions"];

        $transaction = Transaction::fromJson($actions, $token);

        if (isset($transaction)) {
            try {
                $resultingId = $transaction->perform();
                $response = new Success(DbObjectTransactionResponse::response($resultingId));
            }
            catch (RollbackDbObjectException $exc) {
                $response = new InerziaSoft\Core\Api\Outcomes\Error("DbObjectTransactionFallback", -12011, $exc);
            }
            catch (\Exception $exc) {
                $response = new InerziaSoft\Core\Api\Outcomes\Error("DbObjectTransactionException", -12010, $exc);
            }
        }
        else {
            $response = new InerziaSoft\Core\Api\Outcomes\Error("InvalidDataFormat", -12012);
        }
    }
    catch (\InerziaSoft\Core\Api\Exceptions\InvalidParameterException $exc) {
        $response = new Unauthorized("DbObjectOperationNotPermitted", -12002);
    }

    handleStandardApiResponse($response);
    die();
}

function debugPrint($f3) {
    /** @var \Base $f3 */
    $routes = $f3->get("ROUTES");

    echo "<h1>InerziaSoft Automatic Routing System</h1>";
    echo "<h2>".count($routes)." computed routes.</h2>";

    $api = "api/";
    if (defined("API")) {
    	$api = API;
    }
    $apiVersion = "v1";
    if (defined("API_VERSION")) {
    	$apiVersion = API_VERSION;
    }
    
    echo "<h3>Constant Definitions</h3>";
    echo "<ul>
    <li>API_CLASSES_NAMESPACE: ".API_CLASSES_NAMESPACE."</li>
    <li>PAGES_CLASSES_NAMESPACE: ".PAGES_CLASSES_NAMESPACE."</li>
    <li>API: ".$api."</li>
    <li>API_VERSION: ".$apiVersion."</li>
    </ul>";

    echo "<h3>Routes</h3><ul>";
    foreach ($routes as $key => $value) {
        echo "<li>$key</li>";
    }
    echo "</ul>";
}

function dbObjectDebugPrint($dbObjects) {
//	echo "<h1>InerziaSoft DbObject Debug Info</h1>";
//	echo "<h2>".count($dbObjects)." mapped DbObjects.";
//
//	/** @var DbObject $dbObject */
//	foreach ($dbObjects as $dbObject) {
//		echo "<h3>".$dbObject."</h3>";
//		echo "<ul>";
//		echo "<li>Table name: ".DbObjectDefinition::getTableName($dbObject)."</li>";
//		echo "<li>Primary keys: ".implode(", ", DbObjectDefinition::getPrimaryKeysName($dbObject))."</li>";
//		echo "<li></li>";
//		echo "<li>ALL_FIELDS: ".$dbObject::getAllFields()."</li>";
//		echo "<li>ALL_TABLES: ".$dbObject::getAllTables()."</li>";
//		echo "</ul>";
//	}
}

/**
 * Computes and returns the base URI path for any request.
 *
 * @discussion This function supports only one level of iteration.
 * @return string The base URI path with a trailing slash.
 */
function getBaseUri() {
    return \InerziaSoft\Core\Utils\UriUtils::getBaseUri();
}