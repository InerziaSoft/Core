<?php

/*
MIT License

Copyright (c) 2016 Alessio Moiso - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Routes;

use InerziaSoft\Core\Api\Api;
use InerziaSoft\Core\Db\DbObject\DbObject;
use InerziaSoft\Core\Page\Page;

include_once __DIR__."/functions.inc.php";

define("DEFAULT_PAGE", "DefaultPage");

define("DBOBJECT_SUPERCLASS", "\\InerziaSoft\\Core\\Db\\DbObject\\DbObject");
define("GENERIC_PAGES_NAMESPACE", "InerziaSoft\\Core\\Page\\");
define("GENERIC_API_NAMESPACE", "InerziaSoft\\Core\\Api\\");

define("LOCALIZATION_API_CLASS", "InerziaSoft\\Core\\Api\\Localization\\Language");
define("AUTHENTICATION_API_CLASS", "InerziaSoft\\Core\\Api\\Authentication\\Authentication");

class RoutesInitializer {

    /**
     * Automatically initializes all the available routes.
     *
     * @discussion This method works on both the WebApp and the WebApi
     * of the system.
     *
     * WebApp: the RoutesInitializer creates routes for any subclass of
     * PAGES_CLASSES_NAMESPACE\Page. Consequently, you need to define a
     * specific subclass of the generic InerziaSoft\Page from which any
     * of your page should derive.
     *
     * A valid definition of a page that can be handled by RoutesInitializer is:
     *  namespace PAGES_CLASSES_NAMESPACE;
     *  class MyPage extends Page { ... }
     * Routes generation is deferred to the PageRoute class.
     *
     * ---
     *
     * WebApi: the RoutesInitializer configures routes for all
     * public and static methods of any subclass
     * of the abstract class Api.
     *
     * A valid definition of an API class that can be handled by RoutesInitializer is:
     *  namespace API_CLASSES_NAMESPACE;
     *  class MyApi extends Api {
     *      public static function getSomething() { ... }
     *  }
     * Routes generation is deferred to the ApiRoute class.
     *
     * ---
     *
     * DbObjects: the RoutesInitializer reads all mapped DbObjects classes
     * and generates routes according to their annotations.
     *
     * A valid definition of a DbObject class that can be handled by RoutesInitializer is:
     *
     *  namespace API_CLASSES_NAMESPACE;
     *  class MyObject extends DbObject { ... }
     *
     * DbObjects subclasses can customized routes with one or more
     * of the following annotations:
     * - @readonly: Insert/update/delete routes will not be generated.
     * - @public: Routes to this DbObject will not require authentication.
     * - @ignore: No routes will be generated.
     *
     * By default, none of these annotations is considered.
     *
     * Routes generation is deferred to the DbObjectRoute class.
     *
     * ---
     *
     * DbObjects Transaction: the RoutesInitializer creates and handles
     * transaction requests performed on a list of DbObjects.
     *
     * The routes that are generated handle the following requests:
     *
     * "POST /API_CLASSES_NAMESPACE/transaction"
     * "PUT /API_CLASSES_NAMESPACE/transaction"
     * "DELETE /API_CLASSES_NAMESPACE/transaction"
     *
     * All these routes expect a JSON representation of a DbObjectTransaction in their request body.
     *
     * @param $f3 \Base
     */
    public static function initialize($f3 = null) {
    	if (!isset($f3)) $f3 = \Base::instance();
    	
        $classes = get_declared_classes();

	    $dbObjects = [];
	    $pages = [];
	    $apis = [];

	    $defaultPageName = null;

        if ($f3->exists(DEFAULT_PAGE)) {
            $realClassName = PAGES_CLASSES_NAMESPACE.$f3->get(DEFAULT_PAGE);

            if (class_exists($realClassName)) {
                $defaultPageName = $realClassName;
                self::initializePage($realClassName, $f3, true);
            }
        }
	    
        foreach ($classes as $class) {
            if (self::isPage($class)) {
                if (strcmp($class, $defaultPageName) === 0) {

                }
                array_push($pages, $class);
            }
            else if (self::isDbObject($class)) {
	            array_push($dbObjects, $class);
            }
            else if (self::isApi($class)) {
                array_push($apis, $class);
            }
        }

        self::initializeAuthenticationSystem($f3);
        self::initializeLocalizationSystem($f3);
        
        foreach ($pages as $page) {
	        self::initializePage($page, $f3);
        }
        
        foreach ($dbObjects as $dbObject) {
	        self::initializeDbObject($dbObject, $f3);
        }
        
        foreach ($apis as $api) {
	        self::initializeApi($api, $f3);
        }
	
	    LoginRoutes::applyLoginLogoutCallsRoutes($f3);
        DbObjectRoute::applyTransactionRoute($f3);
        LocalizationRoutes::applyLocalizationRoutes($f3);

        if (defined("ROUTES_DEBUG") && ROUTES_DEBUG == "true") {
            $f3->route("GET /debug/routes", function ($f3) {
                debugPrint($f3);
            });
	        
	        $f3->route("GET /debug/dbObjects", function () use ($dbObjects) {
		        dbObjectDebugPrint($dbObjects);
	        });
        }
    }

    private static function isPage($class) {
        return strpos($class, PAGES_CLASSES_NAMESPACE) !== false;
    }

    private static function isApi($class) {
        return (strpos($class, API_CLASSES_NAMESPACE) !== false || strpos($class, GENERIC_API_NAMESPACE) !== false);
    }

    private static function isDbObject($class) {
        $dbObject = DBOBJECT_SUPERCLASS;
        $reflectionClass = new \ReflectionClass($class);

        return (strpos($class, API_CLASSES_NAMESPACE) !== false && $reflectionClass->isSubclassOf($dbObject));
    }

    private static function initializePage($class, $f3, $isHomePage = false) {
        $reflectionClass = new \ReflectionClass($class);
        if (!$reflectionClass->isAbstract() && $reflectionClass->isSubclassOf(GENERIC_PAGES_NAMESPACE."Page")) {
            $className = str_replace(PAGES_CLASSES_NAMESPACE, "", $class);
            $className = str_replace("partials\\", "", $className);

            $page = Page::instanceFromClassName($className);
            $pageRoute = new PageRoute($f3, $page, $isHomePage);

            $pageRoute->apply();

            if (count($page->getLists()) > 0) {
                $listRoute = new ListsRoute($f3, $page);
                $listRoute->apply();
            }
        }
    }

    private static function initializeApi($class, $f3) {
        $reflectionClass = new \ReflectionClass($class);

        if ($reflectionClass->isSubclassOf(GENERIC_API_NAMESPACE."Api")) {
            $className = str_replace(API_CLASSES_NAMESPACE, "", $class);
            $className = str_replace(GENERIC_API_NAMESPACE, "", $className);

            $api = Api::instanceFromClassName($className);
            $apiRoute = new ApiRoute($f3, $api);

            $apiRoute->apply();
        }
    }

    private static function initializeDbObject($class, $f3) {
        $className = str_replace(API_CLASSES_NAMESPACE, "", $class);

        $dbObject = DbObject::instanceFromClassName($className);
        $dbObjectRoute = new DbObjectRoute($f3, $dbObject);

        $dbObjectRoute->apply();
    }
    
    private static function initializeLocalizationSystem($f3) {
    	self::initializeApi(LOCALIZATION_API_CLASS, $f3);
    }
    
    private static function initializeAuthenticationSystem($f3) {
    	self::initializeApi(AUTHENTICATION_API_CLASS, $f3);
    }

}