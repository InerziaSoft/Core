<?php

/*
MIT License

Copyright (c) 2016 Alessio Moiso - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Routes;

define("FILE_UPLOAD_ROUTE_NAME", "upload");
define("PARTIAL_PAGE_ROUTE_NAME", "partials");

use InerziaSoft\Core\Page\ApiRequest;
use InerziaSoft\Core\Page\Page;
use InerziaSoft\Core\Page\PartialPage;

class PageRoute extends Route {

    /**
     * @var Page
     */
    protected $page;
    /**
     * @var string
     */
    protected $className;
    /**
     * @var array
     */
    protected $parameters;
	/**
	 * @var string
	 */
    protected $method;

    private $customRoutePath;
    private $isHomePage;

    public function __construct($f3, $page, $isHomePage = false) {
        $this->page = $page;
        $this->isHomePage = $isHomePage;

        if ($this->isHomePage) {
            $this->customRoutePath = "/";
        }
        else {
            $this->customRoutePath = $this->page->getRoutePath();
        }

        if (!defined("PAGES_CLASSES_NAMESPACE")) {
            throw new \InvalidArgumentException("Your configuration does not define a constant named
                PAGES_CLASSES_NAMESPACE. Please, define it somewhere before the inclusion of the routes.inc.php file
                and set its value to the namespace that groups your Pages classes.");
        }

        $this->className = str_replace(PAGES_CLASSES_NAMESPACE, "", get_class($page));
		$this->method = $this->page->getRouteMethod();
        $this->parameters = $this->page->getRouteParameters();

        parent::__construct($f3);
    }

    /**
     * Returns the standard route for a Page
     * or the custom route specified by the page itself.
     *
     * @discussion The standard route for a page follows this template:
     * "GET /namespace/pageName"
     *
     * For example, a page in namespace InerziaSoft\Web\Products with
     * the constant PAGES_CLASSES_NAMESPACE defined as "InerziaSoft\Web",
     * would produce a route path like the following:
     * "GET /products"
     *
     * If a page is included in a nested namespace, its route path will
     * reflect this. For example, a page like "InerziaSoft\Web\Products\InerziaTimer"
     * would result in something like this:
     * "GET /products/inerziatimer"
     *
     * If the page is a subclass of PartialPage, it will be automatically categorized
     * as a Partial of its containing class. For example, a page in namespace
     * "InerziaSoft\Web\Products\InerziaTimer\Hero", defined as subclass of PartialPage,
     * will produce the following route path:
     * "GET /products/inerziatimer/partials/hero"
     *
     * Pages are always accessible via a GET request.
     *
     * @discussion Pages that requires parameters in their route path must override
     * the getRouteParameters() method and return an array of PageRouteParameter.
     *
     * @discussion If the standard route does not satisfy requirements, subclasses of Page
     * (and consequently subclasses of PartialPage too) are allowed to customize the result of this
     * function, by overriding the getRoutePath() method.
     */
    protected function compute() {
	    $routes = [];
	    
        if (isset($this->customRoutePath)) return $this->method." ".$this->customRoutePath;

        if ($this->page instanceof PartialPage) {
            $classNameAsArray = explode("\\", $this->className);

            $count = count($classNameAsArray);

            if ($count > 1) {
                $last = $classNameAsArray[$count-1];
                $classNameWithoutLast = str_replace($last, "", $this->className);

                $this->className = $classNameWithoutLast.PARTIAL_PAGE_ROUTE_NAME."\\".$last;
            }
        }
	
	    $requiredParameters = [];
	    $optionalParameters = [];
        if ($this->method == ApiRequest::$getMethod) {
	        /** @var PageRouteParameter $parameter */
	        foreach ($this->parameters as $parameter) {
		        if ($parameter->isOptional()) {
			        array_push($optionalParameters, $parameter);
		        }
		        else {
			        array_push($requiredParameters, $parameter);
		        }
	        }
        }
	
	    $parametersToString = "";
	    if (count($requiredParameters) > 0) {
		    $parametersToString = implode("/", $requiredParameters);
		    $parametersToString = "/".$parametersToString;
	    }
        
        $this->handleClassName();
	    $baseWithoutMethod = " /".strtolower(str_replace("\\", "/", $this->className.$parametersToString));
        $base = $this->method.$baseWithoutMethod;
	    
	    array_push($routes, $base);
	    
	    if ($this->page->supportsFileUpload()) {
	    	array_push($routes, "POST ".$baseWithoutMethod."/".FILE_UPLOAD_ROUTE_NAME);
	    }
	    
	    $acc = $base;
	    foreach ($optionalParameters as $parameter) {
		    $acc .= "/".$parameter;
		    array_push($routes, $acc);
	    }
	    
	    return $routes;
    }

    private function handleClassName() {
	    $classNameAsArray = explode("\\", $this->className);
	    $classNameAsArrayCount = count($classNameAsArray);
	    if ($classNameAsArrayCount > 1 && $classNameAsArray[$classNameAsArrayCount-2] == $classNameAsArray[$classNameAsArrayCount-1]) {
		    array_splice($classNameAsArray, $classNameAsArrayCount-2, 1);
	    }
	    $this->className = implode("\\", $classNameAsArray);
    }
    
    /**
     * Applies the route with the standard Page handler
     * or, if specified, a custom callable.
     *
     * @param null $handler
     * @param null $route
     */
    public function apply($handler = null, $route = null) {
        if (!isset($handler)) {
            /** @var \Base $f3 */
            $f3 = $this->f3;
            $className = $this->className;
            $parameters = $this->parameters;

            $handler = function () use ($className, $f3, $parameters) {
	            if ($f3->get("SERVER")["REQUEST_METHOD"] == "POST" && strpos($f3->get("REALM"), FILE_UPLOAD_ROUTE_NAME) !== false) {
            		handleFileUpload($className, $f3, $_POST);
	            }
	            else {
		            handleStandardPage($className, $f3, function ($page) use ($parameters, $f3) {
			            $reflectionClass = new \ReflectionClass($page);
			
			            foreach ($reflectionClass->getProperties(\ReflectionProperty::IS_PUBLIC) as $property) {
				            /** @var PageRouteParameter $par */
				            foreach ($parameters as $par) {
					            if ($par->getPropertyName() == $property->getName()) {
						            $value = $this->getPageRouteParameter($par->getName());
						            $property->setValue($page, $this->handlePageRouteParameterValue($value));
					            }
				            }
			            }
		            });
	            }
            };
        }

        parent::apply($handler, $route);
    }
    
    protected function getPageRouteParameter($parName) {
    	$source = $this->parametersSourceFromHttpMethod($this->method);
    	
    	if (is_array($source) && array_key_exists($parName, $source)) {
    		return $source[$parName];
	    }
	    return null;
    }
    
    protected function handlePageRouteParameterValue($value) {
    	if ($value == "true") {
    		return true;
	    }
	    else if ($value == "false") {
    		return false;
	    }
	    return $value;
    }

}