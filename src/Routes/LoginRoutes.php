<?php

/*
MIT License

Copyright (c) 2016 Alessio Moiso - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Routes;

use InerziaSoft\Core\Api\Authentication\Responses\AuthenticationVerifyResponse;
use InerziaSoft\Core\Page\ApiRequest;

define("JWT_POST_KEY_TOKEN", "jwt");
define("DEFAULT_AUTH_COOKIE_EXPIRATION_TIME", 31536000);

class LoginRoutes {
	
	/**
	 * @param $f3 \Base
	 */
	public static function applyLoginLogoutCallsRoutes($f3) {
		$loginRoute = "POST /login";
		$logoutRoute = "POST /logout";
		
		$f3->route($loginRoute, function () {
			if (isset($_POST[JWT_POST_KEY_TOKEN])) {
				$verifyRequest = new ApiRequest("auth", "verify", ApiRequest::$getMethod, null, $_POST[JWT_POST_KEY_TOKEN]);
				$response = $verifyRequest->request();
				
				if ($response instanceof AuthenticationVerifyResponse && $response->isAuthenticated) {
					setcookie(AUTHORIZATION_TOKEN_COOKIE, $_POST[JWT_POST_KEY_TOKEN], time() + DEFAULT_AUTH_COOKIE_EXPIRATION_TIME, "/", "", false, true);
					
					echo json_encode(["status" => API_RESPONSE_SUCCESSFUL]);
				}
				else {
					echo json_encode([
						"status" => API_RESPONSE_UNAUTHORIZED,
						"error" => "InvalidOrExpiredToken",
						"internalException" => $response
					]);
				}
			}
			else {
				echo json_encode(["status" => API_RESPONSE_BAD_REQUEST]);
			}
		});
		
		$f3->route($logoutRoute, function () {
			if (isset($_POST[JWT_POST_KEY_TOKEN])) {
				$logoutRequest = new ApiRequest("auth", "logout", ApiRequest::$postMethod, null, $_POST[JWT_POST_KEY_TOKEN]);
				$logoutRequest->request();
			}
			
			setcookie(AUTHORIZATION_TOKEN_COOKIE, "", time() - 3600, "/", "", false, true);
		});
	}
	
}