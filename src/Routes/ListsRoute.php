<?php

/*
MIT License

Copyright (c) 2016 Alessio Moiso - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Routes;

use InerziaSoft\Core\Page\Lists\Proposals\ProposedGroupedObjectsList;
use InerziaSoft\Core\Page\Lists\Proposals\ProposedObjectsList;
use InerziaSoft\Core\Page\Page;

class ListsRoute extends PageRoute {

    protected $lists;

    /**
     * ListsRoutes constructor.
     * @param $f3 \Base
     * @param $page Page
     */
    public function __construct($f3, $page) {
        parent::__construct($f3, $page, false);

        $this->lists = $page->getLists();
    }

    /**
     * @return array
     */
    protected function compute() {
        $computed = [];
        foreach ($this->lists as $list) {
	        if ($list instanceof ProposedGroupedObjectsList) {
		        array_push($computed, new ListRoutePresenter(
		        	$this->routeForTableName(
		        		$list->getMainListDefinition()->getTableName()),
				        $list->getMainListDefinition()->getObjectsListItemDefinitionClass()
		        ));
		        array_push($computed, new ListRoutePresenter(
		        	$this->routeForTableName(
		        		$list->getChildListDefinition()->getTableName(), true),
				        $list->getChildListDefinition()->getObjectsListItemDefinitionClass()
		        ));
	        }
	        else if ($list instanceof ProposedObjectsList) {
		        array_push($computed, new ListRoutePresenter(
		        	$this->routeForTableName($list->getTableName()),
				        $list->getObjectsListItemDefinitionClass()
		        ));
	        }
	        else {
		        throw new \InvalidArgumentException("Unrecognized or unsupported ProposedObjectsListDefinition type: ".get_class($list).".");
	        }
        }

        return $computed;
    }
    
    private function routeForTableName($tableName, $isChild = false) {
	    $routeName = "GET /".strtolower($this->className."\\lists\\".$tableName);
	    
	    if ($isChild) {
		    $routeName .= "\\@foreignKey";
	    }
	    
	    return str_replace("\\", "/", $routeName);
    }

    public function apply($handler = null, $route = null) {
	    $computedHandler = $handler;
	
	    /** @var ListRoutePresenter $presenter */
	    foreach ($this->compute() as $presenter) {
            $inspectorHandler = null;
            if (!isset($computedHandler)) {
	            /** @var \Base $f3 */
	            $f3 = $this->f3;
                $fullClassName = $this->className;
                $computed = $presenter->getRoute();
                $objectsListItemDefinitionClass = $presenter->getObjectsListItemDefinitionClass();

                $computedWithoutForeignKey = str_replace("/@foreignKey", "", $computed);
                $classNameAsArray = explode("/", $computedWithoutForeignKey);
                $tableName = $classNameAsArray[count($classNameAsArray)-1];
	            
                $computedHandler = function () use ($f3, $tableName, $fullClassName, $objectsListItemDefinitionClass) {
                    handleListPage($fullClassName, $tableName, $f3, $f3->get("PARAMS")["foreignKey"], $objectsListItemDefinitionClass);
                };

                $inspectorHandler = function () use ($f3, $tableName, $fullClassName) {
                    handleInspectorPage($fullClassName, $tableName, $f3->hive()["PARAMS"]["id"], $f3);
                };
	
	            $listRouteToInspector = str_replace("/lists/", "/inspectors/", $computed);
	            $listRouteToInspector = str_replace("/@foreignKey", "", $listRouteToInspector);
	            $inspectorRoutes = [
		            $listRouteToInspector,
		            $listRouteToInspector."/@id"
	            ];
	
	            parent::apply($computedHandler, $computed);
	            parent::apply($inspectorHandler, $inspectorRoutes);
	            
	            $computedHandler = $handler;
            }
        }
    }
}