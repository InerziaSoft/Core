<?php

/*
MIT License

Copyright (c) 2016 Alessio Moiso - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Routes;

use InerziaSoft\Core\Api\Api;
use InerziaSoft\Core\Routes\Exceptions\MalformedRouteConfiguration;
use InerziaSoft\Core\Utils\UriUtils;

class ApiRoute extends Route {

    /**
     * @var Api
     */
    protected $api;

    /**
     * @var string
     */
    protected $className;

    /**
     * @var array
     */
    protected $routes;

    /**
     * ApiRoute constructor.
     * @param $f3 \Base
     * @param $api Api
     */
    public function __construct($f3, $api) {
        $this->api = $api;

        $this->className = $api->getRouteName();
        if (!isset($this->className)) {
            if (!defined("API_CLASSES_NAMESPACE")) {
                throw new \InvalidArgumentException("Your configuration does not define a constant named
                API_CLASSES_NAMESPACE. Please, define it somewhere before the inclusion of the routes.inc.php file
                and set its value to the namespace that groups your Api classes.");
            }

            $this->className = str_replace(API_CLASSES_NAMESPACE, "", get_class($api));
            $this->className = str_replace(GENERIC_API_NAMESPACE, "", $this->className);
            $this->className = str_replace("\\", "/", $this->className);
            $this->className = str_replace("Api", "", $this->className);
            $this->className = strtolower($this->className);
        }

        parent::__construct($f3);
    }

    protected function compute() {
        $routes = [];
        $reflectionClass = new \ReflectionClass($this->api);

        $methods = $reflectionClass->getMethods(\ReflectionMethod::IS_PUBLIC);

        foreach ($methods as $method) {
            $methodName = $method->getName();
            $route = $this->getRouteForMethod($methodName, $method->getDocComment(), $method->getParameters());

            if ($route != null) {
                array_push($routes, $route);
            }
        }

        return $routes;
    }

    private function getRouteForMethod($methodName, $comment, $parameters) {
        if (strpos($comment, "@ignore") !== false || strtolower($methodName) == "getroutename") {
            return null;
        }

        $attributes = [];
        if ($comment !== false) {
            $attributes = getAttributesFromComment($comment)[0];
        }

        $method = "GET";
        $route = strtolower($methodName);

        foreach ($attributes as $attribute) {
            if (strpos($attribute, "@route") !== false) {
                $attributeSplitBySpace = explode(" ", $attribute);

                if (count($attributeSplitBySpace) > 1) {
                    $route = strtolower(trim($attributeSplitBySpace[1]));
                }
                else {
                    throw new MalformedRouteConfiguration(get_class($this->api), $methodName, "@route");
                }
            }
            else if (strpos($attribute, "@method") !== false) {
                $attributeSplitBySpace = explode(" ", $attribute);

                if (count($attributeSplitBySpace) > 1) {
                    $method = strtoupper(trim($attributeSplitBySpace[1]));
                }
            }
        }

        if (strlen($method) == 0) throw new \InvalidArgumentException("Invalid method detected for route $route: $method");

        $computedPars = [];

        if ($method == "GET" || $method == "DELETE") {
            /** @var \ReflectionParameter $parameter */
            foreach ($parameters as $parameter) {
                $originalParName = $parameter->getName();
                if (strtolower(trim($originalParName)) == "token") continue;

                $parameterName = "@".$originalParName;

                array_push($computedPars, $parameterName);
            }
        }

        if (count($computedPars) > 0) {
            if (strlen($route) > 0) $route .= "/";

            $route .= implode("/", $computedPars);
        }

        $route = $method." /".UriUtils::getApiFolder().UriUtils::getApiVersion()."/".$this->className."/".$route;

        return new ApiRoutePresenter($route, $methodName, $method);
    }

    public function apply($handler = null, $route = null) {
        $routes = $this->compute();

        /** @var ApiRoutePresenter $route */
        foreach ($routes as $route) {
	        /** @var \Base $f3 */
	        $f3 = $this->f3;
            $methodName = $route->getMethodName();
            $httpMethod = $route->getHttpMethod();
            $className = get_class($this->api);

            parent::apply(function () use ($methodName, $f3, $className, $httpMethod) {
                callApiMethod($className, $methodName, $httpMethod, getAuthorizationToken($f3->hive()));
            }, $route->getRoute());
        }
    }

}

