<?php

/*
MIT License

Copyright (c) 2016 Alessio Moiso - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Routes;

/**
 * Class PageRouteParameter
 *
 * Represents a parameter that must be passed
 * to the route path of a Page.
 *
 * For example, a PageRouteParameter with the following values:
 *  $name = "name"
 *  $propertyName = "objectName"
 * maps to the 'name' parameter in the following route path:
 *  "GET /namespace/page/@name"
 * and fills the following property in the destination page:
 *  public $objectName
 */
class PageRouteParameter {

    private $name;
    private $propertyName;
	private $optional;
	
	/**
	 * PageRouteParameter constructor.
	 *
	 * Returns a new instance of a PageRouteParameter.
	 *
	 * @param $name string: The parameter identifier.
	 * @param $propertyName string|null: The name of the page property to fill with the passed value (if null, $name will be used).
	 * @param bool $optional: Defines whether this parameter is optional or not.
	 */
    public function __construct($name, $propertyName = null, $optional = false) {
        $this->name = $name;
        
        if (isset($propertyName)) {
	        $this->propertyName = $propertyName;
        }
        else {
        	$this->propertyName = $name;
        }
        
	    $this->optional = $optional;
    }

    public function __toString() {
        return "@".$this->name;
    }

    /**
     * @return string
     */
    public function getPropertyName() {
        return $this->propertyName;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }
	
	/**
	 * @return bool
	 */
    public function isOptional() {
	    return $this->optional;
    }

}