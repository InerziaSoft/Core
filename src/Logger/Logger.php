<?php

namespace InerziaSoft\Core\Logger;

use Monolog\Handler\StreamHandler;

define("DEFAULT_LOG_NAME", "InerziaSoft Logger");

class Logger {
	
	/**
	 * Logs something on the PHP standard log.
	 *
	 * @param $text string: A string to log.
	 * @param int $level : A constant defined in the LogLevel class representing the level of this log.
	 * @return bool
	 */
	static function log($text, $level = LogLevel::DebugLevel) {
		$logString = LogLevel::textFromLogLevel($level);

		$name = DEFAULT_LOG_NAME;
		if (defined("NAME")) {
			$name = NAME;
		}
		
		return error_log($name." - ".$logString.": ".$text);
	}
	
	protected $className;
	
	/**
	 * Logger constructor.
	 *
	 * Instantiates a new Logger based on a specific class.
	 *
	 * @param $className
	 */
	public function __construct($className) {
		$this->className = $className;
	}
	
	/**
	 * Log to the default file handler or to a custom Monolog handler.
	 *
	 * @param $text string
	 * @param int $level
	 * @param array $context
	 * @param $handler \Monolog\Handler\StreamHandler|null
	 * @return bool
	 */
	public function logToHandler($text, $level = LogLevel::DebugLevel, $context = [], $handler = null) {
		if (!isset($handler)) $handler = new StreamHandler(__DIR__."/../../log/core.log");
		
		$logger = new \Monolog\Logger($this->className." - Logger");
		$logger->pushHandler($handler);
		
		$context = array_merge($context, ["LogLevel" => $level]);
		switch ($level) {
			case LogLevel::DebugLevel:
				return $logger->debug($text, $context);
			
			case LogLevel::InformativeLevel:
				return $logger->info($text, $context);
			
			case LogLevel::WarningLevel:
				return $logger->warning($text, $context);
				
			case LogLevel::ErrorLevel:
				return $logger->error($text, $context);
				
			case LogLevel::CriticalLevel:
				return $logger->critical($text, $context);
				
			case LogLevel::AlertLevel:
				return $logger->alert($text, $context);
				
			case LogLevel::EmergencyLevel:
				return $logger->emergency($text, $context);
		}
		
		return false;
	}

}