<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
namespace InerziaSoft\Core\Logger;

class LogLevel {
	const DebugLevel = 0;
	const InformativeLevel = 1;
	const WarningLevel = 2;
	const ErrorLevel = 3;
	const CriticalLevel = 4;
	const AlertLevel = 5;
	const EmergencyLevel = 6;
	
	static function textFromLogLevel($logLevel) {
		switch ($logLevel) {
			case LogLevel::DebugLevel:
				return "[DEBUG]";
			case LogLevel::InformativeLevel:
				return "[INFO]";
			case LogLevel::WarningLevel:
				return "[WARNING]";
			case LogLevel::ErrorLevel:
				return "[ERROR]";
			case LogLevel::CriticalLevel:
				return "[CRITICAL]";
			case LogLevel::AlertLevel:
				return "[ALERT]";
			case LogLevel::EmergencyLevel:
				return "[EMERGENCY]";
		}
		return "[UNKNOWN]";
	}
	
}