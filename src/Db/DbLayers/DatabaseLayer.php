<?php
/*
MIT License

Copyright (c) 2016-2017 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Db\DbLayers;

interface DatabaseLayer {
	/**
	 * Gets the connection status
	 *
	 * @return bool: true if the DbLayer is connected.
	 */
	public function isConnected();

	/**
	 * Gets the hostname of the machine which is running this database.
	 *
	 * @return string: The hostname.
	 */
	public function getHostname();

	/**
	 * Gets the host port on which the server is listening.
	 *
	 * @return int: The port number on which the server is listening.
	 */
	public function getHostPort();

	/**
	 * Gets the server version.
	 */
	public function getServerVersion();

	/**
	 * Returns a new PreparedStatement with the specified query.
	 *
	 * @param $query string: A valid query.
	 * @param $name string: The name to associate to this PreparedStatement (optional).
	 * @return PreparedStatement: A PreparedStatement ready to be executed.
	 */
	public function prepare($query, $name = "");

	/**
	 * Performs a simple query without parameters.
	 *
	 * @param $query string: The query to be performed.
	 * @return resource: The result of a call to pg_query().
	 */
	public function query($query);

	/**
	 * Creates a schema with the specifed name
	 *
	 * @param $schema : The schema name.
	 * @return mixed: The pg_query performed.
	 */
	public function createSchema($schema);

	/**
	 * Perform a drop of the specified schema.
	 *
	 * @param $schema string: The name of the schema.
	 * @return mixed: The pg_query performed.
	 */
	public function dropSchema($schema);

	/**
	 * Perform a query or a set of queries from a file.
	 *
	 * @param $filename string: The file path.
	 * @param $printOutput bool: toggle output.
	 * @return bool: true if the operation has been completed successfully and everything has been committed to the database.
	 */
	public function queryFromFile($filename, $printOutput = false);

	/**
	 * Begin a transaction on the current connection.
	 */
	public function beginTransaction();

	/**
	 * Closes and commits any change made on the current connection.
	 */
	public function commitTransaction();

	/**
	 * Cancels any change made on the current connection.
	 */
	public function rollbackTransaction();

	/**
	 * Returns the Primary Key name of a specified table.
	 *
	 * @param $table string: The table name.
	 * @discussion Warning! This method must be used only under controlled circumstances.
	 * @return string: The Primary Key column name.
	 */
	public function getPrimaryKey($table);

	/**
	 * Returns the last reported error from the database on this connection.
	 *
	 * @return string
	 */
	public function getLastError();
	
	/**
	 * Returns the parameter placeholder that the database driver uses.
	 *
	 * @return string
	 */
	public function getParametersPlaceholder();
}