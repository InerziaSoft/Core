<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Db\DbLayers\MySql;


use InerziaSoft\Core\Db\DbLayers\DatabaseLayer;
use InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseConfigurationException;
use InerziaSoft\Core\Db\DbLayers\PreparedStatement;
use InerziaSoft\Core\Utils\UriUtils;

class MySqlDbLayer implements DatabaseLayer {
	
	const ConfigFilename = "db.ini";
	
	/**
	 * @var \mysqli
	 */
	protected $connection;
	
	public function __construct($filename = null) {
		if (!file_exists($filename)) $filename = self::ConfigFilename;
		$config = UriUtils::getConfig($filename);
		
		if ($config !== false) {
			$result = new \mysqli($config["ServerHost"], $config["DatabaseUser"], $config["DatabasePassword"], $config["DatabaseName"], $config["ServerPort"]);
			
			if ($result !== false) {
				$this->connection = $result;
				$this->connection->set_charset("utf8");
			}
			else {
				$this->connection = null;
			}
		}
		else {
			throw new DatabaseConfigurationException($filename);
		}
	}
		
	/**
	 * Gets the connection status
	 *
	 * @return bool: true if the DbLayer is connected.
	 */
	public function isConnected() {
		return isset($this->connection);
	}
	
	/**
	 * Gets the hostname of the machine which is running this database.
	 *
	 * @return string: The hostname.
	 */
	public function getHostname() {
		return $this->connection->get_server_info();
	}
	
	/**
	 * Gets the host port on which the server is listening.
	 *
	 * @return int: The port number on which the server is listening.
	 */
	public function getHostPort() {
		return $this->connection->get_server_info();
	}
	
	/**
	 * Gets the server version.
	 */
	public function getServerVersion() {
		return $this->connection->get_server_info();
	}
	
	/**
	 * Returns a new PreparedStatement with the specified query.
	 *
	 * @param $query string: A valid query.
	 * @param $name string: The name to associate to this PreparedStatement (optional).
	 * @return PreparedStatement: A PreparedStatement ready to be executed.
	 */
	public function prepare($query, $name = "") {
		return new MySqlPreparedStatement($this->connection, $query, $name);
	}
	
	/**
	 * Performs a simple query without parameters.
	 *
	 * @param $query string: The query to be performed.
	 * @return bool|\mysqli_result
	 */
	public function query($query) {
		return $this->connection->query($query);
	}
	
	/**
	 * Creates a schema with the specifed name
	 *
	 * @param $schema : The schema name.
	 * @return mixed: The pg_query performed.
	 */
	public function createSchema($schema) {
		// TODO: Implement createSchema() method.
	}
	
	/**
	 * Perform a drop of the specified schema.
	 *
	 * @param $schema string: The name of the schema.
	 * @return mixed: The pg_query performed.
	 */
	public function dropSchema($schema) {
		// TODO: Implement dropSchema() method.
	}
	
	/**
	 * Perform a query or a set of queries from a file.
	 *
	 * @param $filename string: The file path.
	 * @param $printOutput bool: toggle output.
	 * @return bool: true if the operation has been completed successfully and everything has been committed to the database.
	 */
	public function queryFromFile($filename, $printOutput = false) {
		// TODO: Implement queryFromFile() method.
	}
	
	/**
	 * Begin a transaction on the current connection.
	 */
	public function beginTransaction() {
		// TODO: Implement beginTransaction() method.
	}
	
	/**
	 * Closes and commits any change made on the current connection.
	 */
	public function commitTransaction() {
		// TODO: Implement commitTransaction() method.
	}
	
	/**
	 * Cancels any change made on the current connection.
	 */
	public function rollbackTransaction() {
		// TODO: Implement rollbackTransaction() method.
	}
	
	/**
	 * Returns the Primary Key name of a specified table.
	 *
	 * @param $table string: The table name.
	 * @discussion Warning! This method must be used only under controlled circumstances.
	 * @return string: The Primary Key column name.
	 */
	public function getPrimaryKey($table) {
		// TODO: Implement getPrimaryKey() method.
	}
	
	/**
	 * Returns the last reported error from the database on this connection.
	 *
	 * @return string
	 */
	public function getLastError() {
		// TODO: Implement getLastError() method.
	}
	
	public function getParametersPlaceholder() {
		return "?";
	}
	
}