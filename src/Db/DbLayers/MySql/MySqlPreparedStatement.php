<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Db\DbLayers\MySql;

use InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseException;
use InerziaSoft\Core\Db\DbLayers\PreparedStatement;
use InerziaSoft\Core\Logger\Logger;
use InerziaSoft\Core\Logger\LogLevel;

class MySqlPreparedStatement implements PreparedStatement {
	
	/**
	 * @var \mysqli
	 */
	private $connection;
	private $name;
	
	protected $query;
	/**
	 * @var \mysqli_stmt
	 */
	protected $stmt;
	
	function __construct($connection, $query, $name) {
		$this->connection = $connection;
		$this->query = $query;
		$this->name = $name;
		
		// TODO: insert prepared statements name check
		
		if (!($this->stmt = $this->connection->prepare($this->query))) {
			throw new DatabaseException($this->query, $this->connection->error);
		}
	}
	
	private function bindParameters($parameters) {
		if (count($parameters) == 0) return;
		
		$actualValues = [];
		$types = "";
		foreach ($parameters as $value) {
			if ($value === "true") {
				$value = 1;
			}
			else if ($value === "false") {
				$value = 0;
			}
			
			$type = "b";
			if (is_int($value) || is_bool($value)) {
				$type = "i";
			}
			else if (is_double($value) || is_float($value)) {
				$type = "d";
			}
			else if (is_string($value)) {
				$type = "s";
			}
			else if ($value instanceof \DateTime) {
				$type = "s";
				$value = $value->format("Y-m-d H:i:s");
			}
			
			$types .= $type;
			array_push($actualValues, $value);
		}
		
		$this->stmt->bind_param($types, ...$actualValues);
	}
	
	/**
	 * Runs this PreparedStatement with the specified parameters.
	 *
	 * @param $parameters array: An array of parameters.
	 * @return array : An ordered indexed array with query results.
	 * @throws DatabaseException
	 */
	public function execute($parameters = []) {
		$this->bindParameters($parameters);
		
		if (!$this->stmt->execute()) {
			throw new DatabaseException($this->query, $this->stmt->error." - ".$this->connection->error);
		}
		
		$res = $this->stmt->get_result();
		if ($res === false)  {
			if (strlen($this->stmt->error) == 0 && strlen($this->connection->error)) {
				throw new DatabaseException($this->query, $this->stmt->error." - ".$this->connection->error);
			}
			return null;
		}
		
		return $res->fetch_all(MYSQLI_ASSOC);
	}
	
	/**
	 * Runs this PreparedStatement with the specified parameters
	 * and returns the first row.
	 *
	 * @param $parameters array: An array of parameters.
	 * @return array|null: An indexed array with the first resulting row or empty.
	 */
	public function executeReturningFirstRow($parameters = array()) {
		return $this->execute($parameters)[0];
	}
	
	/**
	 * Runs this PreparedStatement with the specified parameters,
	 * returning an associative array that uses the first column values
	 * as keys and joins the remaining columns as value with the specified
	 * character.
	 *
	 * Example:
	 *          query result        array("id"=>10, "name"=>"Mark", "surname"=>"Doe")
	 *          parsed result       array("10"=>"Mark - Doe")
	 *
	 * @param $parameters array: An array of parameters.
	 * @param $joinCharacter string: One or more characters to join multiple columns.
	 * @return array : An indexed array as described above or empty.
	 * @throws \Exception
	 */
	public function executeGroupingOnFirstColumn($parameters = array(), $joinCharacter = " - ") {
		throw new \Exception("executeGroupingOnFirstColumn is not supported in MySql adapter");
	}
	
	/**
	 * Runs this PreparedStatement with the specified parameters and returns
	 * an ordered array containing the first column values. Any other column is skipped.
	 *
	 * @param $parameters array: An array of parameters.
	 * @return array : An array with the sequence of values of the first column or empty.
	 * @throws \Exception
	 */
	public function executeReturningFirstColumn($parameters = array()) {
		throw new \Exception("executeGroupingOnFirstColumn is not supported in MySql adapter");
	}
}