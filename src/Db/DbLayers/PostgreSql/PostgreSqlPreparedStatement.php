<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
namespace InerziaSoft\Core\Db\DbLayers\PostgreSql;

use InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseException;
use InerziaSoft\Core\Db\DbLayers\PreparedStatement;

class PostgreSqlPreparedStatement implements PreparedStatement {
	
	static $StatementStatusReady = 0;
	static $StatementStatusExecuted = 1;
	
	private $resource;
	private $name;
	
	protected $query;

	private $status;
	private $affectedRows;
	
	public function __construct($resource, $query, $name) {
		$this->resource = $resource;
		$this->name = $name;
		$this->query = $query;
		$this->status = static::$StatementStatusReady;
		$this->affectedRows = -1;
		
		$shouldRebuild = true;
		if (strlen($name) > 0) {
			/** @noinspection SqlNoDataSourceInspection */
			/** @noinspection SqlResolve */
			$result = pg_query_params($this->resource, 'SELECT name FROM pg_prepared_statements WHERE name = $1', array($this->name));
			$shouldRebuild = (pg_num_rows($result) == 0);
		}
		
		if ($shouldRebuild) {
			$result = @pg_prepare($this->resource, $this->name, $this->query);
			
			if ($result === false) {
				throw new DatabaseException($this->query, pg_last_error($this->resource));
			}
		}
	}
	
	public function getStatus() {
		return $this->status;
	}
	
	public function getAffectedRows() {
		return $this->affectedRows;
	}

	public function execute($parameters = []) {
		$result = @pg_execute($this->resource, $this->name, $parameters ?? []);
		
		if ($result === false || $result === null) {
			throw new DatabaseException($this->query, pg_last_error($this->resource), $parameters);
		}
		
		$fetchedArray = pg_fetch_all($result);
		$this->affectedRows = pg_affected_rows($result);
		$this->status = static::$StatementStatusExecuted;
		
		pg_free_result($result);
		
		if ($fetchedArray !== false) {
			return $fetchedArray;
		}
		return [];
	}

	public function executeReturningFirstRow($parameters = []) {
		$result = $this->execute($parameters);
		
		if (count($result) > 0) {
			return $result[0];
		}
		return [];
	}

	public function executeGroupingOnFirstColumn($parameters = [], $joinCharacter = " - ") {
		$result = $this->execute($parameters);
		$assocResult = [];
		if (count($result) > 0) {
			$keys = array_keys($result[0]);
			$numKeys = count($keys);
			foreach ($result as $value) {
				if ($numKeys > 1) {
					$addString = "";
					for ($i = 1; $i < $numKeys; $i++) {
						$addString = $addString . $value[$keys[$i]] . $joinCharacter;
					}
					$assocResult[$value[$keys[0]]] = trim($addString, $joinCharacter);
				} else {
					$assocResult[$value[$keys[0]]] = $value[$keys[1]];
				}
			}
		}
		return $assocResult;
	}

	public function executeReturningFirstColumn($parameters = array()) {
		$result = $this->execute($parameters);
		$std_array = [];
		if (count($result) > 0) {
			$keys = array_keys($result[0]);
			foreach ($result as $value) {
				$std_array[] = $value[$keys[0]];
			}
		}
		return $std_array;
	}
	
}