<?php

namespace InerziaSoft\Core\Db\DbLayers\PostgreSql;

use InerziaSoft\Core\Db\DbLayers\DatabaseLayer;
use InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseConfigurationException;
use InerziaSoft\Core\Utils\UriUtils;

class PostgresSqlDbLayer implements DatabaseLayer {
	
	const ConfigFilename = "db.ini";
	
	protected $connection;

	/**
	 * DbLayer constructor.
	 * Instantiates and connect a new DbLayer.
	 *
	 * @discussion If the connection cannot be established, the is_connected() method will return false.
	 * @param $filename string|null
	 * @throws DatabaseConfigurationException
	 */
	public function __construct($filename = null) {
		if (!file_exists($filename)) $filename = self::ConfigFilename;
		$config = UriUtils::getConfig($filename);

		if ($config !== false) {
			$result = pg_connect("host=".$config["ServerHost"]." port=".$config["ServerPort"]." dbname=".$config["DatabaseName"]." user=".$config["DatabaseUser"]." password=".$config["DatabasePassword"]);

			if ($result !== false) {
				$this->connection = $result;
			}
			else {
				$this->connection = null;
			}
		}
		else {
			throw new DatabaseConfigurationException($filename);
		}
	}
	
	/**
	 * Gets the connection status
	 *
	 * @return bool: true if the DbLayer is connected.
	 */
	public function isConnected() {
		return isset($this->connection);
	}
	
	/**
	 * Gets the hostname of the machine which is running this database.
	 *
	 * @return string: The hostname.
	 */
	public function getHostname() {
		return pg_host($this->connection);
	}
	
	/**
	 * Gets the host port on which the server is listening.
	 *
	 * @return int: The port number on which the server is listening.
	 */
	public function getHostPort() {
		return pg_port($this->connection);
	}
	
	/**
	 * Gets an array with useful information on the server.
	 *
	 * @return array: The same array returned by pg_version().
	 * @see pg_version().
	 */
	public function getServerVersion() {
		return pg_version($this->connection);
	}
	
	/**
	 * Returns a new PreparedStatement with the specified query.
	 *
	 * @param $query string: A valid query.
	 * @param $name string: The name to associate to this PreparedStatement (optional).
	 * @return PostgreSqlPreparedStatement: A PreparedStatement ready to be executed.
	 */
	public function prepare($query, $name = "") {
		return new PostgreSqlPreparedStatement($this->connection, $query, $name);
	}
	
	/**
	 * Performs a simple query without parameters.
	 *
	 * @param $query string: The query to be performed.
	 * @return resource: The result of a call to pg_query().
	 */
	public function query($query) {
		return pg_query($this->connection, $query);
	}
	
	/**
	 * Creates a schema with the specifed name
	 *
	 * @param $schema: The schema name.
	 * @return mixed: The pg_query performed.
	 */
	public function createSchema($schema) {
		return pg_query($this->connection, "CREATE SCHEMA ".$schema);
	}
	
	/**
	 * Perform a drop of the specified schema.
	 *
	 * @param $schema string: The name of the schema.
	 * @return mixed: The pg_query performed.
	 */
	public function dropSchema($schema) {
		return pg_query($this->connection, "DROP SCHEMA ".$schema." CASCADE");
	}
	
	/**
	 * Perform a query or a set of queries from a file.
	 *
	 * @param $filename string: The file path.
	 * @param $printOutput bool: toggle output.
	 * @return bool: true if the operation has been completed successfully and everything has been committed to the database.
	 */
	public function queryFromFile($filename, $printOutput = false) {
		$this->beginTransaction();
		
		$templine = '';
		$lines = file($filename);
		
		foreach ($lines as $line) {
			if (substr($line, 0, 2) == '--' || $line == '')
				continue;
			
			$templine .= $line;
			if (substr(trim($line), -1, 1) == ';')
			{
				$result = $this->query($templine);
				if($result === false) {
					if ($printOutput) {
						echo "<span class='installError'>Errore nella query: ".$templine."! ".pg_last_error($this->connection)."</span>";
					}
					
					$this->rollbackTransaction();
					
					return false;
				}
				else {
					if ($printOutput) {
						echo "<span class='installSuccess'>Query eseguita: ".$templine."</span><br/>";
					}
				}
				
				$templine = '';
			}
		}
		
		$this->commitTransaction();
		return true;
	}
	
	/**
	 * Begin a transaction on the current connection.
	 */
	public function beginTransaction() {
		$this->query("BEGIN");
	}
	
	/**
	 * Closes and commits any change made on the current connection.
	 */
	public function commitTransaction() {
		$this->query("COMMIT");
	}
	
	/**
	 * Cancels any change made on the current connection.
	 */
	public function rollbackTransaction() {
		$this->query("ROLLBACK");
	}
	
	/**
	 * Returns the Primary Key name of a specified table.
	 *
	 * @param $table string: The table name.
	 * @discussion Warning! This method must be used only under controlled circumstances.
	 * @return string: The Primary Key column name.
	 */
	public function getPrimaryKey($table) {
		/** @noinspection SqlDialectInspection */
		/** @noinspection SqlNoDataSourceInspection */
		/** @noinspection SqlResolve */
		$query = $this->query("SELECT a.attname FROM pg_index i JOIN pg_attribute a ON a.attrelid = i.indrelid AND a.attnum = ANY(i.indkey) WHERE i.indrelid = '$table'::regclass AND i.indisprimary");
		
		$result = pg_fetch_all($query);
		
		return $result[0]["attname"];
	}
	
	/**
	 * Returns the last reported error from the database on this connection.
	 *
	 * @return string
	 */
	public function getLastError() {
		return pg_last_error($this->connection);
	}
	
	public function getParametersPlaceholder() {
		return "$";
	}
}