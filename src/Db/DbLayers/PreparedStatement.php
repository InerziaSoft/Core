<?php
/*
MIT License

Copyright (c) 2016-2017 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Db\DbLayers;

interface PreparedStatement {
	/**
	 * Runs this PreparedStatement with the specified parameters.
	 *
	 * @param $parameters array: An array of parameters.
	 * @return array: An ordered indexed array with query results.
	 */
	public function execute($parameters = array());

	/**
	 * Runs this PreparedStatement with the specified parameters
	 * and returns the first row.
	 *
	 * @param $parameters array: An array of parameters.
	 * @return array|null: An indexed array with the first resulting row or empty.
	 */
	public function executeReturningFirstRow($parameters = array());

	/**
	 * Runs this PreparedStatement with the specified parameters,
	 * returning an associative array that uses the first column values
	 * as keys and joins the remaining columns as value with the specified
	 * character.
	 *
	 * Example:
	 *          query result        array("id"=>10, "name"=>"Mark", "surname"=>"Doe")
	 *          parsed result       array("10"=>"Mark - Doe")
	 *
	 * @param $parameters array: An array of parameters.
	 * @param $joinCharacter string: One or more characters to join multiple columns.
	 * @return array: An indexed array as described above or empty.
	 */
	public function executeGroupingOnFirstColumn($parameters = array(), $joinCharacter = " - ");

	/**
	 * Runs this PreparedStatement with the specified parameters and returns
	 * an ordered array containing the first column values. Any other column is skipped.
	 *
	 * @param $parameters array: An array of parameters.
	 * @return array: An array with the sequence of values of the first column or empty.
	 */
	public function executeReturningFirstColumn($parameters = array());
}