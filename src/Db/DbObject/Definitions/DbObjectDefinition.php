<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
namespace InerziaSoft\Core\Db\DbObject\Definitions;

use InerziaSoft\Core\Db\DbObject\DbObject;
use InerziaSoft\Core\Db\DbObject\Exceptions;

class DbObjectDefinition {

	const DefaultIdentifierKeyName = "id";
	
	private static function valueOfAttributeOfClass($dbObject, $attribute, $required = true) {
		$class = new \ReflectionClass($dbObject);
		$phpDocs = $class->getDocComment();

		$value = null;

		$phpDocsByEndLine = explode("\n", $phpDocs);

		foreach ($phpDocsByEndLine as $phpDoc) {
			if (strpos($phpDoc, "@".$attribute) !== false) {
				$phpDocBySeparator = explode(" ", trim($phpDoc));
				
				$value = "";
				foreach ($phpDocBySeparator as $item) {
					if (strpos($item, "*") !== false || strpos($item, "@") !== false) continue;
					$value .= $item;
				}

				if ($value != null) {
					break;
				}
			}
		}

		if ($dbObject instanceof DbObject) {
		    $dbObject = get_class($dbObject);
        }
        
		if ($required && $value == null) throw new Exceptions\WrongDbObjectConfiguration($dbObject, $attribute);

		return $value;
	}
	
	private static function identifiersAsArray($identifiers) {
		if (strlen($identifiers) == 0) return [];
		
		$identifiersAsArray = explode(",", $identifiers);
		$realIdentifiers = [];
		foreach ($identifiersAsArray as $identifier) {
			array_push($realIdentifiers, trim($identifier));
		}
		
		return $identifiersAsArray;
	}
	
	/**
	 * Returns an array containing the identifiers of a specified DbObject.
	 *
	 * @param $dbObject|string DbObject
	 * @return array
	 */
    public static function getPrimaryKeysName($dbObject) {
	    $identifiers = self::valueOfAttributeOfClass($dbObject, "identifier", false);
	    
	    if (!isset($identifiers)) {
	    	$identifiers = DbObjectDefinition::DefaultIdentifierKeyName;
	    }
	    
	    return self::identifiersAsArray($identifiers);
    }
	
	/**
	 * Returns an array containing the columns names that should be used as
	 * default sorting operators.
	 *
	 * @param $dbObject
	 * @return array
	 */
    public static function getDefaultSortNames($dbObject) {
	    $identifiers = self::valueOfAttributeOfClass($dbObject, "sort", false);
	
	    return self::identifiersAsArray($identifiers);
    }
	
	/**
	 * Returns the name of the table associated with a specific DbObject.
	 *
	 * @param $dbObject
	 * @return null
	 */
    public static function getTableName($dbObject) {
	    $tableName = self::valueOfAttributeOfClass($dbObject, "table", false);
	    
	    if (!isset($tableName)) {
		    $class = new \ReflectionClass($dbObject);
		    $tableName = $class->getShortName();
	    }
	    
	    return lcfirst($tableName);
    }

}