<?php

namespace InerziaSoft\Core\Db\DbObject;

use DI\DependencyException;
use DI\NotFoundException;
use InerziaSoft\Core\Api\Authentication\SessionToken;
use InerziaSoft\Core\Api\Outcomes\Response;
use InerziaSoft\Core\Attributes\AttributesParser;
use InerziaSoft\Core\Db\DbObject\Attributes\DbObjectAttribute;
use InerziaSoft\Core\Db\DbObject\Attributes\DbObjectAttributesFactory;
use InerziaSoft\Core\Db\DbObject\Conditions\Condition;
use InerziaSoft\Core\Db\DbObject\Conditions\ConditionsList;
use InerziaSoft\Core\Db\DbObject\Definitions\DbObjectDefinition;
use InerziaSoft\Core\Db\DbObject\Evaluators\StringQueryEvaluator;
use InerziaSoft\Core\Db\DbObject\Exceptions\FillManualPropertiesException;
use InerziaSoft\Core\Db\DbObject\Exceptions\ReadOnlyException;
use InerziaSoft\Core\Db\DbObject\Exceptions\UnexistingDbObjectClassException;
use InerziaSoft\Core\Db\DbObject\Exceptions\UnexistingDbObjectException;
use InerziaSoft\Core\Db\DbObject\Exceptions\UnexistingKeyException;
use InerziaSoft\Core\Db\DbObject\Exceptions\UnknownIdKeyException;
use InerziaSoft\Core\Db\DbObject\Exceptions\WrongDbObjectConfiguration;
use InerziaSoft\Core\Db\DbObject\Interfaces\Queryable;
use InerziaSoft\Core\Db\DbObject\Interfaces\QueryPerformer;
use InerziaSoft\Core\Db\DbObject\Operators\ComparisonOperator;
use InerziaSoft\Core\Db\DbObject\Operators\SortOperatorsList;
use InerziaSoft\Core\Db\DbObject\Reflection\Reflectable;
use InerziaSoft\Core\Db\DbObject\Reflection\ReflectionDbObjectProperty;
use InerziaSoft\Core\Db\DbObject\Literals\ValueInterpreter;
use InerziaSoft\Core\Definitions\ClassNameInitializable;
use InerziaSoft\Core\Utils\ArrayUtils;
use InerziaSoft\Core\Utils\DIUtils;

define('ALL_FIELDS', "'allFields'");
define('ALL_TABLES', "'allTables'");

/**
 * Class DbObject
 *
 * This class represents an object stored in an attached database.
 * Provides methods for reading, writing, inserting, updating and deleting the represented object.
 *
 * @discussion You must subclass DbObject and map your database tables to specific classes.
 *
 * @package InerziaSoft\Db
 */
class DbObject extends Response implements Queryable, ClassNameInitializable {
	
	use Reflectable;

	/**
	 * @var array The array of identifiers of this table.
	 */
	private $idKeys;
	
	/**
	 * @var string The name of the associated table.
	 */
	private $tableName;
	
	/**
	 * @var string The name that this table has used in the query.
	 */
	private $tableNameInQuery;
	
	/**
	 * @var bool Determines if this DbObject can reflect any changes to the underlying database.
	 */
    private $readOnly;

	/**
	 * Database Bindings
	 */
	const ConfigFilename = "db.ini";
	
	/**
	 * DbObject constructor.
	 *
	 * Instantiates a new DbObject with the primary key name, the table name and associated values.
	 *
	 * @discussion Your subclasses should always call this method to instantiate a new DbObject.
	 * There's no need to worry about empty response from the storage provider (for example, when the requested object
	 * does not exists).
	 *
	 * @param $values array: An indexed array of key/values with data coming from the associated database.
	 * @param $env TablesEnvironment
	 * @throws UnexistingDbObjectException If $values is empty.
	 * @throws UnexistingKeyException
	 * @throws UnknownIdKeyException
	 * @throws FillManualPropertiesException
	 * @throws \ReflectionException
	 */
	public function __construct($values, $env = null) {
		if (!isset($env)) $env = new TablesEnvironment();

	    $this->idKeys = DbObjectDefinition::getPrimaryKeysName($this);
        $this->tableName = DbObjectDefinition::getTableName($this);
        $this->tableNameInQuery = $env->getName($this->tableName);
        $this->readOnly = false;
        
		if (isset($values)) {
			if (array_key_exists("__empty__", $values)) return;
			
			/** @var ReflectionDbObjectProperty $property */
			foreach (static::getReflectionClass()->getProperties(true) as $property) {
				$key = strtolower($property->getName());
                $namespace = $this->tableNameInQuery;

                $value = ArrayUtils::namespaceOrStandaloneKeyValue($values, $namespace, $key);
				$property->setValue($this, $value, $values, $env);
			}
		}
		
		if ($this->getId() == null) {
			throw new UnexistingDbObjectException($this, $values);
		}
		
		try {
			$this->fillManualProperties();
		}
		catch (\Exception $exception) {
			throw new FillManualPropertiesException($this, $exception);
		}
	}
	
	/**
	 * Returns an empty DbObject of the specified class.
	 *
	 * @param $className string: the name of a DbObject subclass.
	 * @return static
	 * @throws FillManualPropertiesException
	 * @throws UnexistingDbObjectClassException If the specified class cannot be found.
	 * @throws WrongDbObjectConfiguration
	 * @throws \ReflectionException
	 */
    static function instanceFromClassName($className) {
    	if (!defined("API_CLASSES_NAMESPACE")) {
    		throw new WrongDbObjectConfiguration($className, "API_CLASSES_NAMESPACE");
	    }

        $classNameWithNamespace = API_CLASSES_NAMESPACE.$className;

        if (class_exists($classNameWithNamespace)) {
	        /** @var DbObject $classNameWithNamespace */
	        return $classNameWithNamespace::make();
        }
        else {
            throw new UnexistingDbObjectClassException($className);
        }
    }
	
	/**
	 * DbObject convenience constructor.
	 *
	 * Instantiates a new DbObject with empty values.
	 *
	 * @discussion When creating a new object, this method should be used to create an empty DbObject
	 * without raising exceptions.
	 *
	 * @return static An empty DbObject.
	 * @throws FillManualPropertiesException
	 * @throws \ReflectionException
	 */
	public static function make() {
		$values = ["__empty__" => 0];
		
		try {
			return new static($values);
		} catch (UnexistingDbObjectException $e) {
		} catch (UnexistingKeyException $e) {
		} catch (UnknownIdKeyException $e) {
		}
		
		return null;
	}
	
	/**
	 * Returns a DbObject from a custom SQL query.
	 *
	 * @param $query string
	 * @param array $parameters
	 * @return static
	 * @throws DependencyException
	 * @throws FillManualPropertiesException
	 * @throws NotFoundException
	 * @throws UnexistingDbObjectException
	 * @throws UnexistingKeyException
	 * @throws UnknownIdKeyException
	 * @throws \ReflectionException
	 */
	public static function query($query, $parameters = []) {
		$stringQueryEvaluator = new StringQueryEvaluator();
		return new static(DIUtils::getContainer()->get(QueryPerformer::class)->performReturningFirstRow($stringQueryEvaluator->evaluate(static::class, $query), $parameters));
	}
	
	/**
	 * Returns an array of DbObjects from a custom SQL query.
	 *
	 * @param $query string
	 * @param array $parameters
	 * @return array
	 * @throws DependencyException
	 * @throws FillManualPropertiesException
	 * @throws NotFoundException
	 * @throws UnexistingDbObjectException
	 * @throws UnexistingKeyException
	 * @throws UnknownIdKeyException
	 * @throws \ReflectionException
	 */
	public static function queryAll($query, $parameters = []) {
		$stringQueryEvaluator = new StringQueryEvaluator();
		return self::instancesFromArray(DIUtils::getContainer()->get(QueryPerformer::class)->perform($stringQueryEvaluator->evaluate(static::class, $query), $parameters));
	}
	
	/**
	 * Initializes a set of DbObjects from an array of values.
	 *
	 * @param $values array: An array of indexed arrays.
	 * @return array
	 * @throws FillManualPropertiesException
	 * @throws UnexistingDbObjectException
	 * @throws UnexistingKeyException
	 * @throws UnknownIdKeyException
	 * @throws \ReflectionException
	 */
    protected static function instancesFromArray($values) {
	    $dbObjects = [];

	    if (isset($values)) {
		    foreach ($values as $value) {
			    $dbObject = new static($value);
			    array_push($dbObjects, $dbObject);
		    }
	    }

        return $dbObjects;
    }
	
	/**
	 * Gives a chance to subclasses to fill manual properties
	 * before returning the DbObject.
	 *
	 * @discussion This method is called by Queryable methods and
	 * should be used to fill properties marked as manual that could
	 * not have been filled by the default query.
	 *
	 * @discussion The default implementation does nothing.
	 */
	protected function fillManualProperties() { }
	
	/**
	 * DbObject JSON initializer.
	 *
	 * Converts the passed associative array to its flat
	 * representation and initializes a read-only DbObject containing
	 * the specified values.
	 *
	 * @param array $json
	 * @param $responseClassKey
	 * @param $responseClassName
	 * @return DbObject
	 * @throws FillManualPropertiesException
	 * @throws UnexistingDbObjectException
	 * @throws UnexistingKeyException
	 * @throws UnknownIdKeyException
	 * @throws \ReflectionException
	 */
	public static function fromJson($json, $responseClassKey, $responseClassName) {
		return static::readOnly(ArrayUtils::flatDbObjectJson($json));
	}
	
	/**
	 * DbObject read-only initializer.
	 *
	 * Initializes a new DbObject with the specified values
	 * and converts it to read-only mode.
	 *
	 * Attempting to apply operations such as insert(), update()
	 * or delete() on a read-only DbObject will throw an exception.
	 *
	 * @discussion $values keys should follow the DbObject ReadOnly naming convention,
	 * as the following:
	 *  "<tablename>.<key>"
	 * Foreign keys in this table should append their respective keys to the end of the
	 * above formula, such as:
	 *  "<tablename>.<key>.<foreign>"
	 *
	 * @param $values array
	 * @return static
	 * @throws FillManualPropertiesException
	 * @throws UnexistingDbObjectException
	 * @throws UnexistingKeyException
	 * @throws UnknownIdKeyException
	 * @throws \ReflectionException
	 */
	public static function readOnly($values) {
	    $dbObject = static::make();
        $dbObject->readOnly = true;

        $values = ArrayUtils::normalizeArrayForReadOnly($values);

        /** @var ReflectionDbObjectProperty $property */
        foreach (static::getReflectionClass()->getProperties(true) as $property) {
            $finalValue = ArrayUtils::namespaceOrStandaloneKeyValue($values, "", strtolower($property->getName()));
            
            $property->setValue($dbObject, $finalValue, $values, $environment, true);
        }

        return $dbObject;
    }
	
	/**
	 * DbObject convenience initializer.
	 *
	 * Instantiates a new DbObject by looking into the specified table for $id.
	 *
	 * @param $id int: The id to look for.
	 *
	 * @return static
	 * @throws DependencyException
	 * @throws FillManualPropertiesException
	 * @throws NotFoundException
	 * @throws UnexistingDbObjectException
	 * @throws UnexistingKeyException
	 * @throws UnknownIdKeyException
	 * @throws \ReflectionException
	 */
	public final static function objectWithId($id) {
		return static::objectWithIdOnUniqueKey($id, DbObjectDefinition::getPrimaryKeysName(get_called_class())[0]);
	}
	
	/**
	 * DbObject convenience initializer.
	 *
	 * Instantiates a new DbObject by looking into the specified table for $idKey = $id.
	 *
	 * @param $id int: The id to look for.
	 * @param $idKey string: The column to consider.
	 *
	 * @return static
	 * @throws DependencyException
	 * @throws FillManualPropertiesException
	 * @throws NotFoundException
	 * @throws UnexistingDbObjectException
	 * @throws UnexistingKeyException
	 * @throws UnknownIdKeyException
	 * @throws \ReflectionException
	 */
	public final static function objectWithIdOnUniqueKey($id, $idKey) {
		return static::objectWithIdentifierOnUniqueKey($id, $idKey, ComparisonOperator::equal());
	}
	
	/**
	 * DbObject convenience initializer.
	 *
	 * Instantiates a new DbObject by looking into the specified table for the condition
	 * specified between $identifierKey and $identifier.
	 *
	 * @param $identifier mixed: An object to use as identifier.
	 * @param $identifierKey string: The column to consider.
	 * @param $comparison ComparisonOperator: The comparison condition to use when considering the values in $identifierKey.
	 *
	 * @return static
	 * @throws DependencyException
	 * @throws FillManualPropertiesException
	 * @throws NotFoundException
	 * @throws UnexistingDbObjectException
	 * @throws UnexistingKeyException
	 * @throws UnknownIdKeyException
	 * @throws \ReflectionException
	 */
	public static function objectWithIdentifierOnUniqueKey($identifier, $identifierKey, $comparison = null) {
		if (!isset($comparison)) $comparison = ComparisonOperator::equal();

		return static::objectWithIds([$identifierKey => $identifier], $comparison);
	}
	
	/**
	 * Instantiates a new DbObject by looking into the specified table for
	 * for a row that satisfies all the association between keys and values
	 * specified in $keysAndValues.
	 *
	 * For example, if your object has two primary keys named keyOne and keyTwo,
	 * you would pass an array such as:
	 * ["keyOne" => 1, "keyTwo" => 2]
	 *
	 * @param $keysAndValues
	 * @param $comparison ComparisonOperator
	 * @return static
	 * @throws DependencyException
	 * @throws FillManualPropertiesException
	 * @throws NotFoundException
	 * @throws UnexistingDbObjectException
	 * @throws UnexistingKeyException
	 * @throws UnknownIdKeyException
	 * @throws \ReflectionException
	 */
	public static function objectWithIds($keysAndValues, $comparison = null) {
		if (!isset($comparison)) $comparison = ComparisonOperator::equal();
		
		$result = DIUtils::getContainer()->get(QueryPerformer::class)->getObjectWithIds(get_called_class(), $keysAndValues, $comparison);
		
		$dbObject = new static($result);
		
		return $dbObject;
	}
	
	/**
	 * DbObject(s) convenience initializer.
	 *
	 * Returns an array of DbObject(s) by gathering all the items from the specified table.
	 *
	 * @param $sort SortOperatorsList
	 * @param null|int $limit
	 * @return array : An array of DbObjects (or one of its subclasses).
	 * @throws DependencyException
	 * @throws FillManualPropertiesException
	 * @throws NotFoundException
	 * @throws UnexistingDbObjectException
	 * @throws UnexistingKeyException
	 * @throws UnknownIdKeyException
	 * @throws \ReflectionException
	 */
	public static function all($sort = null, $limit = null) {
        return self::instancesFromArray(
        	DIUtils::getContainer()->get(QueryPerformer::class)->getAll(
        		get_called_class(),
		        static::autoConvertList($sort, SortOperatorsList::class),
		        $limit
            )
        );
	}
	
	/**
	 * Automatically converts an array of objects to the
	 * containing class $listClass.
	 *
	 * @param $list ConditionsList|SortOperatorsList|array
	 * @param $listClass string
	 *
	 * @return ConditionsList|SortOperatorsList
	 */
    private static function autoConvertList($list, $listClass) {
    	if ($list === null) return null;

	    if (is_array($list)) {
		    $list = new $listClass($list);
	    }

	    return $list;
    }
	
	/**
	 * Returns the count of items for a query.
	 *
	 * @param $conditions ConditionsList|array
	 * @return int
	 * @throws \DI\DependencyException
	 * @throws \DI\NotFoundException
	 */
	public static function count($conditions) {
		return DIUtils::getContainer()->get(QueryPerformer::class)->getCount(get_called_class(),
			static::autoConvertList(
				$conditions,
				ConditionsList::class
			)
		);
	}
	
	/**
	 * DbObject batch delete.
	 *
	 * Deletes a set of objects that satisfy the specified conditions.
	 *
	 * @param $conditions ConditionsList|array
	 * @return bool
	 * @throws \DI\DependencyException
	 * @throws \DI\NotFoundException
	 */
	public static function deleteAll($conditions) {
		return DIUtils::getContainer()->get(QueryPerformer::class)->deleteAll(get_called_class(),
			static::autoConvertList(
				$conditions,
				ConditionsList::class
			)
		);
	}
	
	/**
	 * Returns an indexed array using IDs column names as keys
	 * associated to their values.
	 *
	 * @return array
	 * @throws UnexistingKeyException
	 * @throws UnknownIdKeyException
	 * @throws \ReflectionException
	 */
    function getIdsWithKeys() {
        $idKeys = [];
        foreach ($this->idKeys as $idKey) {
            $property = @static::getReflectionClass()->getProperty($idKey);

            $value = $property->getValue($this);
            if ($value instanceof DbObject) {
            	$value = $value->getId();
            }
            
            $idKeys[$idKey] = $value;
        }

        return $idKeys;
    }
	
	/**
	 * Returns the value of a specified key.
	 *
	 * @param $key string: The key name (case insensitive).
	 * @return mixed: The value of the specified key.
	 * @throws UnexistingKeyException If the specified key is not mapped to any property in the DbObject.
	 * @throws \ReflectionException
	 */
	public function get($key) {
		return static::getReflectionClass()->getProperty($key)->getValue($this);
	}
	
	/**
	 * DbObject(s) convenience initializer.
	 *
	 * Returns an array of DbObject(s) which satisfy the specified conditions and are contained in the specified table.
	 *
	 * @param $conditions ConditionsList|array
	 * @param $sort SortOperatorsList
	 * @return array : An array of DbObjects (or one of its subclasses).
	 * @throws DependencyException
	 * @throws FillManualPropertiesException
	 * @throws NotFoundException
	 * @throws UnexistingDbObjectException
	 * @throws UnexistingKeyException
	 * @throws UnknownIdKeyException
	 * @throws \ReflectionException
	 */
	public static function where($conditions, $sort = null) {
        return self::instancesFromArray(
        	DIUtils::getContainer()->get(QueryPerformer::class)->getWhere(get_called_class(),
		        static::autoConvertList($conditions, ConditionsList::class),
		        static::autoConvertList($sort, SortOperatorsList::class)
	        )
        );
    }
	
	/**
	 * Updates the database object into the associated storage
	 * using values from mapped properties.
	 *
	 * @throws DependencyException
	 * @throws NotFoundException
	 * @throws ReadOnlyException
	 * @throws UnexistingKeyException
	 * @throws UnknownIdKeyException
	 * @throws \ReflectionException
	 */
	public function update() {
		return $this->updateRelevantValues();
	}
	
	/**
	 * Updates the database object into the associated storage using
	 * values from the mapped properties listed in $relevantValues.
	 * Properties which are not listed in $relevantValues will not be updated,
	 * regardless of their values.
	 *
	 * @param $relevantValues array|null
	 * @return bool
	 * @throws ReadOnlyException
	 * @throws UnexistingKeyException
	 * @throws UnknownIdKeyException
	 * @throws \DI\DependencyException
	 * @throws \DI\NotFoundException
	 * @throws \ReflectionException
	 */
	public function updateRelevantValues($relevantValues = null) {
        if ($this->readOnly) throw new ReadOnlyException(get_called_class());

//        if (isset($relevantValues)) {
//            $relevantValuesToLower = [];
//            foreach ($relevantValues as $relevantValue) {
//                array_push($relevantValuesToLower, strtolower($relevantValue));
//            }
//            $relevantValues = $relevantValuesToLower;
//        }
		
		if (isset($relevantValues)) {
			foreach ($relevantValues as $relevantValue) {
				static::getReflectionClass()->getProperty($relevantValue);
			}
		}

		$keysAndValues = [];
		/** @var ReflectionDbObjectProperty $property */
		foreach (static::getReflectionClass()->getProperties() as $property) {
	        $propertyName = $property->getName();

	        if ($propertyName == "status") continue;

	        $parser = new AttributesParser($property, new DbObjectAttributesFactory());
	        /** @var DbObjectAttribute $attribute */
	        $shouldBeSkipped = false;
	        foreach ($parser->getAttributes() as $attribute) {
	        	if ($attribute->isVirtual()) {
	        		$shouldBeSkipped = true;
	        		break;
		        }
	        }
	        if ($shouldBeSkipped) continue;

	        if ($property->isPrimaryKeyOnDbObject($this)) {
		        $idValues[$propertyName] = $property->getValue($this);
		        continue;
	        }

            if (isset($relevantValues) && !in_array($propertyName, $relevantValues)) continue;

	        $keysAndValues[$propertyName] = DIUtils::getContainer()->get(ValueInterpreter::class)->interpretValue($property->getValue($this), $property);
        }

        return DIUtils::getContainer()->get(QueryPerformer::class)->update(get_called_class(), $keysAndValues, $this->getIdsWithKeys());
    }
	
	/**
	 * Returns an array containing the primary
	 * keys of this table.
	 *
	 * @return array
	 */
    function getIdKeys() {
	    return $this->idKeys;
    }
	
	/**
	 * Returns the values of all the primary keys of this table.
	 *
	 * @discussion The order is the same as the @identifier declaration.
	 * @return array
	 * @throws UnexistingKeyException
	 * @throws UnknownIdKeyException
	 * @throws \ReflectionException
	 */
    function getIds() {
	    $idKeys = [];
	    foreach ($this->idKeys as $idKey) {
		    $property = @static::getReflectionClass()->getProperty($idKey);

		    if ($property === false) {
			    throw new UnknownIdKeyException($this, $idKey);
		    }

		    array_push($idKeys, $property->getValue($this));
	    }

	    return $idKeys;
    }
	
	/**
	 * Inserts the database object into the associated storage using values from mapped properties.
	 *
	 * @return bool
	 * @throws DependencyException
	 * @throws NotFoundException
	 * @throws ReadOnlyException
	 * @throws UnexistingDbObjectException
	 * @throws UnexistingKeyException
	 * @throws UnknownIdKeyException
	 * @throws \ReflectionException
	 */
	public function insert() {
        if ($this->readOnly) throw new ReadOnlyException(get_called_class());

		$keys = [];
		$values = [];

		$properties = static::getReflectionClass()->getProperties();
		/** @var ReflectionDbObjectProperty $property */
		foreach ($properties as $property) {
		    if (strtolower($property->getName()) == "status") continue;

			if ($this->keyHasDefaultValue($property)) continue;

			$key = strtolower($property->getName());
			
			$rawValue = $property->getValue($this);
			if (!isset($rawValue)) continue;
			
			$value = DIUtils::getContainer()->get(ValueInterpreter::class)->interpretValue($rawValue, $property);

			array_push($keys, $key);
			array_push($values, $value);
		}
		
		$result = DIUtils::getContainer()->get(QueryPerformer::class)->insert(get_called_class(), $keys, $values, $this->getIdKeys());
		
		if ($result !== false) {
			/** @var ReflectionDbObjectProperty $property */
			foreach ($properties as $property) {
				if ($property->isPrimaryKeyOnDbObject($this)) {
					$property->setValue($this, $result[0][strtolower($property->getName())]);
				}
			}
		}

		return $result;
	}
	
	/**
	 * Returns whether the specified key should be included in INSERT operations.
	 *
	 * @discussion Subclasses may override this method to prevent certain properties from being used in register queries or change the value of their properties
	 * just before they are used in a database operation.
	 * The default implementation skips the primary key if there's only one; otherwise, none is skipped.
	 *
	 * @discussion Properties whose value is NULL will always be excluded from INSERT operations.
	 * The condition returned by this method determines if the key should be excluded even if its value
	 * is not NULL.
	 *
	 * @param $property \ReflectionProperty: A property.
	 * @return bool true if the column has a default value and should be excluded from queries.
	 * @throws UnexistingKeyException
	 * @throws \ReflectionException
	 */
	protected function keyHasDefaultValue($property) {
		return
			(static::getReflectionClass()->getProperty($property->getName())->isPrimaryKeyOnDbObject($this))
			&&
			(count($this->idKeys) == 1);
	}
	
	/**
	 * Sets the value of a specified key.
	 *
	 * @param $key string: The key name (case insensitive).
	 * @param $value mixed: The value to assign to the specified key.
	 * @throws UnexistingDbObjectException
	 * @throws UnexistingKeyException If the specified key is not mapped to any property in the DbObject.
	 * @throws UnknownIdKeyException
	 * @throws \ReflectionException
	 */
	public function set($key, $value) {
		static::getReflectionClass()->getProperty($key)->setValue($this, $value);
	}
	
	/**
	 * Returns whether or not this object comes
	 * from the underlying storage or not.
	 *
	 * @return bool
	 * @throws UnexistingKeyException
	 * @throws UnknownIdKeyException
	 * @throws \ReflectionException
	 */
	public function isNew() {
		$ids = $this->getIds();

		foreach ($ids as $id) {
			if (isset($id)) {
				return false;
			}
		}

		return true;
	}
	
	/**
	 * Deletes the object from the associated storage.
	 *
	 * @return bool
	 * @throws ReadOnlyException
	 * @throws UnexistingKeyException
	 * @throws UnknownIdKeyException
	 * @throws \DI\DependencyException
	 * @throws \DI\NotFoundException
	 * @throws \ReflectionException
	 */
	public function delete() {
        if ($this->readOnly) throw new ReadOnlyException(get_called_class());

		return DIUtils::getContainer()->get(QueryPerformer::class)->delete(get_called_class(), $this->getIdsWithKeys());
	}
	
	/**
	 * @param $dbObject DbObject
	 * @throws UnexistingKeyException
	 * @throws \ReflectionException
	 */
	public function copy($dbObject) {
		if ($dbObject instanceof static) {
			/** @var \ReflectionProperty $property */
			foreach ($dbObject->getReflectionClass()->getProperties(true) as $property) {
				if ($this->keyHasDefaultValue($property)) continue;

				$property->setValue($this, $property->getValue($dbObject));
			}
		}
		else {
			throw new \InvalidArgumentException("copy() method only works on DbObjects of the same class.");
		}
	}
	
	/**
	 * Takes an object of the same class as this one
	 * and looks if already exists in the associated table:
	 * if it does, calls the copy() method of the found object,
	 * otherwise inserts the specified one.
	 *
	 * @discussion This method supports passing an arbitrary ConditionsList
	 * object which will be used to match an existing object on the underlying database
	 * (if any). The collection of Conditions must use as values the name of the column that they
	 * want to match, such as:
	 *  new ConditionsList([
	 *      new Equal("column", "matchWithColumn")
	 *  ]);
	 * At runtime, "matchWithColumn" will be replaced with the value of $dbObject->get("matchWithColumn").
	 *
	 * Please, bear in mind that the DbObject's get() method will throw an exception if the specified key
	 * does not exist.
	 *
	 * @param $dbObject DbObject
	 * @param $staticConditionsList ConditionsList
	 * @return true if the object has been added as new, false if another object has been found and that instance has been updated.
	 * @throws DependencyException
	 * @throws FillManualPropertiesException
	 * @throws NotFoundException
	 * @throws ReadOnlyException
	 * @throws UnexistingDbObjectException
	 * @throws UnexistingKeyException
	 * @throws UnknownIdKeyException
	 * @throws \ReflectionException
	 */
	public static function importOrUpdate($dbObject, $staticConditionsList = null) {
		if ($dbObject instanceof static) {
			try {
				if (!isset($staticConditionsList)) {
					$storedDbObject = static::objectWithIds($dbObject->getIdsWithKeys());
				}
				else {
					$conditionsList = [];

					/** @var Condition $condition */
					foreach ($staticConditionsList->getConditions() as $condition) {
						$clonedCondition = clone($condition);
						$clonedCondition->setValue($dbObject->get($condition->getValue()));

						array_push($conditionsList, $clonedCondition);
					}

					$results = static::where(new ConditionsList($conditionsList));

					if (count($results) > 0) {
						$storedDbObject = $results[0];
					}
					else {
						throw new UnexistingDbObjectException();
					}
				}

				$storedDbObject->copy($dbObject);
				$storedDbObject->update();

				return false;
			}
			catch (UnexistingDbObjectException $exception) {
				$dbObject->insert();

				return true;
			}
		}
		else {
			throw new \InvalidArgumentException("importOrUpdate() method only works on DbObjects of the same class.");
		}
	}
	
	/**
	 * Returns the first identifier of this DbObject.
	 *
	 * @return string
	 * @throws UnexistingKeyException
	 * @throws UnknownIdKeyException
	 * @throws \ReflectionException
	 */
	public function __toString() {
		return "".$this->getId();
	}

	/**
	 * Called before performing a transaction which includes
	 * this DbObject.
	 *
	 * @discussion Subclasses can use this method to parse or
	 * override existing values.
	 *
	 * @discussion The default implementation is empty.
	 *
	 * @param $token SessionToken
	 */
	public function handleValuesForTransaction($token) { }
	
	/**
	 * Returns the value of the first primary key of this table.
	 *
	 * @return mixed
	 * @throws UnexistingKeyException
	 * @throws UnknownIdKeyException
	 * @throws \ReflectionException
	 */
    function getId() {
        return $this->getIds()[0];
    }

}