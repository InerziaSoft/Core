<?php
/**
 * Created by PhpStorm.
 * User: amois
 * Date: 29/03/2017
 * Time: 19:11
 */

namespace Db\DbObject\Exceptions;


class WrongDbObjectClassException extends \Exception {

	public function __construct($operation, $expectedClass, $actualClass) {
		parent::__construct("A '$operation' operation requires an instance of type '$expectedClass', but got '$actualClass' instead.");
	}

}