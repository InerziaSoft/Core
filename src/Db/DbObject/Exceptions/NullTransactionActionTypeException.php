<?php

namespace InerziaSoft\Core\Db\DbObject\Exceptions;

class NullTransactionActionTypeException extends \Exception {
	
	public function __construct($jsonString) {
		parent::__construct("JSON for transaction action does not specify action type. Please, check the following request: ".$jsonString);
	}
	
}