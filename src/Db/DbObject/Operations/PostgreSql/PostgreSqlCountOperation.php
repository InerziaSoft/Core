<?php

namespace InerziaSoft\Core\Db\DbObject\Operations\PostgreSql;

use InerziaSoft\Core\Db\DbObject\Operations\CountOperation;

class PostgreSqlCountOperation implements CountOperation {

    public function toSql($countTable, $countKey, $tableName, $primaryKey) {
        return "(SELECT COUNT(*) FROM $countTable WHERE $countKey = $tableName.$primaryKey)";
    }

}