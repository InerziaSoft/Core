<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Db\DbObject\Evaluators;

use InerziaSoft\Core\Attributes\AttributesParser;
use InerziaSoft\Core\Db\DbObject\Attributes\CastDbObjectAttribute;
use InerziaSoft\Core\Db\DbObject\Attributes\CountDbObjectAttribute;
use InerziaSoft\Core\Db\DbObject\Attributes\DbObjectAttribute;
use InerziaSoft\Core\Db\DbObject\Attributes\DbObjectAttributesFactory;
use InerziaSoft\Core\Db\DbObject\Attributes\ForeignKeyDbObjectAttribute;
use InerziaSoft\Core\Db\DbObject\Conditions\Condition;
use InerziaSoft\Core\Db\DbObject\Conditions\ConditionsList;
use InerziaSoft\Core\Db\DbObject\DbObject;
use InerziaSoft\Core\Db\DbObject\Definitions\DbObjectDefinition;
use InerziaSoft\Core\Db\DbObject\Definitions\ForeignKeyDefinition;
use InerziaSoft\Core\Db\DbObject\Interfaces\QueryEvaluator;
use InerziaSoft\Core\Db\DbObject\Interfaces\TypesSystem;
use InerziaSoft\Core\Db\DbObject\Operations\CastOperation;
use InerziaSoft\Core\Db\DbObject\Operations\CountOperation;
use InerziaSoft\Core\Db\DbObject\Operations\LeftJoinOperation;
use InerziaSoft\Core\Db\DbObject\Operations\RenameOperation;
use InerziaSoft\Core\Db\DbObject\Operators\SortOperator;
use InerziaSoft\Core\Db\DbObject\Operators\SortOperatorsList;
use InerziaSoft\Core\Db\DbObject\Reflection\ReflectionDbObjectProperty;
use InerziaSoft\Core\Db\DbObject\TablesEnvironment;
use InerziaSoft\Core\Utils\DIUtils;
use InerziaSoft\Core\Utils\StringUtils;

class ReflectionQueryEvaluator implements QueryEvaluator {
	
	protected $typesSystem;
	
	/**
	 * QueryEvaluator constructor.
	 */
	public function __construct() {
		$this->typesSystem = DIUtils::getContainer()->get(TypesSystem::class);
	}
	
	/**
	 * Returns a query in which placeholders defined at the top of this file are automatically replaced
	 * with the correct values.
	 *
	 * @param $className string: A DbObject class name.
	 * @param $query string: A query to evaluate.
	 * @param $conditionsList ConditionsList: An environment to get by reference.
	 * @param $sortOperatorsList SortOperatorsList|null|bool: An optional list of SortOperators. Pass "false" to disable automatic sort operators.
	 * @param $columnNamesBorder string|null
	 * @return QueryEvaluatorResult
	 * @throws \DI\DependencyException
	 * @throws \DI\NotFoundException
	 */
	public function evaluate($className, $query, $conditionsList = null, $sortOperatorsList = null, $columnNamesBorder = null) : QueryEvaluatorResult {
		$environment = new TablesEnvironment();
		
		$query = str_replace(ALL_FIELDS, $this->getFields($className, $columnNamesBorder), $query);
		$query = str_replace(ALL_TABLES, $this->getTables($className, $environment), $query);
		
		if (!isset($conditionsList)) $conditionsList = new ConditionsList([]);
		if (!isset($sortOperatorsList)) $sortOperatorsList = $this->getDefaultSortOperatorsList($className);
		
		return new QueryEvaluatorResult(
			$query,
			$this->getConditionsListInEnvironment($conditionsList, $environment),
			$this->getSortOperatorsListInEnvironment($className, $sortOperatorsList, $environment)
		);
	}
	
	public function getClassName($className) {
		if (!is_string($className)) {
			$completeClassName = get_class($className);
		}
		else {
			$completeClassName = $className;
		}
		
		return $completeClassName;
	}
	
	public function getTableName($className) {
		return DbObjectDefinition::getTableName($this->getClassName($className));
	}
	
	/**
	 * @param $conditionsList ConditionsList
	 * @param $environment TablesEnvironment
	 * @return ConditionsList
	 */
	protected function getConditionsListInEnvironment($conditionsList, $environment) {
		$newList = [];
		foreach ($conditionsList->getConditions() as $condition) {
			if ($condition instanceof Condition) {
				$condition->setTableName($environment->getName($this->getTableName($condition->getClassName())));
			}
			array_push($newList, $condition);
		}
		return new ConditionsList($newList);
	}
	
	/**
	 * @param $className string
	 * @param $sortOperatorsList SortOperatorsList
	 * @param $environment TablesEnvironment
	 * @return SortOperatorsList
	 */
	protected function getSortOperatorsListInEnvironment($className, $sortOperatorsList, $environment) {
		if (!$sortOperatorsList) return new SortOperatorsList([]);
		
		$newList = [];
		/** @var SortOperator $operator */
		foreach ($sortOperatorsList->getOperators() as $operator) {
			$operator->setTableName($environment->getName($this->getTableName($className)));
			array_push($newList, $operator);
		}
		return new SortOperatorsList($newList);
	}
	
	/**
	 * @param $className string
	 * @return string
	 */
	protected function getDefaultSortOperatorsList($className) {
		$sortArray = [];
		$sortIdentifiers = DbObjectDefinition::getDefaultSortNames($this->getClassName($className));
		foreach ($sortIdentifiers as $identifier) {
			array_push($sortArray, new SortOperator($identifier, true));
		}
		
		return new SortOperatorsList($sortArray);
	}
	
	/**
	 * Returns a comma separated list of properties to be used
	 * in a SQL query.
	 *
	 * @discussion This function cycles through all the keys
	 * that specify the @foreign attribute and appends their
	 * associated tables keys.
	 *
	 * @discussion This function handles casting too, by reading
	 * (if present) the type specified in the "@var" attribute.
	 * For a list of supported casts, please refer to the CastOperation class documentation.
	 *
	 * @param $className
	 * @param $columnNamesBorder
	 * @param $env TablesEnvironment
	 * @return string
	 * @throws \DI\DependencyException
	 * @throws \DI\NotFoundException
	 */
	protected function getFields($className, $columnNamesBorder, $env = null) {
		if (!isset($env)) $env = new TablesEnvironment();
		
		$propertiesNames = [];
		
		/** @var DbObject $completeClassName */
		$completeClassName = $this->getClassName($className);
		
		$originalTableName = $this->getTableName($className);
		$tableName = $env->getName($originalTableName);
		$tablesToAdd = [];
		
		/** @var ReflectionDbObjectProperty $property */
		foreach ($completeClassName::getReflectionClass()->getProperties() as $property) {
			if (strtolower($property->getName()) == "status") continue;
			
			$propertyName = strtolower($property->getName());
			
			if (isset($this->columnNamesBorder)) {
				$propertyName = $columnNamesBorder.$propertyName.$columnNamesBorder;
			}
			
			$fullPropertyName = $tableName.".".$propertyName;
			
			/** @var DbObjectAttribute $attribute */
			foreach ($property->getAttributesParser()->getAttributes() as $attribute) {
				if ($attribute instanceof CastDbObjectAttribute) {
					$fullPropertyName = DIUtils::getContainer()->get(CastOperation::class)->toSql($tableName, $propertyName, $this->typesSystem->getType($attribute->getType()));
				}
				else if ($attribute instanceof ForeignKeyDbObjectAttribute) {
					array_push($tablesToAdd, $attribute->getClassName());
				}
				else if ($attribute instanceof CountDbObjectAttribute) {
					$countTableName = DbObjectDefinition::getTableName($attribute->getClassName());
					$countKey = strtolower($attribute->getColumnName());
					$myPrimaryKey = DbObjectDefinition::getPrimaryKeysName($completeClassName)[0];
					
					$fullPropertyName = DIUtils::getContainer()->get(CountOperation::class)->toSql($countTableName, $countKey, $tableName, $myPrimaryKey);
				}
			}
			
			$fullPropertyName .= DIUtils::getContainer()->get(RenameOperation::class)->toSql($tableName.".".$propertyName);
			
			array_push($propertiesNames, $fullPropertyName);
		}
		
		$propertiesImploded = implode(", ", $propertiesNames);
		
		foreach ($tablesToAdd as $table) {
			if (class_exists($table)) {
				$foreignTableName = DbObjectDefinition::getTableName($table);
				$env->useName($foreignTableName);
				
				$queryEvaluator = new static();
				$allKeys = $queryEvaluator->getFields($table, $columnNamesBorder, $env);
				$propertiesImploded .= ", ".$allKeys;
			}
			else {
				throw new \InvalidArgumentException("Class '$completeClassName' declares a foreign key to class '$table', but that class is either not visible or cannot be found.");
			}
		}
		
		return $propertiesImploded;
	}
	
	/**
	 * Returns the FROM part of a SQL query containing
	 * all the tables required to build this DbObject.
	 *
	 * @discussion The default implementation returns
	 * the name of the associated table followed by all the
	 * names of the tables that are connected via @foreign attribute.
	 *
	 * @discussion The implementation takes into account the possibility to
	 * join the same table multiple times on different keys. This is supported
	 * by a DbObjectQueryEnvironment object which is passed through as a parameter
	 * and should be queried every time a table name has to be written in the query.
	 *
	 * @param $className string
	 * @param null $env
	 * @return string
	 * @throws \DI\DependencyException
	 * @throws \DI\NotFoundException
	 * @internal param TablesEnvironment $tablesEnvironment : An existing environment.
	 */
	protected function getTables($className, $env = null) {
		if (!isset($env)) $env = new TablesEnvironment();
		
		$tablesClassesNames = [];
		
		/** @var DbObject $completeClassName */
		$completeClassName = $this->getClassName($className);
		
		/** @var ReflectionDbObjectProperty $property */
		foreach ($completeClassName::getReflectionClass()->getProperties() as $property) {
			// TODO: a DbObject might actually define a column named "status". We should check the inheritance for this property.
			if (strtolower($property->getName()) == "status") continue;
			
			$propertyName = strtolower($property->getName());
			
			$parser = new AttributesParser($property, new DbObjectAttributesFactory());
			/** @var DbObjectAttribute $attribute */
			foreach ($parser->getAttributes() as $attribute) {
				if ($attribute instanceof ForeignKeyDbObjectAttribute) {
					array_push($tablesClassesNames, new ForeignKeyDefinition($propertyName, $attribute->getClassName(), $attribute->getForeignPropertyKey()));
				}
			}
		}
		
		$tableName = $this->getTableName($className);
		$sqlFroms = [$tableName];
		
		/** @var ForeignKeyDefinition $tableClassDefinition */
		foreach ($tablesClassesNames as $tableClassDefinition) {
			/** @var DbObject $className */
			$className = $tableClassDefinition->getClassName();
			$propertyName = $tableClassDefinition->getForeignKeyName();

			if (class_exists($className)) {
				$foreignTableName = DbObjectDefinition::getTableName($className);
				$foreignPropertyName = $tableClassDefinition->getForeignJoinKey();
				if (!isset($foreignPropertyName)) {
					$propertyName = DbObjectDefinition::getPrimaryKeysName($className)[0];
				}

				$foreignTableInEnv = $env->useName($foreignTableName);
				$thisTableInEnv = $env->getName($tableName);

				$foreignQueryEvaluator = new static();
				$allTablesOnForeign = StringUtils::replaceFirstOccurrence($foreignTableName, "", $foreignQueryEvaluator->getTables($className, $env));
				
				$joinOperation = DIUtils::getContainer()->get(LeftJoinOperation::class)->toSql($thisTableInEnv, $propertyName, $foreignTableName, $foreignPropertyName, $foreignTableInEnv);
				
				array_push($sqlFroms, $joinOperation.$allTablesOnForeign);
			}
			else {
				throw new \InvalidArgumentException("Class '$completeClassName' declares a foreign key represented by class '$tableClassDefinition' which cannot be found.");
			}
		}
		
		return implode(" ", $sqlFroms);
	}
	
}