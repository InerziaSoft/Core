<?php

namespace InerziaSoft\Core\Db\DbObject;

use InerziaSoft\Core\Db\DbObject\Interfaces\NamesEnvironment;

class TablesEnvironment implements NamesEnvironment {
	
	protected $environment;
	
	public function __construct() {
		$this->environment = [];
	}
	
	public final function getTablesCount() {
		return count($this->environment);
	}
	
	public function getUsagesCount($tableName) {
		$tableName = strtolower($tableName);
		
		if (array_key_exists($tableName, $this->environment)) {
			return $this->environment[$tableName];
		}
		return 0;
	}
	
	protected function markUsage($tableName) {
		$tableName = strtolower($tableName);
		
		$currentUsages = $this->getUsagesCount($tableName);
		$currentUsages++;
		
		$this->environment[$tableName] = $currentUsages;
	}
	
	public function useName($tableName) {
		$this->markUsage($tableName);
		return $this->getName($tableName);
	}
	
	public function getName($tableName) {
		$usages = $this->getUsagesCount($tableName);
		
		return $usages > 0 ? $tableName.$usages : $tableName;
	}
	
	public function __toString() {
		return "<Env>: ".print_r($this->environment, true);
	}
	
}

