<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Db\DbObject\Literals;

use InerziaSoft\Core\Db\DbObject\DbObject;
use InerziaSoft\Core\Db\DbObject\Reflection\ReflectionDbObjectProperty;

class PostgreSqlValueInterpreter implements ValueInterpreter {
	
	private static $Null = "NULL";
	
	/**
	 * Handles a property value to be sent to the database.
	 *
	 * @param $value
	 * @param $property ReflectionDbObjectProperty
	 * @return string
	 */
	function interpretValue($value, $property = null) {
		if (is_bool($value)) {
			if ($value) {
				return "true";
			}
			else {
				return "false";
			}
		}
		else if (!is_string($value) && is_numeric($value)) {
			if (isset($property) && $property->hasCastOfType("bool")) {
				if ($value == 0) {
					return $this->interpretValue(false, $property);
				}
				else {
					return $this->interpretValue(true, $property);
				}
			}
		}
		else if (!isset($value) || $value == "" || (is_string($value) && strtolower($value) == "null")) {
			$value = null;
		}
		else if ($value instanceof DbObject) {
			$value = $this->interpretValue($value[$value->getIdKeys()[0]], $property);
		}
		else if ($value instanceof \DateTime) {
			$value = "'".$value->format("Y-m-d H:i:s")."'";
		}
		else {
			/* PostgreSQL Escapes */
            $value = str_replace("'", "''", $value);
            /* End PostgreSQL Escapes */

//            if (!is_numeric($value)) {
//                $value = "'{$value}'";
//            }
		}
		
		return $value;
	}
	
}