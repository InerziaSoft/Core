<?php

namespace InerziaSoft\Core\Db\DbObject\Transactions;

class ActionType {

    static $Insert = "insert";
    static $Update = "update";
    static $Delete = "delete";

}