<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
namespace InerziaSoft\Core\Db\DbObject\Transactions;

use InerziaSoft\Core\Api\Api;
use InerziaSoft\Core\Api\Authentication\SessionToken;
use InerziaSoft\Core\Db\DbObject\Exceptions\RollbackDbObjectException;
use InerziaSoft\Core\Db\DbObject\Interfaces\Performable;

class Transaction implements Performable {
	
	/**
	 * @var array
	 */
	protected $actions;
	
	/**
	 * @var SessionToken
	 */
	protected $token;
	
	private $masterKeyId;
	private $lastKeyId;
	
	public function __construct($actions, $token) {
		$this->actions = $actions;
		$this->token = $token;
	}
	
	public static function fromJson($json, $token) {
		$json = json_decode($json, true);
		
		if (is_array($json)) {
			$convertedJson = [];
			foreach ($json as $item) {
				array_push($convertedJson, Action::fromJson(json_encode($item), $token));
			}
			
			return new static($convertedJson, $token);
		}
		
		return null;
	}
	
	public function perform($dbLayer = null, $masterId = null) {
		if (!isset($dbLayer)) $dbLayer = Api::dbLayer();
		
		$dbLayer->beginTransaction();
		
		try {
			/** @var Performable $action */
			foreach ($this->actions as $action) {
				try {
					$this->lastKeyId = $action->perform($dbLayer, $this->masterKeyId);
				} catch (\InvalidArgumentException $exc) { // Thrown if no master key can be found
					throw new \InvalidArgumentException("Unhandled exception in DbObjectTransaction: '" . $exc->getMessage() . "'.");
				}
				
				if ($action instanceof Action) {
					if ($action->isMaster()) {
						$this->masterKeyId = $this->lastKeyId;
					}
				}
			}
			
			$dbLayer->commitTransaction();
			
			if (isset($this->masterKeyId)) {
				return $this->masterKeyId;
			}
			return $this->lastKeyId;
		} catch (\Exception $exc) {
			$dbLayer->rollbackTransaction();
			throw new RollbackDbObjectException($exc);
		}
	}
	
}