<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
namespace InerziaSoft\Core\Db\DbObject\Transactions;

use InerziaSoft\Core\Api\Authentication\SessionToken;
use InerziaSoft\Core\Db\DbObject\DbObject;
use InerziaSoft\Core\Db\DbObject\Definitions\DbObjectDefinition;
use InerziaSoft\Core\Db\DbObject\Exceptions\NullTransactionActionTypeException;
use InerziaSoft\Core\Db\DbObject\Exceptions\UndefinedTransactionActionTypeException;
use InerziaSoft\Core\Db\DbObject\Interfaces\Performable;

define("MASTER_KEY_PLACEHOLDER", "@master_key");

class Action implements Performable {
	
	/**
	 * @var DbObject
	 */
	protected $dbObject;
	protected $action;
	protected $master;
	
	protected $token;
	
	protected $relevantValues;
	
	public function __construct($dbObject, $token, $action, $master = false, $relevantValues = null) {
		$this->dbObject = $dbObject;
		$this->action = $action;
		$this->master = $master;
		$this->token = $token;
		$this->relevantValues = $relevantValues;
	}
	
	/**
	 * Instantiates a DbObjectAction from its JSON representation.
	 *
	 * A valid JSON structure is something like:
	 * {
	 *      className: 'table',
	 *      object: { <DbObject Representation> },
	 *      action: <A value from DbObjectActionType>
	 * }
	 *
	 * @param $json
	 * @param $token SessionToken
	 * @return static
	 * @throws NullTransactionActionTypeException
	 */
	public static function fromJson($json, $token) {
		$jsonToArray = json_decode($json, true);
		
		$dbObject = null;
		$action = null;
		$master = false;
		
		$relevantValues = [];
		
		if (array_key_exists("object", $jsonToArray) && array_key_exists("className", $jsonToArray)) {
			$className = $jsonToArray["className"];
			
			$object = $jsonToArray["object"];
			foreach ($object as $key => $value) {
				array_push($relevantValues, $key);
			}
			
			$objectWithNames = [];
			$tableName = DbObjectDefinition::getTableName($className);
			foreach ($object as $key => $value) {
				$objectWithNames[strtolower($tableName . "." . $key)] = $value;
			}
			
			$dbObject = new $className($objectWithNames);
		}
		
		if (array_key_exists("action", $jsonToArray)) {
			$action = $jsonToArray["action"];
		} else {
			throw new NullTransactionActionTypeException($json);
		}
		
		if (array_key_exists("master", $jsonToArray)) {
			$master = (strtolower($jsonToArray["master"]) == "true");
		}
		
		return new static($dbObject, $token, $action, $master, $relevantValues);
	}
	
	public function perform($dbLayer, $masterId) {
		/** @var \ReflectionProperty $property */
		foreach ($this->dbObject::getReflectionClass()->getProperties() as $property) {
			$value = $property->getValue($this->dbObject);
			
			if ($value === MASTER_KEY_PLACEHOLDER) {
				if (isset($masterId)) {
					$property->setValue($this->dbObject, $masterId);
				} else {
					throw new \InvalidArgumentException("DbObjectAction '" . $this . "' requires master key on property '" . $property->getName() . "' but no master key value has been found.");
				}
			}
		}
		
		$this->dbObject->handleValuesForTransaction($this->token);
		
		switch ($this->action) {
			case ActionType::$Insert:
				$this->dbObject->insert();
				break;
			
			case ActionType::$Update:
				$this->dbObject->updateRelevantValues($this->relevantValues);
				break;
			
			case ActionType::$Delete:
				$this->dbObject->delete();
				break;
			
			default:
				throw new UndefinedTransactionActionTypeException($this->action);
		}
		
		return $this->dbObject->getId();
	}
	
	public function isMaster() {
		return $this->master;
	}
	
	public function __toString() {
		return "<DbObjectAction on " . get_class($this->dbObject) . "> (type: $this->action, master: " . $this->isMaster() . ")";
	}
	
}