<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
namespace InerziaSoft\Core\Db\DbObject\Operators;

use InerziaSoft\Core\Db\DbObject\Interfaces\SqlConvertible;

class SortOperatorsList implements SqlConvertible {
	
	private $operators;
	
	public function __construct($operators) {
		$this->operators = $operators;
	}
	
	function getCount() {
		return count($this->getOperators());
	}
	
	function getOperators() {
		return $this->operators;
	}
	
	public function toSql() {
		/** @var SortOperator $operator */
		$operatorsAsString = [];
		foreach ($this->operators as $operator) {
			array_push($operatorsAsString, $operator->toSql());
		}
		
		return implode(", ", $operatorsAsString);
	}
	
}