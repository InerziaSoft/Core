<?php

namespace InerziaSoft\Core\Db\DbObject\Operators;

use InerziaSoft\Core\Db\DbObject\Interfaces\SqlConvertible;

class SortOperator implements SqlConvertible {

    private $tableName;
    private $column;
    private $ascending;

    public function __construct($column, $ascending) {
        $this->column = $column;
        $this->ascending = $ascending;
    }

    function setTableName($tableName) {
        $this->tableName = $tableName;
    }

    public function toSql() {
        return $this->tableName.".".$this->column." ".($this->ascending ? "ASC":"DESC");
    }

}