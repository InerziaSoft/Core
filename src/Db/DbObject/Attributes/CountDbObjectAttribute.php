<?php
/*
MIT License

Copyright (c) 2016-2017 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Db\DbObject\Attributes;

class CountDbObjectAttribute extends DbObjectAttribute {

	const RegexClassGroup = "Class";
	const RegexColumnGroup = "Column";

	const ValidRegex = '/^(?\''.self::RegexClassGroup.'\'([a-zA-Z]+\\\\)+([a-zA-Z]+)) on (?\''.self::RegexColumnGroup.'\'[a-zA-Z]*)/';

	protected $className;
	protected $column;

	public function __construct($value) {
		$match = static::getFirstMatch($value);

		$this->className = $match[self::RegexClassGroup];
		$this->column = $match[self::RegexColumnGroup];
	}

	public function getColumnName() {
		return $this->column;
	}

	public function getClassName() {
		return $this->className;
	}

	protected static function getValidRegex() {
		return self::ValidRegex;
	}

	public function isVirtual() {
		return true;
	}



}