<?php

namespace InerziaSoft\Core\Db\DbObject\Attributes;

use InerziaSoft\Core\Db\DbObject\Attributes\Exceptions\InvalidDefinitionDbObjectAttributeException;
use InerziaSoft\Core\Db\DbObject\Definitions\DbObjectDefinition;
use InerziaSoft\Core\Db\DbObject\Exceptions\UnexistingDbObjectClassException;

class ForeignKeyDbObjectAttribute extends DbObjectAttribute {

	const OnKeyword = " on ";

	const AutomaticKeyType = "AutoKey";
	const ManualKeyType = "ManualKey";

	const RegexClassGroup = "Class";
	const RegexColumnGroup = "Column";

	protected $originalValue;

	private $type;
	protected $className;
	protected $propertyName;

	const ValidRegex = '/^(?\''.self::RegexClassGroup.'\'([a-zA-Z]+\\\\)+([a-zA-Z]+))(\son\s(?\''.self::RegexColumnGroup.'\'[a-zA-Z]+))?$/';

	/**
	 * ForeignKeyDbObjectAttribute constructor.
	 *
	 * Parses the value of a @foreign attribute in the following form:
	 * <full class name> [on <foreign property key>]
	 *
	 * @param $value
	 * @throws InvalidDefinitionDbObjectAttributeException
	 * @throws UnexistingDbObjectClassException
	 */
	public function __construct($value) {
		$this->originalValue = $value;

		$match = static::getFirstMatch($value);

		$this->className = $match[self::RegexClassGroup];
		if (!class_exists($this->className)) {
			throw new UnexistingDbObjectClassException($this->className);
		}

		if (array_key_exists(self::RegexColumnGroup, $match)) {
			$this->type = self::ManualKeyType;

			$this->propertyName = $match[self::RegexColumnGroup];
		}
		else {
			$this->type = self::AutomaticKeyType;

			$this->propertyName = DbObjectDefinition::getPrimaryKeysName($this->className)[0];
		}
	}

	public final function getClassName() {
		return $this->className;
	}

	public final function getForeignPropertyKey() {
		return $this->propertyName;
	}

	public function getType() {
		return $this->type;
	}

	protected static function getValidRegex() {
		return self::ValidRegex;
	}
}