<?php

namespace InerziaSoft\Core\Db\DbObject\Attributes;

use InerziaSoft\Core\Attributes\AttributesFactory;

class DbObjectAttributesFactory implements AttributesFactory {

	const ForeignKeyAttribute = "foreign";
	const CastAttribute = "var";
	const CountAttribute = "count";
	const ManualAttribute = "manual";

	const RegexTypeGroup = "Type";

	const ValidRegex = '/^@(?\''.self::RegexTypeGroup.'\'('.self::ForeignKeyAttribute.'|'.self::CastAttribute.'|'.self::CountAttribute.'|'.self::ManualAttribute.'))(\s.*)?/i';

    public static function getAttributeForComment($comment) {
	    $matches = [];
	    $count = preg_match(self::ValidRegex, $comment, $matches);

	    if ($count > 0) {
		    switch ($matches[self::RegexTypeGroup]) {
			    case self::ForeignKeyAttribute:
				    return new ForeignKeyDbObjectAttribute(self::handleValueForInstances($comment, self::ForeignKeyAttribute));

			    case self::CastAttribute:
				    return new CastDbObjectAttribute(self::handleValueForInstances($comment, self::CastAttribute));

			    case self::CountAttribute:
				    return new CountDbObjectAttribute(self::handleValueForInstances($comment, self::CountAttribute));

			    case self::ManualAttribute:
			    	return new ManualDbObjectAttribute(self::handleValueForInstances($comment, self::ManualAttribute));
		    }
	    }

	    return null;
    }

    private static function handleValueForInstances($value, $attribute) {
    	return trim(str_replace("@".$attribute, "", $value));
    }}