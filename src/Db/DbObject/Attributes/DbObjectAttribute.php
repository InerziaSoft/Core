<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
namespace InerziaSoft\Core\Db\DbObject\Attributes;

use InerziaSoft\Core\Db\DbObject\Attributes\Exceptions\InvalidDefinitionDbObjectAttributeException;

abstract class DbObjectAttribute {
	
	protected abstract static function getValidRegex();
	
	/**
	 * @param $value
	 * @return mixed
	 * @throws InvalidDefinitionDbObjectAttributeException
	 */
	protected static function getFirstMatch($value) {
		$matches = [];
		$count = preg_match_all(static::getValidRegex(), $value, $matches, PREG_SET_ORDER);

		if ($count == 1) {
			return $matches[0];
		}

		throw new InvalidDefinitionDbObjectAttributeException(static::class, $value);
	}

	public function isVirtual() {
		return false;
	}
	
}