<?php

namespace InerziaSoft\Core\Db\DbObject\Attributes;

use InerziaSoft\Core\Db\DbObject\Attributes\Exceptions\UnknownCastDbObjectAttributeException;

class CastDbObjectAttribute extends DbObjectAttribute {

	/**
	 * Standard Types
	 *
	 * The following constants define the recognized
	 * standard types.
	 */
	const StringType = "string";
	const IntegerType = "integer";
	const BooleanType = "boolean";
	const DecimalType = "decimal";
	const DateTimeType = "DateTime";

	/**
	 * Accepted Types
	 *
	 * The following constants define set of
	 * strings that represents all the recognized
	 * types, including their synonyms.
	 *
	 * The first item must always be the
	 * corresponding standard type defined above.
	 */
	const IntegerTypes = [self::IntegerType, "int"];
	const DecimalTypes = [self::DecimalType, "double", "float"];
	const BooleanTypes = [self::BooleanType, "bool"];
	const DateTimeTypes = [self::DateTimeType, "\\DateTime"];

	/**
	 * Recognized Types
	 *
	 * Groups all the recognized types in a single array.
	 */
	const Types = [self::StringType, self::IntegerTypes, self::BooleanTypes, self::DecimalTypes, self::DateTimeTypes];

	const RegexTypeGroup = "Type";

	const ValidRegex = '/^(?\''.self::RegexTypeGroup.'\'(\\\\)?[a-zA-Z]+)$/';

	protected $rawType;
	protected $type;
	
	/**
	 * CastDbObjectAttribute constructor.
	 *
	 * @param $value
	 * @throws Exceptions\InvalidDefinitionDbObjectAttributeException
	 */
	public function __construct($value) {
		$match = static::getFirstMatch($value);

		$this->rawType = $match[self::RegexTypeGroup];
		$this->type = static::matchToStandardType($this->rawType);

//		if ($this->type === false) {
//			throw new UnknownCastDbObjectAttributeException($this->rawType);
//		}
	}

	public function getType() {
		return $this->type;
	}

	public function getRawType() {
		return $this->rawType;
	}

	protected static function matchToStandardType($type) {
		foreach (self::Types as $knownType) {
			if (is_array($knownType)) {
				foreach ($knownType as $innerKnownType) {
					if (static::typeEqualsKnownType($type, $innerKnownType)) {
						return $knownType[0];
					}
				}
			}
			else {
				if (static::typeEqualsKnownType($type, $knownType)) {
					return $knownType;
				}
			}
		}

		return false;
	}

	protected static function typeEqualsKnownType($type, $knownType) {
		return trim(strtolower($knownType)) == trim(strtolower($type));
	}

	protected static function getValidRegex() {
		return self::ValidRegex;
	}
}