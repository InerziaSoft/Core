<?php

namespace InerziaSoft\Core\Db\DbObject\Attributes\Exceptions;

class UnknownCastDbObjectAttributeException extends \Exception {

	public function __construct($type) {
		parent::__construct("Unknown or unrecognized cast type '$type'!");
	}

}