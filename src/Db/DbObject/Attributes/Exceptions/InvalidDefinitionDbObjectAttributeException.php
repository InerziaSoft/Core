<?php

namespace InerziaSoft\Core\Db\DbObject\Attributes\Exceptions;

class InvalidDefinitionDbObjectAttributeException extends \Exception {

	public function __construct($type, $value) {
		parent::__construct("Invalid definition for DbObject Attribute of type '$type': '$value'");
	}

}