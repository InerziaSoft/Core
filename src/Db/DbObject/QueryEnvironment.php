<?php

namespace InerziaSoft\Core\Db\DbObject;

use InerziaSoft\Core\Db\DbObject\Interfaces\TableEnvironment;

class QueryEnvironment implements TableEnvironment {
	
	private $environment;
	
	public function __construct() {
		$this->environment = [];
	}
	
	public function getUsages($tableName) {
		$tableName = strtolower($tableName);
		
		if (array_key_exists($tableName, $this->environment)) {
			return $this->environment[$tableName];
		}
		return 0;
	}
	
	public function markUsage($tableName) {
		$tableName = strtolower($tableName);
		
		$currentUsages = $this->getUsages($tableName);
		$currentUsages++;
		
		$this->environment[$tableName] = $currentUsages;
	}
	
	public function useTableNameInEnvironment($tableName) {
		$this->markUsage($tableName);
		return $this->tableNameInEnvironment($tableName);
	}
	
	public function tableNameInEnvironment($tableName) {
		$usages = $this->getUsages($tableName);
		
		return $usages > 0 ? $tableName.$usages : $tableName;
	}
	
	public function __toString() {
		return "<Env>: ".print_r($this->environment, true);
	}
	
}

