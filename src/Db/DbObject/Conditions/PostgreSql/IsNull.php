<?php

namespace InerziaSoft\Core\Db\DbObject\Conditions\PostgreSql;

use InerziaSoft\Core\Db\DbObject\Conditions\Condition;

class IsNull extends Condition {
	
	protected $notNull;
	
	function __construct($className, $column, $notNull = false) {
		parent::__construct($className, $column, null);
		$this->notNull = $notNull;
	}
	
	function toSql() {
		return $this->column." IS ".($this->notNull ? "NOT" : "")." NULL";
	}
	
}