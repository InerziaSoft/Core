<?php

namespace InerziaSoft\Core\Db\DbObject\Conditions;

class Contains extends Condition {
	
	private $valueIsQuery = false;
	
	static function useQuery($className, $column, $query) {
		$contains = new static($className, $column, $query);
		$contains->valueIsQuery = true;
		
		return $contains;
	}
	
	function getValue() {
		if ($this->valueIsQuery) {
			return null;
		}
		
		return parent::getValue();
	}
	
	function toSql() {
		return $this->getTableName().".$this->column ".$this->getOperator()." (".$this->value.")";
	}
	
	function getOperator() {
		return "IN";
	}
	
}