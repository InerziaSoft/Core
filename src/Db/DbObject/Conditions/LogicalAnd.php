<?php

namespace InerziaSoft\Core\Db\DbObject\Conditions;

class LogicalAnd extends LogicalConnective {

    function toSql() {
        return "AND";
    }

}

