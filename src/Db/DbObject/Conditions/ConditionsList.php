<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
namespace InerziaSoft\Core\Db\DbObject\Conditions;

use InerziaSoft\Core\Db\DbObject\Exceptions\UnexpectedConditionBlockException;
use InerziaSoft\Core\Db\DbObject\Interfaces\SqlConvertible;

class ConditionsList implements SqlConvertible {

    private $conditions;

    /**
     * Conditions constructor.
     *
     * This method initializes a SQL condition, which is basically
     * everything that follows the keyword "WHERE" in a SQL query.
     *
     * @param $conditions array: An ordered array of conditions and
     * logical connectives, such as ["something LIKE 'something'", "AND", "somethingElse LIKE 'somethingElse']".
     * If the series of [condition - logical connective - condition] is broken at some point in the array,
     * an exception will be thrown.
     *
     * @discussion Conditions and logical connectives must be represented by classes
     * implementing the SqlConvertible interface.
     */
    public function __construct($conditions) {
        $this->conditions = $conditions;
    }

    public static function joinWithLogicalConnective($conditions, $logicalConnectiveClass) {
    	$result = [];
    	$i = 0;
    	foreach ($conditions as $condition) {
    		if ($i != 0) {
    			array_push($result, new $logicalConnectiveClass());
		    }
            array_push($result, $condition);
		    $i++;
	    }
	    
	    return new static($result);
    }
    
    function getCount() {
    	return count($this->getConditions());
    }
    
    public function getConditions() {
        return $this->conditions;
    }

    public function getValues() {
	    $pars = [];
	    /** @var Condition $condition */
	    foreach ($this->getConditions() as $condition) {
		    if ($condition instanceof Condition) {
			    if ($condition->hasParameters()) {
				    $value = $condition->getValue();
				
				    if (isset($value)) {
					    array_push($pars, $value);
				    }
			    }
		    }
	    }
	
	    return $pars;
    }
	
	/**
	 * @param int $existingParametersCount
	 * @return string
	 * @throws UnexpectedConditionBlockException
	 */
	public function toSql(int $existingParametersCount = 1) {
        $query = "";

        /**
         * The expected block represents the type of class
         * that is expected.
         *
         * 0 = Condition
         * 1 = Logical Connective
         *
         * If the wrong type is found, an exception will be thrown.
         */
        $expectedBlock = 0;
        $parameterCount = $existingParametersCount;

        /** @var SqlConvertible $condition */
        foreach ($this->conditions as $condition) {
            if ($condition instanceof LogicalConnective) {
                if ($expectedBlock == 1) {
                    $expectedBlock = 0;
                }
                else {
                    throw new UnexpectedConditionBlockException(UnexpectedConditionBlockException::$UnexpectedConditionBlockCondition);
                }
            }
            else if ($condition instanceof Condition) {
                if ($expectedBlock == 0) {
                    $expectedBlock = 1;
                }
                else {
                    throw new UnexpectedConditionBlockException(UnexpectedConditionBlockException::$UnexpectedConditionBlockLogicalConnective);
                }
            }

            $conditionToSql = $condition->toSql();

            if ($condition instanceof Condition) {
            	if ($condition->hasParameters()) {
		            $replacedPlaceholders = 0;
		            $conditionToSql = str_replace("$@", "$".$parameterCount, $conditionToSql, $replacedPlaceholders);
		
		            if ($replacedPlaceholders > 0) $parameterCount++;
	            }
            }

            $query .= " ".$conditionToSql;
        }

        return $query;
    }
}

