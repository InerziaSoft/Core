<?php

namespace InerziaSoft\Core\Db\DbObject\Interfaces;

interface NamesEnvironment {
	
	public function getName($name);
	public function useName($name);
	public function getUsagesCount($name);
	
}
