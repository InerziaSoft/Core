<?php

namespace InerziaSoft\Core\Db\DbObject\Interfaces;

interface TableEnvironment {
	
	public function tableNameInEnvironment($tableName);
	public function useTableNameInEnvironment($tableName);
	
}
