<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Db\DbObject\Reflection;

use InerziaSoft\Core\Attributes\AttributesParser;
use InerziaSoft\Core\Db\DbObject\Attributes\CastDbObjectAttribute;
use InerziaSoft\Core\Db\DbObject\Attributes\DbObjectAttribute;
use InerziaSoft\Core\Db\DbObject\Attributes\DbObjectAttributesFactory;
use InerziaSoft\Core\Db\DbObject\Attributes\ForeignKeyDbObjectAttribute;
use InerziaSoft\Core\Db\DbObject\Attributes\ManualDbObjectAttribute;
use InerziaSoft\Core\Db\DbObject\DbObject;
use InerziaSoft\Core\Db\DbObject\Definitions\DbObjectDefinition;
use InerziaSoft\Core\Db\DbObject\Exceptions\UnexistingDbObjectException;
use InerziaSoft\Core\Db\DbObject\Interfaces\NamesEnvironment;
use InerziaSoft\Core\Logger\Logger;
use InerziaSoft\Core\Utils\ArrayUtils;

class ReflectionDbObjectProperty extends \ReflectionProperty {

	/**
	 * @var AttributesParser
	 */
	protected $parser;

    /**
     * @var DbObjectAttributesFactory
     */
	protected $attributesFactory;
	
	/**
	 * ReflectionDbObjectProperty constructor.
	 * @param mixed $class
	 * @param string $name
	 * @throws \ReflectionException
	 */
	public function __construct($class, $name) {
		if ($class instanceof \ReflectionClass) {
			$class = $class->getName();
		}
		
		parent::__construct($class, $name);

        $this->attributesFactory = new DbObjectAttributesFactory();
        $this->parser = new AttributesParser($this, $this->getAttributesFactory());
	}
	
	/**
	 * Returns an instance of AttributesParser associated
	 * with this class.
	 *
	 * @return AttributesParser
	 */
	public function getAttributesParser() {
		return $this->parser;
	}

    /**
     * Returns an instance of DbObjectAttributesFactory
     * to use with AttributeParse instances.
     *
     * @return DbObjectAttributesFactory
     */
	public function getAttributesFactory() {
	    return $this->attributesFactory;
    }
	
	/**
	 * @param $property \ReflectionProperty
	 * @return static
	 * @throws \ReflectionException
	 */
	public static function fromReflectionProperty($property) {
		return new static($property->getDeclaringClass(), $property->getName());
	}
	
	/**
	 * Returns true if this property is manual.
	 *
	 * @discussion A manual property uses the @manual attribute.
	 *
	 * @return bool
	 */
	public function isManual() {
		return $this->parser->contains(ManualDbObjectAttribute::class);
	}
	
	/**
	 * Returns true if this property represents
	 * one of the primary keys of the DbObject class.
	 *
	 * @param $dbObject DbObject
	 * @return bool
	 */
	public function isPrimaryKeyOnDbObject($dbObject) {
		$propertyName = strtolower($this->getName());
		
		foreach ($dbObject->getIdKeys() as $idKey) {
			if ($propertyName == strtolower($idKey)) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Returns true if this property has a cast of the specified generic type.
	 *
	 * @discussion A casted property uses the @var attribute.
	 *
	 * @param $type
	 * @return bool
	 */
	public function hasCastOfType($type) {
		/** @var DbObjectAttribute $attribute */
		foreach ($this->parser->getAttributes() as $attribute) {
			if ($attribute instanceof CastDbObjectAttribute) {
				return strtolower($attribute->getType() == strtolower($type));
			}
		}
		
		return false;
	}
	
	/**
	 * @param object $object [optional]
	 * @param mixed $value
	 * @param array $initValues
	 * @param $environment NamesEnvironment
	 * @param bool $readOnly
	 * @throws UnexistingDbObjectException
	 * @throws \InerziaSoft\Core\Db\DbObject\Exceptions\UnexistingKeyException
	 * @throws \InerziaSoft\Core\Db\DbObject\Exceptions\UnknownIdKeyException
	 */
	public function setValue($object, $value = null, $initValues = [], &$environment = null, $readOnly = false) {
		$propertyName = strtolower($this->getName());
		$finalValue = $value;
		
		/** @var DbObjectAttribute $attribute */
		foreach ($this->parser->getAttributes() as $attribute) {
			if ($attribute instanceof ForeignKeyDbObjectAttribute) {
				if (isset($environment)) $environment->useName(DbObjectDefinition::getTableName($attribute->getClassName()));
				$foreignObject = $this->foreignDbObjectFromValues($propertyName, $attribute->getClassName(), $initValues, $environment, $readOnly);
				if (!isset($foreignObject)) break;

				$finalValue = $foreignObject;
			}
			else if ($attribute instanceof CastDbObjectAttribute && isset($finalValue)) {
				$className = $attribute->getType();

				if (!($finalValue instanceof \DateTime) && strpos($className, CastDbObjectAttribute::DateTimeType) !== false) {
					$allowedFormats = ["Y-m-d", "Y-m-d H:i:s", "Y-m-d H:i:s.u"];
					$dateTime = false;
					$tryIndex = 0;
					$maximumIndex = count($allowedFormats);
					while ($dateTime === false) {
						if ($tryIndex >= $maximumIndex) {
							$dateTime = $value;
							break;
						}
						else {
							$dateTime = \DateTime::createFromFormat($allowedFormats[$tryIndex], $finalValue);
							$tryIndex++;
						}
					}
					
					$finalValue = $dateTime;
				}
			}
			else if ($attribute instanceof ManualDbObjectAttribute) {
				$finalValue = $this->parseManualProperty($propertyName, $initValues, $readOnly);
			}
		}
		
		parent::setValue($object, $finalValue);
	}
	
	/**
	 * @param $propertyName string
	 * @param $foreignClassName DbObject
	 * @param $initValues array
	 * @param $environment NamesEnvironment
	 * @param bool $readOnly
	 * @return DbObject|null
	 * @throws \InerziaSoft\Core\Db\DbObject\Exceptions\UnexistingKeyException
	 * @throws \InerziaSoft\Core\Db\DbObject\Exceptions\UnknownIdKeyException
	 * @internal param string $attribute
	 * @throws UnexistingDbObjectException
	 */
	private function foreignDbObjectFromValues($propertyName, $foreignClassName, $initValues, &$environment = null, $readOnly = false) {
		/** @var DbObject $foreignClassName */
		$foreignTableName = DbObjectDefinition::getTableName($foreignClassName);

		if ($readOnly) {
			$foreignValues = ArrayUtils::arrayKeysStartingWith($initValues, $propertyName);
			/** @var DbObject $foreignObject */
			$foreignObject = $foreignClassName::readOnly($foreignValues);
		}
		else {
			$foreignValues = $initValues;
			/** @var DbObject $foreignObject */
			try {
				$foreignObject = new $foreignClassName($foreignValues, $environment);
			}
			catch (UnexistingDbObjectException $exception) {
				$foreignObject = $foreignClassName::make();
			}
		}

		//if (isset($environment)) $environment->useName($foreignTableName);

		$id = $foreignObject->getId();
		if ($id === null || $id == "") return null;
		
		return $foreignObject;
	}
	
	private function parseManualProperty($propertyName, $valueToParse, $readOnly = false) {
		if (is_array($valueToParse)) {
			$valuesForProperty = ArrayUtils::arrayKeysStartingWith($valueToParse, $propertyName);
			
			$responseClass = self::getResponseClassFromValues($propertyName, $valuesForProperty);
			if ($responseClass != null) {
				try {
					$reflectionClass = new \ReflectionClass($responseClass);
					
					if ($reflectionClass->isSubclassOf(DbObject::class)) {
						return self::foreignDbObjectFromValues($propertyName, $responseClass, $valuesForProperty, $environment, $readOnly);
					}
				}
				catch (\Exception $exc) {
					return "RESPONSE_CLASS $responseClass NOT FOUND";
				}
			}
			else {
				if (array_key_exists($propertyName, $valuesForProperty)) {
					$valuesForProperty = $valuesForProperty[$propertyName];
					
					return $valuesForProperty;
				}
			}
		}
		
		return $valueToParse;
	}
	
	private static function getResponseClassFromValues($propertyName, $values) {
		$responseClassKey = $propertyName.".responseClass";
		
		if (array_key_exists($responseClassKey, $values)) {
			return $values[$responseClassKey];
		}
		
		return null;
	}
	
}