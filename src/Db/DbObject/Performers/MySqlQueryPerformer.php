<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Db\DbObject\Performers;

use InerziaSoft\Core\Db\DbLayers\MySql\MySqlDbLayer;
use InerziaSoft\Core\Db\DbObject\Conditions\ConditionsList;
use InerziaSoft\Core\Db\DbObject\Conditions\LogicalAnd;
use InerziaSoft\Core\Db\DbObject\Conditions\MySql\Equal;
use InerziaSoft\Core\Db\DbObject\Evaluators\QueryEvaluatorResult;
use InerziaSoft\Core\Db\DbObject\Interfaces\QueryEvaluator;
use InerziaSoft\Core\Db\DbObject\Interfaces\QueryPerformer;
use InerziaSoft\Core\Db\DbObject\Operators\SortOperatorsList;
use InerziaSoft\Core\Logger\Logger;
use InerziaSoft\Core\Utils\DIUtils;

class MySqlQueryPerformer implements QueryPerformer {
	
	private static $ColumnNamesBorder = "`";
	
	/**
	 * @var QueryEvaluator
	 */
	protected $queryEvaluator;
	
	/**
	 * MySqlQueryPerformer constructor.
	 * @throws \DI\DependencyException
	 * @throws \DI\NotFoundException
	 */
	public function __construct() {
		$this->queryEvaluator = DIUtils::getContainer()->get(QueryEvaluator::class);
	}
	
	/**
	 * @param $className
	 * @param $idKeysAndValues
	 * @param $comparisonOperator
	 * @return array|null
	 * @throws \InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseConfigurationException
	 * @throws \InerziaSoft\Core\Db\DbObject\Exceptions\UnexpectedConditionBlockException
	 */
	public function getObjectWithIds($className, $idKeysAndValues, $comparisonOperator) {
		$query = "SELECT ".ALL_FIELDS." FROM ".ALL_TABLES;
		
		return $this->performReturningFirstRow(
			$this->queryEvaluator->evaluate(
				$className,
				$query,
				$this->idKeysAsConditions($className, $idKeysAndValues),
				null,
				static::$ColumnNamesBorder),
			array_values($idKeysAndValues));
	}
	
	/**
	 * @param $className
	 * @param $sortOperatorsList
	 * @return array
	 * @throws \InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseConfigurationException
	 * @throws \InerziaSoft\Core\Db\DbObject\Exceptions\UnexpectedConditionBlockException
	 */
	public function getAll($className, $sortOperatorsList, $limit) {
		$query = "SELECT ".ALL_FIELDS." FROM ".ALL_TABLES;
		$queryEvaluatorResult = $this->queryEvaluator->evaluate($className, $query, null, $sortOperatorsList, static::$ColumnNamesBorder);
		
		return $this->perform($queryEvaluatorResult);
	}
	
	/**
	 * @param $className
	 * @param $conditionsList ConditionsList
	 * @param $sortOperatorsList SortOperatorsList
	 * @return array
	 * @throws \InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseConfigurationException
	 * @throws \InerziaSoft\Core\Db\DbObject\Exceptions\UnexpectedConditionBlockException
	 */
	public function getWhere($className, $conditionsList, $sortOperatorsList) {
		$query = "SELECT ".ALL_FIELDS." FROM ".ALL_TABLES;
		$queryEvaluatorResult = $this->queryEvaluator->evaluate($className, $query, $conditionsList, $sortOperatorsList, static::$ColumnNamesBorder);
		
		return $this->perform($queryEvaluatorResult, $conditionsList->getValues());
	}
	
	/**
	 * @param $className
	 * @param $conditionsList ConditionsList
	 * @return mixed
	 * @throws \InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseConfigurationException
	 * @throws \InerziaSoft\Core\Db\DbObject\Exceptions\UnexpectedConditionBlockException
	 */
	public function getCount($className, $conditionsList) {
		$query = "SELECT COUNT(*) AS count FROM ".ALL_TABLES;
		$queryEvaluatorResult = $this->queryEvaluator->evaluate($className, $query, $conditionsList, null, static::$ColumnNamesBorder);
		
		$result = $this->performReturningFirstRow($queryEvaluatorResult, $conditionsList->getValues());
		
		return $result["count"];
	}
	
	private function valuesAsParameters($values) {
		return implode(",", array_map(function () {
			return "?";
		}, $values));
	}
	
	private function idKeysAsConditions($className, $idKeysAndValues) {
		$conditions = [];
		foreach ($idKeysAndValues as $idKey => $idValue) {
			array_push($conditions, new Equal($className, $idKey, $idValue));
		}
		
		return ConditionsList::joinWithLogicalConnective($conditions, LogicalAnd::class);
	}
	
	/**
	 * @param $className
	 * @param $keys
	 * @param $values
	 * @param $idKeys
	 * @return array
	 * @throws \InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseConfigurationException
	 * @throws \InerziaSoft\Core\Db\DbObject\Exceptions\UnexpectedConditionBlockException
	 */
	public function insert($className, $keys, $values, $idKeys) {
		$keysAsString = implode(", ", array_map(function ($item) {
			return "`$item`";
		}, $keys));
		$valuesAsString = $this->valuesAsParameters($values);
		
		$query = "INSERT INTO ".$this->queryEvaluator->getTableName($className)." ($keysAsString) VALUES ($valuesAsString)";
		$query = $this->queryEvaluator->evaluate($className, $query, null, false);
		
		// TODO: handle returning calculated id keys to the caller
		
		return $this->perform($query, $values);
	}
	
	/**
	 * @param $className
	 * @param $keysValues
	 * @param $idKeysAndValues
	 * @return array
	 * @throws \InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseConfigurationException
	 * @throws \InerziaSoft\Core\Db\DbObject\Exceptions\UnexpectedConditionBlockException
	 */
	public function update($className, $keysValues, $idKeysAndValues) {
		$keysValuesAsString = [];
		$values = [];
		$idValues = array_values($idKeysAndValues);
		
		foreach ($keysValues as $key => $value) {
			array_push($keysValuesAsString, "$key = ?");
			array_push($values, $value);
		}
		
		$values = array_merge($values, $idValues);
		$keysValuesAsString = implode(", ", $keysValuesAsString);
		
		$query = "UPDATE ".$this->queryEvaluator->getTableName($className)." SET $keysValuesAsString";
		$query = $this->queryEvaluator->evaluate(
			$className,
			$query,
			$this->idKeysAsConditions($className, $idKeysAndValues),
			false,
			static::$ColumnNamesBorder
		);
		
		return $this->perform($query, $values);
	}
	
	/**
	 * @param $className
	 * @param $idKeysAndValues
	 * @return array
	 * @throws \InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseConfigurationException
	 * @throws \InerziaSoft\Core\Db\DbObject\Exceptions\UnexpectedConditionBlockException
	 */
	public function delete($className, $idKeysAndValues) {
		$query = "DELETE FROM ".$this->queryEvaluator->getTableName($className);
		return $this->perform(
			$this->queryEvaluator->evaluate(
				$className,
				$query,
				$this->idKeysAsConditions($className, $idKeysAndValues),
				false,
				static::$ColumnNamesBorder
			),
			array_values($idKeysAndValues)
		);
	}
	
	/**
	 * @param $className
	 * @param $conditions ConditionsList
	 * @return array
	 * @throws \InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseConfigurationException
	 * @throws \InerziaSoft\Core\Db\DbObject\Exceptions\UnexpectedConditionBlockException
	 */
	public function deleteAll($className, $conditions) {
		$query = "DELETE FROM ".$this->queryEvaluator->getTableName($className);
		$query = $this->queryEvaluator->evaluate($className, $query, $conditions, false, static::$ColumnNamesBorder);
		
		return $this->perform($query, $conditions->getValues());
	}
	
	/**
	 * @param $queryEvaluatorResult QueryEvaluatorResult
	 * @return \InerziaSoft\Core\Db\DbLayers\PreparedStatement
	 * @throws \InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseConfigurationException
	 * @throws \InerziaSoft\Core\Db\DbObject\Exceptions\UnexpectedConditionBlockException
	 */
	private function buildQuery($queryEvaluatorResult) {
		$query = $queryEvaluatorResult->getQuery();
		$conditionsList = $queryEvaluatorResult->getConditionsList();
		$sortOperatorsList = $queryEvaluatorResult->getSortOperatorsList();
		if ($conditionsList->getCount() > 0) {
			$query .= " WHERE ".$queryEvaluatorResult->getConditionsList()->toSql();
		}
		if ($sortOperatorsList->getCount() > 0) {
			$query .= " ORDER BY ".$queryEvaluatorResult->getSortOperatorsList()->toSql();
		}
		
		$dbLayer = new MySqlDbLayer();
		$stmt = $dbLayer->prepare($query);
		
		return $stmt;
	}
	
	/**
	 * @param $queryEvaluatorResult QueryEvaluatorResult
	 * @param array $parameters
	 * @return array|null
	 * @throws \InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseConfigurationException
	 * @throws \InerziaSoft\Core\Db\DbObject\Exceptions\UnexpectedConditionBlockException
	 */
	public function performReturningFirstRow(QueryEvaluatorResult $queryEvaluatorResult, array $parameters = []) {
		return $this->buildQuery($queryEvaluatorResult)->executeReturningFirstRow($parameters);
	}
	
	/**
	 * @param $queryEvaluatorResult QueryEvaluatorResult
	 * @param array $parameters
	 * @return array
	 * @throws \InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseConfigurationException
	 * @throws \InerziaSoft\Core\Db\DbObject\Exceptions\UnexpectedConditionBlockException
	 */
	public function perform(QueryEvaluatorResult $queryEvaluatorResult, array $parameters = []) {
		return $this->buildQuery($queryEvaluatorResult)->execute($parameters);
	}
	
}