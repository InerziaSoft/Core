<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Db\DbObject\Performers;

use InerziaSoft\Core\Logger\Loggable;
use InerziaSoft\Core\Logger\Logger;
use InerziaSoft\Core\Logger\LogLevel;

class FakePostgreSqlQueryPerformer extends PostgreSqlQueryPerformer {
	
	use Loggable;
	
	protected $dataSet;
	
	public function getDataSet() {
		return $this->dataSet;
	}
	
	public function setDataSet($dataSet) {
		$this->dataSet = $dataSet;
	}
	
	protected function log($query, $parameters) {
		return $this->getLogger()->logToHandler(
			"Perform query: '".$query."'",
			LogLevel::DebugLevel,
			["parameters" => $parameters]
		);
	}
	
	public function performReturningFirstRow($queryEvaluatorResult, $parameters = []) {
		return $this->log($queryEvaluatorResult, $parameters);
	}
	
	public function perform($query, $parameters = []) {
		return $this->log($query, $parameters);
	}

	public function getAll($className, $sortOperatorsList) {
		parent::getAll($className, $sortOperatorsList);

		return $this->getDataSet();
    }
    
    public function getWhere($className, $conditionsList, $sortOperatorsList) {
	    parent::getWhere($className, $conditionsList, $sortOperatorsList);
	    
	    return $this->getDataSet();
    }
    
    public function getCount($className, $conditionsList) {
	    parent::getCount($className, $conditionsList);
	    
	    return 15;
    }
    
    public function deleteAll($className, $conditionsList) {
		parent::deleteAll($className, $conditionsList);
		
		return true;
    }
	
}