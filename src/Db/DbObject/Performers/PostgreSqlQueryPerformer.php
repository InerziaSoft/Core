<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Db\DbObject\Performers;

use InerziaSoft\Core\Db\DbLayers\PostgreSql\PostgreSqlPreparedStatement;
use InerziaSoft\Core\Db\DbLayers\PostgreSql\PostgresSqlDbLayer;
use InerziaSoft\Core\Db\DbObject\Conditions\ConditionsList;
use InerziaSoft\Core\Db\DbObject\Conditions\LogicalAnd;
use InerziaSoft\Core\Db\DbObject\Conditions\PostgreSql\Equal;
use InerziaSoft\Core\Db\DbObject\Evaluators\QueryEvaluatorResult;
use InerziaSoft\Core\Db\DbObject\Interfaces\QueryEvaluator;
use InerziaSoft\Core\Db\DbObject\Interfaces\QueryPerformer;
use InerziaSoft\Core\Db\DbObject\Literals\ValueInterpreter;
use InerziaSoft\Core\Db\DbObject\Operators\SortOperatorsList;
use InerziaSoft\Core\Utils\DIUtils;

class PostgreSqlQueryPerformer implements QueryPerformer {
	
	/**
	 * @var QueryEvaluator
	 */
	protected $queryEvaluator;
	
	/**
	 * @var PostgreSqlPreparedStatement
	 */
	protected $stmt;
	
	/**
	 * PostgreSqlQueryPerformer constructor.
	 *
	 * @throws \DI\DependencyException
	 * @throws \DI\NotFoundException
	 */
	public function __construct() {
		$this->queryEvaluator = DIUtils::getContainer()->get(QueryEvaluator::class);
	}
	
	private function valuesAsParameters($values) {
		$valuesAsString = [];
		for ($i = 1; $i <= count($values); $i++) {
			array_push($valuesAsString, "$".$i);
		}
		return implode(",", $valuesAsString);
	}
	
	private function idKeysAsConditions($className, $idKeysAndValues) {
		$conditions = [];
		foreach ($idKeysAndValues as $idKey => $idValue) {
			array_push($conditions, new Equal($className, $idKey, $idValue));
		}
		
		return ConditionsList::joinWithLogicalConnective($conditions, LogicalAnd::class);
	}
	
	/**
	 * @param $className
	 * @param $keys
	 * @param $values
	 * @param $idKeys
	 * @return array|int
	 * @throws \DI\DependencyException
	 * @throws \DI\NotFoundException
	 * @throws \InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseConfigurationException
	 * @throws \InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseException
	 * @throws \InerziaSoft\Core\Db\DbObject\Exceptions\UnexpectedConditionBlockException
	 */
	public function insert($className, $keys, $values, $idKeys) {
		$keysAsString = implode(",", $keys);
		$idKeysAsString = implode(",", $idKeys);
		$valuesAsString = $this->valuesAsParameters($values);
		
		$query = "INSERT INTO ".$this->queryEvaluator->getTableName($className)." ($keysAsString) VALUES ($valuesAsString) RETURNING $idKeysAsString";
		
		return $this->perform($this->queryEvaluator->evaluate($className, $query, null, false), $values);
	}
	
	/**
	 * @param $className
	 * @param $keysValues
	 * @param $idKeysAndValues
	 * @return int
	 * @throws \DI\DependencyException
	 * @throws \DI\NotFoundException
	 * @throws \InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseConfigurationException
	 * @throws \InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseException
	 * @throws \InerziaSoft\Core\Db\DbObject\Exceptions\UnexpectedConditionBlockException
	 */
	public function update($className, $keysValues, $idKeysAndValues) {
		$keysValuesAsString = [];
		$values = [];
		
		foreach ($keysValues as $key => $value) {
			array_push($keysValuesAsString, "$key = $@");
			array_push($values, $value);
		}
		$values = array_merge($values, array_values($idKeysAndValues));
		$keysValuesAsString = implode(", ", $keysValuesAsString);
		
		$query = "UPDATE ".$this->queryEvaluator->getTableName($className)." SET $keysValuesAsString";
		
		$this->perform(
			$this->queryEvaluator->evaluate(
				$className,
				$query,
				$this->idKeysAsConditions($className, $idKeysAndValues),
				false
			),
			$values);
		
		return $this->stmt->getAffectedRows();
	}
	
	/**
	 * @param $className
	 * @param $idKeysAndValues
	 * @return int
	 * @throws \DI\DependencyException
	 * @throws \DI\NotFoundException
	 * @throws \InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseConfigurationException
	 * @throws \InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseException
	 * @throws \InerziaSoft\Core\Db\DbObject\Exceptions\UnexpectedConditionBlockException
	 */
	public function delete($className, $idKeysAndValues) {
		$query = "DELETE FROM ".$this->queryEvaluator->getTableName($className);
		
		$this->perform(
			$this->queryEvaluator->evaluate(
				$className,
				$query,
				$this->idKeysAsConditions($className, $idKeysAndValues),
				false
			),
			array_values($idKeysAndValues));
		
		return $this->stmt->getAffectedRows();
	}
	
	/**
	 * @param $className
	 * @param $idKeysAndValues
	 * @param $comparisonOperator
	 * @return array|mixed|null
	 * @throws \InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseConfigurationException
	 * @throws \InerziaSoft\Core\Db\DbObject\Exceptions\UnexpectedConditionBlockException
	 */
	public function getObjectWithIds($className, $idKeysAndValues, $comparisonOperator) {
		$query = "SELECT ".ALL_FIELDS." FROM ".ALL_TABLES;
		
		return $this->performReturningFirstRow(
			$this->queryEvaluator->evaluate(
				$className,
				$query,
				$this->idKeysAsConditions($className, $idKeysAndValues)
			),
			array_values($idKeysAndValues));
	}
	
	/**
	 * @param $className
	 * @param $sortOperatorsList
	 * @param $limit
	 * @return array
	 * @throws \InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseConfigurationException
	 * @throws \InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseException
	 * @throws \InerziaSoft\Core\Db\DbObject\Exceptions\UnexpectedConditionBlockException
	 */
	public function getAll($className, $sortOperatorsList, $limit) {
		$query = "SELECT ".ALL_FIELDS." FROM ".ALL_TABLES;
		
		$queryEvaluatorResult = $this->queryEvaluator->evaluate($className, $query, null, $sortOperatorsList);
		
		return $this->perform($queryEvaluatorResult, null, $limit);
	}
	
	/**
	 * @param $className string
	 * @param $conditionsList ConditionsList
	 * @param $sortOperatorsList SortOperatorsList
	 * @return array
	 * @throws \InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseConfigurationException
	 * @throws \InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseException
	 * @throws \InerziaSoft\Core\Db\DbObject\Exceptions\UnexpectedConditionBlockException
	 */
	public function getWhere($className, $conditionsList, $sortOperatorsList) {
		$query = "SELECT ".ALL_FIELDS." FROM ".ALL_TABLES;
		$queryEvaluatorResult = $this->queryEvaluator->evaluate($className, $query, $conditionsList, $sortOperatorsList);
		
		return $this->perform($queryEvaluatorResult, $conditionsList->getValues());
	}
	
	/**
	 * @param $className string
	 * @param $conditionsList ConditionsList
	 * @return int
	 * @throws \InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseConfigurationException
	 * @throws \InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseException
	 * @throws \InerziaSoft\Core\Db\DbObject\Exceptions\UnexpectedConditionBlockException
	 */
	public function getCount($className, $conditionsList) {
		$query = "SELECT COUNT(*) AS count FROM ".ALL_TABLES;
		
		$result = $this->perform($this->queryEvaluator->evaluate($className, $query, $conditionsList, false), $conditionsList->getValues());
		
		return $result["count"];
	}
	
	/**
	 * @param $className string
	 * @param $conditions ConditionsList
	 * @return array
	 * @throws \InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseConfigurationException
	 * @throws \InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseException
	 * @throws \InerziaSoft\Core\Db\DbObject\Exceptions\UnexpectedConditionBlockException
	 */
	public function deleteAll($className, $conditions) {
		$query = "DELETE FROM ".$this->queryEvaluator->getTableName($className);
		
		$this->perform($this->queryEvaluator->evaluate($className, $query, $conditions, false), $conditions->getValues());
		return $this->stmt->getAffectedRows();
	}
	
	/**
	 * @param $queryEvaluatorResult QueryEvaluatorResult
	 * @param $limit int|null
	 * @throws \InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseConfigurationException
	 * @throws \InerziaSoft\Core\Db\DbObject\Exceptions\UnexpectedConditionBlockException
	 */
	private function buildQuery($queryEvaluatorResult, $limit) {
		$query = $queryEvaluatorResult->getQuery();
		
		$parameterCount = 1;
		$needle = "$@";
		$pos = strpos($query, $needle);
		while ($pos !== false) {
			$query = substr_replace($query, "$".$parameterCount, $pos, strlen($needle));
			$pos = strpos($query, $needle);
			$parameterCount++;
		}
		
		$conditionsList = $queryEvaluatorResult->getConditionsList();
		$sortOperatorsList = $queryEvaluatorResult->getSortOperatorsList();
		if ($conditionsList->getCount() > 0) {
			$query .= " WHERE ".$queryEvaluatorResult->getConditionsList()->toSql($parameterCount);
		}
		if ($sortOperatorsList->getCount() > 0) {
			$query .= " ORDER BY ".$queryEvaluatorResult->getSortOperatorsList()->toSql();
		}
		if (isset($limit)) {
			$query .= " LIMIT ".intval($limit);
		}
		
		$dbLayer = new PostgresSqlDbLayer();
		$this->stmt = $dbLayer->prepare($query);
	}
	
	/**
	 * @param array|null $parameters
	 * @return array|null
	 * @throws \DI\DependencyException
	 * @throws \DI\NotFoundException
	 */
	private function handleParameters(?array $parameters) : ?array {
		$valueInterpreter = DIUtils::getContainer()->get(ValueInterpreter::class);
		$convertedParameters = [];
		if (isset($parameters)) {
			foreach ($parameters as $parameter) {
				array_push($convertedParameters, $valueInterpreter->interpretValue($parameter));
			}
		}
		return $convertedParameters;
	}
	
	/**
	 * @param $queryEvaluatorResult QueryEvaluatorResult
	 * @param array $parameters
	 * @return array|mixed|null
	 * @throws \DI\DependencyException
	 * @throws \DI\NotFoundException
	 * @throws \InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseConfigurationException
	 * @throws \InerziaSoft\Core\Db\DbObject\Exceptions\UnexpectedConditionBlockException
	 */
	public function performReturningFirstRow(QueryEvaluatorResult $queryEvaluatorResult, array $parameters = []) {
		$this->buildQuery($queryEvaluatorResult, 1);
		return $this->stmt->executeReturningFirstRow($this->handleParameters($parameters));
	}
	
	/**
	 * @param $query QueryEvaluatorResult
	 * @param array $parameters
	 * @param null|int $limit
	 * @return array
	 * @throws \DI\DependencyException
	 * @throws \DI\NotFoundException
	 * @throws \InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseConfigurationException
	 * @throws \InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseException
	 * @throws \InerziaSoft\Core\Db\DbObject\Exceptions\UnexpectedConditionBlockException
	 */
	public function perform(QueryEvaluatorResult $query, ?array $parameters = [], $limit = null) {
		$this->buildQuery($query, $limit);
		return $this->stmt->execute($this->handleParameters($parameters));
	}
}