<?php

namespace InerziaSoft\Core\Api;

use InerziaSoft\Core\Api\Exceptions\UnexistingApiException;
use InerziaSoft\Core\Api\Exceptions\WrongApiConfigurationException;
use InerziaSoft\Core\Db\DbLayers\DatabaseLayer;
use InerziaSoft\Core\Db\DbLayers\PostgreSql\PostgresSqlDbLayer;
use InerziaSoft\Core\Definitions\ClassNameInitializable;

/**
 * Class Api
 *
 * This class represents an API class.
 * Subclasses that would like to offer API services must be extended from this class.
 *
 * In order to get automatic routes initialization, you should provide an overridden version of this class
 * in your own namespace and then extend from that.
 *
 * @package InerziaSoft\Api
 */
abstract class Api implements ClassNameInitializable {
	
	/**
	 * Instantiates a new API with its representing class.
	 *
	 * @ignore
	 *
	 * @param $className string: The name of the class that represents this page.
	 * @return Api : a subclass of Api.
	 * @throws UnexistingApiException : If the specified class cannot be found.
	 * @throws WrongApiConfigurationException
	 */
    static function instanceFromClassName($className) {
    	if (!defined("API_CLASSES_NAMESPACE")) {
    		throw new WrongApiConfigurationException("API_CLASSES_NAMESPACE");
	    }
    	
        $classNameWithNamespace = API_CLASSES_NAMESPACE.$className;
        $classNameWithGeneric = GENERIC_API_NAMESPACE.$className;

        if (class_exists($classNameWithNamespace)) {
            return new $classNameWithNamespace();
        }
        else if (class_exists($classNameWithGeneric)) {
            return new $classNameWithGeneric();
        }
        else {
            throw new UnexistingApiException($className);
        }
    }

	/**
	 * Returns the DbLayer.
	 *
	 * @ignore
	 *
	 * @discussion Systems that do not use the standard DbLayer
	 * can override this method to return a different object.
	 *
	 * @return DatabaseLayer.
	 */
    static function dbLayer() {
        return new PostgresSqlDbLayer();
    }

    /**
     * Returns the route name of this API group.
     *
     * @ignore
     *
     * @discussion The default implementation returns
     * null, leaving to the the caller.
     *
     * @return string|null
     */
    static function getRouteName() {
        return null;
    }

}