<?php

/*
MIT License

Copyright (c) 2016 Alessio Moiso - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Api\Outcomes;

use InerziaSoft\Core\Db\DbObject\Exceptions\DatabaseMisconfigurationException;

/**
 * Class DatabaseError
 *
 * Represents an error thrown by the underlying database.
 *
 * @package InerziaSoft\Api
 */
class DatabaseError extends Error {
	
	static $DatabaseExceptionError = "DatabaseConfigurationError";
	static $DatabaseExceptionErrorCode = -1003;
	
	static function fromException($exception) {
		if ($exception instanceof DatabaseMisconfigurationException) {
			return new self(self::$DatabaseExceptionError, self::$DatabaseExceptionErrorCode, $exception);
		}
		
		throw new \InvalidArgumentException("Unable to instantiate \\InerziaSoft\\Api\\DatabaseError from ".get_class($exception));
	}
	
}