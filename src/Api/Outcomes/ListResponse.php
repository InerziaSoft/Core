<?php

namespace InerziaSoft\Core\Api\Outcomes;

use InerziaSoft\Core\Parser\JsonParser;

/**
 * Class ListResponse
 *
 * Implements a standard response compatible with ObjectLists.
 *
 * @package InerziaSoft\Api
 */
class ListResponse extends Response {
	/**
	 * @var array: The list of items.
	 */
	public $items;
	
	/**
	 * @var int: The count of $items.
	 */
	public $count;
	
	/**
	 * Initializes a ListResponse with a list of items.
	 *
	 * @param array $items Should be an array with a single element: the list of items.
	 * @discussion This method is required to declare $items as an array of parameters
	 * because it must comply with the overridden method, but only the first
	 * parameter is considered.
	 * @discussion If the items inside $items[0] are subclasses of Response, they will be
	 * converted to array by calling toArray() on each of them.
	 * @return ListResponse|Response
	 */
	public static function response(...$items) {
		$itemsList = $items[0];
		
		return parent::response($itemsList, count($itemsList));
	}
	
	public static function fromJson($json, $responseClassKey, $responseClassName) {
		$mappedItems = [];
		foreach ($json["items"] as $item) {
			array_push($mappedItems, (new JsonParser($item, $responseClassKey, $responseClassName))->parse());
		}
		
		return ListResponse::response($mappedItems);
	}
}