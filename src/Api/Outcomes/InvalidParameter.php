<?php

/*
MIT License

Copyright (c) 2016 Alessio Moiso - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Api\Outcomes;
use InerziaSoft\Core\Api\Exceptions\InvalidParameterException;
use InerziaSoft\Core\Api\Exceptions\InvalidTokenException;

/**
 * Represents an error returned by an API call which does not contain the requested number of parameters
 * or the passed values do not comply with the requirements of the API call.
 *
 * Class InvalidParameter
 * @package InerziaSoft\Api
 */
class InvalidParameter extends Error {

	static $NullOrInvalidParameterError = "NullOrInvalidParameter";
	static $NullOrInvalidTokenError = "NullOrInvalidToken";
	static $NullOrInvalidParameterErrorCode = -1001;
	static $NullOrInvalidTokenErrorCode = -1002;

	private $parameterName;
	private $expectedValueType;
	private $exception;
	private $receivedValue;
	
	const InvalidParameterStatusCode = 400;

	/**
	 * Returns an InvalidParameter instance from an InvalidParameterException object.
	 *
	 * @param $exception InvalidParameterException: the exception to get data from.
	 * @throws \InvalidArgumentException if $exception is not an InvalidParameterException.
	 * @return InvalidParameter|Unauthorized a new instance that can be returned as an API response.
	 */
	static function fromException($exception) {
		if ($exception instanceof InvalidTokenException) {
			return new Unauthorized(self::$NullOrInvalidTokenError, self::$NullOrInvalidTokenErrorCode, $exception);
		}
		else if ($exception instanceof InvalidParameterException) {
			return new self($exception->getParameterName(), $exception->getExpectedType(), $exception, $exception->getReceivedValue());
		}

		throw new \InvalidArgumentException("Unable to instantiate \\InerziaSoft\\Api\\InvalidParameter from ".get_class($exception));
	}
	
	/**
	 * InvalidParameter constructor.
	 *
	 * Initializes a new instance with the name of the wrong parameter,
	 * the expected type and an optional exception raised by the API.
	 *
	 * @param $parameterName string: The name of the parameter which raised the error.
	 * @param $expectedValueType string: A PHP/JSON type that the API call expects.
	 * @param $exception \Exception: an exception raised by the API call (optional).
	 * @param $receivedValue string: The received value.
	 */
	function __construct($parameterName, $expectedValueType, $exception = null, $receivedValue = null) {
		parent::__construct(self::$NullOrInvalidParameterError, self::$NullOrInvalidParameterErrorCode);
		$this->parameterName = $parameterName;
		$this->expectedValueType = $expectedValueType;
		$this->exception = $exception;
		$this->receivedValue = $receivedValue;
	}
	
	function getStatusCode() {
		if (defined("API_RESPONSE_BAD_REQUEST")) {
			return API_RESPONSE_BAD_REQUEST;
		}
		
		return InvalidParameter::InvalidParameterStatusCode;
	}
	
	function toArray() {
		$array = parent::toArray();

		$array["parameterName"] = $this->parameterName;
		$array["expectedValueType"] = $this->expectedValueType;

		if (isset($this->exception)) {
			$array["internalException"] = $this->exception->getMessage();
		}
		
		$array["receivedValue"] = $this->receivedValue;
		
		return $array;
	}

}
