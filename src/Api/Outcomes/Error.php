<?php

/*
MIT License

Copyright (c) 2016 Alessio Moiso - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Api\Outcomes;

/**
 * Class Error
 *
 * Represents a generic error thrown by an API command.
 *
 * @package InerziaSoft\Api
 */
class Error implements Outcome {
	
	private $name;
	private $code;
	private $innerException;
	
	const ErrorStatusCode = 500;
	
	/**
	 * Error constructor.
	 *
	 * Instantiates a new error with a display name, a dedicated code and, if available, an internal exception.
	 *
	 * @param $name string: The display name of the error.
	 * @param $code int: The dedicated code of the error.
	 * @param $innerException \Exception: the internal exception that raised this error (optional)
	 */
	function __construct($name, $code, $innerException = null) {
		$this->code = $code;
		$this->name = $name;
		$this->innerException = $innerException;
	}
	
	function getStatusCode() {
		if (defined("API_RESPONSE_ERROR")) {
			return API_RESPONSE_ERROR;
		}
		
		return Error::ErrorStatusCode;
	}
	
	function toArray() {
		$innerExceptionMessage = "An internal exception has been raised";
		if ($this->innerException != null && strlen($this->innerException->getMessage()) > 0) {
			$innerExceptionMessage = $this->innerException->getMessage();
		}
		
		return ["status" => $this->getStatusCode(), "error" => $this->name, "errorCode" => $this->code, "internalException" => $innerExceptionMessage];
	}
	
}