<?php

namespace InerziaSoft\Core\Api\Outcomes;

use InerziaSoft\Core\Logger\Logger;
use InerziaSoft\Core\Parser\JsonInitializable;
use InerziaSoft\Core\Parser\JsonParser;
use InerziaSoft\Core\Parser\JsonReflector;

/**
 * Class Response
 *
 * A response is an abstract class that wraps a JSON response into a PHP class.
 *
 * @discussion You must subclass this abstract class to implement specific responses in your API.
 *
 * @package InerziaSoft\Api
 */
abstract class Response extends JsonReflector implements \ArrayAccess {
	
	public $status = 200;
	
//	public static function fromJson($json) {
//		foreach ($json as $key => $item) {
//			$json[$key] = (new JsonParser($item))->parse();
//		}
//
//		return static::response($json);
//	}
	
	protected function __construct() { }
	
	/**
	 * Initializes a Response using an array of parameters
	 * or a single indexed array of keys/values.
	 *
	 * @param array ...$pars
	 * @return static
	 */
	public static function response(...$pars) {
		$instance = new static();
		
		$reflectionClass = new \ReflectionClass($instance);
		
		$properties = $reflectionClass->getProperties(\ReflectionProperty::IS_PUBLIC);
		
//		if (count($pars) == 1 && is_array($pars[0])) {
//			$arrayPars = $pars[0];
//
//			foreach ($properties as $property) {
//				$propertyName = strtolower($property->getName());
//
//				if (array_key_exists($propertyName, $arrayPars)) {
//					$property->setValue($instance, $arrayPars[$propertyName]);
//				}
//			}
//		} else if (count($pars) > 0) {
//
//		}

        $i = 0;
        foreach ($properties as $property) {
            if ($i < count($pars)) {
                $property->setValue($instance, $pars[$i]);

                $i++;
            } else {
                break;
            }
        }

		return $instance;
	}
	
	function toArray() {
		$reflectionClass = new \ReflectionClass($this);
		
		$array = [];
		foreach ($reflectionClass->getProperties(\ReflectionProperty::IS_PUBLIC) as $property) {
			$stringDoc = $property->getDocComment();
			$phpDocs = $this->getAttributesFromComment($stringDoc)[0];
			
			$canEncode = true;
			foreach ($phpDocs as $doc) {
				if (strlen($doc) == 0) continue;
				
				if (strpos($doc, "@options") !== false && strpos(strtolower($doc), strtolower("preventEncoding")) !== false) {
					$canEncode = false;
					break;
				}
			}
			
			if ($canEncode) {
				$value = $this->encodeValue($property->getValue($this));
				
				$array[strtolower($property->getName())] = $value;
			}
		}
		
		$array["responseClass"] = get_class($this);
		
		return $array;
	}
	
	private function encodeValue($value) {
		if ($value instanceof Response) {
			$value = $value->toArray();
		}
		else if ($value instanceof Outcome) {
			$value = $value->toArray();
		}
		else if (is_array($value)) {
			$convertedValue = [];
			foreach ($value as $valueItem) {
				array_push($convertedValue, $this->encodeValue($valueItem));
			}
			$value = $convertedValue;
		}
		
		return $value;
	}
	
	private static function getAttributesFromComment($phpComment) {
		$pattern = "#(@[a-zA-Z]+\\s*[a-zA-Z0-9, ()_].*)#";
		
		preg_match_all($pattern, $phpComment, $matches, PREG_PATTERN_ORDER);
		
		return $matches;
	}
	
	function offsetGet($offset) {
		$reflectionClass = new \ReflectionClass($this);
		
		foreach ($reflectionClass->getProperties(\ReflectionProperty::IS_PUBLIC) as $property) {
			if (strtolower($offset) == strtolower($property->getName())) {
				return $property->getValue($this);
			}
		}
		
		return null;
	}
	
	function offsetExists($offset) {
		$reflectionClass = new \ReflectionClass($this);
		
		foreach ($reflectionClass->getProperties(\ReflectionProperty::IS_PUBLIC) as $property) {
			if (strtolower($offset) == strtolower($property->getName())) {
				return true;
			}
		}
		
		return false;
	}
	
	function offsetSet($offset, $value) {
		$reflectionClass = new \ReflectionClass($this);
		
		foreach ($reflectionClass->getProperties(\ReflectionProperty::IS_PUBLIC) as $property) {
			if (strtolower($offset) == strtolower($property->getName())) {
				$property->setValue($this, $value);
			}
		}
	}
	
	function offsetUnset($offset) {
		$this->offsetSet($offset, null);
	}
	
}