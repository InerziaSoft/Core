<?php

namespace InerziaSoft\Core\Api;

use InerziaSoft\Core\Api\Authentication\SessionToken;
use InerziaSoft\Core\Api\Exceptions\InvalidParameterException;
use InerziaSoft\Core\Api\Exceptions\InvalidTokenException;
use InerziaSoft\Core\Exceptions\InvalidArgumentException;

class Checker {

	private $parameterName;
	
	/**
	 * Automatically checks the validity of a given token.
	 *
	 * @param $token string
	 * @return SessionToken|null
	 * @throws InvalidTokenException
	 */
	public static function forToken($token) {
		return static::for("token")->checkToken($token);
	}

	/**
	 * Convenience initializer to check a parameter.
	 *
	 * @param $parameterName string
	 * @return static
	 */
	public static function for($parameterName) {
		return new static($parameterName);
	}

	/**
	 * Checker constructor.
	 *
	 * Initialize a Checker with a parameter name.
	 *
	 * @param $parameterName string
	 * @throws InvalidArgumentException
	 */
	public function __construct($parameterName) {
		if (!isset($parameterName)) throw new InvalidArgumentException("parameterName", static::class);

		$this->parameterName = $parameterName;
	}
	
	/**
	 * Checks that the passed parameter is not null.
	 *
	 * @param $par mixed: A potential nullable parameter.
	 * @return mixed
	 * @throws InvalidParameterException if $par is null.
	 */
	function checkNotNull($par) {
		if (!isset($par)) {
			throw new InvalidParameterException($this->parameterName, "notNull", $par);
		}
		
		return $par;
	}
	
	/**
	 * Checks standard requirements of a string parameter.
	 *
	 * @discussion Requirements of a string parameter are satisfied if it's not null and its length is greater than zero.
	 *
	 * @param $par mixed: A string parameter.
	 * @return string
	 * @throws InvalidParameterException if $par does not satisfy requirements.
	 */
	function checkString($par) {
		if (!isset($par) || !is_string($par) || strlen($par) == 0) {
			throw new InvalidParameterException($this->parameterName, "string", $par);
		}

		return $par;
	}

	/**
	 * Checks standard requirements of a token parameter.
	 *
	 * @param $token mixed: A token parameter.
	 * @param $validForIntegration mixed: The expected integration type for the token.
	 *
	 * @return SessionToken|null If all the checks passed
	 * @throws InvalidTokenException
	 *
	 * @discussion Requirements of a token parameter are exactly the same as a string parameter.
	 * The InerziaSoft\Api\SessionToken class will be used to perform a real active check on the token.
	 *
	 */
	function checkToken($token, $validForIntegration = null) {
		if (!isset($token) || strlen($token) == 0) {
			throw new InvalidTokenException($this->parameterName, "token");
		}

		try {
			$token = SessionToken::fromJWT($token);

			if (isset($validForIntegration)) {
				if ($token->getIntegrationType() != $validForIntegration) {
					throw new InvalidTokenException($this->parameterName, "token valid for integration '$validForIntegration'");
				}
			}

			return $token;
		}
		catch (\Exception $ex) {
			if ($ex instanceof InvalidTokenException) {
				throw $ex;
			}
			else {
				throw new InvalidTokenException($this->parameterName, "token");
			}
		}
	}
	
	/**
	 * Checks standard requirements of a string parameters, then check the validity of an email address using PHP functions.
	 *
	 * @param $email mixed: An email address.
	 * @discussion Requirements of an email parameter are satisfied when both requirements of a string parameter and
	 * validity of the email address are satisfied.
	 * @return string
	 * @throws InvalidParameterException If $par does not satisfy requirements.
	 */
	function checkEmail($email) {
		self::checkString($email);

		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			throw new InvalidParameterException($this->parameterName, "email", $email);
		}
		
		return $email;
	}

	static $IdentifierTypeString = 0;
    static $IdentifierTypeEmail = 1;
    static $IdentifierTypeUsername = 2;
    static $IdentifierTypeDatabaseId = 3;

    /**
     * Validates the uniqueness of a specified parameter against a database table.
     *
     * @param $identifier string|int: A string or an integer to check for uniqueness.
     * @param $type int: The type of the identifier (see the constants above for accepted values).
     * @param $table string: The database table name.
     * @param $column string: The database column name on which the $identifier should be unique (optional: if not specified, the parameter name will be used).
     *
     * @deprecated 2.0: This function only works with PostgreSQL databases. Please, use a DbObject instead.
     *
     * @discussion To access the database, the InerziaSoft\Db\DbLayer class must be available.
     * @throws InvalidParameterException If $identifier does not satisfy requirements.
     */
	function checkUniqueness($identifier, $type, $table, $column = null) {
	    $comparisonOp = "";
		if (!isset($column)) $column = strtolower($this->parameterName);

        switch ($type) {
            case self::$IdentifierTypeEmail:
                $this->checkEmail($identifier);
                $comparisonOp = "ILIKE";
                break;
            case self::$IdentifierTypeUsername:
            case self::$IdentifierTypeString:
                $this->checkString($identifier);
                $comparisonOp = "ILIKE";
                break;
            case self::$IdentifierTypeDatabaseId:
                $this->checkDatabaseId($identifier);
                $comparisonOp = "=";
                break;
        }

        $dbLayer = Api::dbLayer();
		/** @noinspection SqlNoDataSourceInspection */
		$query = "SELECT {$column} FROM {$table} WHERE {$column} {$comparisonOp} $1";
        $stmt = $dbLayer->prepare($query);
        $result = $stmt->execute([$identifier]);

        if ($result === false) {
            throw new \InvalidArgumentException("Unable to perform query: {$query}");
        }
        else if (count($result) > 0) {
            throw new InvalidParameterException($this->parameterName, "unique value of type {$type}", $identifier);
        }
    }

    private static $MinimumPasswordLength = 8;
	
	/**
	 * Checks if the specified password satisfies default requirements.
	 *
	 * @param $par string: A proposed password.
	 * @discussion The default implementation of this method only checks if the
	 * specified password is longer than $MinimumPasswordLength characters.
	 * Subclasses are invited to override this method to implement custom
	 * password verification techniques.
	 * @return string
	 * @throws InvalidParameterException If $par does not satisfy requirements.
	 */
    function checkPassword($par) {
        $this->checkString($par);

        if (strlen($par) < self::$MinimumPasswordLength) {
            throw new InvalidParameterException($this->parameterName, "password");
        }
        
        return $par;
    }
	
	/**
	 * Checks standard requirements of an array parameter.
	 *
	 * @param $array mixed: An array object.
	 * @discussion A valid array has one or more items inside.
	 * @return mixed
	 * @throws InvalidParameterException If $par does not satisfy requirements.
	 */
	function checkArray($array) {
	    if (count($array) == 0) {
	        throw new InvalidParameterException($this->parameterName, "array");
        }
        
        return $array;
    }
	
	/**
	 * Checks standard requirements for a numeric value.
	 *
	 * @discussion A valid numeric value is an object that PHP can interpret or cast as a number.
	 *
	 * @param $number mixed: A proposed number.
	 * @return float
	 * @throws InvalidParameterException If $par does not satisfy requirements.
	 */
	function checkNumber($number) {
		if (!is_numeric($number)) {
			throw new InvalidParameterException($this->parameterName, "number", $number);
		}
		
		return doubleval($number);
	}
	
	/**
	 * Checks a numeric value against a specified range.
	 *
	 * @discussion If $number is either less than $minimum or greater than $maximum,
	 * the check will fail.
	 *
	 * @param $number int
	 * @param $minimum int
	 * @param $maximum int
	 * @return float|int
	 * @throws InvalidParameterException If $number does not satisfy requirements.
	 */
	function checkRange($number, $minimum, $maximum) {
		$number = $this->checkNumber($number);
		
		if ($number < $minimum) {
			throw new InvalidParameterException($this->parameterName, "number in range [$minimum, $maximum]");
		}
		else if ($number > $maximum) {
			throw new InvalidParameterException($this->parameterName, "number in range [$minimum, $maximum]");
		}
		
		return $number;
	}
	
	/**
	 * Checks standard requirements for a database ID.
	 *
	 * @discussion A valid database ID is an integer number greater than zero.
	 *
	 * @param $id mixed: A proposed database ID.
	 * @return int
	 * @throws InvalidParameterException If $par does not satisfy requirements.
	 */
	function checkDatabaseId($id) {
		$id = self::checkNumber($id);

		if ($id <= 0) {
			throw new InvalidParameterException($this->parameterName, "databaseId", $id);
		}
		
		return $id;
	}
	
	/**
	 * Checks standard requirements for a date.
	 *
	 * @discussion A valid date is a point in time in one of the following formats:
	 *  - Y-m-d
	 *  - Y-m-d H:i:s
	 *  - ISO 8601
	 *
	 * @param $date
	 * @return mixed
	 * @throws InvalidParameterException
	 */
	function checkDate($date) {
		$allowedFormats = ["Y-m-d", "Y-m-d H:i:s", "Y-m-d H:i:s.u", \DateTime::ISO8601, \DateTime::W3C, \DateTime::ATOM];
		
		$dateTime = false;
		$tryIndex = 0;
		$maximumIndex = count($allowedFormats);
		while ($dateTime === false) {
			if ($tryIndex >= $maximumIndex) {
				throw new InvalidParameterException($this->parameterName, "date", $date);
			}
			else {
				$dateTime = \DateTime::createFromFormat($allowedFormats[$tryIndex], $date);
				$tryIndex++;
			}
		}
		
		return $dateTime;
	}

}