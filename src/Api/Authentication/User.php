<?php

namespace InerziaSoft\Core\Api\Authentication;

use InerziaSoft\Core\Api\Authentication\Responses\AuthenticationSuccessfulResponse;

interface User {
	
	/**
	 * Returns a user (if any) with the specified email.
	 *
	 * @param $identifier string: A valid unique identifier for the user (depending on the login system, this might be an email or a username).
	 * @return User
	 */
	public static function userWithIdentifier($identifier);
	
	/**
	 * If the application supports passwords double-hashing, this method should return an hashed version of an existing hash.
	 *
	 * @param $password string: An hash of a valid password.
	 * @return array An indexed array containing "hash" and "salt" as keys.
	 */
	public static function hashPassword($password);
	
	/**
	 * Delegates business logic to check the authentication of a user against a specified password.
	 *
	 * @param $password
	 * @return mixed
	 */
	public function validatePassword($password);
	
	/**
	 * Returns the id value of a User.
	 *
	 * @discussion This value will be used to instantiate a SessionToken related to this user.
	 * @return int
	 */
	public function getUniqueIdentifier();
	
	/**
	 * Delegates updating the underlying database to reflect
	 * the last successful login attempt.
	 *
	 * If your system do not need this information,
	 * just override this method and leave it empty.
	 *
	 * @param $date \DateTime
	 */
	public function updateLastSuccessfulLogin($date);
	
	/**
	 * The User interface is called to verify if an user
	 * is allowed to login.
	 *
	 * Use this method to implement your custom logic
	 * to prevent users from log in based on a specific
	 * condition.
	 *
	 * If your system do not need this, just return true.
	 *
	 * @return bool
	 */
	public function verifyLoginAllowance();
	
	/**
	 * If the login operation is successful,
	 * your system can append additional data to the
	 * response, by returning a subclass of
	 * \InerziaSoft\Api\AuthenticationSuccessfulResponse.
	 *
	 * If your system does not need this, just return null.
	 *
	 * @param $jwt: The AuthenticationSuccessfulResponse MUST include the JWT
	 * specified here, otherwise the whole authentication will not work.
	 * This parameter should be appended to the response() method.
	 *
	 * @return AuthenticationSuccessfulResponse
	 */
	public function successfulResponse($jwt);
	
	/**
	 * After a successful login, if this method returns true, all
	 * previously existing sessions are cleared from the system.
	 *
	 * @return bool
	 */
	public function supportsMultipleSessions();
}