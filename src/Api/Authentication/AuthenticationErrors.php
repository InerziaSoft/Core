<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
namespace InerziaSoft\Core\Api\Authentication;

class AuthenticationErrors {
	static $NotAuthenticatedError = "NotAuthenticated";
	static $NotAuthenticatedErrorCode = -41001;
	
	static $WakeSessionError = "WakeSession";
	static $WakeSessionErrorCode = -41002;
	
	static $UnexistingUserError = "UnexistingUser";
	static $UnexistingUserErrorCode = -41003;
	
	static $InvalidPublicKeyError = "UnknownOrInvalidPublicKey";
	static $InvalidPublicKeyErrorCode = -41099;
	
	static $LoginError = "LoginError";
	static $LoginErrorCode = -41006;
	
	static $InvalidCredentialsError = "InvalidCredentials";
	static $InvalidCredentialsErrorCode = -41007;
	
	static $MalformedTokenError = "MalformedToken";
	static $MalformedTokenErrorCode = -41008;
	
	static $LogoutError = "Logout";
	static $LogoutErrorCode = -41009;
	
	static $LoginNotAllowed = "LoginNotAllowed";
	static $LoginNotAllowedErrorCode = -41010;
}