<?php

/*
MIT License

Copyright (c) 2016 Alessio Moiso - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Core\Api\Authentication;
use InerziaSoft\Core\Api\Api;
use InerziaSoft\Core\Api\Authentication\Exceptions\MalformedTokenException;
use InerziaSoft\Core\Api\Authentication\Exceptions\UnauthorizedUserException;
use InerziaSoft\Core\Api\Authentication\Exceptions\WakeTokenException;
use InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseException;
use InerziaSoft\Core\Db\DbLayers\PostgreSql\PostgresSqlDbLayer;
use InerziaSoft\Core\Utils\UriUtils;

/**
 * Class SessionToken
 *
 * This class represents a SessionToken in the system.
 * It is backed by a "session" table on the underlying database which must be configured as per the following script:
 *
	CREATE TABLE IF NOT EXISTS session (
		token         VARCHAR PRIMARY KEY,
		expiration    TIMESTAMP NOT NULL,
	    lastActivity  TIMESTAMP NOT NULL,
		person        INTEGER REFERENCES person(id),
        integration   VARCHAR
	);
 *
 * @package InerziaSoft\Api
 */
class SessionToken {
	
	const ConfigFilename = "auth.ini";

	private static $TokenIssuerName = "TokenIssuerName";
	private static $TokenJWTKey = "TokenJWT";
	private static $AllowedPublicKeysKey = "AllowedPublicKeys";
	private static $TokenWakeInterval = "TokenWakeInterval";
	private static $DbLayer = "DbLayer";

	private $userId;
	private $token;
	private $publicKey;
	private $integrationType;

	private static function getDbLayer() {
		$config = UriUtils::getConfig(static::ConfigFilename);
		
		if ($config !== false && array_key_exists(SessionToken::$DbLayer, $config)) {
			return new $config[SessionToken::$DbLayer]();
		}
		
		return Api::dbLayer();
	}
	
	public static function clearSessionsForId($userId) {
		$db = static::getDbLayer();
		
		$query = "";
		if ($db instanceof PostgresSqlDbLayer) {
			$query = AuthenticationQueries::$DeleteSessionsQuery;
		}
		else {
			$query = AuthenticationQueries::$MySqlDeleteSessionsQuery;
		}
		
		$stmt = $db->prepare($query);
		$result = $stmt->execute([$userId]);
		
		if ($result === false) {
			throw new DatabaseException($query, $db->getLastError());
		}
		
		return $result;
	}
	
	public static function fromPublicKey($key, $integrationType) {
		$config = UriUtils::getConfig(self::ConfigFilename);
		
		$allowedKeys = explode(",", $config[SessionToken::$AllowedPublicKeysKey]);
		
		$granted = false;
		foreach ($allowedKeys as $allowedKeyFile) {
			$diskKey = @file_get_contents(UriUtils::getRootPath()."/keys/".$allowedKeyFile);
			
			if ($diskKey !== false && trim($diskKey) == $key) {
				$granted = true;
			}
		}
		
		if ($granted) {
			return new SessionToken(null, null, $key, $integrationType);
		}
		else {
			throw new UnauthorizedUserException("Unknown or invalid public key!");
		}
	}
	
	public static function fromJWT($jwt) {
		$config = UriUtils::getConfig(self::ConfigFilename);
		try {
			$token = \JWT::decode($jwt, $config[SessionToken::$TokenJWTKey], ['HS512']);
		}
		catch (\Exception $ex) {
			throw new MalformedTokenException($ex);
		}

		$db = static::getDbLayer();

		$query = "";
		if ($db instanceof PostgresSqlDbLayer) {
			$query = AuthenticationQueries::$SelectNonExpiredTokenQuery;
		}
		else {
			$query = AuthenticationQueries::$MySqlSelectNonExpiredTokenQuery;
		}
		
		$stmt = $db->prepare($query);
		$result = $stmt->executeReturningFirstRow([$token->jti]);

		if (count($result) > 0) {
			$query = "";
			if ($db instanceof PostgresSqlDbLayer) {
				$query = AuthenticationQueries::$UpdateTokenExpirationQuery;
			}
			else {
				$query = AuthenticationQueries::$MySqlUpdateTokenExpirationQuery;
			}
			
			$stmt = $db->prepare($query);

			$newExpirationDate = self::defaultSessionExpirationDateFromNow()->format("Y-m-d H:i:s");
			$resultExpirationDate = $stmt->execute([$newExpirationDate, $token->jti]);

			if ($resultExpirationDate === false) {
				throw new WakeTokenException();
			}

			return new self($result["person"], $token->jti);
		}
		else {
			throw new UnauthorizedUserException("Session token looks invalid!");
		}
	}

	public function __construct($userId = null, $token = null, $publicKey = null, $integrationType = null) {
		$this->userId = $userId;
		$this->publicKey = $publicKey;
		$this->integrationType = $integrationType;

		if ($token == null) {
			$this->token = base64_encode(self::randomString(16).($this->userId ?? self::randomString(4)).self::randomString(16));
		}
		else {
			$this->token = $token;
		}
	}

	public function getToken() {
		return $this->token;
	}

	public function getUserId() {
		return $this->userId;
	}
	
	public function getIntegrationType() {
		return $this->integrationType;
	}

	public function store() {
		$sessionTokenString = $this->getToken();

		$db = static::getDbLayer();

		$query = "";
		if ($db instanceof PostgresSqlDbLayer) {
			$query = AuthenticationQueries::$InsertSessionQuery;
		}
		else {
			$query = AuthenticationQueries::$MySqlInsertSessionQuery;
		}
		
		$stmt = $db->prepare($query);
		$result = $stmt->execute([$sessionTokenString, self::defaultSessionExpirationDateFromNow()->format("Y-m-d H:i:s"), $this->userId, $this->integrationType]);

        if ($result === false) {
            throw new DatabaseException($query, $db->getLastError());
        }

		return $result;
	}

	public function encode() {
		$jwtKey = UriUtils::getConfig(self::ConfigFilename);

		$issuedAt = time();
		$issuer = $jwtKey[SessionToken::$TokenIssuerName];

		$data = [
			"iat" => $issuedAt,
			"jti" => $this->token,
			"iss" => $issuer,
			"nbf" => time(),
			'data' => [
				'userId' => $this->userId,
				'integrationType' => $this->integrationType
			]
		];

		$secretKey = base64_decode($jwtKey[SessionToken::$TokenJWTKey]);

		$jwt = \JWT::encode($data, $secretKey, "HS512");
		return ["jwt" => $jwt];
	}

	/**
	 * Returns a cryptographic secure random string.
	 *
	 * @param $length int: the length of the string.
	 * @param string $keySpace: the allowed characters to be used.
	 * @return string a random string.
	 */
	public static function randomString($length, $keySpace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') {
		$str = '';
		$max = mb_strlen($keySpace, '8bit') - 1;
		for ($i = 0; $i < $length; ++$i) {
			$str .= $keySpace[random_int(0, $max)];
		}
		return $str;
	}

	private static function defaultSessionExpirationDateFromNow() {
		$config = UriUtils::getConfig(self::ConfigFilename);
		
		$interval = 'P30D';
		if (array_key_exists(self::$TokenWakeInterval, $config)) {
			$interval = $config[self::$TokenWakeInterval];
		}
		
		$date = new \DateTime(null, new \DateTimeZone("Europe/Rome"));
		$date->add(new \DateInterval($interval));

		return $date;
	}

}