<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
namespace InerziaSoft\Core\Api\Authentication;

use InerziaSoft\Core\Api\Api;
use InerziaSoft\Core\Api\Authentication\Exceptions\UnauthorizedUserException;
use InerziaSoft\Core\Api\Authentication\Responses\AuthenticationSuccessfulResponse;
use InerziaSoft\Core\Api\Checker;
use InerziaSoft\Core\Api\Exceptions\InvalidParameterException;
use InerziaSoft\Core\Api\Outcomes\DatabaseError;
use InerziaSoft\Core\Api\Outcomes\Error;
use InerziaSoft\Core\Api\Outcomes\InvalidParameter;
use InerziaSoft\Core\Api\Outcomes\Success;
use InerziaSoft\Core\Api\Outcomes\Unauthorized;
use InerziaSoft\Core\Db\DbLayers\Exceptions\DatabaseException;
use InerziaSoft\Core\Db\DbObject\Exceptions\DatabaseMisconfigurationException;
use InerziaSoft\Core\Db\DbObject\Exceptions\UnexistingDbObjectException;
use InerziaSoft\Core\Exceptions\DependencyInjectionMisconfigurationException;
use InerziaSoft\Core\Utils\UriUtils;

define("DEPENDENCY_INJECTION_USER_CLASS_KEY", "AuthenticationPersonClassName");

/**
 * Class Authentication
 *
 * Handles authentication with the client.
 *
 * @discussion To each client that wants to be authenticated is assigned
 * a Unique Identifier that is registered as encrypted cookie on the client
 * and in the Session table on the server.
 * Each token gets an end date which determines when it wont't be valid anymore.
 * This date is increased on any operation that calls the wakeAuthentication() method.
 *
 * @test authenticationTest.php
 * @package InerziaSoft\Api
 */
class Authentication extends Api {
	
	public static function getRouteName() {
		return "auth";
	}
	
	/**
	 * Returns the correct class that implements the User interface.
	 *
	 * @discussion The class name is defined in a config.ini file in the parent folder of this file.
	 * @return User A class complaint with the User interface
	 * @throws DependencyInjectionMisconfigurationException If the class cannot be found.
	 */
	private static function userClass() {
		$config = UriUtils::getConfig(SessionToken::ConfigFilename);
		
		if (isset($config[DEPENDENCY_INJECTION_USER_CLASS_KEY]) && class_exists($config[DEPENDENCY_INJECTION_USER_CLASS_KEY])) {
			return $config[DEPENDENCY_INJECTION_USER_CLASS_KEY];
		}
		else {
			throw new DependencyInjectionMisconfigurationException(User::class);
		}
	}
	
	/**
	 * @route login
	 * @method POST
	 *
	 * @param $identifier
	 * @synonyms $identifier: $username, $email
	 *
	 * @param $password
	 * @return Error|InvalidParameter|Success|Unauthorized
	 */
	public static function authenticate($identifier, $password) {
		try {
			try {
				Checker::for("email")->checkEmail($identifier);
			}
			catch (InvalidParameterException $ex) {
				Checker::for("username")->checkString($identifier);
			}
		}
		catch (InvalidParameterException $ex) {
			return InvalidParameter::fromException($ex);
		}
		
		$user = null;
		try {
			$user = self::userWithIdentifier($identifier);
		}
		catch (Exceptions\UnexistingUserException $ex) {
			return new Error(AuthenticationErrors::$UnexistingUserError, AuthenticationErrors::$UnexistingUserErrorCode, $ex);
		}
		catch (DatabaseMisconfigurationException $ex) {
			return DatabaseError::fromException($ex);
		}
		
		return self::authenticateUser($user, $password);
	}

    /**
     * @route login/key
     * @method POST
     *
     * @param $publicKey string
     * @param $systemType string
     * @return DatabaseError|Error|Success
     */
	public static function authenticateWithPublicKey($publicKey, $systemType) {
		try {
			Checker::for("publicKey")->checkString($publicKey);
			Checker::for("systemType")->checkString($systemType);
		}
		catch (InvalidParameterException $exception) {
			return InvalidParameter::fromException($exception);
		}
		
		try {
			$sessionToken = SessionToken::fromPublicKey($publicKey, $systemType);
			
			try {
				$sessionToken->store();
			}
			catch (DatabaseException $ex) {
				return new Error(AuthenticationErrors::$LoginError, AuthenticationErrors::$LoginErrorCode, $ex);
			}
			
			$jwtDict = $sessionToken->encode();
			$jwt = $jwtDict["jwt"];
			
			return new Success(AuthenticationSuccessfulResponse::response($jwt));
		}
		catch (UnauthorizedUserException $ex) {
			return new Error(AuthenticationErrors::$InvalidPublicKeyError, AuthenticationErrors::$InvalidPublicKeyErrorCode, $ex);
		}
		catch (DatabaseMisconfigurationException $ex) {
			return DatabaseError::fromException($ex);
		}
	}
	
	/**
	 * Process the authentication of a User against a specified password (if any).
	 *
	 * @discussion If $password is null, this method will assume that the credentials validation has been already
	 * performed by the caller: this results in a successful authentication.
	 *
	 * @param $user User: An object complaint with the User interface.
	 * @param $password string|null: A proposed password (optional).
	 * @param $store bool: If true, a successful login will result in a new SessionToken; otherwise, this method will just check and won't store anything.
	 * @return Error|Success
	 */
	protected static function authenticateUser($user, $password = null, $store = true) {
		if (!isset($password) || $user->validatePassword($password)) {
			if (!$user->verifyLoginAllowance()) {
				return new Error(AuthenticationErrors::$LoginNotAllowed, AuthenticationErrors::$LoginNotAllowedErrorCode);
			}
			
			$userId = $user->getUniqueIdentifier();
			
			if (!$user->supportsMultipleSessions()) {
				SessionToken::clearSessionsForId($userId);
			}
			
			$sessionToken = new SessionToken($userId);
			
			if ($store) {
				try {
					$sessionToken->store();
				}
				catch (DatabaseException $ex) {
					return new Error(AuthenticationErrors::$LoginError, AuthenticationErrors::$LoginErrorCode, $ex);
				}
			}
			
			$jwtDict = $sessionToken->encode();
			$jwt = $jwtDict["jwt"];
			$user->updateLastSuccessfulLogin(new \DateTime());
			
			$response = $user->successfulResponse($jwt);
			if ($response == null) {
				$response = Responses\AuthenticationSuccessfulResponse::response($jwt);
			}
			
			return new Success($response);
		}
		else {
			return new Error(AuthenticationErrors::$InvalidCredentialsError, AuthenticationErrors::$InvalidCredentialsErrorCode);
		}
	}
	
	/**
	 * Returns an object compliant with the User interface starting from its identifier.
	 *
	 * @param $identifier mixed: A unique identifier of the user.
	 * @return User
	 * @throws \InerziaSoft\Core\Api\Authentication\Exceptions\UnexistingUserException If the User doesn't exist.
	 */
	protected static function userWithIdentifier($identifier) {
		try {
			$user = self::userClass();
			return $user::userWithIdentifier($identifier);
		}
		catch (UnexistingDbObjectException $ex) {
			throw new Exceptions\UnexistingUserException("No user with identifier equal to '{$identifier}' can be found!");
		}
	}
	
	/**
	 * @route verify
	 * @method GET
	 *
	 * @param $token
	 * @return InvalidParameter|Success|Unauthorized
	 */
	public static function isAuthenticated($token) {
		try {
			Checker::forToken($token);
		}
		catch (InvalidParameterException $ex) {
			return InvalidParameter::fromException($ex);
		}
		
		return new Success(Responses\AuthenticationVerifyResponse::response(true));
	}
	
	/**
	 * @route logout
	 * @method POST
	 *
	 * @param $token
	 * @return Error|InvalidParameter|Success|Unauthorized
	 */
	public static function invalidateAuthentication($token) {
		try {
			$sessionToken = Checker::forToken($token);
		}
		catch (InvalidParameterException $ex) {
			return InvalidParameter::fromException($ex);
		}
		
		$db = Api::dbLayer();
		
		$stmt = $db->prepare(AuthenticationQueries::$DeleteSessionQuery);
		$result = $stmt->execute([$sessionToken->getToken()]);
		
		if ($result === false) {
			return new Error(AuthenticationErrors::$LogoutError, AuthenticationErrors::$LogoutErrorCode);
		}
		else {
			return new Success();
		}
	}
}