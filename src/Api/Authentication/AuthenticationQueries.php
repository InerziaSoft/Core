<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
namespace InerziaSoft\Core\Api\Authentication;

// FIXME: we should support any kind of database and rely on DbObjects in some ways
class AuthenticationQueries {
	static /** @noinspection SqlNoDataSourceInspection */
		/** @noinspection SqlResolve */
		$UpdateSessionExpirationQuery = "UPDATE session SET expiration = $1 WHERE token = $2";
	
	static /** @noinspection SqlNoDataSourceInspection */
		/** @noinspection SqlResolve */
		$DeleteSessionQuery = "DELETE FROM session WHERE token = $1";
		
	static /** @noinspection SqlNoDataSourceInspection */
		/** @noinspection SqlResolve */
		$SelectNonExpiredTokenQuery = "SELECT person FROM session WHERE token = $1 AND expiration > NOW()";
		
	static /** @noinspection SqlNoDataSourceInspection */
		/** @noinspection SqlResolve */
		$UpdateTokenExpirationQuery = "UPDATE session SET expiration = $1, lastactivity = NOW() WHERE token = $2";
	
	static /** @noinspection SqlNoDataSourceInspection */
		/** @noinspection SqlResolve */
		$InsertSessionQuery = "INSERT INTO session (token, expiration, person, integration) VALUES ($1, $2, $3, $4)";
		
	static /** @noinspection SqlNoDataSourceInspection */
		/** @noinspection SqlResolve */
		$DeleteSessionsQuery = "DELETE FROM session WHERE person = $1";
		
		
	static $MySqlUpdateSessionExpirationQuery = "UPDATE session SET expiration = ? WHERE token = ?";
	static $MySqlDeleteSessionQuery = "DELETE FROM session WHERE token = ?";
	static $MySqlSelectNonExpiredTokenQuery = "SELECT person FROM session WHERE token = ? AND expiration > NOW()";
	static $MySqlUpdateTokenExpirationQuery = "UPDATE session SET expiration = ?, lastactivity = NOW() WHERE token = ?";
	static $MySqlInsertSessionQuery = "INSERT INTO session (token, expiration, person, integration) VALUES (?, ?, ?, ?)";
	static $MySqlDeleteSessionsQuery = "DELETE FROM session WHERE person = ?";
}