<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
namespace InerziaSoft\Core\Api\Localization;

use InerziaSoft\Core\Api\Api;
use InerziaSoft\Core\Api\Checker;
use InerziaSoft\Core\Api\Exceptions\InvalidParameterException;
use InerziaSoft\Core\Api\Localization\Responses\DefaultLanguageResponse;
use InerziaSoft\Core\Api\Localization\Responses\KeyForLocalizationResponse;
use InerziaSoft\Core\Api\Localization\Responses\LocalizedStringResponse;
use InerziaSoft\Core\Api\Localization\Responses\MultipleLocalizedStringResponse;
use InerziaSoft\Core\Api\Outcomes\Error;
use InerziaSoft\Core\Api\Outcomes\InvalidParameter;
use InerziaSoft\Core\Api\Outcomes\ListResponse;
use InerziaSoft\Core\Api\Outcomes\Success;
use InerziaSoft\Core\Api\Outcomes\Unauthorized;
use InerziaSoft\Core\Db\DbLayers\DatabaseLayer;

/**
 * Class Language
 *
 * Handles communications with the database to retrieve localized versions of a string.
 *
 * @package InerziaSoft\Api
 */
class Language extends Api {
	
	private static $ConfigFilename = "/config/lang.ini";
	
	private static $SupportedLanguages = "Languages";
	private static $FallbackLanguage = "Fallback";
	private static $DbLayerClass = "DbLayer";
	
	private static $DefaultFallbackLanguage = "en";
	
	static function getRouteName() {
		return "language";
	}
	
	protected static function getRootPath() {
		return str_replace("index.php", "", \Base::instance()->get("ROOT").\Base::instance()->get("BASE"));
	}
	
	protected static function getConfig() {
		return parse_ini_file(static::getRootPath().self::$ConfigFilename);
	}
	
	/**
	 * Reads the lang.ini config file and returns the appropriate language,
	 * depending on whether $language is among those supported or not.
	 *
	 * @discussion If $language is null, this function assumes that the
	 * request comes from a JavaScript session, hence tries to guess the language
	 * using the HTTP_ACCEPT_LANGUAGE header.
	 *
	 * @param $language
	 * @return mixed|string
	 */
	static function getLanguageOrFallback($language = null) {
		if (!isset($language)) {
			$language = substr($_SERVER["HTTP_ACCEPT_LANGUAGE"], 0, 2);
		}
		
		$config = static::getConfig();
		
		$supportedLangs = [];
		if (array_key_exists(static::$SupportedLanguages, $config)) {
			$supportedLangs = explode(",", $config[static::$SupportedLanguages]);
		}
		
		foreach ($supportedLangs as $supportedLang) {
			$realLang = trim(strtolower($supportedLang));
			if ($realLang == trim(strtolower($language))) {
				return $realLang;
			}
		}
		
		$fallback = static::$DefaultFallbackLanguage;
		if (array_key_exists(static::$FallbackLanguage, $config)) {
			$fallback = trim(strtolower($config[static::$FallbackLanguage]));
		}
		
		return $fallback;
	}
	
	/**
	 * Returns the configured database connection (if any)
	 * or the default one.
	 *
	 * @return \InerziaSoft\Core\Db\DbLayers\DatabaseLayer|null
	 */
	private static function getDatabaseConnection() {
		$config = static::getConfig();
		$connection = null;
		if (isset($config[static::$DbLayerClass]) && class_exists($config[static::$DbLayerClass])) {
			$connection = new $config[static::$DbLayerClass]();
		}
		else {
			$connection = Api::dbLayer();
		}
		
		return $connection;
	}
	
	/**
	 * Returns whether the specified connection is
	 * based upon a MySQL database or not.
	 *
	 * @param $connection DatabaseLayer
	 * @return bool
	 */
	private static function usesQuestionMarksAsPlaceholders($connection) {
		return $connection->getParametersPlaceholder() == "?";
	}
	
	/**
	 * @route default
	 */
	static public function getDefaultLanguage() {
		$language = static::getLanguageOrFallback();
		
		return new Success(DefaultLanguageResponse::response($language));
	}
	
	/**
     * @route string
     *
     * Returns the localized string for a specified key.
     *
     * @param $key string: The key that identifies the desired string.
     * @param $page string: The title of the page which contains the desired string.
     * @param $language string: The language identifier in which the desired string should be returned (defaults to null).
     *
     * @return Success|Error
     */
    static function getLocalizedString($key, $page, $language) {
        try {
            Checker::for("key")->checkString($key);
            Checker::for("page")->checkString($page);
        }
        catch (InvalidParameterException $ex) {
	        return InvalidParameter::fromException($ex);
        }

        $language = static::getLanguageOrFallback($language);
        
        $connection = static::getDatabaseConnection();
        if (static::usesQuestionMarksAsPlaceholders($connection)) {
            $query = LanguageQueries::$MySQLSelectLocalizedStringQuery;
        }
        else {
        	$query = LanguageQueries::$SelectLocalizedStringQuery;
        }

        $stmt = $connection->prepare($query);
        $localizedString = $stmt->executeReturningFirstRow([$key, strtolower($page), $language]);
        
        if (count($localizedString) > 0 && array_key_exists("value", $localizedString) && array_key_exists("key", $localizedString)) {
            return new Success(LocalizedStringResponse::response($localizedString["key"], $localizedString["value"]));
        }
        else {
            return new Error(LanguageErrors::$KeyNotFoundError, LanguageErrors::$KeyNotFoundErrorCode);
        }
    }

    /**
     * @route strings
     * @method POST
     *
     * @param $keysAndPages
     * @param string $language
     * @return InvalidParameter|Success|Unauthorized
     */
    static function getLocalizedStrings($keysAndPages, $language) {
        try {
	        Checker::for("keysAndPages")->checkString($keysAndPages);
        }
        catch (InvalidParameterException $ex) {
	        return InvalidParameter::fromException($ex);
        }
        
        $keysAndPages = json_decode($keysAndPages, true);

        $strings = [];
        foreach ($keysAndPages as $keyAndPage) {
	        $successResponse = self::getLocalizedString($keyAndPage["key"], $keyAndPage["page"], $language);
	        if ($successResponse instanceof Success) {
		        $originalObj = $successResponse->getOriginalObject();
		        array_push($strings, $originalObj);
	        }
	        else {
		        array_push($strings, LocalizedStringResponse::response($keyAndPage["key"], $keyAndPage["key"]));
	        }
        }
        
        return new Success(MultipleLocalizedStringResponse::response($strings, count($strings)));
    }
	
	/**
	 * @route keys
	 * @method GET
	 *
	 * @param $token
	 * @param $source
	 * @param $destination
	 * @return InvalidParameter|Success|Unauthorized
	 */
    static function getKeysForLocalization($token, $source = "none", $destination = "none") {
    	try {
    		Checker::forToken($token);
    		Checker::for("source")->checkString($source);
    		Checker::for("destination")->checkString($destination);
    	}
    	catch (InvalidParameterException $exception) {
    	    return InvalidParameter::fromException($exception);
    	}
    	
    	$connection = static::getDatabaseConnection();
    	if (static::usesQuestionMarksAsPlaceholders($connection)) {
    		$query = LanguageQueries::$MySQLSelectKeysQuery;
	    }
	    else {
    		$query = LanguageQueries::$SelectKeysQuery;
	    }
	    
	    $stmt = $connection->prepare($query);
    	$results = $stmt->execute();
    	
    	$responses = [];
    	foreach ($results as $row) {
    		$key = $row["key"];
    		$page = $row["page"];
    		
    	    $response = new KeyForLocalizationResponse($key, $page);
    	    $response->source = static::optionallyGetLocalizedValue($source, $key, $page);
    	    $response->destination = static::optionallyGetLocalizedValue($destination, $key, $page);
    	    
    	    array_push($responses, $response);
	    }
	    
	    return new Success(ListResponse::response($responses));
    }
    
    private static function optionallyGetLocalizedValue($language, $key, $page) {
    	if ($language !== "none") {
    		$response = static::getLocalizedString($key, $page, $language);
    		
    		if ($response instanceof Success) {
    			return $response->getOriginalObject()->string;
		    }
	    }
	    
	    return "";
    }
	
	/**
	 * @route key
	 * @method PUT
	 *
	 * @param $token
	 * @param $key
	 * @param $page
	 * @param $language
	 * @param $newValue
	 * @return InvalidParameter|Success|Unauthorized
	 */
    static function updateKeyForLocalization($token, $key, $page, $language, $newValue) {
    	try {
    		Checker::forToken($token);
    		Checker::for("key")->checkString($key);
    		Checker::for("page")->checkString($page);
    		Checker::for("language")->checkString($language);
    	}
    	catch (InvalidParameterException $exception) {
    	    return InvalidParameter::fromException($exception);
    	}
	    
	    $connection = static::getDatabaseConnection();
    	
	    $string = static::getLocalizedString($key, $page, $language);
	    if ($string instanceof Success) {
		    if (static::usesQuestionMarksAsPlaceholders($connection)) {
			    $query = LanguageQueries::$MySQLUpdateKeyQuery;
		    }
		    else {
			    $query = LanguageQueries::$UpdateKeyQuery;
		    }
		    $pars = [$newValue, $key, $page, $language];
	    }
	    else {
		    if (static::usesQuestionMarksAsPlaceholders($connection)) {
			    $query = LanguageQueries::$MySQLInsertKeyQuery;
		    }
		    else {
			    $query = LanguageQueries::$InsertKeyQuery;
		    }
		    $pars = [$key, $page, $language, $newValue];
	    }
	    
	    $stmt = $connection->prepare($query);
	    $stmt->execute($pars);
	    
	    return new Success();
    }

}