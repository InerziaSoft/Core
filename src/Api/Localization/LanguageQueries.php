<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
namespace InerziaSoft\Core\Api\Localization;

class LanguageQueries {
	static /** @noinspection SqlNoDataSourceInspection */
		/** @noinspection SqlResolve */
		$SelectLocalizedStringQuery = "SELECT key, value FROM string WHERE key ILIKE $1 AND page ILIKE $2 AND language = $3 LIMIT 1";
		
	static $MySQLSelectLocalizedStringQuery = "SELECT `key`, `value` FROM string WHERE upper(`key`) = upper(?) AND upper(`page`) = upper(?) AND upper(`language`) = upper(?)";
	
	static $SelectKeysQuery = "SELECT key, page FROM string GROUP BY key, value";
	
	static $MySQLSelectKeysQuery = "SELECT `key`, `page` FROM string GROUP BY `key`, `page`";
	
	static $UpdateKeyQuery = "UPDATE string SET value = $1 WHERE key ILIKE $2 AND page ILIKE $3 AND language = $4";
	
	static $MySQLUpdateKeyQuery = "UPDATE string SET `value` = ? WHERE upper(`key`) = upper(?) AND upper(`page`) = upper(?) AND upper(`language`) = upper(?)";
	
	static $InsertKeyQuery = "INSERT INTO string (key, page, language, value) VALUES ($1, $2, $3, $4)";
	
	static $MySQLInsertKeyQuery = "INSERT INTO string (`key`, `page`, `language`, `value`) VALUES (?, ?, ?, ?)";
	
}