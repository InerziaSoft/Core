<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
namespace InerziaSoft\Core\Api\Exceptions;

class InvalidParameterException extends \Exception {
	private $parameterName;
	private $parameterType;
	private $receivedValue;
	
	/**
	 * InvalidParameterException constructor.
	 * @param string $parameterName
	 * @param string $expectedType
	 * @param mixed $receivedValue
	 */
	public function __construct($parameterName, $expectedType, $receivedValue = null) {
		parent::__construct("Invalid argument or non conforming type for parameter '{$parameterName}'!");
		$this->parameterName = $parameterName;
		$this->parameterType = $expectedType;
		$this->receivedValue = $receivedValue;
	}
	
	public function getParameterName() {
		return $this->parameterName;
	}
	
	public function getExpectedType() {
		return $this->parameterType;
	}
	
	public function getReceivedValue() {
		return $this->receivedValue;
	}
}