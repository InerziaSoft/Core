# Summary
Insert a brief summary of the new feature here

# Desired Behavior
Describe thoroughly how Core should behave on this new feature. This may
include information on various new functions and the workflow to use them.

# Implementation Plan
If applicable and useful, describe a possible implementation plan of this improvement (i.e. describe the technical changes to the code and/or split the improvement in steps)

# Benefits
Describe what are the benefits of this improvement, in your opinion