# Summary
Insert a brief summary of the improvement here

# Current Behavior
Describe the current behavior of Core

# Desired Behavior
Describe thoroughly how Core should behave considering the improvement

# Implementation Plan
If applicable and useful, describe a possible implementation plan of this improvement (i.e. describe the technical changes to the code and/or split the improvement in steps)

# Migration Plan
If the changes of this improvement are changing the behavior of Core in a way that breaks backward compatibility, describe a possible migration plan

# Benefits
Describe what are the benefits of this improvement, in your opinion