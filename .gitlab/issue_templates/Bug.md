# Summary
Insert a brief summary of the bug/issue/malfunction here

# Expected Behavior
Describe the behavior that Core should follow

# Actual Behavior
Describe thoroughly how Core actually behaved, because of the bug

# Test Case
Describe one or more ways to reproduce this issue