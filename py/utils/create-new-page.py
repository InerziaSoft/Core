#!/usr/bin/python

import sys

if len(sys.argv) > 1:
    open('pages/'+str(sys.argv[1])+'.php', 'w+')
    print 'Creata pagina php con questo path \'pages/'+str(sys.argv[1])+'.php\''

    open('pages/views/'+str(sys.argv[1])+'.htm', 'w+')
    print 'Creata pagina html chiamata', str(sys.argv[1])+'.htm'

    response = ''

    if str(sys.argv[1]) != 'y':
        response = raw_input('Vuoi creare il file javascript?')
    else:
        response = 'y'

    if (response == 'y') or (response == 'yes'):
        open('js/'+str(sys.argv[1])+'.js', 'w+')
        print 'Creato file javascript chiamato', str(sys.argv[1])+'.js'

    response2 = ''

    if str(sys.argv[1]) != 'y':
        response2 = raw_input('Vuoi creare il file css?')
    else:
        response2 = 'y'

    if (response2 == 'y') or (response2 == 'yes'):
        open('sass/'+str(sys.argv[1])+'.sass', 'w+')
        print 'Creato file javascript chiamato', str(sys.argv[1])+'.sass'
else:
    print 'Non hai inserito il nome da dare alla pagina'
