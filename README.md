# Core

Core is a PHP, JavaScript and Python framework that combines a large set of classes to build
a fully featured environment to simplify the making of modern Web applications.

Core is divided in various modules. Each one offers specific classes to achieve common
targets in modern Web applications: from building a WebApi interface, to generating
complex HTML pages following the MVC (Model-View-Controller) design pattern.

Some of the main features of Core are based upon the 
[FatFree Framework](https://fatfreeframework.com).

# Install

The preferred way to use Core in your projects is to install it with Composer.
Take a look this page get further information on the required steps: 
[Install with Composer](https://gitlab.com/InerziaSoft/Core/wikis/install-composer).

# Getting Started

Even if we strongly suggest to integrate Core in a new application, there are no
contraindications in implementing it in an existing WebApp.