import $ from "jquery";

import ApiRequest, {ApiRequestMethod} from "./../base/api"
import Path from "../../path";

export class LocalizationService {
	
	constructor() {
		this.preferredLanguage = localStorage.getItem("language") || $("#defaultLanguage").text();
	}
	
	getPreferredLanguage() {
		return this.preferredLanguage;
	}
	
	setPreferredLanguage(language) {
		localStorage.setItem("language", language);
		this.preferredLanguage = language;
	}
	
	hasPreferredLanguage() {
		return this.preferredLanguage !== undefined;
	}
	
	static changePreferredLanguage(sender) {
		let language = $(sender).attr("data-language");
		
		let localizationService = new LocalizationService();
		let hasPreferredLanguage = localizationService.hasPreferredLanguage();
		
		if (!hasPreferredLanguage || (hasPreferredLanguage && localizationService.getPreferredLanguage() !== language)) {
			$.post("/" + Path.prefixFromContext() + "Language", {
				language: language
			}, function (answer) {
				try {
					answer = JSON.parse(answer);
					
					if (answer["status"] === 200) {
						localizationService.setPreferredLanguage(language);
						window.location.reload();
					}
					else {
						console.log(answer);
					}
				}
				catch (ex) {
					console.error("An error occurred while trying to save language settings!");
					console.log(ex);
				}
			})
		}
		else {
			console.log("User has selected the same preferred language, ignoring request.");
		}
	}
	
}

export default class LocalizedStringsRequest {
	
	constructor(localizedStrings, language) {
		this.localizedStrings = localizedStrings;
		this.language = language || new LocalizationService().getPreferredLanguage();
	}
	
	request(completionHandler) {
		let obj = this;
		
		let request = new ApiRequest("language", "strings", ApiRequestMethod.Post, {
			keysAndPages: JSON.stringify(obj.localizedStrings),
			language: obj.language
		});
		request.request(function (answer) {
			let strings = answer["strings"];
			obj.localizedStrings.forEach(function (obj, index) {
				let value = strings[index];
				
				if (value !== undefined) {
					obj.value = value["string"];
				}
			});
			
			try {
				completionHandler(obj.localizedStrings);
			}
			catch (exc) {
				console.error("LocalizedStringsRequest completion handler thrown an exception");
				console.log(exc);
			}
		}, function (error) {
			console.error("Localized string request returned an error!");
			console.log(error);
		});
	}
	
	apply(object, completionHandler) {
		this.request(function (strings) {
			let dictionary = {};
			strings.forEach(function (string) {
				dictionary[string.key] = string.value;
			});
			object.setState(dictionary);
			
			if (completionHandler !== undefined) completionHandler();
		})
	}

}

export class LocalizedString {
	
	constructor(key, page) {
		this.key = key;
		this.page = page || "global";
	}
	
}