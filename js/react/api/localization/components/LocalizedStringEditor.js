import * as React from "react";
import * as PropTypes from "prop-types";
import ApiRequest, {ApiRequestMethod} from "../../base/api";
import SessionToken from "./../../authentication/sessionToken";

export default class LocalizedStringEditor extends React.Component {
	
	constructor(props) {
		super(props);
		
		this.state = {
			realOriginalValue: props.originalValue,
			originalValue: props.localizedValue,
			value: props.localizedValue,
			status: 0,
			isSyncing: false
		};
		
		this.handleChange = this.handleChange.bind(this);
		this.sync = this.sync.bind(this);
	}
	
	getParsedValue() {
		if (this.state.realOriginalValue === null) return "";
		
		let regex = /%\S*/g;
		return this.state.realOriginalValue.replace(regex, function (value) {
			return "<span>" + value + "</span>";
		});
	}
	
	handleChange(event) {
		let originalValue = this.state.originalValue;
		let changedValue = event.target.value;
		
		this.setState({
			value: changedValue,
			status: (originalValue !== changedValue ? 1 : 0)
		});
	}
	
	getStatus() {
		if (this.props.destination === "none") {
			return "No destination selected";
		}
		
		switch (this.state.status) {
			case 0:
				return "Not changed";
				
			case 1:
				return <div><span>Changed</span><button disabled={this.state.isSyncing} onClick={this.sync} type="button">Sync</button></div>;
		}
		
		return "";
	}
	
	getStatusClass() {
		switch (this.state.status) {
			case 0:
				return "unchanged";
				
			case 1:
				return "changed";
		}
	}
	
	sync() {
		this.setState({
			isSyncing: true
		});
		
		let that = this;
		let syncRequest = new ApiRequest("language", "key", ApiRequestMethod.Put, {
			key: this.props.stringKey,
			page: this.props.stringPage,
			language: this.props.destination,
			newValue: this.state.value
		}, new SessionToken());
		syncRequest.request(function () {
			that.setState({
				originalValue: that.state.value,
				isSyncing: false,
				status: 0,
				realOriginalValue: (that.props.source === that.props.destination ? that.state.value : that.state.realOriginalValue)
			});
		})
	}
	
	render() {
		return <tr>
			<td className="keyColumn">
				{this.props.stringKey + " / " + this.props.stringPage}
			</td>
			<td className="originalColumn" dangerouslySetInnerHTML={{__html: this.getParsedValue()}} />
			<td className="valueColumn">
				<textarea disabled={this.props.destination === "none"} value={this.state.value} onChange={this.handleChange}/>
			</td>
			<td className={"statusColumn " + this.getStatusClass()}>
				{this.getStatus()}
			</td>
		</tr>;
	}
	
}

LocalizedStringEditor.propTypes = {
	source: PropTypes.string,
	destination: PropTypes.string,
	stringKey: PropTypes.string.isRequired,
	stringPage: PropTypes.string.isRequired,
	originalValue: PropTypes.string,
	localizedValue: PropTypes.string
};