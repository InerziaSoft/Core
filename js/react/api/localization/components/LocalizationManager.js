import * as React from "react";
import ApiRequest, {ApiRequestMethod} from "../../base/api";
import LocalizedStringEditor from "./LocalizedStringEditor";
import SessionToken from "./../../authentication/sessionToken";

export default class LocalizationManager extends React.Component {
	
	constructor(props) {
		super(props);
		
		this.state = {
			languages: [],
			source: "none",
			destination: "none",
			keys: [],
			isLoading: false
		};
		
		this.handleSourceLanguageChange = this.handleSourceLanguageChange.bind(this);
		this.requestKeys = this.requestKeys.bind(this);
	}
	
	componentWillMount() {
		let that = this;
		let request = new ApiRequest("language", "all", ApiRequestMethod.Get);
		request.request(function (answer) {
			that.setState({
				languages: answer.items
			});
		});
	}
	
	componentDidMount() {
		this.requestKeys();
	}
	
	handleSourceLanguageChange(event) {
		const target = event.target;
		const value = target.value;
		const name = target.name;
		
		this.setState({
			[name]: value
		}, function () {
			this.requestKeys();
		});
	}
	
	requestKeys() {
		if (this.state.source === "none") {
			this.setState({
				keys: []
			});
			return;
		}
		
		this.setState({
			isLoading: true
		});
		
		let that = this;
		let keys = new ApiRequest("language", "keys/" + this.state.source + "/" + this.state.destination, ApiRequest.Get, undefined, new SessionToken());
		keys.request(function (answer) {
			that.setState({
				keys: []
			}, function () {
				that.setState({
					keys: answer.items,
					isLoading: false
				})
			});
		})
	}
	
	render() {
		return <div className="localizationManager">
			<header>
				<h1>Localization Manager</h1>
				<div>
					<label htmlFor="from">Source</label>
					<select name="source" id="source" onChange={this.handleSourceLanguageChange}>
						<option value="none">None</option>
						{this.state.languages.map((language) => <option key={language.code} id={language.code} value={language.code}>{language.name}</option>)}
					</select>
					
					<label htmlFor="to">Destination</label>
					<select name="destination" id="destination" onChange={this.handleSourceLanguageChange}>
						<option value="none">None</option>
						{this.state.languages.map((language) => <option key={language.code} id={language.code} value={language.code}>{language.name}</option>)}
					</select>
				</div>
			</header>
			
			{this.state.keys.length > 0 ?
			<section>
				<p>Make the required changes and when ready press the "Sync" button near each string to synchronize it with the database.
				<br/>Ideally, you should sync each string one by one as soon as you've finished translating it.</p>
			</section> : <div/> }
			
			{this.state.isLoading ? <section className="loadingSection"><h3>Loading...</h3></section> : <div/>}
			
			<div className="localizationTableContainer">
				{(this.state.keys.length > 0 ? <table>
					<thead>
						<tr>
							<th>
								Key / Page
							</th>
							<th>
								Original
							</th>
							<th>
								Destination
							</th>
							<th>
								Status
							</th>
						</tr>
					</thead>
					<tbody>
						{this.state.keys.map((key) => <LocalizedStringEditor source={this.state.source} stringPage={key.page} destination={this.state.destination} key={key.name + "_" + key.page} stringKey={key.name} originalValue={key.source} localizedValue={key.destination} />)}
					</tbody>
				</table> : <h3>No keys available. Please, select a source and a destination using the menus above.</h3> )}
			</div>
		</div>
	}
	
}