import Url from "domurl";
import $ from "jquery";

import Path from "./../../path";
import SessionToken from "../authentication/sessionToken";

export const ApiRequestMethod = {
	Get: "GET",
	Post: "POST",
	Put: "PUT",
	Delete: "DELETE"
};

class ApiRequest {
	
	/**
	 * Instantiates a new ApiRequest.
	 *
	 * @param apiClass
	 * @param command
	 * @param method
	 * @param parameters
	 * @param token
	 */
	constructor(apiClass, command, method, parameters, token) {
		this.apiContext = window.ApiContext;
		this.apiClass = apiClass;
		this.command = command;
		this.method = method;
		this.parameters = parameters;
		
		if (token instanceof SessionToken) {
			token = token.token();
		}
		
		this.token = token;
		this.requestUri = this.getApiAddress() + this.apiClass + "/" + command.toLowerCase();
		this.handleParameters();
	}
	
	handleParameters() {
		if (this.method === ApiRequestMethod.Get || this.method === ApiRequestMethod.Delete) {
			if (this.parameters !== undefined) {
				if (this.command !== undefined && this.command.length > 0) {
					this.requestUri += "/";
				}
				
				for (let key in this.parameters) {
					if (this.parameters.hasOwnProperty(key)) {
						let parameter = this.parameters[key];
						
						this.requestUri += parameter + "/";
					}
				}
				
				this.parameters = undefined
			}
		}
		else if (this.method !== ApiRequestMethod.Post) {
			this.parameters = JSON.stringify(this.parameters);
		}
	}
	
	getApiAddress() {
		const url = new Url();
		return url.protocol + "://" + url.host + ":" + url.port + "/" + Path.prefixFromContext(this.apiContext) + "api/v1/";
	}
	
	request(successHandler, errorHandler) {
		let obj = this;
		
		//noinspection JSUnusedGlobalSymbols
		$.ajax({
			url: obj.requestUri,
			data: obj.parameters,
			dataType: "application/json",
			type: obj.method,
			beforeSend: function(xhr) {
				xhr.setRequestHeader('Authentication', 'JWT token=' + obj.token);
				xhr.uri = obj.requestUri;
			},
			complete: function (xhr) {
				try {
					let answer = JSON.parse(xhr.responseText);
					
					if (answer["status"] === undefined) {
						throw new DOMException();
					}
					
					if (answer["status"] == 200) {
						if (successHandler !== undefined) {
							try {
								successHandler(answer);
							}
							catch (exc) {
								console.error(StandardApiResponse.userExceptionError(xhr, exc));
							}
						}
					}
					else {
						if (errorHandler !== undefined) {
							errorHandler(answer);
						}
						else {
							console.error(answer);
						}
					}
				}
				catch (ex) {
					let errorResponse = StandardApiResponse.invalidApiResponse(xhr.responseText, ex);
					if (errorHandler !== undefined) {
						errorHandler(errorResponse);
					}
					else {
						console.error(errorResponse)
					}
				}
			},
			statusCode: {
				500: function(xhr) {
					let errorResponse = StandardApiResponse.internalServerError(xhr);
					if (errorHandler !== undefined) {
						errorHandler(errorResponse);
					}
					else {
						console.error(errorHandler);
					}
				},
				404: function(xhr) {
					let errorResponse = StandardApiResponse.requestNotFound(xhr);
					if (errorHandler !== undefined) {
						errorHandler(errorResponse);
					}
					else {
						console.warn(errorResponse);
					}
				}
			},
			fail: function (answer) {
				if (errorHandler !== undefined) {
					errorHandler(answer);
				}
				else {
					console.error(answer);
				}
			}
		});
	}
	
}

class StandardApiResponse {
	
	static invalidApiResponse(answer, exception) {
		return {
			status: 500,
			error: 'Api request returned invalid response: ' + answer,
			internalException: exception,
			'errorCode': 500
		};
	}
	
	static internalServerError(xhr) {
		return {
			status: 500,
			'internalException': 'InternalServerErrorException',
			error: 'The resource at ' + xhr.uri + ' raised an internal exception: ' + xhr.responseText,
			'errorCode': 500
		};
	}
	
	static requestNotFound(xhr) {
		return {
			status: 404,
			'internalException': 'RequestNotFoundException',
			error: 'The requested at ' + xhr.uri + ' with the specified method cannot be found',
			'errorCode': 404
		}
	}
	
	static userExceptionError(xhr, exc) {
		return {
			status: 500,
			internalException: exc,
			error: 'The handler for request ' + xhr.uri + ' thrown an exception',
			errorCode: 500
		}
	}
	
}

export default ApiRequest;