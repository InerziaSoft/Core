export default class SessionToken {
	
	constructor(sessionToken) {
		if (sessionToken === undefined) sessionToken = window.localStorage.getItem("JWT");
		
		this.sessionToken = sessionToken;
	}
	
	getStorage() {
		return window.localStorage;
	}
	
	store() {
		this.getStorage().setItem("JWT", this.sessionToken);
	}
	
	delete() {
		this.getStorage().removeItem("JWT");
	}
	
	token() {
		return this.sessionToken;
	}
	
}