import $ from "jquery";
import ApiRequest, {ApiRequestMethod} from "../base/api";
import Path from "../../path";
import SessionToken from "./sessionToken";

export class LoginManager {
	
	static performLogin(identifier, passwordHash, completionHandler, errorHandler) {
		let request = new ApiRequest("auth", "login", ApiRequestMethod.Post, {
			identifier: identifier,
			password: passwordHash
		});
		request.request(function (answer) {
			LoginManager.__storeLoginOnAnswer(answer, completionHandler, errorHandler);
		}, errorHandler);
	}
	
	static __storeLoginOnAnswer(success, completionHandler, errorHandler) {
		if (success["status"] === 200) {
			let jwt = success["jwt"];
			
			if (jwt !== undefined && jwt.length > 0) {
				let sessionToken = new SessionToken(jwt);
				sessionToken.store();
				
				$.post("/" + Path.prefixFromContext(window.ApiContext) + "Login", {
					jwt: jwt
				}, function (answer) {
					try {
						answer = JSON.parse(answer);
						
						if (answer["status"] === 200) {
							completionHandler(success);
						}
						else {
							errorHandler(answer);
						}
					}
					catch (ex) {
						errorHandler({'errorCode': 500, 'internalException': ex.message})
					}
				});
			}
			else {
				errorHandler({'errorCode': 500, 'internalException': "Login response returned empty token."})
			}
		}
		else {
			errorHandler({'errorCode': 500, 'internalException': "Login response returned invalid state.", 'response': success});
		}
	}
	
	static performLogout(completionHandler) {
		let sessionToken = new SessionToken();
		$.post("/" + Path.prefixFromContext(window.ApiContext) + "Logout", {
			jwt: sessionToken.token()
		}, function () {
			sessionToken.delete();
			
			if (completionHandler !== undefined) {
				completionHandler();
			}
			else {
				window.location.reload();
			}
		});
	}
	
}

