export default class Path {
	
	static prefixedAbsolutePath(path) {
		return "/" + Path.prefixFromContext() + path;
	}
	
	static prefixFromContext() {
		if (window.location.toString().indexOf(window.ApiContext) !== -1) {
			return window.ApiContext + "/";
		}
		return "";
	}
	
}