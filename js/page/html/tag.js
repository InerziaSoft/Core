function HtmlTag (tagName, content, classes, id, attributes, dataAttributes, autoClose, onclick){

    this.tagName = tagName;

    this.id = id;

    this.classes = classes || [];

    this.attributes = attributes || {};

    this.dataAttributes = dataAttributes || {};

    this.autoClose = (autoClose === undefined)? false : autoClose;

    this.content = content || [];

    this.onclick = onclick;

    this.addAttribute = function (key, value) {
        this.__tag.attributes[key] = value;
    };

    this.addClass = function (className) {
        this.__tag.classes.push(className)
    };

    this.toHtml = function () {

        var html = "<"+this.__tag.tagName;

        if(id != undefined){
            html += " id='"+this.__tag.id+"'";
        }

        if(this.__tag.classes.length != 0) {
            html += " class='"+this.__tag.classes.join(" ")+"'";
        }

        for (var attributeKey in this.__tag.attributes){
            html += " "+attributeKey+"='"+this.__tag.attributes[attributeKey]+"'";
        }

        for (var dataAttributeKey in this.__tag.dataAttributes){
            html += " "+dataAttributeKey+"='"+this.__tag.dataAttributes[dataAttributeKey]+"'";
        }

        if(this.__tag.onclick != undefined){
            html += " onclick='"+this.__tag.onclick+"'";
        }

        if(this.__tag.autoClose) {
            html += "/>";
        }
        else {

            html += ">";

            if(this.__tag.content.length != 0) {
                if(this.__tag.content[0].__tag === undefined) html += this.__tag.content;
                else this.__tag.content.forEach(function (value){
                        html += value.toHtml();
                    });
            }

            html += "</"+this.__tag.tagName+">"
        }

        return html;
    }
}