Select.FromJsonResponse = function (jsonResponse, valueKey, displayNameKey, addRowLabel, name, classes, id, onchange, attributes, dataAttributes) {

    var content = selectContentFromJsonResponse(jsonResponse, valueKey, displayNameKey);

    return new Select(content, name, classes, id, onchange, attributes, dataAttributes)
};


function selectContentFromJsonResponse (jsonResponse, valueKey, displayNameKey, addRowLabel, classes, attributes, dataAttributes){

    var selectContent = [];

    if(addRowLabel){
        var option = new Option(-2, addRowLabel);
        selectContent.push(new OptionGroup([option]));
    }

    jsonResponse["items"].forEach(function (item) {
        selectContent.push(new Option(item[valueKey], item[displayNameKey], classes, undefined, attributes, dataAttributes))
    });

    return selectContent;
}

function Select(content, name, classes, onchange, defaultSelectionText, id, attributes, dataAttributes) {

    attributes = attributes || {};
    attributes["name"] = name;
    attributes["onchange"] = onchange;

    var items = [new Option(-1, defaultSelectionText || "Non specificato")];

    this.__tag = new HtmlTag("select", items.concat(content), classes, id, attributes, dataAttributes);

    this.selectOption = function (id) {
        this.__tag.content.forEach(function (option) {
            if(option instanceof OptionGroup) option.selectOption(id);
            else {
                if (option.value == id) {
                    option.select();
                    return false;
                }
            }
        })
    };

    this.toHtml = this.__tag.toHtml;
}

function OptionGroup(content, classes, id, attributes, dataAttributes) {

    this.__tag = new HtmlTag("optgroup", content, classes, id, attributes, dataAttributes);

    this.selectOption = function (id) {
        this.__tag.content.forEach(function (option) {
            if (option.value == id) {
                option.select();
                return false;
            }
        });
    };

    this.toHtml = this.__tag.toHtml;
}

function Option(value, displayName, classes, id, attributes, dataAttributes){

    this.selected = false;
    this.value = value;

    attributes = attributes || {};
    attributes["value"] = value;

    this.__tag = new HtmlTag("option", displayName, classes, id, attributes, dataAttributes);

    this.select = function () {
        this.tag.addAttribute("selected");
        this.selected = true;
    };

    this.toHtml = this.__tag.toHtml;
}