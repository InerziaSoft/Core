DefaultStepContainer = ".steps";
StepPrefix = "step_";
SelectedStepClass = "step_selected";

function __directionFromIndexes(oldIndex, newIndex) {
	if (oldIndex < newIndex) {
		return "right";
	}
	return "left";
}

function Steps(ul, startIndex) {
	this.ulIdentifier = ul;
	this.start = startIndex;
	
	if (this.ulIdentifier === undefined) this.ulIdentifier = DefaultStepContainer;
	if (this.start === undefined) this.start = 0;
	
	this.configure = function () {
		$(this.ulIdentifier).find("[id^='" + StepPrefix + "']").hide();
		this.show(this.start);
	};
	
	this.hasNext = function () {
		return this.getStepFromIndex(this.getShownIndex() + 1).length > 0;
	};
	
	this.hasPrevious = function () {
		return this.getStepFromIndex(this.getShownIndex() - 1).length > 0;
	};
	
	this.next = function () {
		this.slide(this.getShownIndex() + 1);
	};
	
	this.previous = function () {
		this.slide(this.getShownIndex() - 1);
	};
	
	this.slide = function(index) {
		var nextStep = this.getStepFromIndex(index);
		var shownIndex = this.getShownIndex();
		
		var obj = this;
		this.getShownStep().hide("slide", {direction: __directionFromIndexes(index, shownIndex)}, 500, function () {
			nextStep.show("slide", {direction: __directionFromIndexes(shownIndex, index)}, 200);
			obj.show(index);
		});
	};
	
	this.show = function(index) {
		var nextStep = this.getStepFromIndex(index);
		
		$("." + SelectedStepClass).removeClass(SelectedStepClass);
		nextStep.addClass(SelectedStepClass).show();
	};
	
	this.getShownStep = function() {
		return this.getStepFromIndex(this.getShownIndex());
	};
	
	this.getStepFromIndex = function(index) {
		return $(this.ulIdentifier).find("#" + StepPrefix + index);
	};
	
	this.getShownIndex = function () {
		var selected = $("." + SelectedStepClass);
		
		if (selected.length > 0) {
			return parseInt(selected.attr("id").replace(StepPrefix, ""));
		}
		return -1;
	}
	
}