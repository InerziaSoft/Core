function PartialPage(pageName, partialName, parameters, method) {
	if (method === undefined) method = ApiRequestMethod.Get;
	
	this.pageName = pageName;
	this.partialName = partialName;
	this.partialParameters = parameters;
	this.method = method;
	this.url = "";

	this.request = function(completionHandler) {
		var additionalPrefix = apiPrefix();

		this.url = "/" + additionalPrefix + this.pageName.toLowerCase();

		if (this.partialName !== undefined) {
			this.url += "/partials/" + this.partialName
		}

		if (this.method == ApiRequestMethod.Get) {
			for (var parameter in this.partialParameters) {
				if (this.partialParameters.hasOwnProperty(parameter)) {
					var parameter = this.partialParameters[parameter];
					
					if (typeof(parameter) === "boolean") {
						parameter = (parameter ? "1" : "0")
					}
					
					this.url += "/" + parameter;
				}
			}
			
			this.partialParameters = undefined;
		}
		
		//noinspection JSCheckFunctionSignatures
		$.ajax({
			url: this.url,
			data: this.partialParameters,
			type: this.method,
			complete: function (xhr) {
				try {
					completionHandler(xhr.responseText)
				}
				catch (ex) {
					console.error("The handler for PartialPage request '" + this.url + "' thrown an exception");
					console.log(ex);
				}
			},
			statusCode: {
				500: function(xhr) {
					console.error("PartialPage request '" + this.url + "' returned Internal Server Error status. Attempting to parse as JSON...");
					
					try {
						var jsonResponse = JSON.parse(xhr.responseText);
						console.log(jsonResponse);
					}
					catch (ex) {
						console.warn("Parse failed. Returning raw response from server:");
						console.log(xhr);
					}
				},
				404: function(xhr) {
					console.error("PartialPage request '" + this.url + "' cannot be resolved to a valid resource: the server returned status code 404.");
					console.log(xhr);
				}
			},
			fail: function (answer) {
				try {
					completionHandler(answer)
				}
				catch (ex) {
					console.error("The handler for PartialPage request '" + this.url + "' thrown an exception");
					console.log(ex);
				}
			}
		})
	}
}