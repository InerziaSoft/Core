RadioEvents = {
	Selected: "radio_selected"
}

function RadioButton(items) {
	if (items.length == 0) console.error("Unable to instantiate a RadioButton from an empty array!");
	
	this.items = items;
	this.name = $(items[0]).attr("data-radioname");
	
	this.configure = function (defaultIndex) {
		var radio = this;
		this.items.forEach(function (obj) {
			radio.select(defaultIndex);
			$(obj).off("click").on("click", function () {
				selectRadio(this);
			})
		})
	};
	
	this.select = function (index) {
		this.getSelectedItem().removeClass("radio_selected");
		this.items.forEach(function (obj) {
			if (parseInt(index) == parseInt($(obj).attr("data-radioindex"))) {
				$(obj).addClass("radio_selected");
				$(obj).trigger(RadioEvents.Selected);
			}
		});
	};
	
	this.getSelectedItem = function () {
		return $(".radio_selected[data-radioname='" + this.name + "']");
	};
	
	this.val = function () {
		var value = this.getSelectedItem().attr("value");
		
		if (value.substr(0, 1) == "#" || value.substr(0, 1) == ".") {
			value = $(value).text();
		}
		
		return value;
	}
}

function selectRadio(item) {
	RadioButton.fromItem(item).select($(item).attr("data-radioindex"));
}

RadioButton.canInitFrom = function (item) {
	return ($(item).attr("data-radioname") !== undefined);
};

RadioButton.fromItem = function (item) {
	var radioName = $(item).attr("data-radioname");
	var items = $("[data-radioname='" + radioName + "']").toArray();
	
	return new RadioButton(items);
};