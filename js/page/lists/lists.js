ListEvents = {
	Selected: "list_selected",
	Deselected: "list_deselected",
	Ready: "list_ready",
	Refreshed: "list_refreshed"
};

ListType = {
	Standard: "standard",
	Grouped: "grouped",
	Child: "child"
};

ListActionType = {
	Add: "Add",
	Edit: "Edit",
	Delete: "Delete"
};

function __logFailure(reason, answer) {
	console.log(reason + " Server said:");
	console.log(answer.responseText);
}

function autoConfigureLists(completionHandler) {
	var autoConfigureLists = $(".objectsListDefinition");

	autoConfigureLists.each(function () {
		if ($(this).attr("data-configured") !== undefined) return;
		
		var autoConfigureObject = $(this);
		var listUrl = $(this).find(".objectsListUrl").text();
		var listContainer = $(this).find(".objectsListContainer").text();
		var additionalPrefix = apiPrefix();

		//noinspection JSCheckFunctionSignatures
		$.ajax("/" + additionalPrefix + listUrl)
			.done(function (answer) {
				autoConfigureObject.attr("data-configured", "configured");
				
				$(listContainer)
					.attr("data-listtype", ListType.Standard)
					.attr("data-listurl", listUrl)
					.html(answer);
				deselectItem(listContainer);
				$(listContainer).on("click", function () {
					deselectItem(listContainer)
				});
				
				if (completionHandler !== undefined) {
					completionHandler(listContainer);
				}
				
				$(listContainer).trigger(ListEvents.Ready);
			})
			.fail(function (answer) {
				__logFailure("ObjectsList AutoLoad failed.", answer);
			});
	});
	
	var groupedAutoConfigureLists = $(".groupedObjectsListDefinition");
	
	groupedAutoConfigureLists.each(function () {
		var mainListUrl = $(this).find(".mainObjectsListUrl").text();
		var mainListContainer = $(this).find(".mainObjectsListContainer").text();
		var childListUrl = $(this).find(".childObjectsListUrl").text();
		var childListContainer = $(this).find(".childObjectsListContainer").text();
		var additionalPrefix = apiPrefix();
		
		$.ajax("/" + additionalPrefix + mainListUrl)
			.done(function (answer) {
				$(mainListContainer)
					.attr("data-listtype", ListType.Grouped)
					.attr("data-listurl", mainListUrl)
					.attr("data-childlistcontainer", childListContainer)
					.html(answer);
				
				deselectItem(mainListContainer);
				$(mainListContainer).on("click", function () {
					deselectItem(mainListContainer);
				});
				
				$(childListContainer)
					.attr("data-listtype", ListType.Child)
					.attr("data-listurl", childListUrl)
					.attr("data-mainlistcontainer", mainListContainer)
					.html("");
				
				$(childListContainer).on("click", function () {
					deselectItem(childListContainer);
				});
				
				if (completionHandler !== undefined) {
					completionHandler(mainListContainer);
				}
				
				$(mainListContainer).trigger(ListEvents.Ready);
				$(childListContainer).trigger(ListEvents.Ready);
			})
			.fail(function (answer) {
				__logFailure("ObjectsList AutoLoad failed.", answer);
			});
	});
}

function List(listContainer) {
	this.listContainer = listContainer;
	
	this.empty = function () {
		var inspectorContainer = this.getInspectorContainer();
		if (inspectorContainer !== undefined) {
			$(inspectorContainer).empty();
		}
		
		$(this.listContainer).empty();
	};
	
	this.refresh = function (keepSelection, foreignKeyValue) {
		if (keepSelection === undefined) keepSelection = false;
		
		var additionalPrefix = apiPrefix();
		var listUrl = $(this.listContainer).attr("data-listurl");
		
		var selectedId = undefined;
		if (keepSelection) {
			selectedId = this.getSelectedItemId();
		}
		
		var url = "/" + additionalPrefix + listUrl;
		
		if (foreignKeyValue !== undefined) {
			if (this.getType() != ListType.Child) {
				console.warn("Foreign key value has been passed to a standard list: this is wrong and won't lead to the expected result. Please, refer to the documentation for further information.");
			}
		}
		else {
			if (this.getType() == ListType.Child) {
				foreignKeyValue = new List(this.getMainContainer()).getSelectedItemId();
			}
		}
		
		if (foreignKeyValue !== undefined) {
			url += "/" + foreignKeyValue;
		}
		
		var obj = this;
		$.ajax(url)
			.done(function (answer) {
				$(obj.listContainer).hide();
				$(obj.listContainer).html(answer);
				$(obj.listContainer).fadeIn();
				
				if (keepSelection) {
					obj.select(selectedId);
				}
				else {
					obj.deselect();
				}
				
				$(obj.listContainer).trigger(ListEvents.Refreshed);
			})
			.fail(function (answer) {
				__logFailure("ObjectsList AutoLoad failed.", answer);
			})
	};
	
	this.select = function (id) {
		$(this.listContainer).find(".list_selected").removeClass("list_selected");
		
		var type = this.getType();
		
		if (type != ListType.Child) {
			this.getActionItem(ListActionType.Edit).removeClass("disabled");
			this.getActionItem(ListActionType.Delete).removeClass("disabled");
		}
		
		$(this.listContainer).find("#" + id).addClass("list_selected");
		
		var objectsListUl = $(this.listContainer).find(".objectsList");
		var inspectorContainer = this.getInspectorContainer();
		
		if (type == ListType.Grouped) {
			var childListContainer = this.getChildContainer();
			
			new List(childListContainer).refresh(undefined, this.getSelectedItemId());
		}
		
		if (inspectorContainer !== undefined && $(inspectorContainer).length > 0) {
			var inspectorUrl = objectsListUl.attr("data-objectsListInspector");
			var additionalPrefix = apiPrefix();
			
			//noinspection JSCheckFunctionSignatures
			$.ajax("/" + additionalPrefix + inspectorUrl + id)
				.done(function (answer) {
					$(inspectorContainer).fadeOut(100, function () {
						$(inspectorContainer).html(answer);
						$(inspectorContainer).fadeIn();
					});
				})
				.fail(function (answer) {
					__logFailure("An error occurred when trying to load the inspector for list " + inspectorContainer + " (URL: " + inspectorUrl + ").", answer);
				})
		}
		
		$(this.listContainer).trigger(ListEvents.Selected);
	};
	
	this.deselect = function () {
		$(this.listContainer).find(".list_selected").removeClass("list_selected");
		
		var objectsListUl = $(this.listContainer).find(".objectsList");
		var inspectorContainer = this.getInspectorContainer();
		var inspectorUrl = objectsListUl.attr("data-objectsListInspector");
		var additionalPrefix = apiPrefix();
		var type = this.getType();
		
		if (type != ListType.Child) {
			this.getActionItem(ListActionType.Edit).addClass("disabled");
			this.getActionItem(ListActionType.Delete).addClass("disabled");
		}
		
		if (type == ListType.Grouped) {
			var childListContainer = this.getChildContainer();
			
			new List(childListContainer).empty();
		}
		
		if (inspectorContainer !== undefined && $(inspectorContainer).length > 0) {
			//noinspection JSCheckFunctionSignatures
			$.ajax("/" + additionalPrefix + inspectorUrl)
				.done(function (answer) {
					$(inspectorContainer).fadeOut(100, function () {
						$(inspectorContainer).html(answer);
						$(inspectorContainer).fadeIn();
					});
				})
				.fail(function (answer) {
					__logFailure("An error occurred when trying to load the inspector for list " + inspectorContainer + " (URL: " + inspectorUrl + ").", answer);
				})
		}
		
		$(this.listContainer).trigger(ListEvents.Deselected);
	};
	
	this.togglePlaceholder = function (show, animated) {
		if (animated === undefined) animated = true;
		
		var placeholder = $(this.listContainer).find(".addItemPlaceholder");
		if (animated) {
			if (show) {
				placeholder.fadeIn();
			}
			else {
				placeholder.fadeOut();
			}
		}
		else {
			if (show) {
				placeholder.show();
			}
			else {
				placeholder.hide();
			}
		}
	};
	
	this.getSelectedItem = function () {
		return $(this.listContainer).find(".list_selected");
	};
	
	this.getSelectedItemId = function () {
		return this.getSelectedItem().attr("id");
	};
	
	this.getType = function () {
		var listType = $(this.listContainer).attr("data-listtype");
		switch (listType) {
			case ListType.Standard:
				return ListType.Standard;
				
			case ListType.Grouped:
				return ListType.Grouped;
				
			case ListType.Child:
				return ListType.Child;
				
			default:
				console.error("Unknown or unrecognized list type '" + listType + "'")
		}
	};
	
	this.getChildContainer = function () {
		if (this.getType() != ListType.Grouped) {
			console.error("Only grouped lists have a child container. The list '" + this.listContainer + "' has type '" + this.getType() + "' and does not support children.");
		}
		
		return $(this.listContainer).attr("data-childlistcontainer");
	};
	
	this.getMainContainer = function () {
		if (this.getType() != ListType.Child) {
			console.error("Only children of grouped lists have a main container. The list '" + this.listContainer + "' has type '" + this.getType() + "' and does not define a main container.");
		}
		
		return $(this.listContainer).attr("data-mainlistcontainer");
	};
	
	this.getInspectorContainer = function () {
		return $(this.listContainer).find(".objectsList").attr("data-objectsListInspectorContainer");
	};
	
	this.getActionItem = function (actionType) {
		return $("#objectsList" + actionType);
	}
}

List.fromItem = function (item) {
	var parentDiv = $(item).parent().parent();
	
	if ($(parentDiv).attr("data-listtype") !== undefined) {
		return new List(parentDiv);
	}
	else {
		console.error("Unable to instantiate List from item because no matching list container can be found. Item is: ");
		console.log(item);
	}
};

//noinspection JSUnusedGlobalSymbols
function selectItem(item, event) {
	if (event !== undefined) event.stopPropagation();
	
	var itemId = $(item).attr("id");
	List.fromItem(item).select(itemId);
}

function deselectItem(list) {
	new List(list).deselect();
}