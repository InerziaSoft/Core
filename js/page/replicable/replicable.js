function Replicable(container) {
	if (container === undefined) container = ".replicable";
	
	this.container = container;
	
	this.configure = function () {
		var prototype = this.getPrototype();
		
		if (prototype.length == 0) {
			console.error("Unable to configure Replicable without a prototype!");
		}
		else {
			prototype.hide();
		}
	};
	
	this.append = function (times, animated, configurationHandler) {
		if (times === undefined) times = 1;
		if (animated === undefined) animated = true;
		
		var prototype = this.getPrototype();
		
		for (var i = 0; i < times; i++) {
			var clone = prototype.clone();
			clone.removeClass("prototype");
			
			if (configurationHandler !== undefined) {
				configurationHandler(clone);
			}
			
			$(this.container).append(clone);
			
			if (animated) {
				clone.slideDown();
			}
			else {
				clone.show();
			}
		}
	};
	
	this.getPrototype = function () {
		return $(this.container).find(".prototype");
	};
	
	this.items = function () {
		return $(this.container).find("li:not(.prototype)");
	}
}