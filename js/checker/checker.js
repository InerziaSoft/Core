function Checker(fieldId) {
	this.field = fieldId;
	
	this.check = function() {
		var jField = $(this.field);
		
		if (jField.length == 0) {
			console.error("Checker cannot find an input named '" + this.field + "'.");
		}
		
		switch (jField.attr("type")) {
			case "checkbox":
				return this.__checkCheckbox();
			
			default:
				return this.__standardCheck();
		}
	};
	
	this.__checkCheckbox = function() {
		return $(this.field + ":checked").length > 0;
	};
	
	this.__standardCheck = function() {
		var value = $(this.field).val();
		
		return !(value === undefined || value == "" || value === "-1");
		
	}
}

function checkForms(forms) {
	var hasError = false;
	forms.forEach(function (form) {
		$(form).find(":input[required]").each(function () {
			if (!new Checker("#" + $(this).attr("id")).check()) {
				markAsError(this);
				hasError = true;
			}
			else {
				removeErrorMark(this);
			}
		});
	});
	
	return !hasError;
}

function markAsError(input) {
	$(input).addClass("errorMark").off("focus", removeErrorMark).on("focus", removeErrorMark);
}

function removeErrorMark() {
	$(this).removeClass("errorMark").off("focus", removeErrorMark);
}