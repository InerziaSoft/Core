/**
 * Initiates a login request based on the specified
 * identifier and passwordHash.
 *
 * This function is based upon the PhpSessionToken module
 * and requires the Automatic Routing System.
 *
 * @param identifier The identifier to login (it could be either a username or an email address)
 * @param passwordHash The password hash (DO NOT send the password in clear to the server)
 * @param completionHandler A function to be executed when the operation has been completed successfully: the first parameter will be the JSON response received from the server.
 * @param errorHandler A function to be executed if an error occurred: the first parameter will be the JSON representation of the error.
 */
function performLogin(identifier, passwordHash, completionHandler, errorHandler) {
	var request = new ApiRequest("auth", "login", ApiRequestMethod.Post, {identifier: identifier, password: passwordHash});
	request.request(function (answer) {
		storeLoginOnAnswer(answer, completionHandler, errorHandler);
	}, function (error) {
		errorHandler(error)
	});
}

/**
 * Proceeds the login operation with a valid AuthenticationSuccessfulResponse.
 *
 * Call this function if you have obtained a valid authentication with a non-standard
 * login method.
 *
 * @param success
 * @param completionHandler
 * @param errorHandler
 */
function storeLoginOnAnswer(success, completionHandler, errorHandler) {
	if (success["status"] == 200) {
		var jwt = success["jwt"];
		
		if (jwt !== undefined && jwt.length > 0) {
			var sessionToken = new SessionToken(jwt);
			sessionToken.store();
			
			$.post("/" + apiPrefix() + "Login", {
				jwt: jwt
			}, function (answer) {
				try {
					answer = JSON.parse(answer);
					
					if (answer["status"] == 200) {
						completionHandler(success);
					}
					else {
						errorHandler(answer);
					}
				}
				catch (ex) {
					errorHandler({'errorCode': 500, 'internalException': ex.message})
				}
			});
		}
		else {
			errorHandler({'errorCode': 500, 'internalException': "Login response returned empty token."})
		}
	}
	else {
		errorHandler({'errorCode': 500, 'internalException': "Login response returned invalid state.", 'response': success});
	}
}

/**
 * Initiates a logout request based on the currently authenticated user.
 *
 * This function is based upon the PhpSessionToken module
 * and requires the Automatic Routing System.
 *
 * @param completionHandler: An optional function to be performed when logout is successfully
 * (if none is passed, this function will reload the page hoping that the token verification system
 * will redirect the user correctly to the login page).
 */
function performLogout(completionHandler) {
	var sessionToken = new SessionToken();
	$.post("/" + apiPrefix() + "Logout", { jwt: sessionToken.token() }, function () {
		sessionToken.delete();
		
		if (completionHandler !== undefined) {
			completionHandler();
		}
		else {
			window.location.reload();
		}
	});
}

function SessionToken(sessionToken) {
    this.sessionToken = sessionToken || localStorage.getItem("JWT");
    
    this.store = function() {
        this.getStorage().setItem("JWT", this.sessionToken);
    };

    this.delete = function() {
    	this.getStorage().removeItem("JWT");
    };

    this.token = function () {
        return this.sessionToken;
    };
    
    this.getStorage = function () {
	    return localStorage;
    }
}