/**
 * Submits the specified form
 * to a DbObjectTransaction.
 *
 * @discussion This function computes and
 * creates automatically all the required structures.
 *
 * @param forms An array of forms (as jQuery objects)
 * @param completionHandler A function to be executed when the operation has been completed successfully
 * @param errorHandler A function to be executed when the operation failed
 * @param autoUpdate Automatically update fields values with new ones.
 * @param interpreter Interpret values before sending them to the AjaxAction.
 *
 * @discussion The interpreter is a function that will be called on inputs that
 * specify the data-interpret tag attribute. This function can add or remove keys
 * from the dictionary that will be sent to the AjaxAction.
 */
function submitForms(forms, completionHandler, errorHandler, autoUpdate, interpreter) {
    var actions = [];
	if (autoUpdate === undefined) autoUpdate = false;
	var fieldsToUpdate = [];

    forms.forEach(function (form) {
	    var representedClass = $(form).attr("data-class");
	    var isMaster = $(form).attr("data-master");
	    var representedId = $(form).attr("data-represented");
	
	    if (isMaster === undefined || isMaster === "") isMaster = false;
	
	    var actionType = _getActionOnRepresentedId(representedId);
	
	    var object = valuesFromForm(form, interpreter);
	    
	    for (var key in object) {
	    	if (object.hasOwnProperty(key)) {
			    fieldsToUpdate.push({field: "view_" + key, value: object[key]});
		    }
	    }
	
	    actions.push(new AjaxAction(actionType, isMaster, representedClass, object));
    });

    new AjaxActionPerformer(actions).perform(undefined, function (answer) {
	    if (autoUpdate) {
		    fieldsToUpdate.forEach(function (fieldToUpdate) {
			    var jqueryField = $("#" + fieldToUpdate.field);
			    jqueryField.text(fieldToUpdate.value);
		    });
	    }
	
	    if (completionHandler !== undefined) {
		    completionHandler(answer);
	    }
    }, errorHandler);
}

function _getActionOnRepresentedId(id) {
    if (id !== undefined && id.length > 0) {
        return AjaxActionType.Update;
    }
    return AjaxActionType.Add;
}

function valuesFromForm(form, interpreter) {
	var representedKey = $(form).attr("data-representedKey");
	var representedId = $(form).attr("data-represented");
	
	var object = {};
	$(form).find(":input, [data-radioname]").each(function () {
		if ($(this).is(":disabled")) return;
		
		if ($(this).attr("data-interpret") !== undefined) {
			if (interpreter !== undefined) {
				object = interpreter($(this), object);
			}
			else {
				console.warn("submitForms found an input that requires an interpreter but the caller has not specified any. Please check " + $(this).attr("id") + ".");
			}
		}
		else if ($(this).attr("data-skip") !== undefined) {
			return;
		}
		else {
			var value = valueFromInput($(this));
			var key = $(this).attr("name");
			
			if (key === undefined) {
				key = $(this).attr("data-radioname"); // Supporting RadioButton class
			}
			
			if (key === undefined) {
				console.warn("An input going through AjaxAction parsing process has specified a value but does not have a valid name attribute, thus it will be skipped. Please, check the following:");
				console.log($(this));
				return;
			}
			
			object[key] = value;
		}
	});
	
	if (representedId !== undefined) {
		object[representedKey.toLowerCase()] = representedId;
	}
	
	return object;
}

function valueFromInput(input) {
    if ($(input).attr("type") == "checkbox") {
        value = _valueFromCheckbox(input);
    }
    else if (typeof RadioButton === "function" && RadioButton.canInitFrom(input)) {
    	var radio = new RadioButton.fromItem(input);
    	value = radio.val();
    }
    else if ($(input).is("select")) {
    	value = $(input).val();
    	
    	if (parseInt(value) < 0) {
    		value = undefined;
	    }
    }
    else {
        var ajaxActionType = $(input).attr("data-type");
        var value;
        if (ajaxActionType !== undefined && ajaxActionType != "") {
            switch (ajaxActionType) {
                case "date":
                    value = moment($(input).val(), "DD/MM/YYYY").format("YYYY-MM-DD");
                    break;
            }
        }
        else {
            value = $(input).val();
        }
    }
    
    if (value === undefined || value == "") {
	    value = "NULL";
    }
    
    return value;
}
function _valueFromCheckbox(checkbox) {
    return ($(checkbox).is(':checked'))?"1":"0";
}