AjaxActionConstant = {
    MasterPlaceholder: "@master_key"
};

AjaxActionType = {
    Add: "insert",
    Update: "update",
    Delete: "delete"
};

function AjaxAction(type, master, className, dbObject) {
    if (master === undefined) master = false;

    this.type = type;
    this.master = master;
    this.className = className;
    this.dbObject = dbObject;

    this.toArray = function () {
        return {
            "className": this.className,
            "object": this.dbObject,
            "action": this.type,
            "master": (this.master ? "true" : "false")
        };
    }
}

AjaxAction.Insert = function (master, className, dbObject) {
	return new AjaxAction(AjaxActionType.Add, master, className, dbObject);
};

AjaxAction.Update = function (master, className, dbObject) {
	return new AjaxAction(AjaxActionType.Update, master, className, dbObject);
};

AjaxAction.Delete = function (master, className, objectId, objectIdKey) {
	if (objectIdKey === undefined) { //noinspection JSUnusedAssignment
		objectIdKey = "id";
	}
	var data = {};
	data[objectIdKey] = objectId;
	
	return new AjaxAction(AjaxActionType.Delete, master, className, data);
};

function AjaxActionPerformer(actions) {
    this.actions = actions;

    this.perform = function (destination, completionHandler, errorHandler, customIdToDestination, addIdToDestination) {
        if (this.actions.length > 0) {
            var actions = [];
            this.actions.forEach(function (action) {
                actions.push(action.toArray());
            });

            var token = new SessionToken();
            var transactionRequest = "/" + apiPrefix() + apiPath + apiVersion + "/db/transaction";

            //noinspection JSUnusedGlobalSymbols
            $.ajax({
                url: transactionRequest,
                data: {actions: JSON.stringify(actions)},
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Authentication', 'JWT token=' + token.token());
                },
                complete: function (xhr) {
                    try {
                        var answer = xhr.responseText;
                        var answerJson = JSON.parse(answer);

                        if (answerJson.status != 200) {
                            if (errorHandler !== undefined) {
                                errorHandler(answer);
                            }
                            else {
                                console.error("An unexpected error occurred while performing the transaction. Server said: ");
                                console.log(answerJson);
                            }
                        }
                        else {
                            __completionHandler(answerJson, destination, completionHandler, customIdToDestination, addIdToDestination);
                        }
                    }
                    catch (err) {
                        //noinspection JSUnusedAssignment
	                    console.log("Invalid API response while performing the transaction. Result is unknown. Error: " + err + " Server response is: " + answer);
	                    if (errorHandler !== undefined) { //noinspection JSUnusedAssignment
		                    errorHandler(answer);
	                    }
                    }
                }
            });
        }
        else {
            __completionHandler(undefined, destination, completionHandler, customIdToDestination, addIdToDestination);
        }
    };
}

function __completionHandler(answer, destination, completionHandler, customIdToDestination, addIdToDestination) {
    if (completionHandler !== undefined) {
        completionHandler(answer);
    }
    if (destination !== undefined) {
        //noinspection JSUnresolvedFunction
        var url = new Url(destination);
        if (customIdToDestination !== undefined) {
            url.query[customIdToDestination] = answer["id"];
        }
        else {
            if (addIdToDestination === undefined || (addIdToDestination !== undefined && addIdToDestination)) {
                url.query.id = answer["id"];
            }
        }
        window.location.href = url.toString();
    }
}