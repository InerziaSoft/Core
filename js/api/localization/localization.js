function LocalizedStringsRequest(localizedStrings, language) {
	this.localizedStrings = localizedStrings;
	this.language = language || new LocalizationService().getPreferredLanguage();
	
	this.request = function(completionHandler) {
		var obj = this;
		
		var request = new ApiRequest("language", "strings", ApiRequestMethod.Post, {
			keysAndPages: JSON.stringify(this.localizedStrings),
			language: this.language
		});
		request.request(function (answer) {
			var strings = answer["strings"];
			obj.localizedStrings.forEach(function (obj, index) {
				var value = strings[index];
				
				if (value !== undefined) {
					obj.value = value["string"];
				}
			});
			
			try {
				completionHandler(obj.localizedStrings);
			}
			catch (exc) {
				console.error("LocalizedStringsRequest completion handler thrown an exception");
				console.log(exc);
			}
		}, function (error) {
			console.error("Localized string request returned an error!");
			console.log(error);
		});
	};
}

function LocalizedString(key, page) {
	this.key = key;
	this.page = page || "global";
	
	this.value = key;
}

function LocalizationService() {
	this.preferredLanguage = localStorage.getItem("language");
	
	this.getPreferredLanguage = function () {
		return this.preferredLanguage;
	}
	
	this.setPreferredLanguage = function (language) {
		localStorage.setItem("language", language);
		this.preferredLanguage = language;
	}
	
	this.hasPreferredLanguage = function() {
		return this.preferredLanguage !== undefined;
	}
}

LocalizationService.ChangePreferredLanguage = function(sender) {
	var language = $(sender).attr("data-language");
	
	var localizationService = new LocalizationService();
	var hasPreferredLanguage = localizationService.hasPreferredLanguage();
	if (!hasPreferredLanguage || (hasPreferredLanguage && localizationService.getPreferredLanguage() != language)) {
		$.post("/" + apiPrefix() + "Language", {
			language: language
		}, function (answer) {
			try {
				answer = JSON.parse(answer);
				
				if (answer["status"] == 200) {
					localizationService.setPreferredLanguage(language);
					window.location.reload();
				}
				else {
					console.log(answer);
				}
			}
			catch (ex) {
				console.error("An error occurred while trying to save language settings!");
				console.log(ex);
			}
		})
	}
	else {
		console.log("User has selected the same preferred language, ignoring request.");
	}
}