ApiRequestMethod = {
    Get: "GET",
    Post: "POST",
    Put: "PUT",
    Delete: "DELETE"
};

/** Responses Definitions **/

function checkResponseSyntax(answer) {
	if (answer["status"] === undefined) {
		throw new DOMException();
	}
}

function invalidApiResponse(answer, exception) {
	return {
		status: 500,
		error: 'Api request returned invalid response: ' + answer,
		internalException: exception,
		'errorCode': 500
	};
}

function internalServerError(xhr) {
	return {
		status: 500,
		'internalException': 'InternalServerErrorException',
		error: 'The resource at ' + xhr.uri + ' raised an internal exception: ' + xhr.responseText,
		'errorCode': 500
	};
}

function requestNotFound(xhr) {
	return {
		status: 404,
		'internalException': 'RequestNotFoundException',
		error: 'The requested at ' + xhr.uri + ' with the specified method cannot be found',
		'errorCode': 404
	}
}

function userExceptionError(xhr, exc) {
	return {
		status: 500,
		internalException: exc,
		error: 'The handler for request ' + xhr.uri + ' thrown an exception',
		errorCode: 500
	}
}

function ApiRequest(apiClass, command, method, parameters, token) {

	this.apiAddress = function() {
		//noinspection JSUnresolvedFunction
		var url = new Url();

		var additionalPrefix = apiPrefix();

		return url.protocol + "://" + url.host + ":" + url.port + "/" + additionalPrefix + apiPath + apiVersion + "/";
	};

    this.requestUri = this.apiAddress() + apiClass + "/" + command.toLowerCase();
    this.method = method;
	
	if (token instanceof SessionToken) {
		token = token.token();
	}
	
    this.token = token;
    this.parameters = {};
    this.response = {};

    if (this.method === ApiRequestMethod.Get || this.method === ApiRequestMethod.Delete) {
        if (parameters !== undefined) {
	        if (command !== undefined && command.length > 0) {
		        this.requestUri += "/";
	        }

            for (var key in parameters) {
            	if (parameters.hasOwnProperty(key)) {
		            var parameter = parameters[key];
		            
		            if (typeof(parameter) === "boolean") {
		            	parameter = (parameter ? "1" : "0")
		            }
		            
		            this.requestUri += parameter + "/";
	            }
            }
        }
    }
    else if (this.method === ApiRequestMethod.Post) {
        this.parameters = parameters;
    }
    else {
	    this.parameters = JSON.stringify(parameters);
    }

    this.request = function(successHandler, errorHandler) {
	    var obj = this;

	    //noinspection JSUnusedGlobalSymbols
	    $.ajax({
            url: this.requestUri,
            data: this.parameters,
		    dataType: "application/json",
            type: this.method,
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Authentication', 'JWT token=' + obj.token);
	            xhr.uri = obj.requestUri;
            },
            complete: function (xhr) {
                try {
                	var answer = JSON.parse(xhr.responseText);

	                checkResponseSyntax(answer);

	                if (answer["status"] == 200) {
		                if (successHandler !== undefined) {
			                try {
				                successHandler(answer);
			                }
			                catch (exc) {
				                console.log(userExceptionError(xhr, exc));
			                }
		                }
	                }
	                else {
		                if (errorHandler !== undefined) {
		                	errorHandler(answer);
		                }
		                else {
			                console.log(answer);
		                }
	                }
                }
                catch (ex) {
	                var errorResponse = invalidApiResponse(xhr.responseText, ex);
					if (errorHandler !== undefined) {
						errorHandler(errorResponse);
					}
					else {
						console.log(errorResponse)
					}
                }
            },
	        statusCode: {
            	500: function(xhr) {
		            var errorResponse = internalServerError(xhr);
		            if (errorHandler !== undefined) {
			            errorHandler(errorResponse);
		            }
		            else {
			            console.log(errorHandler);
		            }
	            },
		        404: function(xhr) {
			        var errorResponse = requestNotFound(xhr);
			        if (errorHandler !== undefined) {
				        errorHandler(errorResponse);
			        }
			        else {
				        console.log(errorResponse);
			        }
		        }
	        },
	        fail: function (answer) {
            	if (errorHandler !== undefined) {
		            errorHandler(answer);
	            }
	            else {
		            console.log(answer);
	            }
	        }
        });
    }

}