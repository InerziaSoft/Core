var DialogButtonIdPrefix = "dialogButton_";

__handlePartialLoad = function (htmlRepr, buttons, modal, partial, afterOpen, steps) {
	//noinspection JSUnusedGlobalSymbols
	vex.open({
		className: 'vex-theme-default',
		unsafeContent: htmlRepr,
		escapeButtonCloses: !modal,
		overlayClosesOnClick: !modal,
		afterOpen: function () {
			if (partial !== undefined) {
				partial.request(function (answer) {
					$(".accessory").html(answer).slideDown();
					
					$("[data-autofocus]").focus();
					
					if (steps) {
						var prepareSteps = new Steps();
						prepareSteps.configure();
					}
					
					if (afterOpen !== undefined) {
						afterOpen();
					}
				})
			}
			
			$(".buttonsContainer").find(".button").each(function () {
				var index = parseInt($(this).attr("id").replace(DialogButtonIdPrefix, ""));
				var action = buttons[index].action;
				
				if (action !== undefined) {
					$(this).click(action);
				}
				else {
					$(this).click(function () {
						vex.closeAll();
					});
				}
			});
			
			if (partial === undefined && afterOpen !== undefined) {
				afterOpen();
			}
		}
	})
};

function DialogButton(title, isDefault, action, existingButton) {
	this.title = title;
	this.class = "button" + ((isDefault) ? " buttonDefault" : "");
	this.action = action;
	this.existing = existingButton;
	
	this.setTitle = function(title) {
		$(this.existing).text(title);
	};
	
	this.setEnabled = function(enabled) {
		if (!enabled) {
			$(this.existing).attr("disabled", "disabled").addClass("disabled");
		}
		else {
			$(this.existing).attr("disabled", undefined).removeClass("disabled");
		}
	}
}

DialogButton.AtIndex = function (index) {
	return new DialogButton(undefined, undefined, undefined, $("#" + DialogButtonIdPrefix + index));
};

function Dialog(title, subtitle, partial, buttons, modal) {
	this.title = title;
	this.subtitle = subtitle;
	this.accessoryView = partial;
	this.modal = modal;
	
	this.buttons = buttons;
	this.steps = true;
	
	this.disableSteps = function () {
		this.steps = false;
	};
	
	this.htmlRepresentation = function () {
		var buttonsHtml = [];
		this.buttons.forEach(function (object, index) {
			var classToAppend = object.class;
			
			buttonsHtml.push("<li id='" + DialogButtonIdPrefix + index + "' class='" + classToAppend + "'>" + object.title + "</li>")
		});
		
		var result = "" +
			"<div class='dialog'>" +
			"<h1 class='dialogTitle'>" + this.title + "</h1>" +
			"<h2 class='dialogSubtitle'>" + this.subtitle + "</h2>";
			
		if (this.accessoryView !== undefined) {
			result += "<div class='accessory'>" +
			this.accessoryView +
			"</div>";
		}
		
		result += "<ul class='buttonsContainer'>" +
			buttonsHtml.join("") +
			"</ul>" +
		"</div>";
		
		return result;
	};
	
	this.show = function (afterOpen) {
		var htmlRepr = this.htmlRepresentation();
		var buttons = this.buttons;
		var modal = this.modal;
		var steps = this.steps;
		var accessoryView = this.accessoryView;
		
		if (this.accessoryView !== undefined) {
			if (this.accessoryView instanceof PartialPage) {
				__handlePartialLoad(htmlRepr, buttons, modal, accessoryView, afterOpen, steps);
			}
		}
		else {
			__handlePartialLoad(htmlRepr, buttons, modal, undefined, afterOpen, steps);
		}
	};
	
	this.setTitle = function (title) {
		$(".dialogTitle").text(title);
	};
	
	this.setSubtitle = function (subtitle) {
		$(".dialogSubtitle").text(subtitle);
	}
}

Dialog.confirm = function (title, subtitle, buttonTitle, cancelButtonTitle, completionHandler) {
	var dialog = new Dialog(title, subtitle, undefined, [
		new DialogButton(cancelButtonTitle, false),
		new DialogButton(buttonTitle, true, function () {
			closeTopDialog();
			completionHandler();
		})
	], true);
	dialog.show();
};

Dialog.warning = function (title, subtitle, cancelButtonTitle) {
	var dialog = new Dialog(title, subtitle, undefined, [
		new DialogButton(cancelButtonTitle, true)
	], true);
	dialog.show();
};

function closeTopDialog() {
	vex.closeTop();
}

function closeAllDialogs() {
	vex.closeAll();
}