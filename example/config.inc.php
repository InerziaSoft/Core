<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/**
 * Core - Example
 *
 * Configuration page
 * This page defines constants that are required by Core to function properly
 * and includes all the project files.
 */

/**
 * Include Composer dependencies.
 */
require __DIR__.'/../vendor/autoload.php';

/**
 * Include API entry point.
 */
// TODO: use FatFree autoloader.
require_once __DIR__.'/api/v1/api.php';

#region Configuration

// TODO: move all these constants in FatFree.

/**
 * Project name.
 * Used by Core to identify this project.
 */
define("NAME", "LingueMigranti");

/**
 * Path to reach the API routes.
 */
define("API", "api/");
/**
 * API version.
 * This is appended to each API route.
 */
define("API_VERSION", "v1");

/**
 * Enable debug/routes page.
 */
define("ROUTES_DEBUG", "true");

/**
 * Define the route class to use when verifying a token.
 */
//define("AUTHORIZATION_VERIFY_ROUTE_CLASS", "auth");

/**
 * Define the route method to use when verifying a token.
 */
//define("AUTHORIZATION_VERIFY_ROUTE_METHOD", "verify");

/**
 * Define the way to connect to the API backend from the frontend.
 *
 * If enabled, the frontend will use the hostname to connect to the backend;
 * otherwise, localhost will be used.
 *
 * This is particularly useful in shared machines where `localhost` may not necessarily
 * point to the right webserver.
 */
define("USE_HOST_IN_API_REQUESTS", "false");

// TODO: verify if these constants are not defined.
define("PAGES_CLASSES_NAMESPACE", "InerziaSoft\\LingueMigranti\\");
define("PAGE_LOCALIZED_TITLE", "pageTitle");
define("PAGE_LAYOUT_NAVIGATION_BAR_KEY", "navigation_bar");
define("PAGE_LAYOUT_FOOTER_BAR", "footer_bar");
define("CSS_DEFAULT_FOLDER", "sass/");
define("JS_DEFAULT_FOLDER", "js/");
define("JS_DIST_DEFAULT_FOLDER", "dist/js/");
define("PAGE_LAYOUT_FOLDER", "layout");
define("PAGE_LAYOUT_CONTENT_KEY", "page_content");
define("PAGE_ONLOAD_FUNCTION_KEY", "page_onload");

#endregion

#region Dependency Injection

\InerziaSoft\Core\Utils\DIUtils::setUp("injection.php");

#endregion

// TODO: handle this section in Core.

$f3 = \Base::instance();

$f3->set("DefaultPage", "Home");
$f3->set("AuthUrl", "/login");
$f3->set("UI", __DIR__."/pages/views/");