<?php
/*
MIT License

Copyright (c) 2016 Alessio Moiso, Andrea Gai - InerziaSoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

namespace InerziaSoft\Api\CoreExample;

use InerziaSoft\Core\Api\Api;
use InerziaSoft\Core\Api\Checker;
use InerziaSoft\Core\Api\Exceptions\InvalidParameterException;
use InerziaSoft\Core\Api\Outcomes\InvalidParameter;
use InerziaSoft\Core\Api\Outcomes\Success;

class ExampleApi extends Api {
	
	/**
	 * @method GET
	 *
	 * @param $test1
	 * @param $test2
	 * @return InvalidParameter|Success|\InerziaSoft\Core\Api\Outcomes\Unauthorized
	 */
	public static function get($test1, $test2) {
		try {
			Checker::for("test1")->checkString($test1);
			Checker::for("test2")->checkString($test2);
		}
		catch (InvalidParameterException $exception) {
			return InvalidParameter::fromException($exception);
		}
		
		return new Success();
	}
	
	/**
	 * @method POST
	 *
	 * @param $test1
	 * @param $test2
	 * @return InvalidParameter|Success|\InerziaSoft\Core\Api\Outcomes\Unauthorized
	 */
	public static function post($test1, $test2) {
		try {
		    Checker::for("test1")->checkString($test1);
		    Checker::for("test2")->checkString($test2);
		}
		catch (InvalidParameterException $exception) {
		    return InvalidParameter::fromException($exception);
		}
		
		return new Success();
	}

}