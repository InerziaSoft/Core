<?php

/**
 * Main entry point of the project.
 */

include_once 'config.inc.php';

// TODO: move this call to something more generic.
\InerziaSoft\Core\Routes\RoutesInitializer::initialize();

// TODO: remove this call.
$f3->run();